import numpy as np

class RateParams(object):
    def __init__(self, rate_hz, duration_s):
        self.rate_hz = rate_hz
        self.duration_s = duration_s

        # get one second's worth of time data
        self.time_s = np.arange(0, duration_s, 1/rate_hz)

        # frequency list for fft outputs
        self.dHz = 1/duration_s

        self.freq_Hz = np.array(range(len(self.time_s))) * self.dHz
