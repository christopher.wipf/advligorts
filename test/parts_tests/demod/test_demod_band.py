import sys
import numpy as np
from channel import DacChannel, AdcChannel
import importlib

if len(sys.argv) > 1:
    model_name = sys.argv[1]
    print(f"testing model {model_name}")
    model_class = importlib.import_module(model_name + "_pybind")
else:
    print(f"need a model name as an argument")
    sys.exit(1)



from demod_component import DemodComponent
from rate_params import RateParams

# import scipy.signal as sig
# import matplotlib.pyplot as plot
from collections import namedtuple

model = model_class.create_instance()

rate_params = RateParams(rate_hz=model.get_model_rate_Hz(), duration_s=1.0)



class DemodBandPart(object):
    """Represents parameters and results for an individual band part"""
    def __init__(self, name, bands, adc_chan, dac_start_chan, rate_params):
        """

        :param name:  The name of the part.  For reporting purposes only.
        :param bands: Number of frequency bands in the part.
        :param adc_chan: A Channel, input signal to the part as Tuple of (card, channel)
        :param dac_start_chan: A Channel, first dac channel that takes output from the part.  Each band gets two channels in
        sequence starting at this channel.
        """

        self.name = name
        self.bands = bands
        self.adc_chan = adc_chan
        self.dac_start_chan = dac_start_chan
        self.rate_params = rate_params

        # filled in by self.setup()
        self.components = None

        # filled in by checking
        self.result = None

        self.setup()

    def __repr__(self):
        return f"BandPart(name={self.name}, bands={self.bands}, adc_chan={repr(self.adc_chan)}, dac_start_chan={repr(self.dac_start_chan)})"

    def setup(self):
        """
        Initialize components and prime adc input

        :return:
        """

        # {bands} components from 0 to bands-1/bands of nyquist
        self.comps = [DemodComponent(i * 0.5 + 1, i * self.pitch_hz, i + 1, skip_q=(i==0)) for i in range(self.bands)]

        signal = np.zeros(len(self.rate_params.time_s))

        # populate bands with increasing frequency offset and amplitude
        for comp in self.comps:
            freq_hz = comp.base_freq_hz + comp.freq_offset_hz
            signal += comp.amp*np.sin(self.rate_params.time_s * freq_hz * 2.0 * np.pi)

        model.set_adc_channel_generator(self.adc_chan.card, self.adc_chan.channel, model_class.CannedSignalGenerator(canned_data=signal))

    def check(self):
        """
        Check output of part.  Print some results.

        :return: True if all tests passed.
        """
        for comp_index in range(len(self.comps)):
            comp = self.comps[comp_index]
            i_channel = self.dac_start_chan + (2*comp_index)
            q_channel = i_channel + 1
            comp.out_i = model.get_dac_output_by_id(i_channel.card, i_channel.channel)
            comp.out_q = model.get_dac_output_by_id(q_channel.card, q_channel.channel)


        # positive only frequencies
        pos_freqs_Hz = self.rate_params.freq_Hz[:len(self.rate_params.freq_Hz)//2]

        print(f"Checking {self.name}")

        result = True
        for comp in self.comps:
            result = comp.check(self.pitch_hz / 2, self.rate_params) and result

        self.result = result
        self.summary()
        print()
        print("***********************************")
        return result

    def summary(self):
        print(f"{self.name}: ", self.result and "PASS" or "FAIL")
    @property
    def pitch_hz(self):
        return self.rate_params.rate_hz / (2 * self.bands)



band_parts = [
    DemodBandPart("DEMOD_BAND_1X", 1, AdcChannel(0, 1), DacChannel(1, 0), rate_params),
    DemodBandPart("DEMOD_BAND_2X", 2, AdcChannel(0, 2), DacChannel(1, 2), rate_params),
    DemodBandPart("DEMOD_BAND_4X", 4, AdcChannel(0, 3), DacChannel(1, 6), rate_params),
    DemodBandPart("DEMOD_BAND_8X", 8, AdcChannel(0, 0), DacChannel(0, 0), rate_params),
    DemodBandPart("DEMOD_BAND_16X", 16, AdcChannel(0, 4), DacChannel(2, 0), rate_params),
]



model.record_dac_output(True)

# run model
model.run_model(len(rate_params.time_s));

for band_part in band_parts:
    band_part.check()

print()
print("Test Summary")
total_result = True
for band_part in band_parts:
    band_part.summary()
    total_result = total_result and band_part.result

print()
print("RCG-1095-T DemodBand test: ", total_result and "PASS" or "FAIL")

# create a low pass filter to look only at the demodulated band

#lp = sig.iirdesign(35, 40, 3, 30, fs=rate_hz)

# fft_in = np.abs(np.fft.fft(signal))
# fft_out = np.abs(np.fft.fft(model.get_dac_output_by_id(0,0)))
#
# plot.plot(fft_in)
# plot.plot(fft_out)
# plot.show()


# DAC 0-0 should match signal

if total_result:
    sys.exit(0)
else:
    sys.exit(1)