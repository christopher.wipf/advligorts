import numpy as np

def print_result(title, expected, measured, comparison, tolerance=0):
    """
    Print a line indicating pass/fail, and return True if passed.
    :param title:
    :param expected:
    :param measured:
    :param tolerance:
    :param comparison: one of "near",">=", "<="
    :return:
    """
    if comparison == "near":
        result = np.abs(expected - measured) <= tolerance
        tol_string = f"tol.={tolerance}"
    elif comparison == ">=":
        result = measured >= expected
        tol_string = ""
    elif comparison == "<=":
        result = measured <= expected
        tol_string = ""
    else:
        raise Exception(f"Unknown comparison {comparison}")
    print(f"{title}:\t{measured} {comparison} {expected} {tol_string}\tdiff.={measured-expected} {result and 'PASS' or 'FAIL'}")
    return result
