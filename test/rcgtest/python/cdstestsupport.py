#!/usr/bin/python3
# Automated test support classes

import epics
import time
import string
import sys
import os
import re

import nds2
from epics import PV
from subprocess import call


class sushwwd:
    def __init__(self,chanPrefix):
        s = chanPrefix + '_TIME_REQ'
        self.ToReq = PV(s)
        s = chanPrefix + '_TIME_RD'
        self.ToRd = PV(s)
        s = chanPrefix + '_RMS_REQ'
        self.RmsReq = PV(s)
        s = chanPrefix + '_RMS_RD'
        self.RmsRd = PV(s)
        s = chanPrefix + '_STATUS'
        self.Status = PV(s)
        # Bit 0 = SEI
        # Bit 1 = PD
        # Bit 2 = SUS
        # Bit 3 = LED
        self.StatusStates = ['OK','FAULT']
        self.SeiTrip = (int(self.Status.value) & 1)
        self.SusTrip = (int(self.Status.value) & 4)  >> 2
        self.PdState = (int(self.Status.value) & 2)  >> 1
        self.LedState = (int(self.Status.value) & 8)  >> 3
        self.SeiTripStr = self.StatusStates[(int(self.Status.value) & 1)]
        self.SusTripStr = self.StatusStates[(int(self.Status.value) & 4)  >> 2]
        self.PdStateStr = self.StatusStates[(int(self.Status.value) & 2)  >> 1]
        self.LedStateStr = self.StatusStates[(int(self.Status.value) & 8)  >> 3]
        s = chanPrefix + '_CMD'
        self.Cmd = PV(s)
        # 1 = Reset
        # 2 = Read
        # 3 = Set Values
        s = chanPrefix + '_MODE'
        self.Mode = PV(s)
        # 1 = Reset
        # 2 = Read
        # 3 = Setting new RMS
        # 4 = Setting new TimeOut (TO)
        # 5 = Waiting CMD ACK
        # 6 = Set Values
        s = chanPrefix + '_TTF_SEI'
        self.TttSei = PV(s)
        s = chanPrefix + '_TTF_SUS'
        self.TttSus = PV(s)
        s = chanPrefix + '_STATE'
        self.State = PV(s)
    def getStatus(self):
        self.SeiTrip = (int(self.Status.value) & 1)
        self.SusTrip = (int(self.Status.value) & 4)  >> 2
        self.PdState = (int(self.Status.value) & 2)  >> 1
        self.LedState = (int(self.Status.value) & 8)  >> 3
        self.SeiTripStr = self.StatusStates[(int(self.Status.value) & 1)]
        self.SusTripStr = self.StatusStates[(int(self.Status.value) & 4)  >> 2]
        self.PdStateStr = self.StatusStates[(int(self.Status.value) & 2)  >> 1]
        self.LedStateStr = self.StatusStates[(int(self.Status.value) & 8)  >> 3]
    def getMode(self):
        self.modes = ['Monitor','Reset','Read','Set RMS','Set Timeout','Wait Cmd Ack','Set Vals']
        self.modeStr = self.modes[int(self.Mode.value)]
    def wait4seitrip(self):
        while not self.SeiTrip:
            self.getStatus()
            time.sleep(1)
    def wait4sustrip(self):
        while not self.SusTrip:
            self.getStatus()
            time.sleep(1)
    def setThresholds(self,timeout,rms):
        self.ToReq.value = timeout
        self.RmsReq.value = rms
        time.sleep(2)
        self.Cmd.value = 3
        time.sleep(2)
        while int(self.Mode.value):
            time.sleep(2)
        time.sleep(15)
    def readThresholds(self):
        self.Cmd.value = 2
        time.sleep(2)
        while int(self.Mode.value):
            time.sleep(2)
        time.sleep(15)
    def Reset(self):
        self.Cmd.value = 1
"""
testreport: Class used to help generate CDS standard test reports using doxygen format.
"""
class testreport:
    """
    Initialization routine
    Arguments are:
        reportname = name to be used as the reference page for doxygen
        header - Text to be used as the test report header
        dcuid - CDS standard dcuid used to id code models. This is used to
            retrieve and log the SVN version that code was produced from.
    Product:
        1) Creates the test report file in the standard test reports area.
        2) Writes out the header info, including SVN version and time of test.
    """
    def __init__(self,reportname,header,dcuid):
        self.fn = '/tmp/rcgtest/data/' + reportname + '.dox'
        self.errors = 0
        self.teststatus = 0
        self.teststate = PV('X2:IOP-SAM_TEST_STATUS')
        self.passfail = 'PASS'
        self.teststeptxt = ''
        self.arraydata = []
        s = '/*!     \\page ' + reportname + ' ' + header + ' TEST \n'
        s += '*       \\verbatim \n'
        s += '\n' + header + ' TEST REPORT \n'
        self.rcgname = 'X2:FEC-' + repr(dcuid) + '_RCG_VERSION.VAL'
        self.gpsname = 'X2:FEC-' + repr(dcuid) + '_TIME_DIAG'
        #self.rcg = PV(self.rcgname,form='string')
        #self.rcg = "4.0")
        self.gpstime = PV(self.gpsname)
        tt = time.asctime()     # Get the current local time; used to timestamp test report
        s += 'TIME: ' + tt + ' \n'
        version = PV("X2:FEC-112_RCG_VERSION")
        s += 'RCG VERSION: '+ str(version.value) + '\n'
        s += '       \\endverbatim \n'
        f = open(self.fn,'wt')
        f.write(s)
        f.close()


    """
    section: Produces a doxygen formatted section of text with defined header
         and appends to test report file.
         - Heading = section header text.
         - txt = text to written into section.
    """
    def section(self,heading,txt) :
        # Writes out text for report section, including section header.
        s = '<b>' + heading + '</b> \n'
        s += '*       \\verbatim \n'
        s += txt
        s += '\n       \\endverbatim \n'
        f = open(self.fn,'at')
        f.write(s)
        f.close()
    """
    sectionhead: Produces only the header for a section to follow, in bold,
             and appends to test report file.
             It is required to be followed later by either section text
             or sectionstep to fill in the section text.
         - Heading = section header text.
    """
    def sectionhead(self,heading):
        s = '<b>' + heading + '</b> \n'
        f = open(self.fn,'at')
        f.write(s)
        f.close()
    """
    sectiontext: Writes string to fill in doxygen text section. This is appended
             to test report file. Note: This should be proceeded by sectionhead()
    """
    def sectiontext(self,txt):
        s = '*       \\verbatim \n'
        s += txt
        s += '\n       \\endverbatim \n'
        f = open(self.fn,'at')
        f.write(s)
        f.close()
    """
    sectionstep: Essentially same as sectiontext, but uses .teststeptxt as input
             Primarily written this way for use with test functions, such as
             checkPF below. This allows for multiple test data to be printed
             within the same doxygen section.
    """
    def sectionstep(self):
        s = '*       \\verbatim \n'
        s += self.teststeptxt
        s += '\n       \\endverbatim \n'
        f = open(self.fn,'at')
        f.write(s)
        f.close()
    """
    checkPF: Tests a single data point against requirement and adds PASS/FAIL text
         to .teststeptxt for later print out via sectionstep()
        - sname = Signal label to be used in print
        - req = Measured signal test requirement
        - measure = Measured test data
        - tolerance = Acceptable test window around req
    """
    def checkPF(self, sname, req, measure,tolerance):
        upperlimit = req + tolerance
        lowerlimit = req - tolerance
        self.passfail = 'PASS'
        if measure > upperlimit or measure < lowerlimit:
            self.passfail = 'FAIL'
            self.teststatus = -1
        if tolerance:
            s = repr(req) + ' +/- ' + repr(tolerance) + '\t'
        else:
            s = repr(req) + '\t'
        if len(s) < 9:
            s += '\t'
        self.teststeptxt += sname + '		' + s + repr(measure) + '\t\t' + self.passfail + '\n'
    """
    checkdata: Used to compare EPICS data against min/max requirements.
    This code also prints out the test results to the test report.
    """
    def checkdata( self, chans, reqmin, reqmax ):
        self.errors = 0
        ii = 0;
        s = '*       \\verbatim \n'
        s += '\t\t\t\t\t\t\tMeasured\tMin\tMax\t\tResult\n'
        for chan in chans:
            tperr = 0
            mytabs = '\t'
            if len(chan) < 28:
                mytabs += '\t'
            if len(chan) < 24:
                mytabs += '\t'
            ec = PV(chan)
            if ec.value < reqmin[ii] :
                tperr = 1;
            if ec.value > reqmax[ii]:
                tperr = 1;
            if tperr :
                s += '\t\t' + chan + mytabs + repr(int(ec.value)) + '\t\t' + repr(reqmin[ii]) + '\t' + repr(reqmax[ii]) + '\t\tFAIL\n'
                self.errors += 1
            else:
                s += '\t\t' + chan + mytabs + repr(int(ec.value)) + '\t\t' + repr(reqmin[ii]) + '\t' + repr(reqmax[ii]) + '\t\tPASS\n'
            ii += 1
        s += '\n'
        s +='       \\endverbatim \n'
        f = open(self.fn,'at')
        f.write(s)
        f.close()
    """
    image: Given the name of a .png file, will produce doxygen style output to test report file.
    """
    def image (self,imageName) :
        # Properly formats and writes image file info to the test report.
        s = '\\image html ' + imageName + '.png \n'
        s += '\\image latex ' + imageName + '.png \n'
        f = open(self.fn,'at')
        f.write(s)
        f.close()
    """
    close: Finishes out a doxygen file when test report is complete.
           This must show up at the end of every python test script.
    """
    def close (self) :
        # Required to properly end a test report for use by doxygen.
        s = '*/ \n'
        f = open(self.fn,'at')
        f.write(s)
        f.close()
    """
    setEpicsVals: Generic routine to set multiple EPICS channels given list of names and values.
    """
    def setEpicsVals (self, chans, vals ):
        ii = 0
        for chan in chans:
            ec = PV(chan)
            ec.value = vals[ii]
            ii += 1
    def getEpicsVals (self, chans ):
        self.arraydata = []
        for chan in chans:
            ec = PV(chan)
            self.arraydata.append(ec.value)
    def getEpicsStrings (self, chans, chcnt ):
        self.strdata = ['','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','']
        for ii in range(0,chcnt):
            self.strdata[ii] = ''
            ev = PV(chans[ii])
            wd = ev.value
            for jj in range(0,60):
                mychar = chr(wd[jj])
                self.strdata[ii] += mychar;
    def getEpicsStrData (self, chans ):
        self.strdata = ['','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','']
        ii = 0;
        for chan in chans:
            ec = PV(chan)
            self.strdata[ii] = ec.value
            ii += 1

    def testEpicsVals(self, chans, req ):
        global terr
        global s
        ii = 0;
        self.teststeptxt += '\t\t\t\t\t\t\tMeasured\tRequire\t\tResult\n'
        for chan in chans:
            mytabs = '\t'
            if len(chan) < 28:
                mytabs += '\t'
            if len(chan) < 24:
                mytabs += '\t'
            ec = PV(chan)
            if req[ii] == ec.value:
                self.teststeptxt += '\t\t' + chan + mytabs + repr(ec.value) + '\t\t' + repr(req[ii]) + '\t\tPASS\n'
            else:
                self.teststeptxt += '\t\t' + chan + mytabs + repr(ec.value) + '\t\t' + repr(req[ii]) + '\t\tFAIL\n'
                self.errors += 1
                self.teststatus = -1
            ii += 1
    def testEpicsStr(self, chan, req, chcnt, etype ):
        global terr
        global s
        if etype == 0:
            self.getEpicsStrData(chan)
        else:
            self.getEpicsStrings(chan,chcnt)
        self.teststeptxt += '\t\tChannel\t\t\t\tMeasured\t\t\t\tRequire\t\t\t\tResult\n'
        for ii in range(0,chcnt):
            mytabs = ''
            if len(chan[ii]) < 29:
                mytabs += '\t'
            if len(chan[ii]) < 24:
                mytabs += '\t'
            mytabs2 = ''
            if len(req[ii]) < 29:
                mytabs2 += '\t'
            if len(req[ii]) < 24:
                mytabs2 += '\t'
            mytabs3 = ''
            if len(self.strdata[ii]) < 28:
                mytabs3 += '\t'
            if len(self.strdata[ii]) < 24:
                mytabs3 += '\t'
            self.teststeptxt += '\t' + chan[ii] + mytabs
            self.teststeptxt +=req[ii]
            self.teststeptxt += '\t' + mytabs2
            self.teststeptxt += self.strdata[ii]
            if req[ii] in self.strdata[ii]:
                self.teststeptxt += mytabs2 + '\tPASS\n'
            else:
                self.teststeptxt += mytabs2 + '\tFAIL\n'
                self.errors += 1
                self.te7ststatus = -1
            ii += 1


# Class used to provide common methods for tests which use the GDS diag tool.
"""
Class dtt:
    This class was developed to provide standard support for tests which require the
    use of the diag command line tool and/or NDS capabilities.
"""
class dtt:
    def __init__(self):
        self.xdata = []
        self.ydata = []
        self.rdata = []
        self.tdata = []
        self.xmlfile = ''
        self.template = ''
        self.channum = ''
        # Define file to which generated diag commands are to be written.
        self.dttcmdfile = '/tmp/rcgtest/tmp/dtt.cmd'
        # Define command to be used to start diag process.
        self.dttcmd = 'diag -n x2ats -x -l -f /tmp/rcgtest/tmp/dtt.cmd &>> /tmp/diag.log'
        self.dttcmd2 = 'diag -n x2ats -x -l -f /tmp/rcgtest/tmp/dtt.cmd &>> /tmp/diag.log &'
        self.datafile = ''
        self.datapts = 0
        self.dataavg = 0
        self.datatotal = 0
        self.peakamp = 0
        self.errors = 0
        # Define tool to be used to generate data plots in png format.
        self.plottool = '/usr/bin/gnuplot'
        # Define and make connection to NDS server, for those tests required DAQ data.
        self.nds = nds2.connection('localhost', 8088)
        #self.nds.clear_cache()
        # Define EPICS channel from which to get GPS time.
        self.gpstime = PV('X2:FEC-111_TIME_DIAG.VAL')
    """
    xml2txt: Extracts data from diag produced xml files and writes to standard text file.
    """
    def xml2txt(self,datafile):
        # Make standard call to extract data from diag .xml files and write data to txt file.
        self.xmlfile = datafile+'.xml'
        self.datafile = datafile+'.data'
        call(["/usr/bin/xmlconv","-m",self.xmlfile,self.datafile,'Result[0]'])
    """
    xml2txtM: Extracts data from diag produced xml files and writes to standard text file.
    """
    def xml2txtM(self,datafile,chans):
        # Make standard call to extract data from diag .xml files and write data to txt file.
        self.xmlfile = datafile+'.xml'
        for ii in range (0,chans):
            self.datafile = datafile + repr(ii) + '.data'
            self.channum = 'Result[' + repr(ii) +']'
            call(["/usr/bin/xmlconv","-m",self.xmlfile,self.datafile,self.channum])
    """
    readdata:
        # Read data from data.txt files which have format of x axis data tab y axis data.
        #	Place data in x,y arrays as floating point numbers.
        #	Calculate total value of y points.
        #	Calculate avg value of y points.
    """
    def readdata(self,x,y):
        self.datapts = 0
        self.datatotal = 0
        self.xdata = []
        self.ydata = []
        self.peakamp = -100000000
        ii = 0
        f = open(self.datafile, "rt")
        for line in f:
            word = line.split()
            self.datapts += 1
            self.xdata.append(float(word[x]))
            self.ydata.append(float(word[y]))
            if self.ydata[ii] > self.peakamp:
                self.peakamp = self.ydata[ii]
                self.peakfreq = self.xdata[ii]
            self.datatotal += float(word[y])
            ii += 1
        f.close()
        self.dataavg = self.datatotal/self.datapts
        print (self.xdata[0],self.ydata[0],self.datapts)
        print ('peaks at ', self.peakfreq, 'with amp ',self.peakamp)
    """
    readdataxf:
        Used to read data from files which have multiple columns of data,
        such as the results of transfer functions. It is primarily intended
        to read data from a reference file (rfile) for later comparison with
        measured data in a test data file (tfile).
    """
    def readdataxf(self,rfile,rrow,tfile,trow):
        self.rdata = []
        self.tdata = []
        self.datapts = 0
        rfile += '.data'
        tfile += '.data'
        f = open(rfile, "rt")
        for line in f:
            word = line.split()
            self.rdata.append(float(word[rrow]))
            self.datapts += 1
        f.close()
        f = open(tfile, "rt")
        for line in f:
            word = line.split()
            self.tdata.append(float(word[rrow]))
        f.close()
    """
    comparedata:
        Used to test a set of measured data against a set of reference data,
        such as that read in by readdataxf().
    """
    def comparedata(self,tolerance):
        self.errors = 0
        ii = 0
        for data in self.rdata:
            testval1 = data + tolerance
            testval2 = data - tolerance
            if self.tdata[ii] > testval1 or self.tdata[ii] < testval2:
                self.errors += 1
            ii += 1

    def findpeakfreq(self):
        # Determine peak amp and location within a data set.
        self.peakamp = -100000000
        ii = 0
        for data in self.ydata:
            if data > self.peakamp:
                self.peakamp = data
                self.peakfreq = self.xdata[ii]
            ii += 1
    def creatediagcmd(self):
        # Generate the diag command file to be used by GDS diag tool.
        f = open(self.dttcmdfile,"wt")
        cstr = 'restore ' + self.template+' \n'
        cstr += 'run -w \n'
        cstr += 'save ' + self.xmlfile+' \n'
        cstr += 'quit \n'
        f.write(cstr)
        f.flush()
        f.close()
    def createcmd(self,templatefile,datafile):
        # Generate the diag command file to be used by GDS diag tool.
        self.template = templatefile+'.xml'
        self.xmlfile = datafile+'.xml'
        self.creatediagcmd()
    def rundtt(self):
        # Start the GDS diag process.
        os.system(self.dttcmd)
    def rundttnowait(self):
        # Start the GDS diag process.
        os.system(self.dttcmd2)
    def data2file(self,dbuff,numpts):
        # Write data to file, particularly for use by plotting tool.
        f = open(self.datafile,"wt")
        for ii in range(0,numpts):
            filestring = repr(ii) + '\t' + repr(dbuff[0].data[ii]) + '\n'
            f.write(filestring)
        f.flush()
        f.close()
    def rawdata2file(self,dbuff):
        f = open(self.datafile,"wt")
        ii = 0
        for data in dbuff:
            filestring = repr(ii) + '\t' + repr(data) + '\n'
            f.write(filestring)
            ii += 1
        f.flush()
        f.close()
    def timedata2file(self,dbuff,scale):
        f = open(self.datafile,"wt")
        ii = 0
        timescale = 0.0
        for data in dbuff:
            timescale = float(ii)/float(scale)
            filestring = repr(float(timescale)) + '\t' + repr(data) + '\n'
            f.write(filestring)
            ii += 1
        f.flush()
        f.close()
    def plot(self,pltfile):
        # Call gnuplot to generate data plot from data in file.
        call([self.plottool,pltfile])
    def datacompare(self,dataset,reffile,row,tolerance):
        with open(reffile,"rt") as f:
            self.errors = 0;
            ii = 0
            for line in f:
                word = line.split()
                testval1 = float(word[row]) + tolerance
                testval2 = float(word[row]) - tolerance
                if ii >= len(dataset):
                    print ("ran out of data to compare at item %d" % ii)
                    self.errors += 1
                    break
                if dataset[ii] > testval1 or dataset[ii] < testval2:
                    self.errors += 1
                if ii < 10:
                    print (dataset[ii],word[row],testval1,testval2)
                ii += 1
"""
Class SFM (Standard Filter Module)
Developed for use in tests which require setup and use of RCG filter modules.
"""
class SFM:
    def __init__(self,sfmname):
        tmpstr = sfmname + '_SW1'
        self.sw1 = PV(tmpstr)
        tmpstr = sfmname + '_SW2'
        self.sw2 = PV(tmpstr)
        tmpstr = sfmname + '_SW1R'
        self.sw1r = PV(tmpstr)
        tmpstr = sfmname + '_SW2R'
        self.sw2r = PV(tmpstr)
        tmpstr = sfmname + '_GAIN'
        self.gain = PV(tmpstr)
        tmpstr = sfmname + '_OFFSET'
        self.offset = PV(tmpstr)
        tmpstr = sfmname + '_TRAMP'
        self.ramp = PV(tmpstr)
        self.fmswbits = [16,64,256,1024,4096,16384,1,4,16,64]
        self.fmswin = 0b1111
    def inputsw(self,state):
        tmp = int(self.sw1r.value) & 4
        if state == 'ON' and tmp != 4:
            self.sw1.value = 4
        if state == 'OFF' and tmp != 0:
            self.sw1.value = 4
        time.sleep(0.3)
    def offsetsw(self,state):
        tmp = int(self.sw1r.value) & 8
        if state == 'ON' and tmp != 8:
            self.sw1.value = 8
        if state == 'OFF' and tmp != 0:
            self.sw1.value = 8
        time.sleep(0.3)
    def limitsw(self,state):
        tmp = int(self.sw2r.value) & 256
        if state == 'ON' and tmp != 256:
            self.sw2.value = 256
        if state == 'OFF' and tmp != 0:
            self.sw2.value = 256
        time.sleep(0.3)
    def outputsw(self,state):
        tmp = int(self.sw2r.value) & 1024
        if state == 'ON' and tmp != 1024:
            self.sw2.value = 1024
        if state == 'OFF' and tmp != 0:
            self.sw2.value = 1024
        time.sleep(0.3)
    def fmsw(self,num,state):
        num -= 1
        if num < 6:
            tmp = int(self.sw1r.value) & self.fmswbits[num]
            print ('tmp =',tmp,self.fmswbits[num])
            if state == 'ON' and tmp == 0:
                print ('tmp on =',tmp,self.fmswbits[num])
                self.sw1.value = self.fmswbits[num]
            if state == 'OFF' and tmp != 0:
                self.sw1.value = self.fmswbits[num]
                print ('tmp off =',tmp,self.fmswbits[num])
            time.sleep(0.3)
        else:
            tmp = int(self.sw2r.value) & self.fmswbits[num]
            print ('tmp =',tmp,self.fmswbits[num])
            if state == 'ON' and tmp == 0:
                self.sw2.value = self.fmswbits[num]
            if state == 'OFF' and tmp != 0:
                self.sw2.value = self.fmswbits[num]
                print ('tmp off =',tmp,self.fmswbits[num])
            time.sleep(0.3)
    def fmswall(self,swreq):
        newval1 = 0
        newval2 = 0
        tmp1 = int(self.sw1r.value)
        tmp2 = int(self.sw2r.value)
        for ii in range(0,10):
            if ii < 6:
                tstset = tmp1 & self.fmswbits[ii]
                tstreq = swreq & 1
                if tstset == 0 and tstreq != 0:
                    newval1 += self.fmswbits[ii]
                if tstset != 0 and tstreq == 0:
                    newval1 += self.fmswbits[ii]
            else:
                tstset = tmp2 & self.fmswbits[ii]
                tstreq = swreq & 1
                if tstset == 0 and tstreq != 0:
                    newval2 += self.fmswbits[ii]
                if tstset != 0 and tstreq == 0:
                    newval2 += self.fmswbits[ii]
            swreq = swreq >> 1
        self.sw1.value = newval1
        self.sw2.value = newval2
        time.sleep(0.3)
