set term png
set title "Transfer Function"
set xlabel 'Frequency (Hz)'
set ylabel 'Magnitude '
set style line 100 lt 1 lc rgb "gray" lw 2
set style line 101 lt 1 lc rgb "gray" lw 1
set grid xtics ls 100
set grid ytics ls 100

set output "/tmp/rcgtest/images/xextp.png"

plot "/tmp/rcgtest/tmp/exctestgds4.data" using 1:2 smooth unique title "EXC 1", "/tmp/rcgtest/tmp/exctestgds5.data" using 1:2 smooth unique title "EXC 2", "/tmp/rcgtest/tmp/exctestgds1.data" using 1:2 smooth unique title "EXC 3", "/tmp/rcgtest/tmp/exctestgds2.data" using 1:2 smooth unique title "EXC 4", "/tmp/rcgtest/tmp/exctestgds3.data" using 1:2 smooth unique title "EXC 5", "/tmp/rcgtest/tmp/exctestgds0.data" using 1:2 smooth unique title "EXC 6"

set output "../../../images/xextp2.png"
plot "/tmp/rcgtest/tmp/exctestgds8.data" using 1:2 smooth unique title "TP1", "/tmp/rcgtest/tmp/exctestgds7.data" using 1:2 smooth unique title "TP2", "/tmp/rcgtest/tmp/exctestgds6.data" using 1:2 smooth unique title "TP3", "/tmp/rcgtest/tmp/exctestgds10.data" using 1:2 smooth unique title "TP4", "/tmp/rcgtest/tmp/exctestgds9.data" using 1:2 smooth unique title "TP5", "/tmp/rcgtest/tmp/exctestgds11.data" using 1:2 smooth unique title "TP6"
