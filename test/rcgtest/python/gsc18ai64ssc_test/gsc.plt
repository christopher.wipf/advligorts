set term png
set xlabel 'DAC OUTPUT (Volts)'
set ylabel 'ADC COUNTS '
set style line 100 lt 1 lc rgb "gray" lw 2
set style line 101 lt 1 lc rgb "gray" lw 1
set grid xtics ls 100
set grid ytics ls 100

set output "./gsc18ai64_03.png"
plot "./adc18_ch0.txt" using 1:2 smooth unique title "GSC18AI64 CH0","./adc18_ch1.txt" using 1:2 smooth unique title "GSC18AI64 CH1","./adc18_ch2.txt" using 1:2 smooth unique title "GSC18AI64 CH2","./adc18_ch3.txt" using 1:2 smooth unique title "GSC18AI64 CH3"
set output "./gsc18ai64_47.png"
plot "./adc18_ch4.txt" using 1:2 smooth unique title "GSC18AI64 CH4","./adc18_ch5.txt" using 1:2 smooth unique title "GSC18AI64 CH5","./adc18_ch6.txt" using 1:2 smooth unique title "GSC18AI64 CH6","./adc18_ch7.txt" using 1:2 smooth unique title "GSC18AI64 CH7"
set output "./gsc18ai64_811.png"
plot "./adc18_ch8.txt" using 1:2 smooth unique title "GSC18AI64 CH8","./adc18_ch9.txt" using 1:2 smooth unique title "GSC18AI64 CH9","./adc18_ch10.txt" using 1:2 smooth unique title "GSC18AI64 CH10","./adc18_ch11.txt" using 1:2 smooth unique title "GSC18AI64 CH11"
set output "./gsc18ai64_1215.png"
plot "./adc18_ch12.txt" using 1:2 smooth unique title "GSC18AI64 CH12","./adc18_ch13.txt" using 1:2 smooth unique title "GSC18AI64 CH13","./adc18_ch14.txt" using 1:2 smooth unique title "GSC18AI64 CH14","./adc18_ch15.txt" using 1:2 smooth unique title "GSC18AI64 CH15"
