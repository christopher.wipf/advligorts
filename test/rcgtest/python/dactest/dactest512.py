#!/usr/bin/python3
# Test report generation routines

from subprocess import call
from epics import PV
from epics import caget, caput
from math import sin,cos,radians
import sys
import time
import os
import os.path as path

rcg_dir = os.environ['RCG_DIR']
sys.path.insert(0,path.join(rcg_dir, 'test/rcgtest/python'))
from cdstestsupport import testreport
from cdstestsupport import dtt, SFM

tr = testreport('dactest','DAC Order on Bus',112)
# Standard Header for all Test Scripts ****************************************************************
tr.sectionhead('Purpose')
s = ' Verify IOP passes proper DAC module mapping to user models. \n'
tr.sectiontext(s)

tr.sectionhead('Overview')
s = 'For RTS V4.1, the RCG was modified to allow various models of DAC cards \n'
s += 'to be installed in any order on the PCIe bus. This test is designed to \n'
s += 'to verify that user models get the proper mapping \n'
tr.sectiontext(s)

tr.sectionhead('Test Requirements')
s = 'This test was designed to run on the LLO DTS on x2ats:\n'
s += '  - Models x2iopsam, x2atstim32, x2atstim16, x2atstim04 and x2atstim02 \n'
s += '    must be running on x2ats. \n'
tr.sectiontext(s)


tr.sectionhead('Related MEDM Screen')
s = 'Test MEDM screen is at /opt/rtcds/rtscore/advligorts/test/rcgtest/medm/DAC_TEST.adl \n'
tr.sectiontext(s)
# tr.image('rampmuxmatrixMedm')
# END Standard Header for all Test Scripts ************************************************************

tr.sectionhead('Test Procedure')
s = 'The basic procedure is to drive various offsets in filters connected to DAC'
s += 'outputs and verify that control model and IOP model agree on which DAC'
s += 'module and channel receive the drive signal. '
tr.sectiontext(s)

pdiag = dtt()
# Start the test ******************************************

ii = 0;

tr.teststate.value = 'DAC PART TEST'

testvals = [1.0,2.0,3.0,4.0]
# List is 18_0, 20_0, 16_0 , 18_1, 18_2
iopcard = ['D18-0','D18-0','D20-0','D20-0','D16-0','D16-0',
            'D18-1','D18-1','D18-2','D18-2','D16-0','D18-2']
dacdrive = [2000,2000,8000,8000,2000,2000,2000,2000,2000,2000,2000,2000 ]
# Drive signals
filters = ['X2:ATS-TIM32_T1_DAC_FILTER_6','X2:ATS-TIM16_T1_DAC_FILTER_6',
                'X2:ATS-TIM32_T1_DAC_FILTER_7','X2:ATS-TIM16_T1_DAC_FILTER_7',
                'X2:ATS-TIM32_T1_DAC_FILTER_5','X2:ATS-TIM16_T1_DAC_FILTER_5',
                'X2:ATS-TIM32_T1_DAC_FILTER_1','X2:ATS-TIM16_T1_DAC_FILTER_1',
                'X2:ATS-TIM32_T1_DAC_FILTER_3','X2:ATS-TIM16_T1_DAC_FILTER_3',
                'X2:ATS-TIM04_T1_DAC_FILTER_1','X2:ATS-TIM04_T1_DAC_FILTER_3']

localdac = ['X2:FEC-111_DAC_OUTPUT_0_5','X2:FEC-112_DAC_OUTPUT_2_6',
            'X2:FEC-111_DAC_OUTPUT_2_5','X2:FEC-112_DAC_OUTPUT_1_6',
            'X2:FEC-111_DAC_OUTPUT_1_5','X2:FEC-112_DAC_OUTPUT_3_6',
            'X2:FEC-111_DAC_OUTPUT_3_0','X2:FEC-112_DAC_OUTPUT_0_2',
            'X2:FEC-111_DAC_OUTPUT_4_0','X2:FEC-112_DAC_OUTPUT_4_2',
            'X2:FEC-113_DAC_OUTPUT_1_8','X2:FEC-113_DAC_OUTPUT_3_5']

iopdac = ['X2:FEC-100_DAC_OUTPUT_0_5','X2:FEC-100_DAC_OUTPUT_0_6',
            'X2:FEC-100_DAC_OUTPUT_2_5','X2:FEC-100_DAC_OUTPUT_2_6',
            'X2:FEC-100_DAC_OUTPUT_1_5','X2:FEC-100_DAC_OUTPUT_1_6',
            'X2:FEC-100_DAC_OUTPUT_3_0','X2:FEC-100_DAC_OUTPUT_3_2',
            'X2:FEC-100_DAC_OUTPUT_4_0','X2:FEC-100_DAC_OUTPUT_4_2',
            'X2:FEC-100_DAC_OUTPUT_1_8','X2:FEC-100_DAC_OUTPUT_4_5']

iopadc = ['X2:IOP-S512_MADC0_EPICS_CH5','X2:IOP-S512_MADC0_EPICS_CH6',
            'X2:IOP-S512_MADC0_EPICS_CH13','X2:IOP-S512_MADC0_EPICS_CH14',
            'X2:IOP-S512_MADC1_EPICS_CH5','X2:IOP-S512_MADC1_EPICS_CH6',
            'X2:IOP-S512_MADC0_EPICS_CH16','X2:IOP-S512_MADC0_EPICS_CH18',
            'X2:IOP-S512_MADC0_EPICS_CH24','X2:IOP-S512_MADC0_EPICS_CH26',
            'X2:IOP-S512_MADC1_EPICS_CH8','X2:IOP-S512_MADC0_EPICS_CH29']


ats16_Range = ['X2:FEC-112_DAC_STAT_2','X2:FEC-112_DAC_STAT_1',
                'X2:FEC-112_DAC_STAT_3','X2:FEC-112_DAC_STAT_0',
                'X2:FEC-112_DAC_STAT_4' ]

ats32_Range = ['X2:FEC-111_DAC_STAT_0','X2:FEC-111_DAC_STAT_2',
                'X2:FEC-111_DAC_STAT_1','X2:FEC-111_DAC_STAT_3',
                'X2:FEC-111_DAC_STAT_4' ]

ats04_Range = ['X2:FEC-113_DAC_STAT_0','X2:FEC-113_DAC_STAT_2',
                'X2:FEC-113_DAC_STAT_1','X2:FEC-113_DAC_STAT_3']
testword = "X2:IOP-S512_TEST_STAT_15"


testwordepics = PV(testword)
testwordepics.value = 2
tr.teststatus == 0
#STEP 1 ***************************************************************************************************
tr.sectionhead('Step 1: Initialize Test Settings')
tr.teststeptxt = '\tInitialize all of the DAC driver filter modules to:\n'
tr.teststeptxt += '\t\t-Input Switch OFF\n'
tr.teststeptxt += '\t\t-Offset Switch ON\n'
tr.teststeptxt += '\t\t-Offset Value 2000.0\n'
tr.teststeptxt += '\t\t-Gain Value 1.0\n'
tr.teststeptxt += '\t\t-Limit Switch OFF\n'
tr.teststeptxt += '\t\t-Output Switch OFF\n'
tr.sectionstep()
# Clear all DAC outputs *************************************************
for ii in range (0,12):
    dacmodelfilter = SFM( filters[ii])
    dacmodelfilter.inputsw('OFF')
    dacmodelfilter.gain.value = 1.0
    dacmodelfilter.offset.value = dacdrive[ii]
    dacmodelfilter.offsetsw('ON')
    dacmodelfilter.limitsw('OFF')
    dacmodelfilter.outputsw('OFF')

time.sleep(2)

#STEP 2 *******************************************************************************
tr.sectionhead('Step 2: Verify Channel Mapping')
tr.teststeptxt = '\tRun through 12 DAC channels, a few from each model:\n'
tr.teststeptxt += '\t\t-Turn ON one output at a time.\n'
tr.teststeptxt += '\t\t-Turn OFF remaining DAC outputs.\n'
tr.teststeptxt += '\t\t-Read DAC drive signals both from driving control model and IOP.\n'
tr.teststeptxt += '\t\t-Read ADC channels associated with DAC output signals.\n'
tr.teststeptxt += '\t\t\t - Verify IOP and control models show drive on proper channels.\n'
tr.teststeptxt += '\t\t\t - Verify IOP and control models show zero drive on OFF channels.\n'
tr.teststeptxt += '\t\t\t - Verify associated IOP ADC channels show proper signal.\n'
tststatus = "PASS"
localdata = []
iopdata = []
iopadcdata = []
for ii in range (0,12):
    test1 = PV(localdac[ii])
    localdacread = test1.value
    localdata.append(test1.value)
    testiop = PV(iopdac[ii])
    iopdacread = testiop.value
    iopdata.append(testiop.value)
    testadc = PV(iopadc[ii])
    iopadcread = abs(testadc.value)
    iopadcdata.append(testadc.value)


for jj in range (0,12):
    for ii in range (0,12):
        dacmodelfilter = SFM( filters[ii])
        if ii == jj:
            dacmodelfilter.outputsw('ON')
        else:
            dacmodelfilter.outputsw('OFF')

    time.sleep(2)
    tr.teststeptxt += '\n'

    localdata = []
    iopdata = []
    iopadcdata = []
    s = ""
    t = ""
    a = ""
    c = ""
    for ii in range (0,12):
        test1 = PV(localdac[ii])
        localdata.append(test1.value)
        localdacread = int(test1.value)
        myspaces = 8 - len(repr(localdacread))
        s += repr(localdacread)
        for mm in range (0,myspaces):
            s += " "
        testiop = PV(iopdac[ii])
        iopdata.append(testiop.value)
        iopdacread = int(testiop.value)
        t += repr(iopdacread)
        myspaces = 8 - len(repr(iopdacread))
        for mm in range (0,myspaces):
            t += " "
        testadc = PV(iopadc[ii])
        iopadcdata.append(abs(int(testadc.value)))
        iopadcread = abs(int(testadc.value))
        myspaces = 8 - len(repr(iopadcread))
        a += repr(iopadcread) 
        for mm in range (0,myspaces):
            a += " "
        c += iopcard[ii] + "   "
        #tr.teststeptxt += 'value = ' + repr(localdacread) + '\t' + repr(iopdacread)+ '\n'
    tr.teststeptxt += c + '\n'
    tr.teststeptxt += s + '\n'
    tr.teststeptxt += t + '\n'
    tr.teststeptxt += a + '\n'
    passfail = 'PASS'
    for kk in range (0,12):
        if kk == jj:
            if iopdata[kk] < 1900 or localdata[kk] < 1900 or iopadcdata[kk] < 1900:
                tr.teststatus = -1
                tststatus = "FAIL"
                passfail = 'FAIL'
        else:
            if iopdata[kk] > 0 or localdata[kk] > 0 or iopadcdata[kk] > 500:
                tr.teststatus = -1
                tststatus = "FAIL"
                passfail = 'FAIL'
    tr.teststeptxt += 'PASS/FAIL = ' + passfail + '\n'
    
tr.teststeptxt += 'Step 2 PASS/FAIL = ' + tststatus + '\n'
tr.sectionstep()

tr.sectionhead('Step 3: Check DAC OVERRANGE')
tr.teststeptxt = '\tSend PANIC to IOP Watchdogs.\n'
tr.teststeptxt += '\t  - Do not want to comfuse ADC overrange with\n'
tr.teststeptxt += '\t    DAC overrange \n'
wdpanic = "X2:IOP-S512_DACKILL_HAM3_PANIC"
wdreset = "X2:IOP-S512_DACKILL_HAM3_RESET"
wdpanic4 = "X2:IOP-S512_DACKILL_HAM4_PANIC"
wdreset4 = "X2:IOP-S512_DACKILL_HAM4_RESET"
dackillpanic = PV(wdpanic)
dackillreset = PV(wdreset)
dackillpanic4 = PV(wdpanic4)
dackillreset4 = PV(wdreset4)
dackillpanic.value = 1
dackillpanic4.value = 1

tr.teststeptxt += '\tReset X2ATSTIM32 Overflow Counter.\n'
orreset = "X2:FEC-111_OVERFLOW_RESET"
orcnt = "X2:FEC-111_ACCUM_OVERFLOW"
overreset = PV(orreset)
overcount = PV(orcnt)
overreset.value = 1
time.sleep(1)
statename = "X2:FEC-111_STATE_WORD"
stateword = PV(statename)
toc = overcount.value;
if overcount.value == 0:
    tr.teststeptxt += '\t\t Overflow Count = ' + repr(overcount.value) + ':PASS\n'
else:
    tr.teststeptxt += '\t\t Overflow Count = ' + repr(overcount.value) + ':FAIL\n'

tr.teststeptxt += '\tTurn OFF all DAC drives:\n'
time.sleep(3)
for ii in range (0,12):
    dacmodelfilter = SFM( filters[ii])
    dacmodelfilter.outputsw('OFF')


tr.teststeptxt += '\tTest DAC overrange using x2atstim32 drives:\n'
tnum = 0
for ii in range (0,10,2):
    dacmodelfilter = SFM( filters[ii])
    dacmodelfilter.outputsw('ON')
    dacmodelfilter.offset.value = 600000.0
    time.sleep(2)
    s = ''
    s += '\tDriving ' + filters[ii] + " "
    tval = []
    for jj in range (0,5):
        test_of = PV(ats32_Range[jj])
        tval.append(test_of.value)
        s += repr(test_of.value) + " "
    s += '\n'
    time.sleep(1)
    s += '\tChecking STATEWORD:  '
    teststateword = stateword.value
    if teststateword != 512:
        tr.teststatus = -1
        s += 'FAIL \n'
    else:
        s += 'PASS \n'
    time.sleep(1)
    dacmodelfilter.offset.value = 200.0
    for jj in range (0,5):
        if jj == tnum and tval[jj] != 3:
            tr.teststatus = -1
            s += 'Test Fail: No Overrange' + repr(tnum) + repr(tval[jj]) + '\n'
        if jj != tnum and tval[jj] != 7:
            tr.teststatus = -1
            s += 'Test Fail: Unexpected Overrange' + repr(ii) + '\n'
    time.sleep(3)
    tr.teststeptxt += s + '\n'
    tnum += 1
tr.sectionstep()

tr.sectionhead('Step 4: Cleanup')
tr.teststeptxt = '\tReenable IOP Watchdogs\n'
dackillpanic.value = 0
dackillpanic4.value = 0
time.sleep(3)
dackillreset.value = 1
dackillreset4.value = 1
overreset.value = 1

time.sleep(2)

tr.sectionstep()

if(tr.teststatus == 0):
    tr.sectionhead('TEST SUMMARY:  PASS') # *********************************************
else:
    tr.sectionhead('TEST SUMMARY:  FAIL') #*********************************************
#tr.sectionstep()



# Cleanup
#Clear the test runing flag
testwordepics.value = 0
if tr.teststatus == 0:
    testwordepics.value = 1
    print ("Test Pass \n")
else:
    print ("Test Fail \n")

tr.close()
# Turn INPUT switch back ON to leave FM in default state
tr.teststate.value = 'TEST SYSTEM - IDLE'
call(["cat","/tmp/rcgtest/data/dactest.dox"])
sys.exit(tr.teststatus)

