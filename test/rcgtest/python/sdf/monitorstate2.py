#!/usr/bin/env python
# Automated test support classes

from epics import ca
import time
import string
import sys
import os

jj = 0

def onChanges(pvname=None, value=None, **kw):
	fmt = 'New value: %s  value = %s, kw= %s\n'
	sys.stdout.write(fmt % (pvname, str(value), repr(kw)))
	sys.stdout.flush()


mychannames = []
mychanvals = []
chanfile = open("./totalchans.snap","r")
ii = 0
for line in chanfile:
        word = line.split()
	mychannames.append(word[0])
	ii += 1

print 'Total chans = ',repr(ii)
chanfile.close()
print "Code load at ",time.ctime()

chid = []
eventID = []

ii = 0
for chname in mychannames:
	chid.append(ca.create_channel(chname))
print "Code load at ",time.ctime()
for id in chid:
	eventID.append(ca.create_subscription(id,callback=onChanges))

print "Code ready at ",time.ctime()

t0 = time.time()
while time.time() - t0 < 100.0:
	time.sleep(0.001)

