#!/usr/bin/perl -w -I/opt/rtcds/rtscore/trunk/src/perldaq

# Script for testing IPC components

use threads;
use threads::shared;


BEGIN {
	$epicsbase = $ENV{'EPICS_BASE'};
	$rcgdir = $ENV{'RCG_DIR'};
	$epicslib = $epicsbase . "/lib/linux-x86_64";
	$epicsperl = $epicsbase . "/lib/perl";
	print "EPICSBASE = $epicsbase \n";
	print "EPICSLIB = $epicslib \n";
	print "EPICSPERL = $epicsperl \n";
        push @INC,"/opt/cdscfg/tst/stddir.pl";
        push @INC,"$epicsperl";
        push @INC,"$epicslib";
	push @INC,"$rcgdir/test/rcgtest/perl/modules";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
        push @INC,"$rcgdir/src/perldaq";
}

use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;

$rcgtestdir = $ARGV[0];

$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";

(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
my @strEpics = ("X2:FEC-112_BUILD_SVN","X2:FEC-112_MSG","X2:FEC-112_MSGDAQ","X2:FEC-112_RCG_VERSION");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);

print  "\n\nSTARTING SIMULINK PARTS TESTING *********************************************\n";
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
print "RCG Version: $strEpicsVals[3]\n\n";
print "SVN Version: $strEpicsVals[0]\n\n";




my $status = 0;
my $ii = 0;
my @product;
my $test_summary = "FAIL";

caPut("X2:IOP-SAM_TEST_STATUS","SIMULINK PARTS TEST");
caPut("X2:IOP-SAM_TEST_STAT_18",2);


my @productSetChans = ("X2:ATS-TIM16_SIMP_PIN_1", "X2:ATS-TIM16_SIMP_PIN_2");

my @testSets1 = (20,40,60,80,100,-10,1.5);
my @testSets2 = (20,40,60,80,100,10,20);
my @testSets3 = (1,2,3,1,2,3,1);
my @testResults = (400,3200,10800,6400,20000,-300,30);
my $prodTest = "PASS";

for($ii=0;$ii<7;$ii++)
{
	caPut("X2:ATS-TIM16_SIMP_PIN_1", $testSets1[$ii]);
	caPut("X2:ATS-TIM16_SIMP_PIN_2", $testSets2[$ii]);
	caPut("X2:ATS-TIM16_SIMP_PIN_3", $testSets3[$ii]);
	sleep(1);
	@product = caGet("X2:ATS-TIM16_SIMP_POUT");
	if($product[0] != $testResults[$ii])
	{
		$prodTest = "FAIL";
		$status = -1;
	}
}

my $divTest = "PASS";
@testSets1 = (10,10,0.1,80,100,10,20);
@testSets2 = (20,40,60,80,100,-10,1.5);
@testSets3 = (1,2,1,2,1,2,1);
@testSets4 = (2,1,1,2,1,2,1);
for($ii=0;$ii<7;$ii++)
{
	caPut("X2:ATS-TIM16_SIMP_DIN_1",$testSets1[$ii]);
	caPut("X2:ATS-TIM16_SIMP_DIN_2",$testSets2[$ii]);
	caPut("X2:ATS-TIM16_SIMP_DIN_3",$testSets3[$ii]);
	caPut("X2:ATS-TIM16_SIMP_DIN_4",$testSets4[$ii]);
	$testVal = $testSets2[$ii] * $testSets4[$ii] / $testSets1[$ii] / $testSets3[$ii];
	sleep(1);
	my @product = caGet("X2:ATS-TIM16_SIMP_DOUT");
	if($product[0] != $testVal)
	{
		$divTest = "FAIL";
		$status = -1;
	}
}

my $sumTest = "PASS";
@testSets1 = (20,40,60,80,100,-10,1.5);
@testSets2 = (10,10,0,80,100,10,20);
@testSets3 = (10,10,0,80,100,10,20);
my $testVal = 0.0;
for($ii=0;$ii<7;$ii++)
{
	caPut("X2:ATS-TIM16_SIMP_SUM_IN_1",$testSets1[$ii]);
	caPut("X2:ATS-TIM16_SIMP_SUM_IN_2",$testSets2[$ii]);
	caPut("X2:ATS-TIM16_SIMP_SUM_IN_3",$testSets3[$ii]);
	$testVal = $testSets1[$ii] + $testSets2[$ii] + $testSets3[$ii];
	sleep(1);
	@product = caGet("X2:ATS-TIM16_SIMP_SUM_OUT");
	if($product[0] != $testVal)
	{
		$sumTest = "FAIL";
		$status = -1;
	}
}

my $choiceTest = "PASS";
@testSets1 = (20,20,20,20,20,-10,1.5);
@testSets2 = (10,10,10,10,10,10,20);
@testSets3 = (0,9,10,80,100,10,20);
@testResults = (10,10,20,6400,10000,-100,30);
for($ii=0;$ii<3;$ii++)
{
	caPut("X2:ATS-TIM16_SIMP_CIN_1",$testSets1[$ii]);
	caPut("X2:ATS-TIM16_SIMP_CIN_2",$testSets2[$ii]);
	caPut("X2:ATS-TIM16_SIMP_CSWITCH",$testSets3[$ii]);
	sleep(1);
	@product = caGet("X2:ATS-TIM16_SIMP_COUT");
	if($product[0] != $testResults[$ii])
	{
		$choiceTest = "FAIL";
		$status = -1;
	}
}

my $absTest = "PASS";
@testSets1 = (-20,0,20,80,100,-10,1.5);
$testVal = 0.0;
for($ii=0;$ii<3;$ii++)
{
	caPut("X2:ATS-TIM16_SIMP_ABS_IN",$testSets1[$ii]);
	$testVal = abs($testSets1[$ii]);
	sleep(1);
	@product = caGet("X2:ATS-TIM16_SIMP_ABS_OUT");
	if($product[0] != $testVal)
	{
		$absTest = "FAIL";
		$status = -1;
	}
}

my $bwaTest = "PASS";
@testSets1 = (0x111,0x01110011,60,80,100,-10,1.5);
@testSets2 = (0x111,0x00111100,10,0,80,100,10,20);
@testSets3 = (10,10,0,80,100,10,20);
$testVal = 0.0;
for($ii=0;$ii<2;$ii++)
{
	caPut("X2:ATS-TIM16_SIMP_BWA_IN1",$testSets1[$ii]);
	caPut("X2:ATS-TIM16_SIMP_BWA_IN2",$testSets2[$ii]);
	$testVal = $testSets1[$ii] & $testSets2[$ii];
	sleep(1);
	@product = caGet("X2:ATS-TIM16_SIMP_BWA_OUT");
	if($product[0] != $testVal)
	{
		$bwaTest = "FAIL";
		$status = -1;
	}
}

my $opTest = "PASS";
@testSets1 = (30,30,60,80,100,-10,1.5);
@testSets2 = (30,40,10,0,80,100,10,20);
@testSets3 = (10,10,0,80,100,10,20);
for($ii=0;$ii<3;$ii++)
{
	caPut("X2:ATS-TIM16_SIMP_OP_IN1",$testSets1[$ii]);
	caPut("X2:ATS-TIM16_SIMP_OP_IN2",$testSets2[$ii]);
	if($testSets1[$ii] <= $testSets2[$ii]) {
		$testVal = 1.0;
	} else {
		$testVal = 0.0;
	}
	sleep(1);
	@product = caGet("X2:ATS-TIM16_SIMP_OP_OUT");
	if($product[0] != $testVal)
	{
		$opTest = "FAIL";
		$status = -1;
	}
}

my $sqrtTest = "PASS";
@testSets1 = (0,1,16,-20,20,-10,1.5);
@testResults = (0,1,4,0,10000,-100,30);
for($ii=0;$ii<4;$ii++)
{
	caPut("X2:ATS-TIM16_SIMP_SQRT_IN",$testSets1[$ii]);
	sleep(1);
	@product = caGet("X2:ATS-TIM16_SIMP_SQRT_OUT");
	if($product[0] != $testResults[$ii])
	{
		$sqrtTest = "FAIL";
		$status = -1;
	}
}

my $satTest = "PASS";
@testSets1 = (0.1,0.3,10,-0.2,-0.4,-100,1.5);
@testSets2 = (10,10,0,80,100,10,20);
@testSets3 = (10,10,0,80,100,10,20);
for($ii=0;$ii<6;$ii++)
{
	caPut("X2:ATS-TIM16_SIMP_SAT_IN",$testSets1[$ii]);
	if($testSets1[$ii] <= -0.5) {
		$testVal = -0.5;
	} elsif ($testSets1[$ii] >= 0.5) {
		$testVal = 0.5;
	} else { 
		$testVal = $testSets1[$ii];
	}
	sleep(1);
	@product = caGet("X2:ATS-TIM16_SIMP_SAT_OUT");
	if($product[0] != $testVal)
	{
		$satTest = "FAIL";
		$status = -1;
	}
}

my $fcnTest = "PASS";
@testSets1 = (20,40,60,80,100,-10,1.5);
@testSets2 = (10,10,0,80,100,10,20);
@testSets3 = (10,10,0,80,100,10,20);
$testVal = 0.0;
for($ii=0;$ii<7;$ii++)
{
	caPut("X2:ATS-TIM16_SIMP_FCN_IN1",$testSets1[$ii]);
	caPut("X2:ATS-TIM16_SIMP_FCN_IN2",$testSets2[$ii]);
	caPut("X2:ATS-TIM16_SIMP_FCN_IN3",$testSets3[$ii]);
	$testVal = $testSets1[$ii] * 2  + $testSets2[$ii] * 10 + $testSets3[$ii] * 100;
	sleep(1);
	@product = caGet("X2:ATS-TIM16_SIMP_FCN_OUT");
	if($product[0] != $testVal)
	{
		$fcnTest = "FAIL";
		$status = -1;
	}
}

print "\t$teststatus[6] \n";
if($status == 0) {
    $test_summary =  "PASS;"
} else {
    $test_summary =  "FAIL";
}


# Generate Test Report *****************************************************************************
$testdatafile = $rcgtestdatadir . "/simPartsTest.dox";
print "datafile: $testdatafile\n";
open(OUTM,'>', $testdatafile) || die "cannot open test data file for writing";
print OUTM <<END;
/*!     \\page simPartsTest RCG Simulink Parts Test Data
*       \\verbatim
*************************************************************************************************

SIMULINK PARTS CODE TEST REPORT 

TIME: $hour:$min:$sec  $abbr[$mon]  $day $year

RCG version number: $strEpicsVals[3]
SVN version number: $strEpicsVals[0]

*************************************************************************************************
PURPOSE: Verify the operation of RCG supported standard Simulink parts.

OVERVIEW:	
This test is designed to verify proper compilation and operation of a number of Matlab
Simulink standard parts supported by the RCG. The specic parts under test are shown in the
figure below.

TEST REQUIREMENTS:
This test was designed to run on the LHO DAQ test stand. This test is automatically run by a
script in the userapps/cds/test/scripts area, simPartsTest.pl.

This test requires that the test model h1fetim16.mdl is running on the x1fe3 computer.

TEST PROCEDURE:
This test injects various setpoints into EPICS input channels and verifies correct output via
an EPICS output channel. This channels are shown in the diagram below.

        \\endverbatim
\\image html simPartsTest.png
\\image latex simPartsTest.png
*       \\verbatim

TEST RESULTS:
----------------------------------------------------------------------------------------------------
RCG-1070-T Product  - 			$prodTest
Divide					$divTest
Sum					$sumTest
RCG-1071-T Choice			$choiceTest
Absolute Val				$absTest
RCG-1073-T Bitwise AND			$bwaTest
Operator <=				$opTest
SQRT					$sqrtTest
RCG-1074-T Saturation			$satTest
RCG-1072-T Function			$fcnTest

TEST SUMMARY:  $test_summary

        \\endverbatim

*\/

END
if($status == 0) {
    caPut("X2:IOP-SAM_TEST_STATUS","SIMPARTS TEST SUCCESS");
    caPut("X2:IOP-SAM_TEST_STAT_18",1);
}
if($status == -1) {
    caPut("X2:IOP-SAM_TEST_STAT_18",0);
    caPut("X2:IOP-SAM_TEST_STATUS","SIMPARTS TEST FAILED");
}
sleep(5);

caPut("X2:IOP-SAM_TEST_STATUS","TEST SYSTEM - IDLE");
($sec,$min, $hour, $day, $mon, $year, $wday, $yday, $isdst) = localtime(time);
$year += 1900;
print  "COMPLETION TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";

system("cat $testdatafile");
sleep 10;
exit($status);

