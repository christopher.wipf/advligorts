#!/usr/bin/perl -w -I/opt/rtcds/rtscore/trunk/src/perldaq
##
##
use threads;
use threads::shared;
BEGIN {
	$epicsbase = $ENV{'EPICS_BASE'};
        $epicslib = $epicsbase . "/lib/linux-x86_64";
        $epicsperl = $epicsbase . "/lib/perl";
        $rcgdir = $ENV{'RCG_DIR'};
        print "EPICSBASE = $epicsbase \n";
        print "EPICSLIB = $epicslib \n";
        print "EPICSPERL = $epicsperl \n";
        push @INC,"/opt/cdscfg/tst/stddir.pl";
        push @INC,"$epicsperl";
	push @INC,"$epicslib";
	push @INC,"$rcgdir/test/rcgtest/perl/modules";
        push @INC,"$rcgdir/test/rcgtest/perl";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
        push @INC,"$rcgdir/src/perldaq";
}
#use stdenv;
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;
use DtTools;

(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
my @strEpics = ("X2:FEC-112_RCG_VERSION","X2:FEC-112_MSG","X2:FEC-112_MSGDAQ","X2:FEC-112_RCG_VERSION");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);
my $ii = 0;
my $status = 0;

caPut("X2:IOP-SAM_TEST_STAT_14",2);
print  "\n\nSTARTING STANDARD FILTER MODULE TESTING *********************************************\n";
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
print "RCG Version: $strEpicsVals[3]\n";

my @filt0Chans = ("X2:ATS-TIM16_T1_ADC_INPUT_FILTER_OFFSET", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER_GAIN", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER_GAIN_TRAMP",
		  "X2:ATS-TIM16_T1_ADC_INPUT_FILTER_POLE", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER_POLE_TRAMP", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER_ZERO",
		  "X2:ATS-TIM16_T1_ADC_INPUT_FILTER_ZERO_TRAMP");
my @filt1Chans = ("X2:ATS-TIM16_T1_ADC_INPUT_FILTER1_OFFSET", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER1_GAIN", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER1_GAIN_TRAMP",
		  "X2:ATS-TIM16_T1_ADC_INPUT_FILTER1_POLE", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER1_POLE_TRAMP", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER1_ZERO",
		  "X2:ATS-TIM16_T1_ADC_INPUT_FILTER1_ZERO_TRAMP");
my @filt2Chans = ("X2:ATS-TIM16_T1_ADC_INPUT_FILTER2_OFFSET", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER2_GAIN", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER2_GAIN_TRAMP",
		  "X2:ATS-TIM16_T1_ADC_INPUT_FILTER2_POLE", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER2_POLE_TRAMP", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER2_ZERO",
		  "X2:ATS-TIM16_T1_ADC_INPUT_FILTER2_ZERO_TRAMP");
my @filt3Chans = ("X2:ATS-TIM16_T1_ADC_INPUT_FILTER3_OFFSET", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER3_GAIN", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER3_GAIN_TRAMP",
		  "X2:ATS-TIM16_T1_ADC_INPUT_FILTER3_POLE", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER3_POLE_TRAMP", "X2:ATS-TIM16_T1_ADC_INPUT_FILTER3_ZERO",
		  "X2:ATS-TIM16_T1_ADC_INPUT_FILTER3_ZERO_TRAMP");

my @filt0Sets = (0.0,0.15,0.0,50.0,0.0,500.0,0.0);
my @filt1Sets = (0.0,1.0,0.0,100.0,0.0,1000.0,0.0);
my @filt2Sets = (0.0,1.0,0.0,1000.0,0.0,50.0,0.0);
my @filt3Sets = (0.0,1.0,0.0,1000.0,0.0,3000.0,0.0);


caPut("X2:IOP-SAM_TEST_STATUS","INPUT FILTER TEST ");

# setup filter module which will provide excitation channel for testing.
caSwitch("X2:ATS-TIM16_T1_ADC_FILTER_15","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT ON");

#setup input filter settings
caPut(@filt0Chans,@filt0Sets);
caPut(@filt1Chans,@filt1Sets);
caPut(@filt2Chans,@filt2Sets);
caPut(@filt3Chans,@filt3Sets);

$rcgtestdir = $ARGV[0];

$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";
$rcgtesttmpdir = $rcgtestdir . "/tmp";

$runfile = $rcgdir . "/test/rcgtest/perl/inputfilter/iftrunfile.sh";
$cmdfile = $rcgtesttmpdir . "/iftRunFile.cmd";
$templatefile = $rcgdir . "/test/rcgtest/perl/inputfilter/Template_INPUT_FILTER.xml";
$results_file = $rcgtesttmpdir . "/iftdata.xml";


print "Starting diag \n";
# Start Diag session based on template.
dtTestStart($runfile,$cmdfile,$templatefile,$results_file);

$ref_data_file = $rcgtesttmpdir . "/iftref.data";
$test_data_file = $rcgtesttmpdir . "/ift.data";

#convert .xml files to .data files.
my $cmd = 'xmlconv -m ' . $templatefile . ' ' . $ref_data_file . '  "Result[0]"';
system($cmd);
$cmd = 'xmlconv -m ' . $results_file . ' ' . $test_data_file . '  "Result[0]"';
system($cmd);

#Read xfer function frequency data
(my $testPts,my @freq) = dtReadDataFile($test_data_file,0);

#Read xfer function data for filter 1
($testPts,my @data) = dtReadDataFile($test_data_file,3);
($testPts,my @dataR) = dtReadDataFile($ref_data_file,3);
my $result1 = "PASS";
my $spec1 = 0.2;
(my $errs, my @diff) = dtCheckData($testPts,$spec1,@data,@dataR);
print "1 errors = $errs \n";
if($errs) { $result1 = "FAIL"; $status = -1; }

#Read xfer function data for filter 1
($testPts,@data) = dtReadDataFile($test_data_file,5);
($testPts,@dataR) = dtReadDataFile($ref_data_file,5);
my $result2 = "PASS";
my $spec2 = 0.2;
($errs, @diff) = dtCheckData($testPts,$spec2,@data,@dataR);
print "2 errors = $errs \n";
if($errs) { $result1 = "FAIL"; $status = -1; }

#Read xfer function data for filter 1
($testPts,@data) = dtReadDataFile($test_data_file,7);
($testPts,@dataR) = dtReadDataFile($ref_data_file,7);
my $result3 = "PASS";
my $spec3 = 0.2;
($errs, @diff) = dtCheckData($testPts,$spec3,@data,@dataR);
print "3 errors = $errs \n";
if($errs) { $result3 = "FAIL"; $status = -1; }

#Read xfer function data for filter 1
($testPts,@data) = dtReadDataFile($test_data_file,9);
($testPts,@dataR) = dtReadDataFile($ref_data_file,9);
my $result4 = "PASS";
my $spec4 = 0.2;
($errs, @diff) = dtCheckData($testPts,$spec4,@data,@dataR);
print "4 errors = $errs \n";
if($errs) { $result4 = "FAIL"; $status = -1; }

if($status == 0) {
    $test_summary =  "PASS"
} else {
    $test_summary =  "FAIL"
}


#Make data plots
system("/usr/bin/gnuplot $rcgdir/test/rcgtest/perl/inputfilter/inputFiltTest.plt");

# Generate Test Report
open(OUTM,">/tmp/rcgtest/data/inputFiltTestData.dox") || die "cannot open test data file for writing";
print OUTM <<END;
/*!     \\page inputFiltTest Input Filter Test Data
*       \\verbatim
*************************************************************************************************

INPUT FILTER CODE TEST REPORT 

TIME: $hour:$min:$sec  $abbr[$mon]  $day $year

RCG version number: $strEpicsVals[3]
SVN version number: $strEpicsVals[0]

*************************************************************************************************
PURPOSE: The purpose of this test procedure is to verify proper operation of the RCG input
filter part, located in the filter section of the RCG CDS_PARTS library.

OVERVIEW:

TEST REQUIREMENTS:
This test is designed to run on the LHO DAQ test system. The test is automatically performed by a test
script, rcgtest/perl/inputfilter/inputfilterTest.pl.
This test requires:
	- x2atstim16 code model is running on the x2ats computer. 
	- Dtt template file Template_INPUT_FILTER.xml to load into diag tool.

TEST PROCEDURE:
The following image shows the parts in the h1fe3tim16.mdl file used in this test. Four input filters are
used, with different settings, to allow running this test via a single EXC injection into ADC_FILTER_15.
Dtt is used to do this injection and perform a transfer function, with results shown in the TEST RESULTS section.

TEST MEDM SCREEN:
        \\endverbatim
\\image html inputfiltertestscreen.png
\\image latex inputfiltertestscreen.png
*       \\verbatim
TEST MODEL:
        \\endverbatim
\\image html inputFilterTest.png
\\image latex inputFilterTest.png

*       \\verbatim
TEST RESULTS:

Filter 0 (50/500, gain 0.15)     -       $result1
Filter 1 (100/1000, gain 1)      -       $result2
Filter 2 (1000/50, gain 1)       -       $result3
Filter 3 (1000/3000, gain 1)     -       $result4

RCG-0900-T TEST SUMMARY:  $test_summary


        \\endverbatim
\\image html ift50p500z.png 
\\image latex ift50p500z.png 

\\image html ift100p1000z.png 
\\image latex ift100p1000z.png 

\\image html ift1000p50z.png 
\\image latex ift1000p50z.png 

\\image html ift1000p3000z.png 
\\image latex ift1000p3000z.png 

*/
END
close OUTM;
print "TEST COMPLETE ***********************************************************************************\n";

system("cat /tmp/rcgtest/data/inputFiltTestData.dox");
caPut("X2:IOP-SAM_TEST_STATUS","SYSTEM IDLE ");

caPut("X2:IOP-SAM_TEST_STAT_14",1);
if($status < 0) {
    caPut("X2:IOP-SAM_TEST_STAT_14",0);
}
$cmd = "rm  " . $rcgtesttmpdir . "/*";
system($cmd);

exit($status);
