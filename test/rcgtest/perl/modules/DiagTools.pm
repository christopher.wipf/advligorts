################################################################
# Diagnostic Tools
#
# diagResp - returns response values for a list of channels
#   arguments are $name, $freq, $amp, $ncyc, $navg, $exc, @chans
#
# diagRestoreOnInterrupt - if processes is interrupted
#   epRestore and fmRestore are called and interruption
#   is logged before exiting.
#
################################################################
package DiagTools;
use EpicsTools;
use TdsTools;
use FilterTools;
use LogTools;
use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(&diagResp &diagRestoreOnInterrupt &catchControlC);

# require careful code
use strict;

# time lag for measurements
my $timeLag = 5;

################################ diagResp
sub diagResp {
    my $name = shift(@_);
    my $freq = shift(@_);
    my $amp = shift(@_);
    my $ncyc = shift(@_);
    my $navg = shift(@_);
    my $exc = shift(@_);
    my @chans = @_;
    my $dt = $timeLag + $ncyc * $navg / $freq;
    my @excID;
    my @rslt;

    logMeas($name, $dt);

    @excID = tdsSine($freq, $amp, $exc, 2 * $dt);
    if( !@excID ) {
	logFail("excitation on $exc wouldn't start.");
	return 0;
    } 

    @rslt = tdsDemod($freq, $ncyc, $navg, $exc, @chans);

    tdsStop(@excID);

    if( !pop(@rslt) ) {
	logFail("demodulation of $exc @chans failed.");
	return 0;
    }

#    my @aExc = split(" ", $rslt[0]);
#    if( $aExc[0] < 0.99 || abs(log($aExc[1] / $amp)) > 0.1) {
#	logFail("demodulation of excitation failed. 2");
#	return 0;
#    }

    return @rslt;
}

################################ diagRestoreOnInterrupt

sub diagRestoreOnInterrupt {
  # catch control-C and restore, then die
  $SIG{INT} = sub {
      print "\n";
      logFail("KILLED ... restoring");
      fmRestore;
      epRestore;
      die;
  };
	$SIG{QUIT} = sub {  
		# catch ctrl-\ and quit, _without_ restoring
		# justing - 2007 january 15
		print "\n";
		logFail("QUIT ... not restoring");
		die;
	};
};


################################ catchControlC

sub catchControlC {
  # catch control-C and restore, then die
  $SIG{INT} = sub {
      print "\n";
      print "Restoring Values and Dying ...";
      print "\n";
      fmRestore;
      epRestore;
      die;
  };
	$SIG{QUIT} = sub {  
		# catch ctrl-\ and quit, _without_ restoring
		# justing - 2007 january 15
                # rana - 070115 - removed loggin crap
                print "\n";
                print "Dying without Restoring values...";
		print "\n";
		die;
	};
};


################################ end properly
1;

