################################################################
# Logging Tools
#
# logStart - start a new section in the log file
#   argument is name of section
#
# logEnd - end current section in the log file
#   argument is 0 or 1 (0 => some test failed, 1 => all tests ok)
#
# logFail - end current section with failure
#   arguments are name of section and failure comment
#
# logMeas - note the start of a measurement
#   arguments are name of the measurement and its expected duration
#
# logMin - compare a value to a minimum value
#   arguments are name, number format, min, val
#
# logMax - compare a value to a maximum value
#   arguments are name, number format, max, val
#
# logRange - compare a value to a tolerance range
#   arguments are name, number format, min, max, val
#
################################################################
package LogTools;
use Term::ANSIColor;
use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(&logStart &logEnd &logFail &logMeas
	     &logMin &logMax &logRange);

# require careful code
use strict;

# set logging parameters
my $wcol = 60;			# width of first column
my $startstr = " .... ";
my $okstr    = "  OK  ";
my $failstr  = "FAILED";

# define environment
my $logdir  = "./";
my $logname = "LogTools";

my $LOG;

################################ internal functions
sub getdate {
    my @date = localtime;
    my $datestr = sprintf("%02d%02d%02d-%02d%02d%02d",
			  $date[5] % 100, $date[4] + 1,
			  @date[3, 2, 1, 0]);
    return $datestr;
}

sub getprstr {
    my $str = shift(@_);

    while( length($str) < $wcol )
    { $str .= " "; }
    return $str;
}

sub prok {
    my $str = getprstr(shift(@_));

    print "$str [", color("green bold"), $okstr, color("reset"), "] @_\n";

    $str .= " [$okstr] @_";
    print $LOG "$str\n";
    return $str;
}

sub prfail {
    my $str = getprstr(shift(@_));
    print "$str [", color("red bold"), $failstr, color("reset"), "] @_\n";

    $str .= " [$failstr] @_";
    print $LOG "$str\n";
    return $str;
}

################################ logStart
sub logStart {
    my $logname = shift(@_);
    my $datestr = getdate;
    my $logfile = "$logdir" . "$logname" . "/$logname" . "_$datestr.log";
    my $str = getprstr($logname . " started at " . $datestr);

    open($LOG, "> $logfile") or die "Can't open $logfile for logging: $!\n";

    print "$str [", color("blue bold"), $startstr, color("reset"), "]\n";
    $str .= " [$startstr]";
    print $LOG "$str\n";
}

################################ logEnd
sub logEnd {
    my $isok = shift(@_);
    my $str = "  DONE:  " . getdate;

    $isok ? prok($str) : prfail($str, " <====");
    return close($LOG);
}

################################ logFail
sub logFail {
    my $str = "  FAILED: " . getdate . "   ";
    my $msg = "@_";

    print $str, color("red bold"), $msg, color("reset"), "\n";
    print $LOG "$str$msg\n";
    return close($LOG);
}

################################ logMeas
sub logMeas {
    my $str = "  " . getdate . "  " . shift(@_);
    my $dt = sprintf("  %0.0f sec", shift(@_));

    print(color("bold"), $str, color("reset"),
          color("blue bold"), $dt, color("reset"), "\n");
    print $LOG "$str$dt\n";    
}

################################ logMin
sub logMin {
    my $name = $_[0];
    my $fmt = $_[1];
    my $min = sprintf($fmt, $_[2]);
    my $val = sprintf($fmt, $_[3]);
    my $str = "  MIN:   $name = $val";
    
    if( $val < $min ) {
	# test failed
	$str .= " < $min";
	prfail($str);
	return 0;
    }

    # test passed
    $str .= " >= $min";
    prok($str);
    return 1;
}

################################ logMax
sub logMax {
    my $name = $_[0];
    my $fmt = $_[1];
    my $max = sprintf($fmt, $_[2]);
    my $val = sprintf($fmt, $_[3]);
    my $str = "  MAX:   $name = $val";
    
    if( $val > $max ) {
	# test failed
	$str .= " > $max";
	prfail($str);
	return 0;
    }

    # test passed
    $str .= " <= $max";
    prok($str);
    return 1;
}

################################ logRange
sub logRange {
    my $name = $_[0];
    my $fmt = $_[1];
    my $min = sprintf($fmt, $_[2]);
    my $max = sprintf($fmt, $_[3]);
    my $val = sprintf($fmt, $_[4]);
    my $str = "  RANGE: $name = $val";
    
    if( $val < $min ) {
	# test failed
	$str .= " < $min < $max";
	prfail($str);
	return 0;
    }

    if( $val > $max ) {
	# test failed
	$str .= " > $max > $min";
	prfail($str);
	return 0;
    }

    # test passed
    $str .= " >= $min and <= $max";
    prok($str);
    return 1;
}

################################ end properly
1;

