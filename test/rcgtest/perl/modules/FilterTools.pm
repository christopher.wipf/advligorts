################################################################
# LIGO Generic Filter Module Tools
#
# fmRead - get the state bits of a filter module
#   arguments are a list of names
#   returns list of switch states (suitable for fmWrite)
#   Example:
#     my @banks = ("C1:LSC-PD1_DC", "C1:LSC-PD2_DC");
#     my @vals = fmRead(@banks)
#     my $isok = pop(@vals);
# 
# fmWrite - change the state of a filter module
#   arguments are a list of names, and a list of switch operations
#   returns success
#   Example:
#     my @banks = ("C1:LSC-PD1_DC", "C1:LSC-PD2_DC");
#     my @args = ("FM1 FM2 ON", "FM1 OFF");
#     my $isok = fmWrite(@banks, @args);
# 
# fmIsReady - check to see if a filter is done transitioning
#   argument is a single filter name, return is 0 or 1
#
# fmSave - save the state bits of argument filters for later restore
#   arguments and returns are as fmRead
#   (see also fmRestore)
#
# fmRestore - restore filter state bits saved with fmSave
#   Example:
#     my @banks = ("C1:LSC-PD1_DC", "C1:LSC-PD2_DC");
#     fmSave(@banks) or die;
#
#     ... do a bunch of stuff ...
#
#     fmRestore();
#
################################################################
package FilterTools;
use EpicsTools;
use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(&fmRead &fmWrite &fmIsReady &fmSave &fmRestore);

# require careful code
use strict;

# define environment
my $gdsdir = "/cvs/cds/gds/gds/bin";
my $switch = "$gdsdir/ezcaswitch";

# filter module strings
my @FM_SW1 = (0, 0, "INPUT", "OFFSET", "FM1", 0, "FM2", 0,
	      "FM3", 0, "FM4", 0, "FM5", 0, "FM6", 0);
my @FM_SW2 = ("FM7", 0, "FM8", 0, "FM9", 0, "FM10", 0,
	      "LIMIT", "DECIMATION", "OUTPUT", "HOLD", 0, 0, 0, 0);

# filter module saved states
my @saved_names;
my @saved_states;

################################ fmRead
sub fmRead {
    my @banks = @_;
    my @sw1r;
    my @sw2r;
    my @val1;
    my @val2;
    my @rply;

    # make list of record names
    foreach my $item (@banks) {
	push(@sw1r, $item . "_SW1R");
	push(@sw2r, $item . "_SW2R");
    }

    # read records
    @val1 = epRead(@sw1r);
    @val2 = epRead(@sw2r);
    if (!pop(@val1) && !pop(@val2))
    { return 0; }

    # translate
    foreach my $item (@banks) {
	my $v1 = shift(@val1);
	my $v2 = shift(@val2);
	my $str = "";
	my $bit;

	# translate SW1
	$bit = 1;
	for (my $i = 0; $i < 16; $i++) {
	    if ($FM_SW1[$i]) {
		if ($str ne "")
		{ $str .= " "; }

		$str .= $FM_SW1[$i];
		$str .= ($v1 & $bit) ? " ON" :  " OFF";
	    }
	    $bit *= 2;
	}

	# translate SW2
	$bit = 1;
	for (my $i = 0; $i < 16; $i++) {
	    if ($FM_SW2[$i]) {
		if ($str ne "")
		{ $str .= " "; }

		$str .= $FM_SW2[$i];
		$str .= ($v2 & $bit) ? " ON" :  " OFF";
	    }
	    $bit *= 2;
	}
	push(@rply, $str);
    }

    # done, all ok
    push(@rply, 1);
    return @rply;
}

################################ fmWrite
sub fmWrite {
    my $n = int(@_ / 2);
    my @rply;
    my $args;
    my $str;

    for (my $i = 0; $i < $n; $i++) {
	$args = "$_[$i] $_[$i + $n]";
	$str = `$switch $args`;
	if ($str !~ m/^$_[$i]/) {
	    print "ERROR: switch $args failed.  Response was:\n";
	    print "$str\n";
	    return 0;
	}
    }
    return 1;
}

################################ fmIsReady
sub fmIsReady {
    if( @_ > 1 ) {
	print "ERROR: fmIsReady takes 1 argument.  Got:\n";
	die "@_\n";
    }

    my $name = shift(@_);
    my @swr = ($name . "_SW1R", $name . "_SW2R");
    my @bits = epRead(@swr);			# filter state bits
    my @mask = (21840, 85);			# FM request bit masks
    my @req;					# request bits
    my @fon;					# filter on bits
    my $isok;

    if( !pop(@bits) )
    { return 0; }

    @req = ($bits[0] & $mask[0], $bits[1] & $mask[1]);
    @fon = (($bits[0] >> 1) & $mask[0], ($bits[1] >> 1) & $mask[1]);

    $isok  = ($bits[1] & 4096) == 0;		# TRAMP bit is off
    $isok &= $req[0] == $fon[0];		# FM1 - FM6 ok
    $isok &= $req[1] == $fon[1];;		# FM7 - FM10 ok
    
    return $isok ? 1 : 0;
}

################################ fmSave
sub fmSave {
    my @state = fmRead(@_);
    my $isok = pop(@state);

    # add to saved list
    if( $isok ) {
	push(@saved_names, @_);
	push(@saved_states, @state);
    }

    # return
    push(@state, $isok);
    return @state;
}

################################ fmRestore
sub fmRestore {
    return fmWrite(@saved_names, @saved_states);
}

################################ fmTest
# I've abandoned this function, as it seems beter to get the
# filter state and do string matching.
sub fmTest {
    my $n = int(@_ / 2);
    my @banks = splice(@_, 0, $n);
    my @bits = @_;
    my @sw1r;
    my @sw2r;
    my @val1;
    my @val2;
    my @rply;

    # make list of record names
    foreach my $item (@banks) {
	push(@sw1r, $item . "_SW1R");
	push(@sw2r, $item . "_SW2R");
    }

    # read records
    @val1 = epRead(@sw1r);
    @val2 = epRead(@sw2r);
    if (!pop(@val1) && !pop(@val2))
    { return 0; }

    # match bits
    foreach my $item (@bits) {
	my @strs = split(" ", $item);
	my $v1 = shift(@val1);
	my $v2 = shift(@val2);
	my $m1 = 0;
	my $m2 = 0;
	my $bit;

	# make bit masks
	foreach my $str (@strs) {
	    $bit = 1;
	    for (my $i = 0; $i < 16; $i++) {
		if ($str eq $FM_SW1[$i]) { $m1 |= $bit; }
		if ($str eq $FM_SW2[$i]) { $m2 |= $bit; }
		$bit *= 2;
	    }
	}

	# push reply
	push(@rply, ($m1 == ($m1 & $v1) && $m2 == ($m2 & $v2)) ? 1 : 0);
    }

    # done, all ok
    push(@rply, 1);
    return @rply;
}

################################ end properly
1;

