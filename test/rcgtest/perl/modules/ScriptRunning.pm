# justing - 2007 Jan 7

package ScriptRunning;
use warnings;
#use strict;
use Exporter;
use English;
@ISA = ('Exporter');
@EXPORT = qw/ &script_running $SIG{"INT"} &clean_exit /;

$SIG{"INT"} = 'clean_exit';

# provides function ScriptRunning
#   e.g. ScriptRunning('path/to/log/file');
#      

# update SIG{INT} = '&clean_exit'
# can this be done from within a package?
my $pid    = $PID;
my $host   = $ENV{"HOST"};
my $script = $PROGRAM_NAME;
my $start  = $BASETIME;
my $time   = localtime;

# if log file exists
#    read it and see if it is stale,
#    or has been changed by the new program, exit this one, kill that one?
# else make a new log file
#    write out process info

sub script_running {
my ($log) = @_;
my @current_values = ( $ENV{"HOST"}, $PID, $PROGRAM_NAME, $BASETIME );


if ( -e $log ) {
    open LOG, "<$log" or die "couldn't read log: $!";
    my @logcontents;
    while (<LOG>) {
	my $l = $_;
	chomp($l);
       	push @logcontents, $l;
    }
    close LOG;
    foreach my $l (0..3) {
	# compare values, old and new
	my ($name, $value) = split /\s/, $logcontents[$l];
	if ( $value ne $current_values[$l]) {
	    print "another copy of this script is running!\n";
	    print "$name: $value ne $current_values[$l]\n";
	    exit;
	}
    }
} else {
    print "creatig log file at $log\n";
    open LOG, ">$log" or die "couldn't write log: $!";
    select LOG;
    print "host $ENV{'HOST'}\n";
    print "pid $PID\n";
    print "script_name $PROGRAM_NAME\n";
    print "start_time $BASETIME\n";
    print "update_time $time\n";
    select STDOUT;
    close LOG;
}
}

sub clean_exit() {
    print "exiting...";
    unlink @_;
    print ".\n";
    exit;
}
