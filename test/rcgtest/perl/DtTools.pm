package DtTools;
use Exporter;
@ISA = ('Exporter');
@EXPORT = qw(&dtTestStart &dtReadData &dtReadDataFile &dtFindPeak &dtCheckData);

use POSIX;
BEGIN {
        push @INC,"/opt/cdscfg/tst/stddir.pl";
        push @INC,"/opt/rtapps/epics-3.14.12.2_long/base/lib/perl";
        push @INC,"/opt/rtapps/epics-3.14.12.2_long/base/lib/linux-x86_64";
        push @INC,"/opt/rtcds/userapps/trunk/guardian";
}

# Generate diag command file and start diag test
sub dtTestStart
{
        my $runFile = shift;		#diag command filename
        my $runCmdFile = shift;		# cmd file produced by this script as argument to diag command
        my $diagFile = shift;		# diag tool template file
        my $dataFile = shift;		# file to write output from diag
print "Writing file $runFile $runCmdFile\n";
        open DFILE,">",$runCmdFile or die $!;
        print DFILE "restore $diagFile \n";
        print DFILE "run -w \n";
        print DFILE "save $dataFile \n";
        print DFILE "quit \n";
        close(DATAFILE);

        system ($runFile);
}
# Extract data from a diag xml file output and write to separate text file
sub dtReadData
{
        my $xmlFile = shift;		#xml file from which to extract data
        my $dataFile = shift;		#file to which extracted data is to be written
        my $request = shift;		# Which data to extract 

        my $convCall = "/usr/bin/xmlconv -m ";
        $convCall .= $xmlFile;
        $convCall .= " ";
        $convCall .= $dataFile;
        $convCall .= " \"Result[0]\"";
        system ($convCall);

        my $total = 0;
        my @data;
        my @vals;
        my $ii = 0;

        open DATAFILE,"<","$dataFile" or die $!;
        while(<DATAFILE>) {
                my @vals = split(" ",$_);
                if($request eq "DATA") {
                        $data[$ii] = $vals[1];
                }else{
                        $data[$ii] = $vals[0];
                }
                $total += $data[$ii];
                $ii ++;
        }
        close(DATAFILE);
        return($ii,$total,@data);
}
 
sub dtReadDataFile
{
	my $dataFile = shift;           #file to which extracted data is to be written
        my $request = shift;            # Which column of data to extract 

        my @data;
        my @vals;
        my $ii = 0;

        open DATAFILE,"<","$dataFile" or die $!;
        while(<DATAFILE>) {
                my @vals = split(" ",$_);
		$data[$ii] = $vals[$request];
                $ii ++;
        }
        close(DATAFILE);
        return($ii,@data);
}

#Find the peak amp and associated freq for an FFT
sub dtFindPeak
{
        my $pts = shift;	# Number of points to test
        my @data = @_;		# Data array, assuming first half is freq data and 2nd half is amp data
        my $ii = 0;

        my $PeakAmp = -100000000;
        my $PeakFreq = 0;
        my $jj = $pts;

        for($ii=0;$ii<$pts;$ii++) {
                if($data[$jj] > $PeakAmp) {
                        $PeakAmp = $data[$jj];
                        $PeakFreq = $data[$ii];
                }
                $jj ++;
        }
        return($PeakAmp,$PeakFreq);
}

#Compare test data vs requirement
#return number of errors and diff array between data and spec
sub dtCheckData
{
        my $pts = shift;	# Number of data points to test
        my $spec = shift;	# Test requirement value
        my @data = @_;		# Data array, assuming first half is freq data and 2nd half is amp data
        my $ii = 0;
        my $jj = $pts;
        my @diff;
        my $errCnt = 0;

        for($ii=0;$ii<$pts;$ii++) {
                $diff[$ii] = abs($data[$ii] - $data[$jj]);
                if($diff[$ii] > $spec) {
                        printf "Value $ii is out of spec - $data[$ii]  $data[$jj] $diff[$ii]\n";
                        $errCnt ++;
                }
                $jj ++;
        }
        return($errCnt,@diff);
}

