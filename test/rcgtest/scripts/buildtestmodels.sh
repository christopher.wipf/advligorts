#!/bin/bash

set -e

rtcds env

rtcds build --install --simple-output --yes -p 4 x2iopsam x2atstim32 x2atstim16 x2atstim04 x2atstim02 x2iopsa1 x2iopsa2 x2iops1pps x2iops128 x2iops512

