from log import *
import os.path as path
import subprocess
import os
from system_exception import SystemException


class Module(object):
    """
    Represents a module to possible build and install
    """
    def __init__(self, name, constraints=None, sub_path=None):
        """
        :param name:  string, name of module
        :param constraints: a callable that returns True when the module should be built and installed.
            When None, the constraints are considered always satisfied.
        :param sub_path:
        """
        self.name = name
        self.constraints = constraints
        self.sub_path = sub_path

    def test_contraints(self):
        if self.constraints is None:
            return True
        else:
            return self.constraints()

    @property
    def has_constraints(self):
        return self.constraints is not None

    def get_build_dir(self, base_path):
        modules_dir = "src/drv"

        if self.sub_path is None:
            return path.join(base_path, modules_dir, self.name)
        else:
            return path.join(base_path, modules_dir, self.name, self.sub_path)

    @property
    def kmod_name(self):
        return self.name.replace('-','_')


modules = [
    Module("gpstime"),
    Module("mbuf"),
    Module("rts-cpu-isolator", constraints=lambda: int(os.uname().release.split(".")[0]) >= 5),
    Module("rts-logger", sub_path="module")
    ]


def build_kernel_mods(src_dir):
    """
    Build kernel modules found in src/drv

    Only those listed in the global "modules" will be built.

    If a model is missing, an error is not created.  We assume
    the source is so old that the model doesn't exist.

    the directory src/drv/<module> is searched for.  If it doesn't exist,
    the module is skipped.

    If the directory does exist, it is entered and 'Make' is run.  If that fails,
    an error is generated.

    :param src_dir: Directory where advligorts source resides
    :return: True on success, a list of mods built
    """
    global modules
    log("building kernel modules")
    success = True
    built_mods = []
    for module in modules:
        mod_path = module.get_build_dir(src_dir)
        log(f"starting build of kernel module {module.name} in {mod_path}")
        if not path.isdir(mod_path):
            log(f"kernel module {module.name} did not exist in source")
            continue
        if not module.test_contraints():
            log(f"kernel module {module.name} not built because its constraints were not met")
            continue
        env = os.environ
        env['PWD'] = mod_path
        result = run_log(["make","clean"], cwd=mod_path, env=env)
        if result.returncode != 0:
            log(f"clean failed for kernel module {module.name}")
            success = False
        else:
            result = run_log(["make"], cwd=mod_path, env=env)
            if result.returncode != 0:
                log(f"build failed for kernel module {module.name}")
                success = False
            else:
                built_mods.append(module)
    return success, built_mods


def is_mod_running(module):
    """
    return True if module is running
    :param module:
    :return:
    """
    result = subprocess.run(["lsmod"], capture_output=True, text=True)
    if result.returncode == 0:
        lines = result.stdout.split("\n")
        for line in lines:
            words = line.split()
            if len(words) > 0 and words[0] == module.kmod_name:
                return True
        return False
    raise Exception("failed to run lsmod")


def remove_mod(module):
    """
    Remove a mod if it is running
    :param module:
    :return: True if successful
    """
    log(f"checking if kernel module {module.kmod_name} is installed")
    if is_mod_running(module):
        log(f"removing kernel module {module.kmod_name}")
        result = run_log(["sudo", "rmmod", "-f", module.kmod_name])
        if result.returncode != 0:
            log(f"failed to remove kernel module {module.kmod_name}")
            return False
        return True
    log(f"kernel module {module.kmod_name} is not running")


def install_kernel_mods(src_dir, mods_built):
    """
    install kernel mods built previously by build_kernel_mods

    This function assumes models are no longer running
    (or anything else that needs these mods)

    modules are force removed, since mostly advligorts modules aren't clean
    when all dependencies are removed.

    A failure should not necessarily mean the source fails the test.  Problem could
    be due to module issues on the host.

    :param src_dir: the directory where the advligorts src resides
    :param mods_built: A list of module names ready for install, as returend by
    build_kernel_mods()
    :return: True if successful.
    """
    for module in mods_built:
        log(f"installing kernel module {module.name}")
        if not remove_mod(module):
            raise SystemException(f"Could not remove module '{module.name}'")

        mod_path = module.get_build_dir(src_dir)

        if module.test_contraints():
            log(f"installing kernel module {module.name}")
            result = run_log(["sudo", "insmod",f"{module.name}.ko"], cwd=mod_path)
            if result.returncode != 0:
                log(f"failed to install kernel module {module.name}")
                return False
        elif module.has_constraints:
            log(f"kernel module {module.name} was not installed because the constraints were not met")
    return True


def add_symvers_to_env(src_dir, mods_built, env):
    """
    Fill in an existing environment dictionary with the
    ENV variable MODEL_SYM pointing at the Module.symvers file
    created with build_kernel_mods()
    '-' are turned into '_' and names are made uppercase

    :param src_dir: directory where advligorts src is located
    :param mods_built: a list of modules actually built by build_kernel_mods()
    :param env: An environment dictionary to add to
    :return: none
    """
    for module in mods_built:
        varname = module.kmod_name.upper() + "_SYM"
        sym_path = path.join(module.get_build_dir(src_dir), "Module.symvers")
        env[varname] = sym_path


def test():
    modules.append(Module("notamodule"))
    base_path = "/tmp"
    src = "/opt/rtcds/rtscore/advligorts"
    built, built_modules = build_kernel_mods(src)
    print(f"build result = {built}")
    if built:
        install_kernel_mods(src, built_modules)

def build(src_dir):
    built, built_modules = build_kernel_mods(src_dir)
    print(f"build result = {built}")
    if built:
        install_kernel_mods(src_dir, built_modules)
        env = {}
        add_symvers_to_env(src_dir, built_modules, env)
        if len(env) > 0:
            print("Copy the following lines into bash before building models")
        for var,val in env.items():
            print(f"export {var}={val}")

import sys

if __name__ == "__main__":
    if len(sys.argv) > 1:
        build(sys.argv[1])
    else:
        test()