from git import Repo
from best_commit import find_descendents, find_commit_by_ref
from argparse import ArgumentParser
import subprocess
from math import log
import random

def sub_tree_to_set(root):
    """Return a set of commits in a subtree, including root"""
    children = {root}
    visited = set()
    stack = [root]
    while len(stack) > 0:
        p = stack.pop()
        for c in p.children:
            if c not in children:
                children.add(c)
                stack.append(c)
    return children


test_result = False

def run_test(repo, best_commit, test):
    """
    Return true if best_commit passes the test

    :param repo:
    :param best_commit:
    :param test:
    :return:
    """
    # global test_result
    # x = test_result
    # test_result = True
    # return x
    print("building, installing and starting models")
    result = subprocess.run(f"/usr/local/bin/autotest -bt {best_commit.hexsha}", shell=True, capture_output=True)
    if result.returncode != 0:
        print(result.stdout)
        print(result.stderr)
        raise Exception("failed to build and run models")
    print("models running, starting test")
    result = subprocess.run(test, shell=True, capture_output=True)
    print("test complete")
    return result.returncode == 0


def parents_to_set(all_commits, commit):
    """
    Return subset of commits in all_commits that are parents of commit, including commit
    :param all_commits:
    :param commit:
    :return:
    """
    parents = {commit}
    visited = set()
    stack = [commit]
    while len(stack) > 0:
        c = stack.pop()
        for p in c.parents:
            if p in all_commits and p not in parents:
                parents.add(p)
                stack.append(p)
    return parents

def calc_entropy(p_x):
    """Calculate expected entropy in bits from a list of (probability, size) pairs"""

    expected_bits = 0

    log2 = log(2)

    for prob, size in p_x:
        if prob > 0 and size > 0:
            expected_bits += prob * (log(size) / log2)

    return expected_bits

def calc_bisection_entropy(ancestors, total):
    """ Calculate the expected entropy (bits) by bisecting total in either ancestors or descendants

    The remainder is included if ancestors are dropped.
    :param ancestors:
    :param descendants:
    :param total:
    :return:
    """
    return calc_entropy([((total-ancestors)/total, total - ancestors), (ancestors/total, ancestors-1)])




def binary_search(repo, root, test):
    """
    Do a binary search of a commit tree from repo to find the commit where test starts to fail.

    :param repo:  The repo under test.
    :param root: The function examines only the subtree starting at root
    :param test: A command-line string.  A zero return code is success
    :return:
    """

    # a collection of commits that failed the test and their children
    # we assume the children will also fail

    failed_tests = set()
    successful_tests = set()

    failed = set()

    all_commits = find_descendents(repo, root)

    active_commits = all_commits

    while len(active_commits) > 0:
        print(f"active commits = {len(active_commits)} bits = {calc_entropy([(1.0, len(active_commits))])}")
        # find the best commit for subdividing the remaining commits

        best_entropy = 10000000000
        best_commit = None
        best_commit_set = None
        best_parents_set = None
        for commit in active_commits:

            commit_set = sub_tree_to_set(commit).intersection(active_commits)
            parents_set = parents_to_set(all_commits, commit).intersection(active_commits)
            entropy = calc_bisection_entropy(len(parents_set), len(active_commits))

            # print(f"expected bit {entropy} for {commit}")
            if entropy < best_entropy:
                best_entropy = entropy
                best_commit = commit
                best_parents_set = parents_set
                best_commit_set = commit_set

        print(f"Best commit to test with expected bits {best_entropy} is {best_commit}")

        result = run_test(repo, best_commit, test)

        print(f"{best_commit.hexsha} {result and 'PASS' or 'FAIL'}")

        if result:
            active_commits = active_commits.difference(best_parents_set)
            successful_tests.add(best_commit)
        else:
            failed = failed.union(best_commit_set)
            active_commits = best_parents_set.difference( {best_commit} )
            failed_tests.add(best_commit)

    # find all root nodes in failed
    root_failed = set()
    for commit in failed:
        parents = set(commit.parents)
        if len(failed.intersection(parents)) == 0:
            root_failed.add(commit)

    # print summary
    print()
    print("SUMMARY")
    print("Successful tests")
    print("\n".join([str(c) for c in successful_tests]))
    print()
    print("Fail tests")
    print("\n".join([str(c) for c in failed_tests]))
    print()
    print("Failure roots")
    print("\n".join([str(c) for c in root_failed]))



def run():
    parser = ArgumentParser(
        prog = 'autotest_search',
    )

    parser.add_argument('rootref')
    parser.add_argument('test')

    args = parser.parse_args()

    repo = Repo("/var/opt/rcgtest/advligorts")

    root = find_commit_by_ref(repo, args.rootref)

    binary_search(repo, root, args.test)

if __name__ == "__main__":
    run()