#!/bin/bash
# install autotest.py and associated systemd files

set -e

if [ "$EUID" -ne 0 ]
then
  echo "Please run as root"
  exit
fi

# install script
apt-get install -y rsync
install_path=/usr/local/lib/rcgtest

mkdir -p $install_path

rsync -r ../* "${install_path}/"

cp autotest /usr/local/bin/
cp autotest_search /usr/local/bin

# install virtual environment
apt-get install -y python3-venv
venv_path="${install_path}/autotest/venv"
python3 -m venv ${venv_path}

# install packages
source "${venv_path}/bin/activate"
python -m pip install gitpython

# install systemd units
systemdpath=/usr/local/lib/systemd/system
mkdir -p $systemdpath
cp ../systemd/* "${systemdpath}/"
systemctl daemon-reload
systemctl enable autotest.timer
systemctl start autotest.timer

# create run directory
run_path=/var/opt/rcgtest
mkdir -p $run_path
chmod 777 $run_path
