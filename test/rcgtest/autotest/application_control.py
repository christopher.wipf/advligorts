# stop, build, install, and start DAQ
import time
from log import *
import os.path as path

class ApplicationControl(object):
    def __init__(self, app_name, systemd_unit_name, build_dir, stop_time_s=5, start_time_s=30, control_start=True):
        self.app_name = app_name
        self.systemd_unit_name = systemd_unit_name
        self.build_dir = build_dir
        self.stop_time_s = stop_time_s
        self.start_time_s = start_time_s

        # start also means stop
        self.control_start = control_start
        self.control_stop = control_start

    def stop(self):
        """
        Stop a systemd unit that runs the application

        :return:
        """
        if self.control_stop:
            run_log(f"sudo systemctl stop {self.systemd_unit_name}".split())
            log(f"waiting {self.stop_time_s} seconds for {self.systemd_unit_name} to stop")
            time.sleep(self.stop_time_s)


    def start(self):
        """
        Start a systemd unit that runs the application

        :return:
        """
        if self.control_start:
            run_log(f"sudo systemctl start {self.systemd_unit_name}".split())
            log(f"waiting {self.start_time_s} seconds for {self.systemd_unit_name} to start")
            time.sleep(self.start_time_s)

    def build(self):
        """
        Build the application in source tree
        :return:
        """
        log(f"building {self.app_name} in {self.build_dir}")
        result = run_log("make -j".split(), cwd=self.build_dir)
        if result.returncode != 0:
            raise Exception(f"Failed to build {self.app_name}")

    def install(self):
        log(f"installing {self.app_name} to /usr/bin")
        bin_path = path.join(self.build_dir, self.app_name)
        result = run_log(["sudo","cp",bin_path, "/usr/bin/"])
        if result.returncode != 0:
            raise Exception(f"Failed to install {self.app_name}")