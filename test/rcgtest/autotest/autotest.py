#!/usr/bin/python3
# Run RCG tests automatically and report results.

import os
import os.path as path
from git import Repo
import sys
import time
import history
import shutil
import traceback
from best_commit import get_deserving_commits, find_branch_by_name, find_commit_by_ref
from sendmail import sendmail
from kernelmods import build_kernel_mods, install_kernel_mods, add_symvers_to_env
import platform
from log import *
from application_control import ApplicationControl
import argparse
from system_exception import SystemException

test_mode = False

release_email_list = ['evonreis@caltech.edu', 'ezekiel.dohmen@caltech.edu']
test_email_list = ['evonreis@caltech.edu']

email_list = []

from_addr = "no-reply@ligo-la.caltech.edu"
smtp_server = "x2portal"

autotest_version = (1, 1, 5)
autotest_version_str = ".".join([str(x) for x in autotest_version])


source_path = path.join(base_path,"advligorts")
tmp_rcg_path = "/tmp/rcgtest"
script_path = path.dirname(path.realpath(__file__))
rcgtest_install_path = path.dirname(script_path)

hostname = platform.node()

email_footer = f"""rcg autotest version = {autotest_version_str}
hostname = {hostname}
autotest istall dir = {rcgtest_install_path}
base dir = {base_path}
source dir = {source_path}
"""


def find_rcg_version(rcg_src):
    """
    Get the RCG version as a string directly from rcgversion.h.

    :param rcg_src: path to root of rcg source tree
    :return: a string representation of a version.  "noversion" on failure.
    """

    # then try to parse src version number
    try:
        with open(f"{rcg_src}/src/include/rcgversion.h", "rt") as f:
            lines = f.readlines()
            major = None
            minor = None
            sub = None
            release = None
            for line in lines:
                words = line.split()
                if len(words) >= 3:
                    if words[1] == "RCG_VERSION_MAJOR":
                        major = int(words[2])
                    if words[1] == "RCG_VERSION_MINOR":
                        minor = int(words[2])
                    if words[1] == "RCG_VERSION_SUB":
                        sub = int(words[2])
                    if words[1] == "RCG_VERSION_REL":
                        rel_num = int(words[2])
                        if rel_num == 0:
                            release = "dev"
                        else:
                            release = "release"
            if major is not None and minor is not None and sub is not None and release is not None:
                return f"{major}.{minor}.{sub}~{release}"
    except IOError:
        pass
    except ValueError:
        pass

    # finally, return some default
    return "noversion"


def stop_models(iop_model):
    """
    Stop and start models, using the given iop name.

    :param iop_model: A string
    :return:
    """

    # stop local_dc
    global source_path

    # stop models
    log("stopping models")
    stop_script = path.join(source_path, "test/rcgtest/scripts/stoptestmodels.sh")
    result = run_log([stop_script])
    if result.returncode != 0:
        raise SystemException("Could not stop previously running user models")
    time.sleep(5)
    result = run_log(f"rtcds stop {iop_model}".split())
    if result.returncode != 0:
        raise SystemException("Could not stop previously running iop model")
    time.sleep(20)

def start_models(iop_model):
    """
    Start models, using the given iop name.

    :param iop_model: A string
    :return:
    """
    # start models

    log("starting models")
    result = run_log(f"rtcds start {iop_model}".split())
    if result.returncode != 0:
        raise Exception("Could not start iop model")
    time.sleep(5)
    start_script = path.join(source_path, "test/rcgtest/scripts/starttestmodels.sh")
    result = run_log([start_script])
    if result.returncode != 0:
        raise Exception("Could not start user models")

def report_startup_info():
    """
    Print some info about configuration at start
    """
    started_by = "scheduler"

    log(f"start type was '{started_by}'")

    if build_only:
        log("set to build and install models, apps, and kernel modules, but not test")


def create_repo():
    """
    Create the source path
    :return: return a new repo object pointing at the source_path
    """
    global source_path
    log(f"updating source directory at {source_path}")

    if path.exists(path.join(source_path, ".git")):
        log(f"path already exists, deleting")
        #shutil.rmtree(source_path)
        repo = Repo(source_path)
    else:
        log(f"Cloning the source.")
        repo = Repo.clone_from('https://git.ligo.org/cds/advligorts.git', source_path)
        repo.create_remote('jh','https://git.ligo.org/jonathan-hanks/advligorts.git')
        repo.create_remote('evr','https://git.ligo.org/erik.vonreis/advligorts.git')
        repo.create_remote('ejd','https://git.ligo.org/ezekiel.dohmen/advligorts.git')
    log("fetching from remote")
    if repo.is_dirty():
        repo.head.reset(index=True, working_tree=True)
        if repo.is_dirty():
            raise Exception("Local repo has been modified.")
    return repo


def fetch(repo):
    """
    Fetch everything to a repo
    :param repo:
    :return: None
    """
    if len(repo.remotes) < 1:
        error("There are no remotes in the repo")
    for remote in repo.remotes:
        for fetch_info in remote.fetch():
            log(f"Updated {fetch_info.ref} to {fetch_info.commit}")


def find_commit(repo, commit_request):
    """
    Return a commit that matches the commit request
    :param repo:
    :return: a Commit object that matches the commit request
    """
    log(f"commit requested: {commit_request}")
    test_commit = None
    test_commit = find_commit_by_ref(repo, commit_request)

    if test_commit is not None:
        log("commit request succeeded as reference")
    else:
        log("commit request was not a reference. Checking branches.")
        test_commit = find_branch_by_name(repo, commit_request)
    return test_commit


def choose_commit(repo, history_path):
    """
    Choose the best commit to test from a repo
    :param repo:
    :return: the best commit to test as a Commit object
    """
    # commit list ordered by most deserving
    log("finding most deserving commit")
    commit_list = get_deserving_commits(repo)
    most_deserving = ["The top ten most deserving commits are:"]
    for commit in commit_list[:10]:
        sha_hash = commit.commit.hexsha
        most_deserving.append(commit.names[0] + " " + sha_hash)
    log("\n\t".join(most_deserving))
    histories = history.load_histories(history_path)

    test_commit = None
    for commit in commit_list:
        sha_hash = commit.commit.hexsha
        hist_key = autotest_version_str + " " + sha_hash
        if hist_key in histories and histories[hist_key].finished:
            log(f"test for '{commit.names[0]}' already complete. Skipping")
        else:
            log(f"'{commit.names[0]}' selected for testing")
            test_commit = commit
            break

    return test_commit


def update_source(commit_request, history_path):
    """
    Update the local source tree to the most reasonable commit to test.

    :param commit_request:
    :return: (hist_key, sha_hash, commit_names) associated with the chosen commit
    hist_key is a unique key that can be used to cache run history for commit.
    sha_hash is git generated key that's unique to a commit
    commit_names is a list of strings associated with a commit: branch names, tag names
    """
    global source_path

    repo = create_repo()

    fetch(repo)

    # pick commit
    print(f"commit_request = {commit_request}\n")
    if commit_request is not None:
        test_commit = find_commit(repo, commit_request)
    else:
        test_commit = choose_commit(repo, history_path)

    if test_commit is not None:
        sha_hash = test_commit.commit.hexsha
        hist_key = autotest_version_str + " " + sha_hash
    else:
        log("No commits to test.")
        sys.exit(0)

    if len(test_commit.names) > 0:
        log(f"switching to branch {test_commit.names[0]}")
    else:
        log(f"switching to reference {sha_hash}")

    repo.head.reference = test_commit.commit
    repo.head.reset(index=True, working_tree=True)

    return (hist_key, sha_hash, test_commit.names)

def create_archive_paths(sha_hash, commit_names):
    """
    Create archive path for the test run based on commit hash

    create symlinks for every name in commit_names
    :param sha_hash: A git hash as string for the commit under test
    :param commit_names: list of strings associated with the commit: branch, tag
    :return: the archive path
    """
    global base_path
    archive_path = path.join(base_path, f"archive/by_hash/{sha_hash}")
    os.makedirs(archive_path, exist_ok=True)
    # create symbolic links to archive
    for name in commit_names:
        dirs = name.split('/')
        linkname = dirs[-1]
        name_path = path.join(base_path, "archive", *dirs[:-1])
        os.makedirs(name_path, exist_ok=True)
        name_path = path.join(name_path, linkname)
        if path.lexists(name_path):
            os.remove(name_path)
        os.symlink(archive_path, name_path, target_is_directory=True);
    return archive_path


def copy_test_code():
    """
    Not all commits will have up-to-date test code.
    Copy over the latest test code into the source tree

    copy rcgtest code over pulled code
    get path where python script is installed
    rcgtest install is one directory higher

    :return:
    """
    global script_path, source_path

    rcgtest_from_path = path.dirname(script_path)
    rcgtest_to_path = path.join(source_path, "test/rcgtest")
    log(f"deleting {rcgtest_to_path}")
    shutil.rmtree(rcgtest_to_path)
    log(f"copy rcgtest files from {rcgtest_from_path} to {rcgtest_to_path}")
    shutil.copytree(rcgtest_from_path, rcgtest_to_path)


def configure_project():
    """
    Build parts of advligorts that we need to run the tests
    :return: A list of apps to build
    """
    # cmake in build dir
    global source_path

    app_build_dir = path.join(source_path, ".build")
    if(path.exists(app_build_dir)):
        shutil.rmtree(app_build_dir)
    os.mkdir(app_build_dir)
    run_log("cmake ..".split(), cwd=app_build_dir)

    # create application objects to rebuild
    local_dc = ApplicationControl("local_dc", "rts-local_dc",
                                  path.join(app_build_dir, "src/local_dc"), start_time_s=5)
    daqd = ApplicationControl("daqd", "rts-daqd",
                              path.join(app_build_dir, "src/daqd"))

    # awgtpman is auto-started by models
    awgtpman = ApplicationControl("awgtpman", None,
                              path.join(app_build_dir, "src/gds/awgtpman"), control_start=False)

    return [local_dc, daqd, awgtpman]


def build_project():
    """Configure, build, and run key advligorts parts
    :returns : list of built modules
    """

    global source_path

    copy_test_code()

    apps = configure_project()

    # build kernel modules
    modules_ok, built_modules = build_kernel_mods(source_path)
    if not modules_ok:
        raise Exception("Failed to build some kernel modules")

    # stop apps
    for app in apps:
        app.stop()

    # build apps
    for app in apps:
        app.build()

    return built_modules, apps


def build_install_models(built_modules):
    """
    build and install models
    :return: The environment used to build models
    """
    global rcgtest_install_path, base_path
    model_dirs = path.join(rcgtest_install_path, "models")

    build_path = path.join(base_path, "build")
    if path.exists(build_path):
        shutil.rmtree(build_path)
    os.makedirs(build_path)
    env = os.environ
    env["RTS_VERSION"] = "autotest"
    env["RCG_SRC"] = source_path
    env["RCG_DIR"] = source_path
    env["RCG_BUILD_ROOT"] = build_path
    env["RCG_LIB_PATH"] = model_dirs

    add_symvers_to_env(source_path, built_modules, env)

    log(f"RCG_LIB_PATH={env['RCG_LIB_PATH']}")
    log("building and installing models")
    build_script = path.join(source_path, "test/rcgtest/scripts/buildtestmodels.sh")

    results = run_log([build_script], env=env)
    if results.returncode != 0:
        raise Exception("Failed build or install")
    return env


def run_tests(env):
    """Run the integration tests

    :retyrns : test summary
    """
    global source_path

    test_summary_path = path.join(source_path,"test/rcgtest/doc/test_summary.txt")
    log(f"deleting any potential summary file")
    os.remove(test_summary_path)
    log(f"deleting temporary test directory {tmp_rcg_path}")
    if path.exists(tmp_rcg_path):
        shutil.rmtree(tmp_rcg_path)
    log("running the tests")
    run_log([path.join(source_path, "test/rcgtest/scripts/rcgtest.pl"), 'all', '-w'], env=env)
    log("tests finished")
    log("reading in summary")
    test_summary = ""
    with open(test_summary_path,"rt") as sum:
        test_summary = sum.read()
    return test_summary


def generate_report(archive_path=None, git_names=[]):
    """
    Generate and archive the test report
    :param archive_path:
    :param list of git names to tack on to rts_version, printed on cover of report:
    :return: pat to archived report
    """
    global source_path
    time.sleep(10)
    rts_version = find_rcg_version(source_path)
    names_list = [rts_version] + git_names
    names = ", ".join(names_list)
    log(f"Generating report for version '{names}'")
    system_run_log([path.join(source_path , f"test/rcgtest/doc/makedoc.sh"), source_path],
                       cwd=source_path,
                       env={
                           'RTS_VERSION': names,
                           'PATH': "/home/controls/texlive/2022/bin/x86_64-linux:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/opt/puppetlabs/bin:/opt/rtcds/userapps/release/cds/common/scripts:/opt/rtcds/tst/x2/scripts:/opt/rtcds/userscripts:/opt/rtapps/debian11/mathematica/bin",
                           'HOME': "/home/controls",
                            }
                   )

    # archive report
    if(archive_path != None):
        archive_fname = path.join(archive_path, "rcgtest.pdf")
        report_path = path.join(source_path, "test/rcgtest/doc/rcgtest.pdf")
        shutil.copy(report_path, archive_fname)
    else:
        archive_fname = None

    return archive_fname


def email_report(commit_names, sha_hash, test_summary, archive_report_fname):
    """
    Email
    :param commit_names: list of names for the tested commit
    :param sha_hash: has of the tested commit
    :param test_summary: a string summary of the tests
    :param archive_report_fname: path to archived report file
    :return:
    """
    global email_footer, from_addr, email_list, smtp_server

    alternate_names = "\n".join(commit_names)
    email_body = f"""Automated test were finished for the following RCG commit:
{sha_hash}
    
also named:
        
{alternate_names}
    
summary: 

{test_summary}    
    
A report is attached

{email_footer}
"""
    sendmail(from_addr, email_list, "[RCGTEST] RCG Test results", email_body, [archive_report_fname], server=smtp_server)


def email_fail_message(exception, commit_names, sha_hash):
    """
    Email a failure message when we get an exeption during builds or tests.

    :param exception: exception that caused the failure
    :return:
    """
    global email_footer, from_addr, email_list, smtp_server

    tb = traceback.format_exc()
    exception.emailed = True
    alternate_names = "\n".join(commit_names)
    email_body = f"""An exception occurred when building, installing, or running automated tests on the following commit:
    {sha_hash}
        
    also named:
            
    {alternate_names}
    
    traceback:
    
    {str(tb)}
    
    {email_footer}
    """
    sendmail(from_addr, email_list, "[RCGTEST] RCG Test model build failed", email_body, server=smtp_server)


def email_incomplete_message(tb):
    """email if an exception is thrown outside build and testing.  This usually means there's a bug in autotest or it's
    incompatible with the git commit under test

    :param tb: a traceback from the exception
    """
    global email_footer, from_addr, email_list, smtp_server

    email_body = f"""The automated RCG test didn't complete do tue an exception.  Stack trace below.
        
{str(tb)}      

{email_footer}
"""
    sendmail(from_addr, email_list, "[RCGTEST] RCG Test did not complete", email_body, server=smtp_server)

def auto_test(commit_request, build_only):
    """
    Run all tests and report results.

    if commit_request is None, auto_test picks the most deserving tag or branch.
    it commit_request is a git hash, auto test will try to test that hash
    if commit_request is a string, auto_test looks for the first commit with that exact name
    :param build_only: if true, build and install models, kernel modules, apps, but don't run tests
    :return: None
    """

    # create base path
    global base_path, source_path, test_mode, email_list
    system_error = False
    log(f"email_list = {email_list}")
    os.makedirs(base_path, exist_ok=True)

    clear_log()

    # setup history
    current_history = history.History()
    history_path = path.join(base_path, "histories.json")

    log("RCG autotest version " + autotest_version_str)

    report_startup_info()

    hist_key, sha_hash, commit_names = update_source(commit_request, history_path)

    archive_path = create_archive_paths(sha_hash, commit_names)

    try:
        built_modules, apps = build_project()

        env = build_install_models(built_modules)

        stop_models("x2iopsam")

        #install apps
        for app in apps:
            app.install()

        if not install_kernel_mods(source_path, built_modules):
            raise Exception("Failed to install some kernel modules")

        start_models("x2iopsam")

        # start apps
        for app in apps:
            app.start()

        # exit if build only
        if build_only:
            log("build-install only execution is finished")
            return

        test_summary = run_tests(env)

        # generate report
        archive_report_fname = generate_report(archive_path, commit_names)

        log(f"report archived to {archive_report_fname}")

    except SystemException as e:
        if not build_only:
            current_history.passed = False
            system_error = True
            email_fail_message(e, commit_names, sha_hash)
        raise e
    except Exception as e:
        if not build_only:
            current_history.passed = False
            email_fail_message(e, commit_names, sha_hash)
        raise e
    finally:
        if not build_only and not test_mode:
            # write out history
            log("recording history")
            # on a system error, the problem may be with the ATS machine and not the source.
            current_history.finished = not system_error
            history.add_history(history_path, hist_key, current_history)

            # archive log
            archive_log = path.join(archive_path, "output.log")
            log_path = path.join(base_path, "last_test.log")
            shutil.copy(log_path, archive_log)

    # email on successful report creation
    if not build_only:
        email_report(commit_names, sha_hash, test_summary, archive_report_fname)


if __name__ == "__main__":
    parser = argparse.ArgumentParser('Run an automatic RCG test on x2ats')
    parser.add_argument('commit_request', nargs='?', help="choose branch, tag or hash to test")
    parser.add_argument('-b', '--build_only', action='store_true', help="build and install models, modules and apps but don't run tests")
    parser.add_argument('-t', '--test', action='store_true', help="put into test mode. limits emails and some permanent effects.")
    parser.add_argument('-r', '--report', action='store_true', help="generate report in tmp directory based on past run and exit")
    parser.add_argument('-e', '--email', action='store_true', help="send a test email")

    args = parser.parse_args()
    commit_request = args.commit_request
    build_only = args.build_only

    test_mode = args.test

    if test_mode:
        log("switching to test mode")
        email_list = test_email_list
    else:
        email_list = release_email_list

    if args.email:
        log("sending test email")
        email_incomplete_message("this is a test email")
        sys.exit(0)

    if args.report:
        generate_report(archive_path=None, git_names=["regenerated"])
        sys.exit(0)

    commit_request = args.commit_request

    try:
        auto_test(commit_request, build_only)
    except Exception as e:
        error(str(e))
        tb = traceback.format_exc()
        log(str(tb))
        if not build_only and not (hasattr(e, "emailed") and e.emailed):
            email_incomplete_message(tb)
            sys.exit(1)
