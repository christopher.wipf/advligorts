
import atexit
import os
import os.path
import queue
import shutil
import socket
import subprocess
import sys
import tempfile
import threading
import time
import typing


class IntegrationState(object):
    """Global state for the integration test
    This is a singleton, don't create more."""
    def __init__(self):
        self.__remove_temp_dir = True
        self.__temp_dir = None
        self.__executables = {}
        self.__processes = {}
        self.__lock = threading.Lock()
        self.__unique_number = 0
        atexit.register(self.cleanup)

    def __get_temp_dir(self):
        with self.__lock:
            if self.__temp_dir is None:
                self.__temp_dir = tempfile.mkdtemp()
                print("Temporary Directory for this test is {0}".format(self.__temp_dir))
            return self.__temp_dir

    def get_unique_number(self):
        """Get a unique (to this run) number, helpful for building ids and unique names"""
        with self.__lock:
            val = self.__unique_number
            self.__unique_number += 1
            return val

    def preserve_files(self):
        """If preserve_files is called, then temporary directories will not be removed"""
        with self.__lock:
            self.__remove_temp_dir = False

    def register_executable(self, executable):
        """Register an executable with the system"""
        with self.__lock:
            self.__executables[executable.name()] = executable

    def get_executable(self, name):
        """Get an executable by name"""
        with self.__lock:
            return self.__executables[name]

    def register_process(self, process):
        """Register an process with the system.
        Once registered the system will fail the test if the executable reaches a
        FAILED state."""
        print("\n\nRegistering {0}".format(process.name))
        with self.__lock:
            self.__processes[process.name] = process

    def check_valid(self):
        """Check all processes and raise an exception if one has failed"""
        for process_name in self.__processes:
            if self.__processes[process_name].state() == Process.FAILED:
                raise RuntimeError("Process {0} failed, test aborted".format(process_name))

    def cleanup(self):
        """Cleanup function, closes processes, removed temp dirs, ...
        called at program termination.
        Generally there is no need to directly call this method."""
        # print ("\n\n\n\n\nin cleanup\n\n\n\n\n\n")
        # something has gone wrong if we need the lock, but might as well
        with self.__lock:
            # print("Will check on processes {0}".format(self.__processes.keys()))
            for key in self.__processes.keys():
                try:
                    p = self.__processes[key]
                    # print('checking on {0} state is {1}'.format( key, p.state()))
                    p.stop()
                except:
                    pass
            if self.__remove_temp_dir and self.__temp_dir is not None:
                try:
                    print("Removing temp dirs")
                    shutil.rmtree(self.__temp_dir)
                except:
                    pass

    def temp_dir(self, name):
        """Get a temporary directory of name under the integration test temporary directory tree"""
        final = os.path.join(self.__get_temp_dir(), name)
        os.makedirs(final, exist_ok=True)
        return final

    def log(self, msg):
        """log something into the temp dir"""
        log_dir = os.path.join(self.__get_temp_dir(), "logs")
        try:
            os.mkdir(log_dir)
        except:
            pass
        log_name = os.path.join(log_dir, "log")
        with open(log_name, 'at') as f:
            f.write(msg)
            f.write("\n")


# The instance of the test state.
state = IntegrationState()

def log(msg):
    state.log(msg)


class Executable(object):
    """An Executable represents a binary or script that can be executed.
    This is used to find the location of the item to run, not to run it."""

    def __init__(self, name: str, hints: typing.Sequence = (), description=''):
        self.__description = description
        self.__name = name
        self.__path = None
        if os.path.exists(name):
            self.__path = os.path.join('./', name)
        else:
            environ_hint = os.environ.get('INTEGRATION_EXECUTABLE_HINT', None)
            hint_list = list(hints)
            if environ_hint is not None:
                hint_list.append(environ_hint)
            for root in hint_list:
                location = os.path.join(root, name)
                if os.path.exists(location):
                    self.__path = location
                    break
        if self.__path is None:
            raise RuntimeError("Unable to find executable '{0}'".format(name))
        print("found executable {0}".format(self.__path))
        state.register_executable(self)

    def path(self):
        return self.__path

    def name(self):
        return self.__name

    def description(self):
        return self.__description


class Process(object):
    """A process represents an OS level process that needs to be run for part of the integration test"""

    UNINITIALIZED = 0
    RUNNING = 1
    FAILED = 2
    STOPPED = 3

    def __init__(self, executable_name, arguments: typing.Sequence = (), exit_codes: typing.Sequence = (0,), env=None):
        self.__name = "{0}__{1}".format(state.get_unique_number(), executable_name)
        self.__executable = state.get_executable(executable_name)
        self.__arguments = list(arguments)
        self.__exit_codes = set(exit_codes)

        self.__lock = threading.Lock()
        self.__process_lock = threading.Lock()

        self.__state = Process.UNINITIALIZED
        self.__thread = None
        self.__stopping = threading.Event()
        self.__is_stopped = threading.Event()
        self.__is_failed = threading.Event()
        self.__ignored = False
        self.__process = None
        self.__env = env

        state.register_process(self)

    def name(self):
        return self.__name

    def state(self):
        with self.__lock:
            if self.__state == Process.RUNNING:
                if self.__is_stopped.is_set():
                    self.__state = Process.STOPPED
                if self.__is_failed.is_set():
                    if self.__ignored:
                        self.__state = Process.STOPPED
                    else:
                        self.__state = Process.FAILED
            elif self.__state == Process.STOPPED:
                if self.__is_failed.is_set() and not self.__ignored:
                    self.__state = Process.FAILED
            # print('self.state = {0}'.format(self.__state))
            return self.__state

    def stop(self):
        with self.__lock:
            if self.__state == Process.RUNNING:
                self.__stopping.set()
                with self.__process_lock:
                    if self.__process is not None:
                        self.__process.kill()
                self.__is_stopped.wait()

            if self.__thread is not None:
                self.__thread.join()

            # we may want to also mark it as ignored now.

    def run(self):
        with self.__lock:
            if self.__state != Process.UNINITIALIZED:
                raise RuntimeError("Process {0} has already by started".format(self.__name))
            if self.__thread is not None:
                raise RuntimeError("Possible stale thread object found, not starting another".format(self.__name))

            response_state = queue.Queue()

            args = [self.__executable.path()]
            args.extend(self.__arguments)

            state.log("About to launch process with args {0}".format(args))

            self.__thread = threading.Thread(
                target=self.__process_main_loop,
                args=(args, response_state),
                daemon=True
            )
            self.__thread.start()

            self.__state = response_state.get()
            print("Received state {0} from the process".format(self.__state))
            if self.__state != Process.RUNNING:
                raise RuntimeError('Process not running')

    def get_output_log(self):
        """Read the contents of the stdout log of this process.
        Note that this may not be complete due to output buffering"""
        return self.__get_log("out")

    def get_err_log(self):
        """Read the contents of the stderr log of this process.
        Note that this may not be complete due to output buffering"""
        return self.__get_log("err")

    def __get_log(self, log_type):
        log_root = state.temp_dir("logs")
        stdout_name = os.path.join(log_root, "{0}.{1}".format(self.__name, log_type))
        with open(stdout_name, 'rt') as f:
            return f.read()

    def ignore(self):
        """Ignore unexpected terminations of the process from this point on.  Signals the system that
        the test no longer cares about the state of this process."""
        with self.__lock:
            self.__ignored = True

    def __process_main_loop(self, arguments, response_queue):
        started_ok = False
        try:
            log_root = state.temp_dir("logs")

            stdout_name = os.path.join(log_root, "{0}.out".format(self.__name))
            stderr_name = os.path.join(log_root, "{0}.err".format(self.__name))

            with open(stdout_name, 'wb') as stdout:
                with open(stderr_name, 'wb') as stderr:
                    with subprocess.Popen(args=arguments,
                                          close_fds=True,
                                          shell=False,
                                          bufsize=0,
                                          stdout=stdout,
                                          stderr=stderr,
                                          env=self.__env) as p:
                        with self.__process_lock:
                            self.__process = p
                        response_queue.put(Process.RUNNING)
                        started_ok = True

                        print("Started {0} as pid {1}".format(arguments, p.pid))

                        cur_status = p.wait()
                        with self.__process_lock:
                            self.__process = None
                            
                        if not self.__stopping.set():
                            if cur_status not in self.__exit_codes:
                                print("Process pid {0} ({2}) stopped unexpectedly with an exit code of {1}.".format(p.pid, cur_status, self.__name))
                                self.__is_failed.set()
                            else:
                                print("Process pid {0} ({2}) stopped with an exit code of {1}".format(p.pid, cur_status, self.__name))
        finally:
            if not started_ok:
                response_queue.put(Process.FAILED)
                self.__is_stopped.set()
                self.__is_failed.set()
        self.__is_stopped.set()


def transform_text_file(input: str, output: str, substitutions: typing.Sequence, encoding:str = "utf8"):
    """Given an input file, apply text substitutions to the contents and write to output.
    input is the name of the file, it is searched for in the following locations:
     * the current working directory
     * the directory of the main script"""

    paths = [input, os.path.join(os.path.dirname(sys.argv[0]), input)]
    for file_name in paths:
        try:
            with open(file_name, 'rb') as f:
                print("found {0} at {1}".format(input, file_name))
                data = f.read().decode(encoding=encoding)
                for substitution in substitutions:
                    data = data.replace(substitution[0], substitution[1])
                with open(output, 'wb') as out:
                    out.write(data.encode(encoding=encoding))
                return
        except:
            pass
    raise RuntimeError("Unable to file file {0}".format(input))


def append_to_text_file(file_name: str, text: str, encoding:str = 'utf8'):
    """Add text to the given file.
    Note, this is different from transform_text_file as file_name is expected to
    be in a writeable output area already."""
    with open(file_name, 'ab') as f:
        f.write(text.encode(encoding=encoding))


class Sequence(object):
    """A Sequence represents a series of actions (callables) done in order.
    Failures are checked for between each action"""

    def __init__(self, actions: typing.Sequence):
        for action in actions:
            state.check_valid()
            print("About to run {0}".format(action))
            action()
        state.check_valid()
        print("Testing Sequence completed")


def callable(f, *args, **kwargs):
    def wrapper():
        print("Calling {0} with args = {1} kwargs = {2}".format(f, args, kwargs))
        f(*args, **kwargs)
    return wrapper


def wait_tcp_server(port, hostname="localhost", timeout=5, pause_length=0.1):
    def wrapper():
        start = time.time()
        end = start + timeout
        while True:
            c = None
            try:
                c = socket.create_connection((hostname, port), timeout=timeout)
                print("Can connect to {0}:{1} after {2}s".format(hostname, port, time.time()-start))
                break
            except:
                time.sleep(pause_length)
            finally:
                if c is not None:
                    c.close()
            if time.time() > end:
                raise TimeoutError("Connection timed out")
    return wrapper


def require_readable(files=[], description=""):
    def wrapper():
        for file_name in files:
            print("Accessing {0}".format(file_name))
            try:
                with open(file_name, "rb"):
                    pass
            except Exception as e:
                raise RuntimeError("Error confirming access to {0} {1}, error: {2}".format(file_name, description, str(e)))

    return wrapper


def wait_for(predicate, timeout=1.0, step=0.5, description="Predicate"):
    def wrapper():
        start = time.time()
        end = start + timeout
        while True:
            if predicate():
                return
            time.sleep(step)
            if time.time() > end:
                raise TimeoutError("{0} was not true in time".format(description))

    return wrapper


def wait(seconds=1.0):
    def wrapper():
        time.sleep(seconds)

    return wrapper
