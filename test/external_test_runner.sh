#!/bin/bash

# https://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself

ROOT="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

PYTHONPATH=${ROOT}/python_modules:$PYTHONPATH
export PYTHONPATH
echo Python path is $PYTHONPATH

# tell the Python integration driver to search in /usr/bin as well
INTEGRATION_EXECUTABLE_HINT=/usr/bin
export INTEGRATION_EXECUTABLE_HINT

pushd $ROOT/tests

failures=""
successes=""
exit_status=0
testnum=1

for testname in *.py; do
  printf "\n*****************\n* $testnum Running test $testname\n*****************\n\n"
  /usr/bin/timeout -v 10m python3 -B $testname
  code=$?
  if [ "$code" -ne 0 ]; then
    failures="${failures}\n\t${testnum}) ${testname} failed"
    if [ "$code" -ge "124" ]; then
      echo "Failure - timeout"
      failures="${failures} - timed out"
    else
      echo "Failure"
    fi
    exit_status=1
  else
    echo "Success"
    successes="${successes}\n\t${testnum}) ${testname} succeeded"
  fi
  ((testnum+=1))
done

popd

printf "\n\nTests that succeeded"
if [[ -z $successes ]]; then
  printf "\n\tNone all failed!!!!"
else
  printf "${successes}"
fi

printf "\n\nTests that failed"
if [[ -z $failures ]]; then
  printf "\n\tNone all passed!!!"
else
  printf "${failures}"
fi
printf "\n"

exit $exit_status