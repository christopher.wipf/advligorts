#include "DolphinKernelProxy.hpp"


//libspdlog-dev
#include "spdlog/spdlog.h"

#include <vector>


std::unique_ptr<DolphinKernelProxy> DolphinKernelProxy::create_instance()
{
    std::unique_ptr<DolphinKernelProxy> me_ptr (new DolphinKernelProxy());

    me_ptr->_requests_socket = NetlinkSocket::create_instance(DOLPHIN_DAEMON_REQ_LINK_ID, DOLPHIN_DAEMON_REQ_GRP_ID, 1.0);
    if( !me_ptr->_requests_socket ) return nullptr;

    //We don't need a mcast group for this interface because it is unicast back to us
    me_ptr->_dolphin_proxy_socket = NetlinkSocket::create_instance(DOLPHIN_DAEMON_DPROXY_LINK_ID, 0, 3.0); 
    if( !me_ptr->_dolphin_proxy_socket ) 
    {
	    spdlog::error("NetlinkSocket::create_instance(DOLPHIN_DAEMON_DPROXY_LINK_ID, ...) failed. "
			  "This happens when the dolphin proxy kernel module has not been loaded yet.");
	    return nullptr;
    }


    return me_ptr;
}

DolphinKernelProxy::~DolphinKernelProxy()
{
}

DolphinKernelProxy::DolphinKernelProxy()
{
}

void DolphinKernelProxy::thread_body()
{

    ssize_t ret;
    char buffer[DOLPHIN_DAEMON_MAX_PAYLOAD] = {0};

    while( !should_stop() )
    {
        ret = _requests_socket->recvmsg(buffer, DOLPHIN_DAEMON_MAX_PAYLOAD);

        if (ret < 0)
        {
            if((errno == EAGAIN) || (errno == EWOULDBLOCK)) continue; //Timout

            spdlog::warn("DolphinKernelProxy - Error {}, when receiving netlink multicast message from group {}", 
                         ret, DOLPHIN_DAEMON_REQ_GRP_ID);
            continue;
        }


        unsigned payload_sz =  ret; //Header is removed by NetlinkSocket
        spdlog::debug("Received {} byte request, payload_sz : {}", ret, payload_sz);

        all_dolphin_daemon_msgs_union any_msg;
        any_msg.as_hdr = (dolphin_mc_header*)buffer;//(dolphin_mc_header *) NLMSG_DATA((struct nlmsghdr *) &buffer);
        

        switch ( any_msg.as_hdr->msg_id )
        {
            case DOLPHIN_DAEMON_ALLOC_REQ:

                //Verify message length
                if ( check_alloc_req_valid(any_msg.as_alloc_req, payload_sz) == 0)
                {
                    spdlog::error("DolphinKernelProxy - Invalid message size {}, for num_segments {}", 
                                  payload_sz, any_msg.as_alloc_req->num_segments);
                    build_and_send_alloc_resp_error(DOLPHIN_ERROR_MALFORMED_MSG);
                    break;
                }
                handle_alloc_req( any_msg.as_alloc_req, payload_sz );

            break;
            case DOLPHIN_DAEMON_FREE_REQ:
                handle_free_all_req( any_msg.as_free_req, payload_sz );
            break;
            default:
                spdlog::error("DolphinKernelProxy - Invalid message id of {}, discarding message.", any_msg.as_hdr->msg_id);
                build_and_send_alloc_resp_error(DOLPHIN_ERROR_MALFORMED_MSG);
            break;

        }

        spdlog::debug("DolphinKernelProxy - Done handling kernel multicast request message.");

    }



}

void DolphinKernelProxy::handle_free_all_req( dolphin_mc_free_all_req * req_ptr, unsigned bytes)
{

    //Proxy the request to the kernel module
    req_ptr->header.interface_id = DOLPHIN_KM_INTERFACE_ID;
    _dolphin_proxy_socket->sendmsg(req_ptr, bytes);
    spdlog::info("DolphinKernelProxy::handle_free_all_req() - All Dolphin resources released.");

    return;
}


void DolphinKernelProxy::handle_alloc_req( dolphin_mc_alloc_req * req_ptr, unsigned bytes )
{
    DOLPHIN_ERROR_CODES status;
    char buffer[DOLPHIN_DAEMON_MAX_PAYLOAD] = {0};
    dolphin_mc_alloc_resp * resp_ptr = (dolphin_mc_alloc_resp*)buffer;

    spdlog::info("dolphin_mc_alloc_req message received, proxying to dproxy km.");
    spdlog::debug("There are {} num_segments requested.", req_ptr->num_segments);

    for(int i=0; i < req_ptr->num_segments; ++i)
    {
        spdlog::debug("Segment {}, adapter {}", req_ptr->segments[i].segment_id, req_ptr->segments[i].adapter_num);
    }


    //Proxy the request to the dolphin proxy kernel module
    req_ptr->header.interface_id = DOLPHIN_KM_INTERFACE_ID;
    _dolphin_proxy_socket->sendmsg(req_ptr, bytes);
    int ret = _dolphin_proxy_socket->recvmsg(buffer, DOLPHIN_DAEMON_MAX_PAYLOAD);
    if(ret < 0 ) 
    {
        spdlog::error("The dolphin kernel space module did not respond to the proxied dolphin_mc_alloc_req.");
        return build_and_send_alloc_resp_error(DOLPHIN_ERROR_KERNEL_MODULE_TIMOUT);
    }
    else
    {
        spdlog::info("DolphinKernelProxy::handle_alloc_req() - Got response from dproxy km, "
                      "sending back to requestor. Response status: {}, ret size {}", 
                      resp_ptr->status, ret);

        //If we got a response, proxy back to requestor
        resp_ptr->header.interface_id = RT_KM_INTERFACE_ID;
        send_netlink_request_response(buffer, ret);
    }


    return;
    
}


void DolphinKernelProxy::build_and_send_alloc_resp_error(DOLPHIN_ERROR_CODES status)
{
    //Build error response message
    dolphin_mc_alloc_resp resp;
    memset(&resp, 0, sizeof(resp));
    resp.header.msg_id = DOLPHIN_DAEMON_ALLOC_RESP;
    resp.header.preamble_seq = DOLPHIN_DAEMON_MSG_PREAMBLE;
    resp.status = status;
    resp.num_addrs = 0;

    //No returned segments on error, so msg is just struct size
    send_netlink_request_response(&resp, sizeof(resp));
}

void DolphinKernelProxy::send_netlink_request_response(void * msg_ptr, unsigned sz_bytes)
{
    // Any responses we send with this method are going back to to original requestor
    ((dolphin_mc_header *) msg_ptr)->interface_id = RT_KM_INTERFACE_ID;

   _requests_socket->sendmsg(msg_ptr, sz_bytes); 
}







