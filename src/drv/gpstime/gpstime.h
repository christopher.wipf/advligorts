#ifndef LIGO_GPSTIME_H
#define LIGO_GPSTIME_H

#include <linux/ioctl.h>

// Get GPS card status (locked or unlocked)
#define IOCTL_SYMMETRICOM_STATUS 0

// Get current GPS time
#define IOCTL_SYMMETRICOM_TIME 1

#define IOCTL_GPSTIME_PAUSE _IO('x', 2)
#define IOCTL_GPSTIME_RESUME _IO('x', 3)
#define IOCTL_GPSTIME_RESET _IO('x', 4)
#define IOCTL_GPSTIME_STEP _IO('x', 5)

// Define the type of manual synchronization that
// must be done to the system to get correct time
#define STATUS_SYMMETRICOM_NO_SYNC 0
#define STATUS_SYMMETRICOM_YEAR_SYNC 1
#define STATUS_SYMMETRICOM_LOCALTIME_SYNC 2



#endif
