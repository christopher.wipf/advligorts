add_executable(gps_module_utils
        gps_module_utils.cc)
target_include_directories(gps_module_utils PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/../")

install(TARGETS gps_module_utils DESTINATION bin)