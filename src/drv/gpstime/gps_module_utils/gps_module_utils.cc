//
// Created by jonathan.hanks on 2/28/22.
//

#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <iterator>
#include <string>
#include <vector>

#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <asm/ioctl.h>


#include "gpstime.h"

enum class Action {
    PAUSE,
    RESUME,
    RESET,
    STEP,
    TIME,
    STATUS,
};

class FDHolder {
    constexpr static int invalid() noexcept
    {
        return -1;
    }
public:
    FDHolder() = default;
    explicit FDHolder(const char*name, int mode=O_RDWR): fd_{::open(name, mode)} {}
    FDHolder(const FDHolder&) = delete;
    FDHolder(FDHolder&& other) noexcept: fd_{other.fd_}
    {
        other.fd_ = invalid();
    }
    ~FDHolder()
    {
        close();
    }
    FDHolder& operator=(const FDHolder&) = delete;
    FDHolder& operator=(FDHolder&& other) noexcept
    {
        close();
        fd_ = other.fd_;
        other.fd_ = invalid();
        return *this;
    }
    int get() const
    {
        return fd_;
    }
    operator bool() const noexcept
    {
        return fd_ != invalid();
    }
private:
    void
    close() noexcept
    {
        if (fd_ != invalid())
        {
            ::close(fd_);
            fd_ = invalid();
        }
    }
    int fd_{invalid()};
};

template <typename C, typename OutContainer = std::vector< typename C::key_type > >
OutContainer
keys(const C& container)
{
    OutContainer out{};
    for (const auto& cur:container)
    {
        out.emplace_back(cur.first);
    }
    return out;
}

void
usage(char* progname, const std::vector<std::string>&& options)
{
    std::cout << "Usage:\n\t" << progname << " <option>\n\n";
    std::cout << "Where option is one of:\n\t";
    std::copy(options.begin(), options.end(), std::ostream_iterator<std::string>(std::cout, "\n\t"));
    std::exit(1);
}

Action
select_action(int argc, char* argv[])
{
    std::map<std::string, Action> action_map{
        {"pause", Action::PAUSE},
        {"resume", Action::RESUME},
        {"reset", Action::RESET},
        {"step", Action::STEP},
        {"time", Action::TIME},
        {"status", Action::STATUS}
    };
    for (auto i = 1; i < argc; ++i)
    {
        std::string arg{argv[i]};
        auto it = action_map.find(arg);
        if (it == action_map.end())
        {
            usage(argv[0], keys(action_map));
        }
        return it->second;
    }
    usage(argv[0], keys(action_map));
    throw std::runtime_error("bad arguments");
}

void
ioctl_simple(int fd, int ioctl_command)
{
    long dummy[3];
    ioctl( fd, ioctl_command, &dummy );
}

void
ioctl_time(int fd) {
    unsigned long t[3];
    ioctl( fd, IOCTL_SYMMETRICOM_TIME, &t);
    t[1] *= 1000;
    t[1] += t[2];
    std::cout << t[0] << ":" << t[1] << "\n";
}

void
read_status()
{
    std::fstream input("/sys/kernel/gpstime/debug_pause_status", std::ios::binary | std::ios_base::in );
    if (input)
    {
        std::vector<char> data{};
        data.resize(1024);
        input.read(data.data(), 1024);
        data[1024-1] = '\0';
        std::cout << data.data() << std::endl;
    }

}

int
main(int argc, char* argv[])
{
    FDHolder fd("/dev/gpstime");
    switch (select_action(argc, argv))
    {
    case Action::PAUSE:
        ioctl_simple(fd.get(), IOCTL_GPSTIME_PAUSE);
        break;
    case Action::RESUME:
        ioctl_simple(fd.get(), IOCTL_GPSTIME_RESUME);
        break;
    case Action::RESET:
        ioctl_simple(fd.get(), IOCTL_GPSTIME_RESET );
        break;
    case Action::STEP:
        ioctl_simple(fd.get(), IOCTL_GPSTIME_STEP);
        break;
    case Action::TIME:
        ioctl_time(fd.get());
        break;
    case Action::STATUS:
        read_status();
        break;
    default:
        throw std::runtime_error("Unknown option");
    }
    return 0;
}