
add_executable(mbuf_probe
        gap_check.cc
        mbuf_probe.cc
        mbuf_decoders.cc
        analyze_daq_multi_dc.cc
        analyze_rmipc.cc
        analyze_awg_data.cc
        analyze_header.cc
        check_size.cc
        analyze_tp_cfg.cpp
        timing_delta.cc
        list_dcus.cc)
target_include_directories(mbuf_probe PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/..
        ${CMAKE_CURRENT_SOURCE_DIR}/../../include
        ${CMAKE_SOURCE_DIR}/src/shmem
        )
target_link_libraries(mbuf_probe PUBLIC
        driver::shmem
        driver::ini_parsing
        Threads::Threads
        )
target_requires_cpp11(mbuf_probe PRIVATE)

install(TARGETS mbuf_probe DESTINATION bin)