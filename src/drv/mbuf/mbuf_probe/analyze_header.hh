//
// Created by erik.vonreis on 4/29/21.
//

#ifndef DAQD_TRUNK_ANALYZE_HEADER_H
#define DAQD_TRUNK_ANALYZE_HEADER_H

#endif // DAQD_TRUNK_ANALYZE_HEADER_H

#include <cstddef>
#include "mbuf_probe.hh"

namespace analyze
{

    /**
     * Prints info from standard mbuf header for certain mbuf structures.
     * Returns the structure type if it can be determined and is
     * incorporated in MBufStructures, otherwise returns MBUF_INVALID
     */
    MBufStructures analyze_header(volatile void * header);
}