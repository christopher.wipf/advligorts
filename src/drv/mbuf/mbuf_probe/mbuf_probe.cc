//
// Created by jonathan.hanks on 1/19/18.
//
#include <deque>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <sstream>

#include <stdio.h>
#include <string.h>

#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <unistd.h>

#include "cds-shmem.h"
#include "mbuf.h"

#include "mbuf_probe.hh"
#include "analyze_daq_multi_dc.hh"
#include "analyze_rmipc.hh"
#include "analyze_awg_data.hh"
#include "analyze_tp_cfg.hh"
#include "gap_check.hh"
#include "check_size.hh"
#include "timing_delta.hh"
#include "list_dcus.hh"

const int OK = 0;
const int ERROR = 1;

void
usage( const char* progname )
{
    std::cout << progname << " a tool to manipulate mbuf buffers\n";
    std::cout << "\nUsage " << progname << " options.\n";
    std::cout << "Where options are:\n";
    std::cout << "\tlist - request a list of mbufs and status\n";
    std::cout << "\tcreate - create a mbuf\n";
    std::cout << "\tcopy - copy a mbuf to a file\n";
    std::cout << "\tdelete - decrement the usage count of an mbuf\n";
    std::cout << "\tanalyze - continually read the mbuf and do some analysis\n";
    std::cout << "\tgap_check - continually scan the buffers looking for "
                 "gaps/jumps in the data stream\n";
    std::cout << "\tcheck_sizes - check the given buffer's data segment sizes "
                 "against the expected size as calculated in the ini files.\n";
    std::cout << "\ttiming - calculate timing deltas between two buffers.\n";
    std::cout << "\tlist_dcus - output the dcuids in a given buffer.\n";
    std::cout << "\t-b <buffer name> - The name of the buffer to act on\n";
    std::cout << "\t                   specified twice for the timing mode.\n";
    std::cout << "\t-m <buffer size in MB> - The size of the buffer in "
                 "megabytes (defaults to 64)\n";
    std::cout << "\t-S <buffer size> - Buffer size in bytes (must be a "
                 "multiple of 1MB)\n";
    std::cout << "\t-o <filename> - Output file for the copy operation "
                 "(defaults to probe_out.bin)\n";
    std::cout << "\t-i <filename> - Path to the master ini file (only used "
                 "for the check_size action)\n";
    std::cout
        << "\t--struct <type> - Type of structure to analyze [rmIpcStr]\n";
    std::cout << "\t--dcu <dcuid> - Optional DCU id used to select a dcu for "
                 "analysis\n";
    std::cout << "\t\twhen analyzing daq_multi_cycle buffers.\n";
    std::cout << "\t-d <offset:format> - Decode the data section, optional\n";
    std::cout
        << "\t\tPart of the analyze command.  Decode a specified stretch\n";
    std::cout << "\t\tof data, with a given format specifier (same as python "
                 "struct)\n";
    std::cout << "\t-h|--help - This help\n";
    std::cout << "\n";
    std::cout << "Analysis modes:\n";
    std::cout << "\trmIpcStr (or rmipcstr) Analyze a models output buffer\n";
    std::cout
        << "\tdaq_multi_cycle Analyze the output of a streamer/local_dc\n";
    std::cout << "\tAWG_DATA Analyze the data streaming from awg.\n";
    std::cout << "\tTP_CFG Analyze the test point configuration shared memory.\n";
}

ConfigOpts
parse_options( int argc, char* argv[] )
{
    ConfigOpts                            opts;
    std::map< std::string, MBufCommands > command_lookup;

    command_lookup.insert( std::make_pair( "list", LIST ) );
    command_lookup.insert( std::make_pair( "create", CREATE ) );
    command_lookup.insert( std::make_pair( "copy", COPY ) );
    command_lookup.insert( std::make_pair( "delete", DELETE ) );
    command_lookup.insert( std::make_pair( "analyze", ANALYZE ) );
    command_lookup.insert( std::make_pair( "gap_check", GAP_CHECK ) );
    command_lookup.insert( std::make_pair( "check_size", CHECK_SIZE ) );
    command_lookup.insert( std::make_pair( "timing", TIMING_DELTA ) );
    command_lookup.insert( std::make_pair( "list_dcus", LIST_DCUS ) );

    std::map< std::string, MBufStructures > struct_lookup;
    struct_lookup.insert( std::make_pair( "rmIpcStr", MBUF_RMIPC ) );
    struct_lookup.insert( std::make_pair( "rmipcstr", MBUF_RMIPC ) );
    struct_lookup.insert(
        std::make_pair( "daq_multi_cycle", MBUF_DAQ_MULTI_DC ) );
    struct_lookup.insert( std::make_pair( "awg_data", MBUF_AWG_DATA ) );
    struct_lookup.insert( std::make_pair( "AWG_DATA", MBUF_AWG_DATA ) );
    struct_lookup.insert( std::make_pair( "tp_cfg", MBUF_TP_CFG ) );
    struct_lookup.insert( std::make_pair( "TP_CFG", MBUF_TP_CFG ) );

    std::deque< std::string > args;
    for ( int i = 1; i < argc; ++i )
    {
        args.push_back( argv[ i ] );
    }
    while ( !args.empty( ) )
    {
        std::string arg = args.front( );
        args.pop_front( );
        if ( arg == "-h" || arg == "--help" )
        {
            opts.action = INVALID;
            opts.error_msg = "";
            return opts;
        }
        else if ( arg == "-b" )
        {
            if ( args.empty( ) )
            {
                opts.set_error( "You must specify a buffer name after -b" );
                return opts;
            }
            if ( opts.buffer_name.empty( ) )
            {
                opts.buffer_name = args.front( );
            }
            else if ( opts.secondary_buffer_name.empty( ) )
            {
                opts.secondary_buffer_name = args.front( );
            }
            else
            {
                opts.set_error( "-b may be used a maximum of two times." );
            }
            args.pop_front( );
        }
        else if ( arg == "-m" || arg == "-S" )
        {
            if ( args.empty( ) )
            {
                opts.set_error( "You must specify a size when using -m or -S" );
                return opts;
            }
            std::size_t        multiplier = ( arg == "-m" ? 1024 * 1024 : 1 );
            std::istringstream os( args.front( ) );
            os >> opts.buffer_size;
            args.pop_front( );
            opts.buffer_size *= multiplier;
        }
        else if ( arg == "-o" )
        {
            if ( args.empty( ) )
            {
                opts.set_error( "You must specify a filename when using -o" );
                return opts;
            }
            opts.output_fname = args.front( );
            args.pop_front( );
        }
        else if ( arg == "-d" )
        {
            if ( args.empty( ) )
            {
                opts.set_error(
                    "You must specify a format string when using -d" );
                return opts;
            }
            std::string format = args.front( );
            args.pop_front( );
            std::string::size_type split = format.find( ':' );
            if ( split == std::string::npos )
            {
                opts.set_error( "You must have a format strip when using -d" );
                return opts;
            }
            std::string        offset_str = format.substr( 0, split );
            std::string        format_spec = format.substr( split + 1 );
            std::istringstream is( offset_str );
            std::size_t        offset = 0;
            is >> offset;
            opts.decoder = DataDecoder( offset, format_spec );
        }
        else if ( arg == "-i" )
        {
            if ( args.empty( ) )
            {
                opts.set_error(
                    "You must specify a ini file path when using -i" );
                return opts;
            }
            opts.ini_file_fname = args.front( );
            args.pop_front( );
        }
        else if ( arg == "--struct" )
        {
            if ( args.empty( ) )
            {
                opts.set_error(
                    "You must specify a structure type when using --struct" );
                return opts;
            }
            std::map< std::string, MBufStructures >::iterator it;
            it = struct_lookup.find( args.front( ) );
            if ( it == struct_lookup.end( ) )
            {
                opts.set_error( "Invalid structure type passed to --struct" );
                return opts;
            }
            opts.analysis_type = it->second;
            args.pop_front( );
        }
        else if ( arg == "--dcu" )
        {
            if ( args.empty( ) )
            {
                opts.set_error( "You must specify a dcu id when using --dcu" );
                return opts;
            }
            std::istringstream is( args.front( ) );
            is >> opts.dcu_id;
            args.pop_front( );
        }
        else
        {
            std::map< std::string, MBufCommands >::iterator it;
            it = command_lookup.find( arg );
            if ( it == command_lookup.end( ) )
            {
                std::ostringstream os;
                os << "Unknown argument " << args.front( );
                opts.set_error( os.str( ) );
                return opts;
            }
            else
            {
                if ( !opts.select_action( it->second ) )
                {
                    return opts;
                }
            }
        }
    }
    opts.validate_options( );
    opts.buffer_size_mb = opts.buffer_size/(1024*1024);
    return opts;
};

void
shmem_inc_segment_count( const char* sys_name, size_t buf_sz_mb )
{
    int    fd = -1;
    size_t name_len = 0;
    int ret1, ret2;

    if ( !sys_name )
        return;
    name_len = strlen( sys_name );
    if ( name_len == 0 || name_len > MBUF_NAME_LEN )
    {
        return;
    }

    if ( ( fd = open( "/dev/mbuf", O_RDWR | O_SYNC ) ) < 0 )
    {
        fprintf( stderr, "Couldn't open /dev/mbuf read/write\n" );
        return;
    }
    struct mbuf_request_struct req;
    req.size = buf_sz_mb * 1024 * 1024;
    strcpy( req.name, sys_name );
    //When we close the fd, our shmem will get cleaned up once
    //so we call IOCTL_MBUF_ALLOCATE 2 times so one stays open
    ret1 = ioctl( fd, IOCTL_MBUF_ALLOCATE, &req ); 
    ret2 = ioctl( fd, IOCTL_MBUF_ALLOCATE, &req );

    if(ret1 < req.size || ret2 < req.size)
    {
        fprintf( stderr, "Couldn't lookup the already allocated buffer "
                         "(%s) to increase the usage count.\n", sys_name);
    }


    close( fd );
}

void
shmem_dec_segment_count( const char* sys_name )
{
    int    fd = -1;
    size_t name_len = 0;

    if ( !sys_name )
        return;
    name_len = strlen( sys_name );
    if ( name_len == 0 || name_len > MBUF_NAME_LEN )
    {
        return;
    }

    if ( ( fd = open( "/dev/mbuf", O_RDWR | O_SYNC ) ) < 0 )
    {
        fprintf( stderr, "Couldn't open /dev/mbuf read/write\n" );
        return;
    }
    struct mbuf_request_struct req;
    req.size = 1;
    strcpy( req.name, sys_name );
    if(ioctl( fd, IOCTL_MBUF_DEALLOCATE, &req ) != 0)
    {
        fprintf( stderr, "There was an issue decrementing the use count of the %s shmem.\n", sys_name);
    }
    close( fd );
}

/**
 * @brief Wrapper to make sure we always close C style FILE*
 */
class safe_file
{
    FILE* _f;
    safe_file( );
    safe_file( const safe_file& other );
    safe_file& operator=( const safe_file& other );

public:
    safe_file( FILE* f ) : _f( f ){};
    ~safe_file( )
    {
        if ( _f )
        {
            fclose( _f );
            _f = NULL;
        }
    }
    FILE*
    get( ) const
    {
        return _f;
    }
};

int
copy_shmem_buffer( const volatile void* buffer,
                   const std::string&   output_fname,
                   size_t               req_size )
{
    std::vector< char > dest_buffer( req_size );

    safe_file f( fopen( output_fname.c_str( ), "wb" ) );
    if ( !f.get( ) )
        return 1;
    const volatile char* src =
        reinterpret_cast< const volatile char* >( buffer );
    memcpy( dest_buffer.data( ), const_cast< const char* >( src ), req_size );
    fwrite( dest_buffer.data( ), 1, dest_buffer.size( ), f.get( ) );
    return 0;
}

void
list_shmem_segments( )
{
    std::vector< char > buf( 100 );
    std::ifstream       f( "/sys/kernel/mbuf/status" );
    while ( f.read( buf.data( ), buf.size( ) ) )
    {
        std::cout.write( buf.data( ), buf.size( ) );
    }
    if ( f.gcount( ) > 0 )
    {
        std::cout.write( buf.data( ), f.gcount( ) );
    }
}

int
handle_analyze( const ConfigOpts& opts )
{
    // size in mb
    shmem::shmem mem( opts.buffer_name.c_str( ), opts.buffer_size_mb );
    auto*        buffer = mem.mapping< void >( );
    if(NULL == buffer)
    {
        fprintf(stderr, "Shared memory was not opened.\nbuffer=%x\nmem=%x\n",
                 buffer);
        return ERROR;
    }
    switch ( opts.analysis_type )
    {
    case MBUF_RMIPC:
        analyze::analyze_rmipc( buffer, opts.buffer_size, opts );
        break;
    case MBUF_DAQ_MULTI_DC:
        analyze::analyze_multi_dc( buffer, opts.buffer_size, opts );
        break;
    case MBUF_AWG_DATA:
        analyze::analyze_awg_data( buffer, opts.buffer_size, opts );
        break;
    case MBUF_TP_CFG:
        analyze::analyze_tp_cfg(buffer, opts.buffer_size, opts);
        break;
    case MBUF_INVALID:
    default:
        std::cout << "Unknown analysis type: " << opts.analysis_type
                  << std::endl;
        return ERROR;
    }
    return OK;
}

int
handle_gap_check( const ConfigOpts& opts )
{
    if ( opts.analysis_type != MBUF_DAQ_MULTI_DC )
    {
        std::cout << "daq_multi_cycle is the only mode that the crc_check is "
                     "valid for\n";
        return ERROR;
    }
    shmem::shmem   mem( opts.buffer_name.c_str( ), opts.buffer_size_mb );
    volatile void* buffer = mem.mapping< void >( );
    check_gap::check_gaps( buffer, opts.buffer_size );
    return OK;
}

int
handle_check_size( const ConfigOpts& opts )
{
    shmem::shmem   mem( opts.buffer_name.c_str( ), opts.buffer_size_mb );
    volatile void* buffer = mem.mapping< void >( );
    switch ( opts.analysis_type )
    {
    case MBUF_RMIPC:
        check_mbuf_sizes::check_size_rmipc( buffer, opts.buffer_size, opts );
        break;
    case MBUF_DAQ_MULTI_DC:
        check_mbuf_sizes::check_size_multi( buffer, opts.buffer_size, opts );
        break;
    default:
        std::cout << "Unknown analysis type for the check size command";
        return ERROR;
    }
    return OK;
}

int
handle_timing_delta( const ConfigOpts& opts )
{
    shmem::shmem mem1( opts.buffer_name.c_str( ),
                       opts.buffer_size_mb );
    shmem::shmem mem2( opts.secondary_buffer_name.c_str( ),
                       opts.buffer_size_mb );

    return timing_delta::calculate_timing_delta(
        mem1.mapping< void >( ), mem2.mapping< void >( ), opts );
}

int
handle_list_dcus( const ConfigOpts& opts )
{
    shmem::shmem mem( opts.buffer_name.c_str(), opts.buffer_size_mb );

    return list_dcus::list_dcus( std::cout, mem.mapping<void>(), mem.size(), opts.analysis_type );
}

int
main( int argc, char* argv[] )
{
    ConfigOpts opts = parse_options( argc, argv );
    if ( opts.should_show_help( ) )
    {
        usage( argv[ 0 ] );
        return ( opts.is_in_error( ) ? 1 : 0 );
    }

    switch ( opts.action )
    {
    case LIST:
        list_shmem_segments( );
        break;
    case CREATE:
    {
        shmem::shmem mem( opts.buffer_name.c_str( ), opts.buffer_size_mb );
        if ( !mem.mapping< void >( ) )
        {
            std::cout << "Unable to create/open mbuf buffer "
                      << opts.buffer_name << "\n";
            return 1;
        }
        shmem_inc_segment_count( opts.buffer_name.c_str( ),  opts.buffer_size_mb );
        break;
    }
    case COPY:
    {
        shmem::shmem   mem( opts.buffer_name.c_str( ), opts.buffer_size_mb );
        volatile void* buffer = mem.mapping< void >( );
        if ( !buffer )
        {
            std::cout << "Unable to create/open mbuf buffer "
                      << opts.buffer_name << "\n";
            return 1;
        }
        return copy_shmem_buffer( buffer, opts.output_fname, opts.buffer_size );
    }
    break;
    case DELETE:
        shmem_dec_segment_count( opts.buffer_name.c_str( ) );
        break;
    case ANALYZE:
        return handle_analyze( opts );
    case GAP_CHECK:
        return handle_gap_check( opts );
    case CHECK_SIZE:
        return handle_check_size( opts );
    case TIMING_DELTA:
        return handle_timing_delta( opts );
    case LIST_DCUS:
        return handle_list_dcus( opts );
    }
    return 0;
}
