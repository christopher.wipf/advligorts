//
// Created by jonathan.hanks on 9/14/23.
//
#include "list_dcus.hh"

#include <sstream>

#include "daq_core.h"
#include "rmipc_intl.hh"

namespace list_dcus
{
    int
    list_dcus( std::ostream&    out,
               volatile void*   buffer,
               std::size_t      size,
               ::MBufStructures buffer_type )
    {
        if ( !buffer ||
             ( buffer_type != MBUF_RMIPC && buffer_type != MBUF_DAQ_MULTI_DC ) )
        {
            return 1;
        }
        if ( buffer_type == MBUF_RMIPC )
        {
            if ( size < 1 * 1024 * 1024 )
            {
                throw std::runtime_error( "Invalid buffer size, < 1MB" );
            }
            ::rmipc::memory_layout layout( (volatile char*)buffer );
            std::cout << layout.ipc->dcuId << "\n";
            if ( size != CDS_DAQ_SHMEM_BUFFER_SIZE_MB * 1024 * 1024 )
            {
                std::cerr << "Invalid buffer size for an rmIpcBuffer, "
                             "expecting 64MB\n";
            }
        }
        else
        {
            if ( size < DAQD_MIN_SHMEM_BUFFER_SIZE_MB * 1024 * 1024 )
            {
                std::ostringstream os{ };
                os << "Invalid buffer size for an daq_multi_cycle buffer, must "
                      "be at least ";
                os << DAQD_MIN_SHMEM_BUFFER_SIZE_MB << "MB";
                throw std::runtime_error( os.str( ) );
            }
            auto multi_data =
                reinterpret_cast< volatile daq_multi_cycle_data_t* >( buffer );
            auto cycle = multi_data->header.curCycle;
            if ( cycle >= 16 )
            {
                return 0;
            }
            auto cycle_size = multi_data->header.cycleDataSize;
            if ( cycle_size > ( ( size - sizeof( *multi_data ) / 16 ) ) ||
                 cycle_size < sizeof( daq_multi_dcu_header_t ) )
            {
                throw std::runtime_error( "Invalid cycle size found" );
            }

            auto block = reinterpret_cast< volatile daq_multi_dcu_header_t* >(
                &( multi_data->dataBlock[ 0 ] ) + cycle * cycle_size );
            auto models = block->dcuTotalModels;
            if ( models > 255 )
            {
                return 0;
            }
            for ( auto i = 0; i < models; ++i )
            {
                if ( i > 0 )
                {
                    out << " ";
                }
                out << block->dcuheader[ i ].dcuId;
            }
        }
        return 0;
    }
} // namespace list_dcus