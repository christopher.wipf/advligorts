#ifndef RTS_CPU_ISOLATOR_USP
#define RTS_CPU_ISOLATOR_USP

#include <sys/ioctl.h>
#include <stdio.h>
#include <sys/stat.h> //open
#include <fcntl.h> //open
#include <sys/types.h>
#include <unistd.h> //close

#include "rts-cpu-isolator.h"

int g_isolated_core_fd = -1;

static inline int rts_cpu_isolator_alloc_core(unsigned core_index)
{
    int ret, allocated_core = core_index;

    //If already allocated
    if( g_isolated_core_fd != -1)
        return g_isolated_core_fd;


    g_isolated_core_fd = open("/dev/rts_cpu_isolator", O_RDWR);
    if(g_isolated_core_fd < 0 ) {
        printf("Cannot open /dev/rts_cpu_isolator file, maybe the module is not loaded...\n");
        return -1;
    }

    if ( ioctl(g_isolated_core_fd, GET_CORE_IOCLT, &allocated_core) != ISOLATOR_OK )
    {
        return -1;
    }

    return allocated_core;
}

static inline void rts_cpu_isolator_free_core()
{
    close(g_isolated_core_fd);
}


#endif //RTS_CPU_ISOLATOR_USP
