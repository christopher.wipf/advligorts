#define _GNU_SOURCE
#include "usp-allocate.h"

#include <sched.h>

//
// Example program showing the use of the userspace ioctl interface
//

int main()
{

    int core = -1;

    core = rts_cpu_isolator_alloc_core(core);
    if ( core == -1 )
    {
        printf("There was an error getting a free isolated core\n");
        return 1;
    }
    printf("Allocated core was: %d\n", core);

    //Lock our thread to the allocated core
    cpu_set_t my_set;
    CPU_ZERO(&my_set);
    CPU_SET(core, &my_set);
    if ( sched_setaffinity(0, sizeof(cpu_set_t), &my_set) != 0)
    {
        printf("There was an error locking to the allocated core\n");
    }


    printf("Closing Driver, this frees the allocated core.\n");
    rts_cpu_isolator_free_core(); //Optional because OS will clean up for us 
}
