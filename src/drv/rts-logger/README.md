# RTS Logger
This is a kernel module that allows real time modules to log messages before AND after they are isolated on a core. It works by providing a print function exported by this module that other modules use to pass their messages for printing. Because this module is not CPU isolated it is free to pass those messages to the kernel message buffer. 

# Example Use
```c
//At top of file, define the logging prefix (usually the module name) before logger include
#define RTS_LOG_PREFIX "logger_test"
#include "rts-logger.h"

...

//Log info message
RTSLOG_INFO("There are %d DACs used by the model.\n", num_dacs);

if(some_check < 2)
{
    RTSLOG_ERROR("There was a fatal issue!\n");
    pLocalEpics->epicsOutput.fe_status = DAQ_INIT_ERROR;
    wait_for_exit_signal();
    atomic_set(&g_atom_has_exited, 1);
}

```

### Output (dmesg) Would Look Like
```
[17764.891415] logger_test: INFO - There are 2 DACs used by the model.
[17764.896527] logger_test: ERROR - There was a fatal issue!
```

### journalctl
You can follow all kernel messages using the command:
```
sudo journalctl -t kernel -f
```

If you wanted to filter for the realtime module named `x1tst`, you can use `grep`
```
sudo journalctl -t kernel -f | grep x1tst
```

# Structure
```
force-printk/  # Version that uses printk for logger methods (used by gpstime)
module/        # Main logger module implementation.
test/          # Test module that spams the logger from a couple of threads.
userspace/     # Userspace implementation for the logger
```

## Other Notes
Because the `gpstime` module includes some timing card c files that use RTSLOG functions we provide a printk version for building with `gpstime`.
