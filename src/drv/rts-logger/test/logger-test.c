#include <linux/module.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/kthread.h>  // for threads

//  Define the module metadata.
#define MODULE_NAME "logger_test"
MODULE_AUTHOR("Ezekiel Dohmen ezekiel.dohmen@caltech.edu");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("A logging kernel module for isolated real time LIGO kernel modules");
MODULE_VERSION("0.1");

#define RTS_LOG_PREFIX MODULE_NAME
#include "drv/rts-logger.h"

int MP_JUST_QUERY_NUM_READY = 0;
module_param(MP_JUST_QUERY_NUM_READY, int, S_IRUSR);


static struct task_struct *g_thread;
static char thread_name[] = {"test thread"};

int thread_fn( void * pv ) 
{
    while(!kthread_should_stop()) {
        
         RTSLOG_INFO("Thread print.\n");
         usleep_range(100, 500);

        schedule();
    }
    return 0;
}


static int __init logger_init(void)
{
    int i;

    if (MP_JUST_QUERY_NUM_READY == 1)
    {
         printk(MODULE_NAME " - rtslog_get_num_ready_to_print() : %d\n", rtslog_get_num_ready_to_print());
         return -5;
    }

    
    g_thread = kthread_run(thread_fn, NULL, thread_name);
    if( !g_thread )
    {
        printk(KERN_ERR " - " MODULE_NAME " - Could not create the reader kthread.\n");
        return -5;
    }

    for(i=0; i<500; ++i)
    {
        RTSLOG_INFO("test %d\n", i);
        RTSLOG_DEBUG("we should not see this message\n");
        usleep_range(100, 500);
    }

    printk(MODULE_NAME " - Successfully started the " MODULE_NAME " kernel module.\n");


    return 0;
}

static void __exit logger_exit(void)
{
    kthread_stop(g_thread);

    RTSLOG_RAW("RAW Test\n");
    RTSLOG_RAW("RAW Test with arg (%s)\n", "arg");
    RTSLOG_DEBUG("DEBUG Test\n");
    RTSLOG_DEBUG("DEBUG Test with arg (%s)\n", "arg");
    RTSLOG_INFO("INFO Test\n");
    RTSLOG_INFO("INFO Test with arg (%s)\n", "arg");
    RTSLOG_WARN("WARNING Test\n");
    RTSLOG_WARN("WARNING Test with arg (%s)\n", "arg");
    RTSLOG_ERROR("ERROR Test\n");
    RTSLOG_ERROR("ERROR Test with arg (%s)\n", "arg");

    usleep_range(10000, 10001);

    printk(MODULE_NAME " - There were %d dropped messages because of space.\n", rtslog_get_num_dropped_no_space() );
    printk(MODULE_NAME " - rtslog_get_num_ready_to_print() : %d\n", rtslog_get_num_ready_to_print());

    printk(MODULE_NAME " - Shutting down.\n");
    return;
}

module_init(logger_init);
module_exit(logger_exit);
