//
// Created by jonathan.hanks on 2/7/18.
// This file is pieces of logic pulled out of the bison/yacc file comm.y.
// They have been pulled out to make it easier to consume with external
// tools and debuggers.  Also the hope is that removing all the ${1}...
// makes the code easier to reason about.
//

#ifndef DAQD_TRUNK_COMM_IMPL_HH

#include <ostream>

namespace comm_impl
{
    extern void start_write_impl( void*       lexer,
                                  int         writerType_2_,
                                  const char* optionalAddress_3_,
                                  int         optionalStart_4_,
                                  int         optionalStop_5_,
                                  const char* optionalBroadcast_6_,
                                  int         decimateOption_7_,
                                  int         channelNames_9_ );

    extern void configure_channels_body_begin_end( );

    extern bool configure_test_points( const std::string& par_file );

    extern void clear_crc( );

    extern void start_broadcast( std::ostream*      yyout,
                                 const std::string& broadcast_address,
                                 const std::string& network_address,
                                 int                port );
} // namespace comm_impl

#define DAQD_TRUNK_COMM_IMPL_HH

#endif // DAQD_TRUNK_COMM_IMPL_HH
