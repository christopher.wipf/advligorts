
import os.path

import integration

integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")
integration.Executable(name="daqd", hints=[], description="daqd")

ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")
testpoint_file = ""
broadcast_file = os.path.join(ini_dir, "broadcast")
daqdrc_file = os.path.join(ini_dir, "daqdrc")

integration.transform_text_file(input='daqdrc_broadcast_test', output=daqdrc_file,
                                substitutions=[('MASTER', master_file),
                                               ('TESTPOINT', testpoint_file),
                                               ('BCAST', broadcast_file)])
integration.append_to_text_file(file_name=broadcast_file, text="""[mod5-1236--gpssmd100offc1p--2--1--16]
[X1:NON_EXISTANT1]
[X1:NON_EXISTANT2]
[X1:NON_EXISTANT3]
""")

fe_simulated_streams = integration.Process("fe_simulated_streams",
                                           ["-i", ini_dir,
                                            "-M", master_file,
                                            "-b", "local_dc",
                                            "-m", "100",
                                            "-k", "300",
                                            "-R", "247"])
daqd = integration.Process("daqd",
                           ["-c", daqdrc_file],
                           exit_codes=(1,))


def daqd_not_running():
    state = daqd.state()
    return state not in (integration.Process.UNINITIALIZED, integration.Process.RUNNING)


integration.Sequence(
 [
  integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
  fe_simulated_streams.run,
  daqd.run,
  daqd.ignore,
  integration.wait_for(predicate=daqd_not_running, description="daqd must fail", timeout=10),
 ]
)
