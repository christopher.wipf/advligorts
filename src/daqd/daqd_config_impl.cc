//
// Created by jonathan.hanks on 6/20/23.
//
#include "daqd_config_impl.hh"
#include "daqd.hh"
#include "comm_impl.hh"
#include "debug.h"

#include <chrono>
#include <map>
#include <thread>
#include <iostream>

#include <boost/algorithm/string.hpp>

/**
 * This is the final stage of the daqd declarative config.
 *
 * This types the semi-abstract daqd-config::config_t into
 * the actual daqd.
 */
extern daqd_c daqd;

using handler_t = void ( * )( const daqd_config::config_t& );
using handler_map_t = std::map< daqd_config::action, handler_t >;
namespace
{
    void
    require_main( const std::string& component )
    {
        if ( !daqd.b1 )
        {
            std::string msg = "The main thread must be started before the ";
            msg += component;
            throw std::runtime_error( msg );
        }
    }

    void
    require_trend_channels( )
    {
        if ( daqd.trender.num_channels == 0 )
        {
            throw std::runtime_error(
                "There are no trend channels configured" );
        }
    }

    void
    require_trender( )
    {
        if ( !daqd.trender.tb )
        {
            throw std::runtime_error( "The trender must be running" );
        }
    }

    std::string
    get_string_param( const std::string& key, const std::string& msg )
    {
        auto val = daqd.parameters( ).get( key, std::string( "" ) );
        if ( val.empty( ) )
        {
            throw std::runtime_error( msg );
        }
        return val;
    }

    std::string
    get_frame_dir( const std::string& source,
                   const std::string& key = ":frame_dir" )
    {
        return get_string_param( source + key, "Invalid frame directory" );
    }

    std::string
    get_frame_type( const std::string& source,
                    const std::string& key = ":frame_type" )
    {
        return get_string_param( source + key, "Invalid frame type" );
    }

    void
    sync_to_semaphore( sem_t& sem, const std::string& name )
    {
        for ( ; sem_trywait( &sem ); )
        {
            std::cout << "Waiting on " << name << " semaphore\n";
            sleep( 1 );
        }
        sem_post( &sem );
    }

    void
    apply_configure_channels( const daqd_config::config_t& config )
    {
        std::cout << "config: applying configure_channels\n";
        daqd.master_config = daqd.parameters( ).get( "input:main_ini", "" );
        daqd.num_channels = daqd.trender.num_channels = 0;
        comm_impl::configure_channels_body_begin_end( );
        auto config_number_server =
            daqd.parameters( ).get( "config_number_client:server", "" );
        if ( !config_number_server.empty( ) )
        {
            daqd.update_configuration_number( config_number_server.c_str( ) );
            daqd.trender.set_configuration_number(
                daqd.configuration_number( ) );
        }
    }

    void
    apply_configure_testpoints( const daqd_config::config_t& config )
    {
        std::cout << "config: applying configure test points\n";
        auto param_file = daqd.parameters( ).get( "input:tp_ini", "" );
        if ( param_file.empty( ) )
        {
            return;
        }
        std::cout << "\t" << param_file << "\n";
        if ( !comm_impl::configure_test_points( param_file ) )
        {
            throw std::runtime_error(
                "unable to initialize test point/gds system" );
        }
    }

    void
    apply_configure_location( const daqd_config::config_t& config )
    {
        std::cout << "config: applying location\n";
        daqd.detector_name = config.detector.name;
        daqd.detector_prefix = config.detector.prefix;
        daqd.detector_longitude = config.detector.longitude;
        daqd.detector_latitude = config.detector.latitude;
        daqd.detector_elevation =
            static_cast< float >( config.detector.elevation );
        daqd.detector_arm_x_altitude =
            static_cast< float >( config.detector.altitudes[ 0 ] );
        daqd.detector_arm_y_altitude =
            static_cast< float >( config.detector.altitudes[ 1 ] );
        daqd.detector_arm_x_midpoint =
            static_cast< float >( config.detector.midpoints[ 0 ] );
        daqd.detector_arm_y_midpoint =
            static_cast< float >( config.detector.midpoints[ 1 ] );
        daqd.detector_arm_x_azimuth =
            static_cast< float >( config.detector.azimuths[ 0 ] );
        daqd.detector_arm_y_azimuth =
            static_cast< float >( config.detector.azimuths[ 1 ] );
    }

    void
    apply_enable_gds_broadcast( const daqd_config::config_t& config )
    {
        std::cout << "config: enabling gds_broadcast\n";
        daqd.parameters( ).set( "GDS_BROADCAST", "1" );
    }

    void
    apply_main( const daqd_config::config_t& config )
    {
        std::cout << "config: starting the main thead\n";
        auto buffer_size =
            daqd.parameters( ).get< int >( "buffer:seconds", 16 );
        if ( !daqd.num_channels )
        {
            throw std::runtime_error( "No channels configured for the daqd" );
        }
        if ( buffer_size > MAX_BLOCKS )
        {
            throw std::runtime_error( "Main buffer size is too big" );
        }
        if ( daqd.start_main( buffer_size, &std::cout ) != 0 )
        {
            throw std::runtime_error( "Unable to start the main thread" );
        }
    }

    void
    apply_epics( const daqd_config::config_t& config )
    {
        std::cout << "config: starting the epics thread\n";
        auto prefix = daqd.parameters( ).get( "epics:prefix", "" );
        if ( prefix.empty( ) )
        {
            throw std::runtime_error(
                "The EPICS prefix values is not defined" );
        }
        if ( daqd.start_epics_server(
                 &std::cout, prefix.c_str( ), prefix.c_str( ), nullptr ) != 0 )
        {
            throw std::runtime_error( "Unable to start the EPICS server" );
        }
    };

    void
    apply_profiler( const daqd_config::config_t& config )
    {
        std::cout << "Starting the profiler\n";
        require_main( "profiler" );
        daqd.profile.start_profiler( daqd.b1 );
    }

    void
    apply_producer( const daqd_config::config_t& config )
    {
        std::cout << "Starting the producer\n";
        require_main( "producer" );
        if ( daqd.start_producer( &std::cout ) != 0 )
        {
            throw std::runtime_error( "Unable to start the producer" );
        }
    }

    void
    apply_frame_writer( const daqd_config::config_t& config )
    {
        std::cout << "Starting the frame writer\n";
        require_main( "frame writer" );
        std::string frame_dir{ };
        std::string frame_type{ };

        daqd.frames_per_file = 1;
        if ( daqd.parameters( ).get< int >( "GDS_BROADCAST", 0 ) != 1 )
        {
            auto checksum_dir =
                daqd.parameters( ).get( "frame_writer:checksum_dir", "" );
            if ( !checksum_dir.empty( ) )
            {
                daqd.parameters( ).set( "full_frame_checksum_dir",
                                        checksum_dir );
            }
            daqd.blocks_per_frame = daqd.parameters( ).get< int >(
                "frame_writer:seconds_per_frame", 0 );
            if ( daqd.blocks_per_frame <= 0 )
            {
                throw std::runtime_error( "Invalid frame length found" );
            }
            frame_dir = get_frame_dir( "frame_writer" );
            frame_type = get_frame_type( "frame_writer" );
        }
        else
        {
            std::cout << "> gds_broadcast variant of frame writer\n";
            daqd.blocks_per_frame = 1;
            frame_dir = get_frame_dir( "gds_broadcast" );
            frame_type = get_frame_type( "gds_broadcast" );
        }
        daqd.fsd.set_path( frame_dir );
        daqd.fsd.set_prefix( frame_type );
        daqd.fsd.set_suffix( ".gwf" );

        if ( daqd.start_frame_saver( &std::cout, 0 ) != 0 )
        {
            throw std::runtime_error( "Unable to start the framer writer" );
        }
        sync_to_semaphore( daqd.frame_saver_sem, "frame writer" );
    }

    void
    apply_trender( const daqd_config::config_t& config )
    {
        std::cout << "Starting the trender\n";
        require_main( "trender" );
        require_trend_channels( );
        int buffer_size = 600;
        if ( std::find( config.actions.begin( ),
                        config.actions.end( ),
                        daqd_config::action::STREND_FRAME_WRITER ) ==
             config.actions.end( ) )
        {
            buffer_size = 60;
        }
        std::cout << "trend buffer size set to " << buffer_size << "\n";
        if ( daqd.trender.start_trend( &std::cout, 1, 1, buffer_size, 60 ) !=
             0 )
        {
            throw std::runtime_error( "Unable to start trender" );
        }
    }

    void
    apply_strend_frame_writer( const daqd_config::config_t& config )
    {
        std::cout << "Starting second trend frame writer\n";
        require_trender( );
        daqd.trender.frames_per_file = 1;

        auto checksum_dir =
            daqd.parameters( ).get( "frame_writer:checksum_dir", "" );
        if ( !checksum_dir.empty( ) )
        {
            daqd.parameters( ).set( "trend_frame_checksum_dir", checksum_dir );
        }

        auto frame_dir = get_frame_dir( "strend_writer" );
        auto frame_type = get_frame_type( "strend_writer" );

        daqd.trender.fsd.set_path( frame_dir );
        daqd.trender.fsd.set_prefix( frame_type );
        daqd.trender.fsd.set_suffix( ".gwf" );

        if ( daqd.trender.start_trend_saver( &std::cout ) != 0 )
        {
            throw std::runtime_error(
                "Unable to start the second trend frame writer" );
        }
        sync_to_semaphore( daqd.trender.frame_saver_sem,
                           "strend frame writer" );
    }

    void
    apply_mtrend_frame_writer( const daqd_config::config_t& config )
    {
        std::cout << "Starting minute trend frame writer\n";
        require_trender( );

        auto checksum_dir =
            daqd.parameters( ).get( "frame_writer:checksum_dir", "" );
        if ( !checksum_dir.empty( ) )
        {
            daqd.parameters( ).set( "minute_trend_frame_checksum_dir",
                                    checksum_dir );
        }

        auto frame_dir = get_frame_dir( "mtrend_writer" );
        auto frame_type = get_frame_type( "mtrend_writer" );

        daqd.trender.minute_fsd.set_path( frame_dir );
        daqd.trender.minute_fsd.set_prefix( frame_type );
        daqd.trender.minute_fsd.set_suffix( ".gwf" );

        if ( daqd.trender.start_minute_trend_saver( &std::cout ) != 0 )
        {
            throw std::runtime_error(
                "Unable to start the minute trend frame writer" );
        }
        sync_to_semaphore( daqd.trender.minute_frame_saver_sem,
                           "mtrend frame writer" );
    }

    void
    apply_trend_profiler( const daqd_config::config_t& config )
    {
        std::cout << "Starting trend profiler\n";
        require_trender( );

        daqd.trender.profile.start_profiler( daqd.trender.tb );
        if ( daqd.trender.mtb )
        {
            daqd.trender.profile_mt.start_profiler( daqd.trender.mtb );
        }
    }

    void
    apply_raw_mtrend_writer( const daqd_config::config_t& config )
    {
        std::cout << "Starting raw minute trend writer\n";
        require_trender( );

        auto period = daqd.parameters( ).get< unsigned int >(
            "raw_mtrend_writer:save_period_minutes", 0 );
        if ( period == 0 )
        {
            throw std::runtime_error( "Invalid raw minute trend period" );
        }
        daqd.trender.raw_minute_trend_saving_period = period;
        auto raw_trend_dir =
            daqd.parameters( ).get( "raw_mtrend_writer:trend_dir", "" );
        daqd.trender.raw_minute_fsd.set_path( raw_trend_dir );
        if ( daqd.trender.start_raw_minute_trend_saver( &std::cout ) )
        {
            throw std::runtime_error( "Unable to start the raw trend writer" );
        }
    }

    void
    apply_gds_broadcast( const daqd_config::config_t& config )
    {
        auto address = daqd.parameters( ).get( "gds_broadcast:address", "" );
        auto network = daqd.parameters( ).get( "gds_broadcast:network", "" );
        auto port = daqd.parameters( ).get< int >( "gds_broadcast:port", -1 );
        auto broadcast_ini =
            daqd.parameters( ).get( "gds_broadcast:broadcast_ini", "" );

        if ( address.empty( ) || network.empty( ) || port <= 0 ||
             broadcast_ini.empty( ) )
        {
            throw std::runtime_error(
                "Invalid network parameters to start broadcast" );
        }
        daqd.broadcast_config = broadcast_ini;
        comm_impl::start_broadcast( &std::cout, address, network, port );
    }

    void
    _apply_daqd_protocol( const std::string& port_lookup, int proto_type )
    {
        if ( daqd.num_listeners >= daqd_c::max_listeners )
        {
            throw std::runtime_error( "There are too many listeners" );
        }
        auto port = daqd.parameters( ).get< int >( port_lookup, 0 );
        if ( port <= 0 || port >= 64 * 1024 )
        {
            throw std::runtime_error(
                "Invalid port selected for daqd protocol" );
        }
        daqd.listeners[ daqd.num_listeners ].start_listener(
            &std::cout, port, proto_type );
        daqd.num_listeners++;
    }

    void
    apply_daqd_protocol( const daqd_config::config_t& config )
    {
        std::cout << "Starting the daqd protocol\n";
        auto nds1_port = daqd.parameters( ).get( "nds1:port", "" );
        auto port = daqd.parameters( ).get( "daqd_control:binary_port", "" );
        if ( !nds1_port.empty( ) && nds1_port == port )
        {
            // allow the nds1 startup to do this.
            return;
        }
        _apply_daqd_protocol( "daqd_control:binary_port", 1 );
    }

    void
    apply_daqd_text_protocol( const daqd_config::config_t& config )
    {
        std::cout << "Starting the daqd text protocol\n";
        _apply_daqd_protocol( "daqd_control:text_port", 0 );
    }

    void
    apply_configure_nds1_params( const daqd_config::config_t& config )
    {
        std::cout << "Configuring the nds1 parameters\n";
        auto path = daqd.parameters( ).get( "nds1:job_dir", "" );
        if ( !path.empty( ) )
        {
            daqd.nds_jobs_dir = path;
        }

        auto trend_dirs = daqd.parameters( ).get( "nds1:trend_dirs", "" );
        std::deque< std::string > dirs{ };
        boost::split( dirs, trend_dirs, boost::is_any_of( "," ) );
        if ( dirs.empty( ) )
        {
            throw std::runtime_error(
                "No raw trend directories were specified for the nds server" );
        }
        {
            std::string raw_trend_dir{ dirs.front( ) };
            daqd.trender.raw_minute_fsd.set_path( raw_trend_dir );
            dirs.pop_front( );
            std::reverse( dirs.begin( ), dirs.end( ) );
            std::ostringstream os{ };
            std::copy( dirs.begin( ),
                       dirs.end( ),
                       std::ostream_iterator< std::string >( os, " " ) );
            daqd.old_raw_minute_trend_dirs = os.str( );
        }
        auto frame_dir = get_frame_dir( "nds1" );
        auto strend_dir = get_frame_dir( "nds1", ":strend_dir" );
        auto frame_type = get_frame_type( "nds1" );
        auto strend_type = get_frame_type( "nds1", ":strend_type" );

        daqd.fsd.set_path( frame_dir );
        daqd.fsd.set_prefix( frame_type );
        daqd.fsd.set_suffix( ".gwf" );

        daqd.trender.fsd.set_path( strend_dir );
        daqd.trender.fsd.set_prefix( strend_type );
        daqd.trender.fsd.set_suffix( ".gwf" );

        _apply_daqd_protocol( "nds1:port", 1 );
    }

    void
    apply_final_crc( const daqd_config::config_t& config )
    {
        std::cout << "Applying final crc\n";
        std::this_thread::sleep_for( std::chrono::seconds( 60 ) );
        comm_impl::clear_crc( );
    }

    handler_map_t
    get_handlers( )
    {
        using action = daqd_config::action;
        static handler_map_t handlers{
            { action::CONFIGURE_CHANNELS, apply_configure_channels },
            { action::CONFIGURE_TESTPOINTS, apply_configure_testpoints },
            { action::CONFIGURE_LOCATION, apply_configure_location },
            { action::ENABLE_GDS_BROADCAST, apply_enable_gds_broadcast },
            { action::MAIN, apply_main },
            { action::EPICS, apply_epics },
            { action::PROFILER, apply_profiler },
            { action::PRODUCER, apply_producer },
            { action::FRAME_WRITER, apply_frame_writer },
            { action::TRENDER, apply_trender },
            { action::STREND_FRAME_WRITER, apply_strend_frame_writer },
            { action::MTREND_FRAME_WRITER, apply_mtrend_frame_writer },
            { action::TREND_PROFILER, apply_trend_profiler },
            { action::RAW_MTREND_WRITER, apply_raw_mtrend_writer },
            { action::GDS_BROADCAST, apply_gds_broadcast },
            { action::DAQD_PROTOCOL, apply_daqd_protocol },
            { action::DAQD_TEXT_PROTOCOL, apply_daqd_text_protocol },
            { action::CONFIGURE_NDS1_PARAMS, apply_configure_nds1_params },
            { action::FINAL_CRC, apply_final_crc },
        };
        return handlers;
    }
} // namespace

void
apply_config_to_daqd( const daqd_config::config_t& config )
{
    std::cout << "Daqd config is driven by the declarative config\n";
    std::cout << " configured actions: \n";
    std::copy(
        config.actions.begin( ),
        config.actions.end( ),
        std::ostream_iterator< daqd_config::action >( std::cout, ", " ) );
    std::cout << "\n";
    auto handlers = get_handlers( );
    std::cout << "setting daqd parameters\n";
    for ( const auto& entry : config.general_parameters )
    {
        daqd.parameters( ).set( entry.first, entry.second );
        std::cout << " -> " << entry.first << " = " << entry.second << "\n";
    }
    std::cout << "done with daqd parameters\n";
    auto _stack_size_mb =
        daqd.parameters( ).get< int >( "thread_stack_size_mb", 10 );
    if ( _stack_size_mb < 1 )
    {
        throw std::runtime_error(
            "thread_stack_size_mb set to low, must be greater than 1MB" );
    }
    daqd.thread_stack_size = _stack_size_mb * 1024 * 1024;
    daqd.dcu_status_check =
        ( daqd.parameters( ).get< bool >( "dcu_status_check", true ) ? 1 : 0 );
    daqd.zero_bad_data =
        ( daqd.parameters( ).get< bool >( "zero_bad_data", true ) ? 1 : 0 );

    _debug = daqd.parameters( ).get< int >( "debug", 10 );
    _log_level = daqd.parameters( ).get< int >( "log", 2 );
    std::cout << "setting logging debug = " << _debug
              << " log_level = " << _log_level << "\n";
    for ( const auto& action : config.actions )
    {
        auto it = handlers.find( action );
        if ( it != handlers.end( ) )
        {
            try
            {
                it->second( config );
            }
            catch ( ... )
            {
                std::cout << "Error while processing action " << action << "\n";
                throw;
            }
        }
        else
        {
            std::cout << "No handler defined for action " << action << "\n";
        }
    }
}