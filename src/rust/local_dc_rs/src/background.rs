use std::collections::HashSet;
use std::fs;
use std::io;
use std::io::BufRead;
use std::str::FromStr;
use std::sync::{atomic, Arc};
use util_rs;
use util_rs::lookup_model_rate_and_dcuid;

use crate::structs::{InputBuffer, ModelInfo, SharedBufferQueue};

struct MbufInfo {
    name: String,
    size: usize,
    rc: usize,
}

fn parse_mbuf_status_line(line: io::Result<String>) -> Option<MbufInfo> {
    let line = line.ok()?;
    let (name, components) = line.split_once(":")?;
    let components = components.trim();
    let (size, rc) = components.split_once(" ")?;
    let size = usize::from_str(size).ok()?;
    let rc = usize::from_str(rc).ok()?;
    return Some(MbufInfo {
        name: String::from(name),
        size,
        rc,
    });
}

fn filter_map_daq_buffer(line: io::Result<String>) -> Option<String> {
    let entry = parse_mbuf_status_line(line)?;
    let name = entry.name.strip_suffix("_daq")?;
    if entry.size == advligorts_rs::CDS_DAQ_SHMEM_BUFFER_SIZE_MB as usize * 1024 * 1024 {
        Some(String::from(name))
    } else {
        None
    }
}

fn do_lookup_and_filter_mbuf_names(existing_names: &HashSet<String>) -> io::Result<Vec<String>> {
    let status_file = fs::File::open("/sys/kernel/mbuf/status")?;
    let reader = io::BufReader::new(status_file);
    Ok(reader
        .lines()
        .filter_map(filter_map_daq_buffer)
        .filter(|entry| !existing_names.contains(entry))
        .collect())
}

fn lookup_and_filter_mbuf_names(existing_names: &HashSet<String>) -> Vec<String> {
    match do_lookup_and_filter_mbuf_names(&existing_names) {
        Ok(v) => v,
        Err(_) => Vec::new(),
    }
}

fn name_to_model_info(name: &String, gds_tp_dir: &Option<String>) -> Option<ModelInfo> {
    let mut m = ModelInfo::new(name)?;
    if m.dcuid == 0 || m.rate == 0 {
        match lookup_model_rate_and_dcuid(name, gds_tp_dir) {
            Some(info) => {
                m.dcuid = info.dcuid;
                m.rate = info.rate;
            }
            None => return None,
        }
    }
    Some(m)
}

pub fn detect_models_loop(
    queues: SharedBufferQueue,
    gds_tp_dir: Option<String>,
    stop_flag: Arc<atomic::AtomicBool>,
    period: std::time::Duration,
) {
    println!("model detection loop starting");
    let mut buffer_list = HashSet::<String>::new();
    while !stop_flag.load(atomic::Ordering::SeqCst) {
        // look for new entries, ignoring anything in buffer_list
        // then convert it into a boxed InputBuffer
        let new_inputs: Vec<Box<InputBuffer>> = lookup_and_filter_mbuf_names(&buffer_list)
            .iter()
            .filter_map(|entry| name_to_model_info(entry, &gds_tp_dir))
            .filter_map(|entry| InputBuffer::new(&entry).ok())
            .collect();

        if !new_inputs.is_empty() {
            // record the new inputs
            for entry in new_inputs.iter() {
                buffer_list.insert(entry.info.name.clone());
            }

            // add the new inputs to the queue
            queues.add_inputs(new_inputs);
        }

        // cleanup entries that are being closed, don't track them.
        queues.get_to_close().iter().for_each(|entry| {
            buffer_list.remove(entry.info.name.as_str());
        });

        std::thread::sleep(period);
    }
    println!("model detection loop completed");
}
