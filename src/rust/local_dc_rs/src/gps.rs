use std::ffi;
use std::fs::File;
use util_rs::ioctl_1;

pub struct GpsTime {
    pub sec: u32,
    pub nsec: u32,
}

pub struct GpsClock {
    handle: File,
}

impl GpsClock {
    pub fn new() -> GpsClock {
        let f = File::open("/dev/gpstime").expect("Must be able to open /dev/gpstime");
        return GpsClock { handle: f };
    }

    pub fn now(&self) -> GpsTime {
        let mut buffer: [ffi::c_ulong; 3] = [0, 0, 0];
        ioctl_1(&self.handle, 1, buffer.as_mut_ptr());
        GpsTime {
            sec: buffer[0] as u32,
            nsec: (buffer[1] as u32 * 1000) + buffer[2] as u32,
        }
    }

    pub fn hw_ok(&self) -> bool {
        let mut status: ffi::c_ulong = 0;
        ioctl_1(&self.handle, 0, &mut status);
        status != 0
    }
}
