//
// Created by ezekiel.dohmen 05/06/2022
//

#ifndef SHMEM_IPC_COMMS_H
#define SHMEM_IPC_COMMS_H

#define SHMEM_IPCCOMMS_NAME "shmipc"
#define SHMEM_IPCCOMMS_SIZE_MB 16
#define SHMEM_IPCCOMMS_SIZE ( SHMEM_IPCCOMMS_SIZE_MB * 1024 * 1024 )

#endif // SHMEM_IPC_COMMS_H

