//
// Created by erik.vonreis on 4/29/21.
//

#ifndef DAQD_TRUNK_SHMEM_ALL_H
#define DAQD_TRUNK_SHMEM_ALL_H

#include "shmem_epics.h"
#include "shmem_awg.h"
#include "shmem_iomem.h"
#include "shmem_testpoint.h"
#include "shmem_ipccomms.h"
#include "shmem_daq.h"

#endif // DAQD_TRUNK_SHMEM_ALL_H
