#pragma once

/** @file Demodulation.h
	@brief Contains implementations of down-conversion/devcimation algorithms
	using AVX2 and AVX512
 *****************************************************************************/

/** @defgroup demodavx Vectorized demodulation functions
	These functions implement down-conversion and decimation filters 
	using vector arithmetics based on AVX2 and AVX512.
 *****************************************************************************/
/** @{ */

#include "BiquadDef.h"

#include "util/alignment.h"

/// Maximum stride length
#define MAX_STRIDE 32

#ifdef __cplusplus
extern "C" {
#endif

	/// Structure for demodulation decimation storing history and static data
	/// one rotation, mutiple inputs
	/// 
	/// Allocate one structure for each unique call (each data set) to 
	/// demodulation_decimation_stride8_section3. Call avx_stride_struct_init
	/// immediately afterwards.
	typedef struct {
		/// Filter history
		alignas(64) double hist[4 * MAX_STRIDE * 3];
		///  Old rotation stage [cos, sin]
		alignas(16) double rotation[2];
		/// Cordic angle [cos, sin]
		alignas(16) double anglexy[2];
		/// 2 * pi / (data rate); used to determine the cordic angel from the frequency
		double two_pi_over_rate;
		/// Old frequency used to compute the cordic angle
		double freqhist;
		/// counter to slow down old frequency comparison
		unsigned int pause;
	} avx_stride_struct;

	/// Initialization of the avx_stride_struct
	/// 
	/// @param init pointer to structure avx_stride_struct
	/// @param rate Sampling rate
	void avx_stride_struct_init(avx_stride_struct* init, double rate);

	/// Sets the frequency of the rotation avx_stride_struct
	/// 
	/// @param init pointer to structure avx_stride_struct
	/// @param freq Frequency of rotation
	void avx_stride_struct_set_frequency(avx_stride_struct* self, double freq);

	/// Applies one rotation step using the Cordic algorithm to the avx_stride_struct
	/// 
	/// @param init pointer to structure avx_stride_struct
	void avx_stride_struct_cordic(avx_stride_struct* self);

	/// Structure for demodulation decimation storing history and static data
	/// multiple rotation, one input
	typedef struct {
		/// Filter histroy
		alignas(64) double hist[4 * MAX_STRIDE * 3];
		///  Old rotation stage [cos, sin]
		alignas(64) double rotation[2 * MAX_STRIDE];
		/// Cordic angle [cos, sin]
		alignas(64) double anglexy[2 * MAX_STRIDE];
		/// Old frequency used to compute the cordic angle
		alignas(64) double freqhist[MAX_STRIDE];
		/// 2 * pi / (data rate); used to determine the cordic angel from the frequency
		double two_pi_over_rate;
		/// counter to slow down old frequency comparison
		unsigned int pause;
		/// index to slow down old frequency comparison
		unsigned int idx;
	} avx_rotation_struct;

	/// Initialization of avx_rotation_struct
	/// @param init pointer to structure
	/// @param rate Sampling rate
	void avx_rotation_struct_init(avx_rotation_struct* init, double rate);

	/// Sets the frequency of the rotation avx_rotation_struct
	/// 
	/// @param init pointer to structure avx_stride_struct
	/// @param freq Frequency of rotation
	/// @param idx Index into frequency array
	void avx_rotation_struct_set_frequency(avx_rotation_struct* self, double freq, unsigned int idx);


	/// Demodulation decimation on an array of input values using a single 
	/// down-conversion frequency.
	/// 
	/// Applies a demodulation in both cos and sin, then alculates a set of 
	/// cascaded second-order sections that form an IIR decimation filter. This 
	/// function doesn't have an intrinsic restriction on the number of filter 
	/// sections and input strides, but the corresponding AVX2 and AVX512 routines 
	/// only support strides of 8, 16, 24 or 32, and a filter of order 6th 
	/// (3 second order sections). So, this is enforced here too to be compatible.
	/// 
	/// @param inp Input values arranged as an array of length stride 
	/// @param freq Frequency of down-conversion
	/// @param out Output values arranged as an array 2 x stride long
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History and static values for this function 
	/// @param sections Number of second-order sections in the filter (3)
	/// @param stride Length of data array (8, 16, 24 or 32)
	void demodulation_decimation_stride8_section3_std(const double* inp, double freq,
		double* out, const double* coeff, avx_stride_struct* hist, size_t stride);

	/// Demodulation decimation on an input value using multiple down-conversion
	/// frequencies.
	/// 
	/// Applies a demodulation in both cos and sin, then alculates a set of 
	/// cascaded second-order sections that form an IIR decimation filter. This 
	/// function doesn't have an intrinsic restriction on the number of filter 
	/// sections and input strides, but the corresponding AVX2 and AVX512 routines 
	/// only support strides of 8, 16, 24 or 32, and a filter of order 6th 
	/// (3 second order sections). So, this is enforced here too to be compatible.
	/// 
	/// @param inp Input value
	/// @param freq Frequencies for down-conversion arranged in an array of length stride
	/// @param out Output values arranged as an array 2 x stride long
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History and static values for this function 
	/// @param sections Number of second-order sections in the filter (3)
	/// @param stride Length of data array (8, 16, 24 or 32)
	void demodulation_decimation_rotation8_section3_std(double inp, const double* freq,
		double* out, const double* coeff, avx_rotation_struct* hist, size_t stride);

	/// Demodulation decimation on an array of input values using a single 
	/// down-conversion frequency. 
	/// 
	/// SSE2/FMA version.
	/// 
	/// Applies a demodulation in both cos and sin, then alculates a set of 
	/// cascaded  second-order sections that form an IIR decimation filter. This 
	/// function has a restriction on the number of filter sections and input 
	/// strides, it only support strides of 8, 16, 24 or 32 and a filter of 
	/// order 6th (3 second order sections).
	/// 
	/// @param inp Input values arranged as an array of length stride 
	/// @param freq Frequency of down-conversion
	/// @param out Output values arranged as an array 2 x stride long
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History and static values for this function 
	/// @param sections Number of second-order sections in the filter (3)
	/// @param stride Length of data array (8, 16, 24 or 32)
	void demodulation_decimation_stride8_section3_sse3(const double* inp, double freq,
		double* out, const double* coeff, avx_stride_struct* hist, size_t stride);

	/// Demodulation decimation on an input value using multiple down-conversion
	/// frequencies.
	/// 
	/// Applies a demodulation in both cos and sin, then alculates a set of 
	/// cascaded  second-order sections that form an IIR decimation filter. This 
	/// function has a restriction on the number of filter sections and input 
	/// strides, it only support strides of 8, 16, 24 or 32 and a filter of 
	/// order 6th (3 second order sections).
	/// 
	/// SSE2/FMA version.
	/// 
	/// @param inp Input value
	/// @param freq Frequencies for down-conversion arranged in an array of length stride
	/// @param out Output values arranged as an array 2 x stride long
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History and static values for this function 
	/// @param sections Number of second-order sections in the filter (3)
	/// @param stride Length of data array (8, 16, 24 or 32)
	void demodulation_decimation_rotation8_section3_sse3(double inp, const double* freq,
		double* out, const double* coeff, avx_rotation_struct* hist, size_t stride);



	/// Demodulation decimation on an array of input values using a single 
	/// down-conversion frequency. 
	/// 
	/// AVX2 version.
	/// 
	/// Applies a demodulation in both cos and sin, then alculates a set of 
	/// cascaded  second-order sections that form an IIR decimation filter. This 
	/// function has a restriction on the number of filter sections and input 
	/// strides, it only support strides of 8, 16, 24 or 32 and a filter of 
	/// order 6th (3 second order sections).
	/// 
	/// @param inp Input values arranged as an array of length stride 
	/// @param freq Frequency of down-conversion
	/// @param out Output values arranged as an array 2 x stride long
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History and static values for this function 
	/// @param sections Number of second-order sections in the filter (3)
	/// @param stride Length of data array (8, 16, 24 or 32)
	void demodulation_decimation_stride8_section3_avx2(const double* inp, double freq,
		double* out, const double* coeff, avx_stride_struct* hist, size_t stride);

	/// Demodulation decimation on an input value using multiple down-conversion
	/// frequencies.
	/// 
	/// Applies a demodulation in both cos and sin, then alculates a set of 
	/// cascaded  second-order sections that form an IIR decimation filter. This 
	/// function has a restriction on the number of filter sections and input 
	/// strides, it only support strides of 8, 16, 24 or 32 and a filter of 
	/// order 6th (3 second order sections).
	/// 
	/// AVX2 version.
	/// 
	/// @param inp Input value
	/// @param freq Frequencies for down-conversion arranged in an array of length stride
	/// @param out Output values arranged as an array 2 x stride long
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History and static values for this function 
	/// @param sections Number of second-order sections in the filter (3)
	/// @param stride Length of data array (8, 16, 24 or 32)
	void demodulation_decimation_rotation8_section3_avx2(double inp, const double* freq,
		double* out, const double* coeff, avx_rotation_struct* hist, size_t stride);



	/// Demodulation decimation on an array of input values using a single down-conversion
	/// frequency. 
	/// 
	/// Applies a demodulation in both cos and sin, then alculates a set of 
	/// cascaded  second-order sections that form an IIR decimation filter. This 
	/// function has a restriction on the number of filter sections and input 
	/// strides, it only support strides of 8, 16, 24 or 32 and a filter of 
	/// order 6th (3 second order sections).
	/// 
	/// AVX512 version.
	/// 
	/// @param inp Input values arranged as an array of length stride 
	/// @param freq Frequency of down-conversion
	/// @param out Output values arranged as an array 2 x stride long
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History and static values for this function 
	/// @param sections Number of second-order sections in the filter (3)
	/// @param stride Length of data array (8, 16, 24 or 32)
	void demodulation_decimation_stride8_section3_avx512(const double* inp, double freq,
		double* out, const double* coeff, avx_stride_struct* hist, size_t stride);

	/// Demodulation decimation on an input value using multiple down-conversion
	/// frequencies.
	/// 
	/// Applies a demodulation in both cos and sin, then alculates a set of 
	/// cascaded  second-order sections that form an IIR decimation filter. This 
	/// function has a restriction on the number of filter sections and input 
	/// strides, it only support strides of 8, 16, 24 or 32 and a filter of 
	/// order 6th (3 second order sections).
	/// 
	/// AVX512 version.
	/// 
	/// @param inp Input value
	/// @param freq Frequencies for down-conversion arranged in an array of length stride
	/// @param out Output values arranged as an array 2 x stride long
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History and static values for this function 
	/// @param sections Number of second-order sections in the filter (3)
	/// @param stride Length of data array (8, 16, 24 or 32)
	void demodulation_decimation_rotation8_section3_avx512(double inp, const double* freq,
		double* out, const double* coeff, avx_rotation_struct* hist, size_t stride);


	/// Demodulation decimation on an array of input values using a single 
	/// down-conversion frequency. 
	/// 
	/// Applies a demodulation in both cos and sin, then alculates a set of 
	/// cascaded  second-order sections that form an IIR decimation filter. This 
	/// function has a restriction on the number of filter sections and input 
	/// strides, it only support strides of 8, 16, 24 or 32 and a filter of 
	/// order 6th (3 second order sections).
	/// 
	/// Selects AVX512 when available. If not, checks for AVX2. If neither, 
	/// uses the non-vectorized version.
	/// 
	/// @param inp Input values arranged as an array of length stride 
	/// @param freq Frequency of down-conversion
	/// @param out Output values arranged as an array 2 x stride long
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History and static values for this function 
	/// @param sections Number of second-order sections in the filter (3)
	/// @param stride Length of data array (8, 16, 24 or 32)
	void demodulation_decimation_stride8_section3(const double* inp, double freq,
		double* out, const double* coeff, avx_stride_struct* hist, size_t stride);


        // Call during module initialization
        int demodulation_init(void);

	/// Demodulation decimation on an input value using multiple down-conversion
	/// frequencies.
	/// 
	/// Applies a demodulation in both cos and sin, then alculates a set of 
	/// cascaded  second-order sections that form an IIR decimation filter. This 
	/// function has a restriction on the number of filter sections and input 
	/// strides, it only support strides of 8, 16, 24 or 32 and a filter of 
	/// order 6th (3 second order sections).
	/// 
	/// Selects AVX512 when available. If not, checks for AVX2. If neither, 
	/// uses the non-vectorized version.
	/// 
	/// @param inp Input value
	/// @param freq Frequencies for down-conversion arranged in an array of length stride
	/// @param out Output values arranged as an array 2 x stride long
	/// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
	/// @param hist History and static values for this function 
	/// @param sections Number of second-order sections in the filter (3)
	/// @param stride Length of data array (8, 16, 24 or 32)
	void demodulation_decimation_rotation8_section3(double inp, const double* freq,
		double* out, const double* coeff, avx_rotation_struct* hist, size_t stride);


        /// Demodulation decimation on an input value using multiple down-conversion
        /// frequencies that are spaced apart at a fixed offset starting at 0Hz up to the
        /// Nyquist frequency.
        ///
        /// The return array is 2 values long
        /// Index 0: original time series
        /// Index 1: down-converter at the Nyquist frequency
        ///
        /// Applies a demodulation in both cos and sin, then calculates a set of
        /// cascaded second-order sections that form an IIR decimation filter. This
        /// function doesn't have an intrinsic restriction on the number of filter
        /// sections, but the corresponding AVX2 and AVX512 routines only support filter
        /// of order 6th (3 second order sections). So, this is enforced here too to be
        /// compatible.
        ///
        /// @param inp Input value
        /// @param out Output values arranged as an array 2 values long
        /// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
        /// @param hist History and static values for this function
        /// @param step Step size (uses only the LSB and cycles through 0 to 1 internally)
        void demodulation_decimation_band1_section3(double inp,
                                                         double* out, const double* coeff, double* hist, int step);

        /// Demodulation decimation on an input value using multiple down-conversion
        /// frequencies that are spaced apart at a fixed offset starting at 0Hz up to the
        /// Nyquist frequency.
        ///
        /// The return array is 4 values long
        /// Index 0: original time series
        /// Index 1: down-converter at the Nyquist frequency
        /// Index 2: down-converter at the Nyquist/2 frequency, I phase (cos)
        /// Index 3: down-converter at the Nyquist/2 frequency, Q phase (sin)
        ///
        /// Applies a demodulation in both cos and sin, then calculates a set of
        /// cascaded second-order sections that form an IIR decimation filter. This
        /// function doesn't have an intrinsic restriction on the number of filter
        /// sections, but the corresponding AVX2 and AVX512 routines only support filter
        /// of order 6th (3 second order sections). So, this is enforced here too to be
        /// compatible.
        ///
        /// @param inp Input value
        /// @param out Output values arranged as an array 4 values long
        /// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
        /// @param hist History and static values for this function
        /// @param step Step size (uses only the 2 LSB and cycles through 0 to 3 internally)
        void demodulation_decimation_band2_section3(double inp,
                                                         double* out, const double* coeff, double* hist, int step);

        /// Demodulation decimation on an input value using multiple down-conversion
        /// frequencies that are spaced apart at a fixed offset starting at 0Hz up to the
        /// Nyquist frequency.
        ///
        /// The return array is 8 values long
        /// Index 0: original time series
        /// Index 1: down-converter at the Nyquist frequency
        /// Index 2: down-converter at the Nyquist/4 frequency, I phase (cos)
        /// Index 3: down-converter at the Nyquist/4 frequency, Q phase (sin)
        /// Index 4: down-converter at the Nyquist/2 frequency, I phase (cos)
        /// Index 5: down-converter at the Nyquist/2 frequency, Q phase (sin)
        /// Index 6: down-converter at the 3*Nyquist/4 frequency, I phase (cos)
        /// Index 7: down-converter at the 3*Nyquist/4 frequency, Q phase (sin)
        ///
        /// Applies a demodulation in both cos and sin, then calculates a set of
        /// cascaded second-order sections that form an IIR decimation filter. This
        /// function doesn't have an intrinsic restriction on the number of filter
        /// sections, but the corresponding AVX2 and AVX512 routines only support filter
        /// of order 6th (3 second order sections). So, this is enforced here too to be
        /// compatible.
        ///
        /// @param inp Input value
        /// @param out Output values arranged as an array 8 values long
        /// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
        /// @param hist History and static values for this function
        /// @param step Step size (uses only the 3 LSB and cycles through 0 to 7 internally)
        void demodulation_decimation_band4_section3(double inp,
                                                         double* out, const double* coeff, double* hist, int step);

        /// Demodulation decimation on an input value using multiple down-conversion
        /// frequencies that are spaced apart at a fixed offset starting at 0Hz up to the
        /// Nyquist frequency.
        ///
        /// The return array is 16 values long
        /// Index 0: original time series
        /// Index 1: down-converter at the Nyquist frequency
        /// Index 2: down-converter at the Nyquist/8 frequency, I phase (cos)
        /// Index 3: down-converter at the Nyquist/8 frequency, Q phase (sin)
        /// Index 4: down-converter at the Nyquist/4 frequency, I phase (cos)
        /// Index 5: down-converter at the Nyquist/4 frequency, Q phase (sin)
        /// Index 6: down-converter at the 3*Nyquist/8 frequency, I phase (cos)
        /// Index 7: down-converter at the 3*Nyquist/8 frequency, Q phase (sin)
        /// Index 8: down-converter at the Nyquist/2 frequency, I phase (cos)
        /// Index 9: down-converter at the Nyquist/2 frequency, Q phase (sin)
        /// Index 10: down-converter at the 5*Nyquist/8 frequency, I phase (cos)
        /// Index 11: down-converter at the 5*Nyquist/8 frequency, Q phase (sin)
        /// Index 12: down-converter at the 3*Nyquist/4 frequency, I phase (cos)
        /// Index 13: down-converter at the 3*Nyquist/4 frequency, Q phase (sin)
        /// Index 14: down-converter at the 7*Nyquist/8 frequency, I phase (cos)
        /// Index 15: down-converter at the 7*Nyquist/8 frequency, Q phase (sin)
        ///
        /// Applies a demodulation in both cos and sin, then calculates a set of
        /// cascaded second-order sections that form an IIR decimation filter. This
        /// function doesn't have an intrinsic restriction on the number of filter
        /// sections, but the corresponding AVX2 and AVX512 routines only support filter
        /// of order 6th (3 second order sections). So, this is enforced here too to be
        /// compatible.
        ///
        /// @param inp Input value
        /// @param out Output values arranged as an array 16 values long
        /// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
        /// @param hist History and static values for this function
        /// @param step Step size (uses only the 4 LSB and cycles through 0 to 15 internally)
        void demodulation_decimation_band8_section3(double inp,
                                                         double* out, const double* coeff, double* hist, int step);

        /// Demodulation decimation on an input value using multiple down-conversion
        /// frequencies that are spaced apart at a fixed offset starting at 0Hz up to the
        /// Nyquist frequency.
        ///
        /// The return array is 32 values long
        /// Index 0: original time series
        /// Index 1: down-converter at the Nyquist frequency
        /// Index 2: down-converter at the Nyquist/16 frequency, I phase (cos)
        /// Index 3: down-converter at the Nyquist/16 frequency, Q phase (sin)
        /// Index 4: down-converter at the Nyquist/8 frequency, I phase (cos)
        /// Index 5: down-converter at the Nyquist/8 frequency, Q phase (sin)
        /// Index 6: down-converter at the 3*Nyquist/16 frequency, I phase (cos)
        /// Index 7: down-converter at the 3*Nyquist/16 frequency, Q phase (sin)
        /// ...
        /// Index 28: down-converter at the 7*Nyquist/8 frequency, I phase (cos)
        /// Index 29: down-converter at the 7*Nyquist/8 frequency, Q phase (sin)
        /// Index 30: down-converter at the 15*Nyquist/16 frequency, I phase (cos)
        /// Index 31: down-converter at the 15*Nyquist/16 frequency, Q phase (sin)
        ///
        /// Applies a demodulation in both cos and sin, then calculates a set of
        /// cascaded second-order sections that form an IIR decimation filter. This
        /// function doesn't have an intrinsic restriction on the number of filter
        /// sections, but the corresponding AVX2 and AVX512 routines only support filter
        /// of order 6th (3 second order sections). So, this is enforced here too to be
        /// compatible.
        ///
        /// @param inp Input value
        /// @param out Output values arranged as an array 32 values long
        /// @param coeff Filter coefficienst arranged in LIGO format (same for all filters)
        /// @param hist History and static values for this function
        /// @param step Step size (uses only the 5 LSB and cycles through 0 to 31 internally)
        void demodulation_decimation_band16_section3(double inp,
                                                          double* out, const double* coeff, double* hist, int step);


        /// A good set of decimation filter coefficients
        extern double section3_decim_coeffs[13];

#ifdef __cplusplus
}
#endif
/** @} */
