/*----------------------------------------------------------------------*/
/*                                                                      */
/*                      -------------------                             */
/*                                                                      */
/*                             LIGO                                     */
/*                                                                      */
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.      */
/*                                                                      */
/*                     (C) The LIGO Project, 2012.                      */
/*                                                                      */
/*                                                                      */
/*----------------------------------------------------------------------*/

///	@file controllerIop.c
///	@brief Main scheduler program for compiled real-time kernal object. \n
/// 	@detail More information can be found in the following DCC document:
///<	<a
///< href="https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=7688">T0900607
///< CDS RT Sequencer Software</a>
///	@author R.Bork, A.Ivanov
///     @copyright Copyright (C) 2014 LIGO Project      \n
///<    California Institute of Technology              \n
///<    Massachusetts Institute of Technology           \n\n
///     @license This program is free software: you can redistribute it and/or
///     modify
///<    it under the terms of the GNU General Public License as published by
///<    the Free Software Foundation, version 3 of the License. \n This program
///<    is distributed in the hope that it will be useful, but WITHOUT ANY
///<    WARRANTY; without even the implied warranty of MERCHANTABILITY or
///<    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
///<    for more details.

#include "controllerIop.h" //Externs shared globals
#include "controllerko.h"
#include "cds_types.h"
#include "feComms.h"
#include "fm10Gen_types.h"
#include "controller.h"
#include "util/kernel/exit_signaling.h"
#include "drv/daqLib.h"
#include "../fe/timing_kernel.h"
#include "../fe/sync21pps.h"
#include "drv/gsc_adc_common.h" //GSAI_ALL_CARDS
#include "drv/gsc_dac_common.h" //DAC_CLK_ENABLE
#include "drv/contec1616.h" //stop_tds_clocks()
#include "drv/ligoPcieTiming.h" //lptc_*()
#include "drv/rts-logger.h"
#include "fm10Gen.h" //checkFiltReset()
#include "../fe/timing_common.h" //captureEocTiming()
#include "drv/epicsXfer.h" //checkEpicsReset()
#include "util/ligo_fenv.h"
#include "util/timing.h"
#include "drv/ligo28ao32/ligo28ao32.h"

#include FE_HEADER //fecode()


#include <linux/delay.h> //udelay()
#include <asm/cacheflush.h>

#ifdef NO_CPU_SHUTDOWN
#include <linux/kthread.h> //kthread_should_stop()
#endif

//
// Start Exported Global data (controllerIop.h)
//

#if defined( XMIT_DOLPHIN_TIME ) || defined( USE_DOLPHIN_TIMING )
volatile TIMING_SIGNAL* pcieTimer;
#endif

// Variables for setting IOP->APP I/O
int ioClockDac = DAC_PRELOAD_CNT;
// int ioMemCntr = 0;
int ioMemCntrDac = DAC_PRELOAD_CNT;
// DAC variable
int dacEnable = 0;
int        dacTimingErrorPending[ MAX_DAC_MODULES ];
int dacTimingError = 0;
//int        dacWatchDog = 0; Unused

int pBits[ 9 ] = { 1, 2, 4, 8, 16, 32, 64, 128, 256 };

// Duotone diags struct
duotone_diag_t dt_diag; //Used by iop_dac_functions.c



#if defined( RUN_WO_IO_MODULES ) || defined( USE_DOLPHIN_TIMING )
#include "drv/no_ioc_timing.h"
#include "drv/dac_info.h"
#include "drv/adc_info.h"
#include "drv/no_ioc_dac_preload.h"
#else
#include "drv/iop_adc_functions.h"
#include "drv/iop_dac_functions.h"
#include "drv/dac_info_ioc2.h"
#include "drv/adc_info_ioc2.h"
#include "../fe/sync21pps.h"

#endif

#include "drv/map.h"
#include "verify_slots.h"
#include "drv/spectracomGPS.h"
#include "drv/symmetricomGps.h"

//***********************************************************************
// TASK: fe_start_controller()
//***********************************************************************
/// This function is the main real-time sequencer or scheduler for IOP kernel
/// module code built using the RCG. \n 
/// This code runs in a continuous loop at the rate specified in the RCG model.
/// The loop is synchronized and triggered by the arrival of ADC data, the ADC
/// module in turn is triggered to sample by the clock provided by the LIGO
/// Timing Distribution System.
///	-
void fe_start_controller( void )
{
    int        ii, jj, kk; // Dummy loop counter variables
    static LIGO_TIMER_t cpuClock[ CPU_TIMER_CNT ]; ///  @param cpuClock[] Code timing
    ///  diag variables

    volatile RFM_FE_COMMS* pEpicsComms; /// @param *pEpicsComms Pointer to EPICS
    /// shared memory space
    int status; /// @param status Typical function return value
    int           sync21pps = 0; ///@param flag for code sync to 1PPS signal

    // FOR DIAGNOSTIC TESTING ONLY
#ifdef DIAG_TEST
    loopbacktest_t loopback;
    int lastWDread[ MAX_DAC_MODULES ] ;
    memset( lastWDread, 0, sizeof( lastWDread ) );
#endif
    int        dcuId; /// @param dcuId DAQ ID number for this process
    int diagWord =
        0; /// @param diagWord Code diagnostic bit pattern returned to EPICS
    int system = 0; // Legacy: Allow same code to run muliple instances of a model
    int syncSource =
        SYNC_SRC_NONE; /// @param syncSource Code startup synchronization source
    int mxStat = 0; /// @param mxStat: Used to status local_dc comms
    int mxDiag = 0; ///@param mxDiag: Used to status local_dc comms
    int mxDiagR = 0; ///@param mxDiagR: Used to status local_dc comms

    int feStatus = 0; /// @param Word for storing IOP error flags

    int dac_write_error = 0;
    int dac_write_err_occured = 0; // Used to make dac_write_error sticky

    unsigned int  usec = 0;
    unsigned long cpc;
    float         duotoneTimeDac;
    float         duotoneTime;

    int        usloop = 1;  // Number of upsamle loops when running ADC faster
    // than code
    // This counter ticks at model_rate / UNDERSAMPLE
    // If you trigger code on a specific cycle of this counter it will trigger at 1Hz
    int        hkp_cycle = 0;
    double     adcval[ MAX_ADC_MODULES ][ MAX_ADC_CHN_PER_MOD ];
    adcInfo_t* padcinfo;
    uint64_t time_from_ADC_read_us;

    // Normally preload DAC FIFOs on startup, but can be bypassed
#ifdef NO_DAC_PRELOAD
    int dac_preload = 0;
    // DAC FIFO check will produce DAC error only for FIFO full
    int dac_fault_armed = 0;
#else
    int dac_preload = 1;
    // DAC FIFO check will produce DAC error for all FIFO errors
    int dac_fault_armed = 1;
#endif

#if !defined( RUN_WO_IO_MODULES ) && !defined( USE_DOLPHIN_TIMING )
    volatile u32        dacWDread = 0;
    int                 dacnum = 0;
#endif



    one_pps_sync_t onePps = {0, INVALID_CYCLE_1PPS};




    /// **********************************************************************************************\n
    /// Start Initialization Process \n
    /// **********************************************************************************************\n

    /// \> Flush L1 cache
    memset( fp, 0, 64 * 1024 );
    memset( fp, 1, 64 * 1024 );
    clflush_cache_range( (void*)fp, 64 * 1024 );

    fz_daz( ); /// \> Kill the denorms!
    feclearexcept(FE_ALL_EXCEPT); //Clear all FPU exceptions

    /// \> Init comms with EPICS process */
    pEpicsComms = (volatile RFM_FE_COMMS*)_epics_shm;
    pLocalEpics = (volatile CDS_EPICS*)&pEpicsComms->epicsSpace;
    pEpicsDaq = (volatile char*)&( pLocalEpics->epicsOutput );
    padcinfo = (adcInfo_t*)&adcinfo;
#ifdef OVERSAMPLE
    /// \> Zero out filter histories
    memset( dHistory, 0, sizeof( dHistory ) );
    memset( dDacHistory, 0, sizeof( dDacHistory ) );
#endif

    /// \> Set pointers to filter module data buffers. \n
    /// - ---- Prior to V2.8, separate local/shared memories for FILT_MOD
    /// data.\n
    /// - ---- V2.8 and later, code uses EPICS shared memory only. This was done
    /// to: \n
    /// - -------- Allow daqLib.c to retrieve filter module data directly from
    /// shared memory. \n
    /// - -------- Avoid copy of filter module data between to memory locations,
    /// which was slow. \n
    pDsp[ system ] = (FILT_MOD*)( &pEpicsComms->dspSpace );
    pCoeff[ system ] = (VME_COEF*)( &pEpicsComms->coeffSpace );
    dspPtr[ system ] = (FILT_MOD*)( &pEpicsComms->dspSpace );

    /// \> Clear the FE reset which comes from Epics
    pLocalEpics->epicsInput.vmeReset = 0;

    /// < Read in all Filter Module EPICS coeff settings
    for ( ii = 0; ii < MAX_MODULES; ii++ )
    {
        checkFiltReset( ii,
                dspPtr[ 0 ],
                pDsp[ 0 ],
                &dspCoeff[ 0 ],
                MAX_MODULES,
                pCoeff[ 0 ] );
    }
    dspPtr[ 0 ]->fm_pending_action_index = -1;

    // Need this FE dcuId to make connection to FB
    dcuId = pLocalEpics->epicsInput.dcuId;
    pLocalEpics->epicsOutput.dcuId = dcuId;

    /// \> Init IIR filter banks
    //   Initialize filter banks  *********************************************
    for ( system = 0; system < NUM_SYSTEMS; system++ )
    {
        for ( ii = 0; ii < MAX_MODULES; ii++ )
        {
            for ( jj = 0; jj < FILTERS; jj++ )
            {
                for ( kk = 0; kk < MAX_COEFFS; kk++ )
                {
                    dspCoeff[ system ].coeffs[ ii ].filtCoeff[ jj ][ kk ] = 0.0;
                }
                dspCoeff[ system ].coeffs[ ii ].filtSections[ jj ] = 0;
            }
        }
    }

    /// \> Initialize all filter module excitation signals to zero
    for ( system = 0; system < NUM_SYSTEMS; system++ )
        for ( ii = 0; ii < MAX_MODULES; ii++ )
            pDsp[ 0 ]->data[ ii ].exciteInput = 0.0;

    /// \> Initialize IIR filter bank values
    if ( initVars( pDsp[ 0 ], pDsp[ 0 ], dspCoeff, MAX_MODULES, pCoeff[ 0 ] ) )
    {
        pLocalEpics->epicsOutput.fe_status = FILT_INIT_ERROR;
        fe_status_return = FILT_INIT_ERROR;
        RTSLOG_ERROR("Failed to initialize filter banks, waiting for an exit signal.\n");
        goto error_out;
    }

    udelay( 1000 );

    /// \> Initialize DAQ variable/software
#if !defined( NO_DAQ ) && !defined( IOP_TASK )
    /// - ---- Set data range limits for daqLib routine
    daq.filtExMin = GDS_16K_EXC_MIN;
    daq.filtTpMin = GDS_16K_TP_MIN;
    daq.filtExMax = daq.filtExMin + MAX_MODULES;
    daq.filtExSize = MAX_MODULES;
    daq.xExMin = daq.filtExMax;
    daq.xExMax = daq.xExMin + GDS_MAX_NFM_EXC;
    daq.filtTpMax = daq.filtTpMin + MAX_MODULES * TP_PER_FM;
    daq.filtTpSize = MAX_MODULES * TP_PER_FM;
    daq.xTpMin = daq.filtTpMax;
    daq.xTpMax = daq.xTpMin + GDS_MAX_NFM_TP;

    /// - ---- Initialize DAQ function
    status = daqWrite( 0,
            dcuId,
            daq,
            DAQ_RATE,
            testpoint,
            dspPtr[ 0 ],
            0,
            (int*)( pLocalEpics->epicsOutput.gdsMon ),
            xExc,
            pEpicsDaq );
    if ( status == -1 )
    {
        pLocalEpics->epicsOutput.fe_status = DAQ_INIT_ERROR;
        fe_status_return = DAQ_INIT_ERROR;
        RTSLOG_ERROR("Failed to initialize DAQ, waiting for an exit signal.\n");
        vmeDone = 1;
        goto error_out;
    }

#endif

    /// - ---- Assign DAC testpoint pointers
    for ( ii = 0; ii < cdsPciModules.dacCount; ii++ )
        for ( jj = 0; jj < MAX_DAC_CHN_PER_MOD;
                jj++ ) // 1 per MAX DAC chans regardless of the actual number per DAC type
            testpoint[ MAX_DAC_CHN_PER_MOD * ii + jj ] =
                floatDacOut + MAX_DAC_CHN_PER_MOD * ii + jj;

    // Zero out storage
    memset( floatDacOut, 0, sizeof( floatDacOut ) );

    pLocalEpics->epicsOutput.ipcStat = 0;
    pLocalEpics->epicsOutput.fbNetStat = 0;
    pLocalEpics->epicsOutput.tpCnt = 0;

    // Clear the code exit flag
    vmeDone = 0;
    fe_status_return = 0;

    /// \> Call user model defined application software initialization routine.
    iopDacEnable = feCode( cycleNum,
            adcval,
            dacOut,
            dspPtr[ 0 ],
            &dspCoeff[ 0 ],
            (struct CDS_EPICS*)pLocalEpics,
            FE_CODE_INIT );

    //Check the status of the IPC init called in feCode()
    if ( commData3GetInitStatus() != 0) {
        fe_status_return = IPC_INIT_ERROR;
        pLocalEpics->epicsOutput.fe_status = IPC_INIT_ERROR;
        RTSLOG_ERROR("Failed to initialize IPCs, waiting for an exit signal.\n");
        vmeDone = 1;
        wait_for_exit_signal();
        atomic_set(&g_atom_has_exited, 1);
        return;
    }

    // Initialize timing info variables
    initializeTimingDiags( &timeinfo );

    // Initialize duotone measurement signals
    initializeDuotoneDiags( &dt_diag );

    /// \> Initialize the ADC modules *************************************
    pLocalEpics->epicsOutput.fe_status = INIT_ADC_MODS;
    status = iop_adc_init( padcinfo );

    /// \> Initialize the DAC module variables
    /// **********************************
    pLocalEpics->epicsOutput.fe_status = INIT_DAC_MODS;

    status = iop_dac_init( dacTimingErrorPending );
    if ( status != 0)
    {
        RTSLOG_ERROR("iop_dac_init() failed, Waiting for exit signal...\n");
        goto error_out;
    }

    pLocalEpics->epicsOutput.fe_status = INIT_SYNC;

    // Set ADC read timeout to 2sec (in iop_adc_functions.c)
    // Need to wait longer on startup for ADC data as it can
    // take up to 1.25 second for timing receiver to sync to
    // 1 second mark and start the ADC/DAC clocks.
    adcinfo.startup_sync = 1;

    // Reset timing diagnostics
    pLocalEpics->epicsOutput.diagWord = 0;
    pLocalEpics->epicsOutput.timeDiag = 0;

    /// \> Determine code startup synchronization and timing source.
    // If PCIe timing card found (IOC Gen2), use that for startup sync
    if ( cdsPciModules.card_count[ LPTC ] > 0 )
        syncSource = SYNC_SRC_LTC;
    // if Contec 1616 BIO present, Gen1 TDS receiver will be used for timing.
    else if ( cdsPciModules.card_count[ CON_1616DIO ] )
        syncSource = SYNC_SRC_TDS;
    // If not on aligo timing system, will try to sync to 1PPS signal on ADC
    // This is legacy support for non-LIGO sites that still use an eLIGO
    // timing system.
    else
        syncSource = SYNC_SRC_1PPS;

    // Other options based on Parameter block settings
#ifdef XMIT_DOLPHIN_TIME

    // This is an IOP that transmits time and cycle count over Dolphin net
    if ( cdsPciModules.dolphinPcieWritePtr == NULL)
    {
        RTSLOG_ERROR("We are configured to XMIT dolphin timing, but our dolphin writer pointer is not initialized. Waiting for exit signal...\n");
        goto error_out;
    }

    pcieTimer = (volatile TIMING_SIGNAL*)( 
            (volatile char*)( cdsPciModules.dolphinPcieWritePtr ) + IPC_PCIE_TIME_OFFSET 
            );
#endif
#ifdef USE_DOLPHIN_TIMING
    // This is an IOP w/o an I/O chassis, runs by receiving time/cycle over
    // Dolphin net.


    if ( cdsPciModules.dolphinPcieReadPtr == NULL)
    {
        RTSLOG_ERROR("We are configured to receive dolphin timing, but our dolphin read pointer is not initialized. Waiting for exit signal...\n");
        goto error_out;
    }

    syncSource = SYNC_SRC_DOLPHIN;
    pcieTimer = (volatile TIMING_SIGNAL*)( 
            (volatile char*)( cdsPciModules.dolphinPcieReadPtr ) + IPC_PCIE_TIME_OFFSET 
            );
#endif

#ifdef NO_SYNC
    // Model parameter block set to no_sync=1
    // Code will use an unsynchronized external clock to run
    syncSource = SYNC_SRC_NONE;
#endif

#ifdef RUN_WO_IO_MODULES
    syncSource = SYNC_SRC_TIMER;
#endif

#ifdef USE_ADC_CLOCK
    // Cymac option to run with clock supplied by an ADC card internal timer.
    syncSource = SYNC_SRC_INTERNAL;
#endif

#ifdef TEST_1PPS
    // Test use only.
    // This allows testing of sync to 1PPS on system which has normal LIGO
    // timing system.
    switch ( syncSource )
    {
        // Need to start TDS clocks for testing
        case SYNC_SRC_TDS:
            start_tds_clocks( tdsCount );
            break;
            // If test system has LIGO PCIe timing card, then start clocks
        case SYNC_SRC_LTC:
            // Set status to slot checking
            // pLocalEpics->epicsOutput.fe_status = IOC_SLOT_CHK;
            // Card map to bp slot verification
            lptc_enable_all_slots( &cdsPciModules );
            lptc_start_clock( &cdsPciModules );
            udelay( MAX_UDELAY );
            udelay( MAX_UDELAY );
            udelay( MAX_UDELAY );
            udelay( MAX_UDELAY );
            udelay( MAX_UDELAY );
            udelay( MAX_UDELAY );
            udelay( MAX_UDELAY );
            udelay( MAX_UDELAY );
            udelay( MAX_UDELAY );
            lptc_status_update( &cdsPciModules );
            break;
        default:
            break;
    }
    udelay( MAX_UDELAY );
    udelay( MAX_UDELAY );
    syncSource = SYNC_SRC_1PPS;
#endif

    // usec after second start we expect duotone to cross from negative
    // to positive
    double duotone_offset_usec = 7.0;

    // number of samples to shift collected duotone data
    // and total number of samples to use for duotone calculation
    // these values attempt to center the dutoone data array
    // around the expected duotone crossing point
    //
    // default values start at 5*15.26 usec before zero, extending for
    // 12 * 15.26 usec to give a center near 7 usec, the expected zero-crossing
    // for the TDS and the first version of the LTC.
    //
    // DAC is centered four cycles later.
    int dt_adc_data_offset = 5;
    int dt_dac_data_offset = 1;
    int dt_data_count = 12;

    // Send sync source info to epics
    pLocalEpics->epicsOutput.timeErr = syncSource;
    RTSLOG_INFO( "Sync source = %d\n", syncSource );

    // Start the ADC/DAC clocks based on syncSource
    switch ( syncSource )
    {
        // For Gen2 IOC, use PCIe timing card to setup and sync timing
        case SYNC_SRC_LTC:
            //Get duotone information based of timing card version
            lptc_get_duotone_info( &cdsPciModules, &duotone_offset_usec, &dt_data_count );
            // Standard IOC startup
            lptc_enable_all_slots( &cdsPciModules );
            // Stop ADC/DAC clocks
            lptc_stop_clock( &cdsPciModules );
            // Enable all ADC cards
            gscEnableAdcModule ( &cdsPciModules, GSAI_ALL_CARDS, GSAI_DMA_MODE );
            // Enable all DAC cards 
            gscEnableDacModule ( &cdsPciModules, GSAI_ALL_CARDS, DAC_CLK_ENABLE );
            // Preload DAC FIFOs for timing diags
            if ( dac_preload )
                iop_dac_preload( GSAI_ALL_CARDS );
            // Start ADC/DAC clocks
            lptc_start_clock( &cdsPciModules );
            // Get GPS time
            gps_receiver_locked =
                lptc_get_gps_time( &cdsPciModules, &timeSec, &usec );
            //Wait for any LIGO DACs to sync up with the timing card
            if( lptc_wait_for_dac_sync( &cdsPciModules ) != 0 ) {
                RTSLOG_ERROR("One or more of the DACs did not have its time synced, waiting for exit signal...\n");
                goto error_out;
            }
            // Subtract a second because first cycle will increment time by one
            timeSec--;
            break;
            // For Gen1 IOC, use Contec1616 module as interface to timing receiver
            // card to setup and sync timing
        case SYNC_SRC_TDS:
            // Stop ADC/DAC clocks
            stop_tds_clocks( tdsCount );
            // Enable all ADC/DAC cards
            gscEnableAdcModule ( &cdsPciModules,GSAI_ALL_CARDS,GSAI_DMA_MODE );
            gscEnableDacModule ( &cdsPciModules,GSAI_ALL_CARDS,DAC_CLK_ENABLE );
            // Preload DAC FIFOs for timing diags
            if ( dac_preload )
                iop_dac_preload( GSAI_ALL_CARDS );
            // Start ADC/DAC clocks
            start_tds_clocks( tdsCount );
            timeSec = current_time_fe( ) - 1;
            if ( cdsPciModules.gpsType == TSYNC_RCVR )
            {
                timeSec = getGpsSecTsync( &cdsPciModules );
                gps_receiver_locked = getGpsuSecTsync( &cdsPciModules, &usec );
                // Subtract a second because first cycle will increment by one
                timeSec--;
            }
            break;
            // For legacy systems and Cymacs
        case SYNC_SRC_1PPS:
        case SYNC_SRC_NONE:
            // Do Not Preload values to DAC FIFOs
            dac_fault_armed = 0;
            syncSource = sync2pps_signal( &onePps, padcinfo, cpuClock );
            cycleNum = 0;

            if (syncSource < 0)
            {
                print_sync2pps_error(syncSource);
                RTSLOG_ERROR("The call to sync2pps_signal() failed, waiting for an exit signal...\n");
                vmeDone = 1;
                goto error_out;
            }
            // Already have the cycle 0 data, so setting sync21pps
            // will have the main loop skip adc read on first cycle
            sync21pps = 1;
            // Need to set onePps vars for use in 1PPS timing diag later
            onePps.last_cycle_high = 1;
            pLocalEpics->epicsOutput.timeErr = syncSource;
            timeSec = current_time_fe( ) - 1;
            break;
#ifdef USE_DOLPHIN_TIMING
            // For FE computer without an I/O chassis, use timing info from
            // Dolphin network
        case SYNC_SRC_DOLPHIN:
            timeSec = sync2master( pcieTimer );
            if ( timeSec == -1 )
            {
                timeSec = current_time_fe( ) - 1;
            }
            break;
#endif
#ifdef RUN_WO_IO_MODULES
            //sync2cpuclock() is only declared/defined if RUN_WO_IO_MODULES
        case SYNC_SRC_TIMER:
            timeSec = sync2cpuclock( ); 
            break;
#endif
        case SYNC_SRC_INTERNAL:
            gscEnableAdcModule ( &cdsPciModules,GSAI_ALL_CARDS,GSAI_DMA_MODE );
            gscEnableDacModule ( &cdsPciModules,GSAI_ALL_CARDS,DAC_CLK_ENABLE );
            if ( dac_preload )
                iop_dac_preload( GSAI_ALL_CARDS );
            timeSec = current_time_fe( ) - 1;
            break;
        default:
            dac_preload = 0;
            // Do Not Preload values to DAC FIFOs
            dac_fault_armed = 0;
            gscEnableDacModule ( &cdsPciModules,GSAI_ALL_CARDS,DAC_CLK_ENABLE );
            gscEnableAdcModule ( &cdsPciModules,GSAI_ALL_CARDS,GSAI_DMA_MODE );
            timeSec = current_time_fe( ) - 1;
            break;
    }

    pLocalEpics->epicsOutput.fe_status = NORMAL_RUN;
    RTSLOG_INFO("Controller initialization complete, starting front end control loop\n");
    pLocalEpics->epicsOutput.timeErr = syncSource;

#ifdef REMOTE_GPS
    timeSec = remote_time( (struct CDS_EPICS*)pLocalEpics );
#endif

#ifdef ADD_SEC_ON_START
    // This is a special for LHO PEMMID
    // Can remove once LHO moves FE compujters out of mids
    timeSec++;
#endif

    // Get CPU clock time for diags
    timer_start( &adcinfo.adcTime );

    /// ******************************************************************************\n
    /// Enter the infinite FE control loop
    /// ******************************************\n
    /// ******************************************************************************\n
    // Calculate how many CPU cycles per code cycle
    cpc = cpu_khz * 1000;
    cpc /= MODEL_RATE_HZ;
#ifdef TEST_1PPS
    syncSource = SYNC_SRC_LTC;
#endif

    // offset used to correct expected duotone time for ADCs that use that have
    // non-zero time shift
    // duotone is shifted to the right by the adcs sample rate * time_shift

    double min_dt_diag_val = MIN_DT_DIAG_VAL + duotone_offset_usec;
    double max_dt_diag_val = MAX_DT_DIAG_VAL + duotone_offset_usec;

    // set accepted duotone range in EPICS
    pLocalEpics->SYSDEF.dtTimeFloat_high = max_dt_diag_val;
    pLocalEpics->SYSDEF.dtTimeFloat_low = min_dt_diag_val;

    // add a bit more leeway and 4 extra cycles to dac limits
    pLocalEpics->SYSDEF.dacDtTimeFloat_high = max_dt_diag_val + 2+(int)(4*(1e6/IOP_IO_RATE) + 0.5);
    pLocalEpics->SYSDEF.dacDtTimeFloat_low = min_dt_diag_val - 2+(int)(4*(1e6/IOP_IO_RATE) + 0.5);


    /// calculate the best index to use for duotone values to get closest to the
    /// 1PPS boundary.
    int duotone_offset =  cdsPciModules.adcTimeShift[ADC_DUOTONE_BRD];

    // UNDERSAMPLE Above 4, we might have fast ADC
    // IOP_IO_RATE is slower than the model rate
    // and hkp_cycle increments at the IOP_IO_RATE
#if UNDERSAMPLE > 4
    int duotone_full_cycle_offset = duotone_offset / UNDERSAMPLE;
    int duotone_usloop_offset = duotone_offset % UNDERSAMPLE;
#else
    int duotone_full_cycle_offset = duotone_offset;
#endif //UNDERSAMPLE > 4

#if !defined(USE_DOLPHIN_TIMING) && !defined(RUN_WO_IO_MODULES) //No IO chassis, so no timeshift
    /// prime the ADC read module to setup time_shift
    iop_adc_setup_timeshift();
#endif

    // Run forever
#ifdef NO_CPU_SHUTDOWN  // Run without core lock
    while ( !kthread_should_stop( ) && !vmeDone )
    {
#else
        while ( !vmeDone )
        { 
#endif
            // NORMAL OPERATION -- Wait for ADC data ready
            // *****************************************************************************************
            /// \> On 1PPS mark \n
            if ( cycleNum == 0 )
            {
                /// - ---- Check awgtpman status.
                pLocalEpics->epicsOutput.awgStat =
                    ( pEpicsComms->padSpace.awgtpman_gps != timeSec );
                if ( pLocalEpics->epicsOutput.awgStat )
                    feStatus |= FE_ERROR_AWG;
                /// - ---- Check if DAC outputs are enabled, report error.
                if ( !iopDacEnable ) {
                    //RTSLOG_INFO("iopDacEnable: %d, dac_write_error: %d\n",iopDacEnable, dac_write_error);
                    feStatus |= FE_ERROR_DAC_KILLED;
                }
                if( dac_write_err_occured ) { //Keep the DAC STATEWORD red until diag reset
                    feStatus |= FE_ERROR_DAC;
                }


                /// - ---- Increment GPS second
                timeSec++;
                pLocalEpics->epicsOutput.timeDiag = timeSec;
                // Capture code startup time
                if ( cycle_gps_time == 0 )
                {
                    timeinfo.startGpsTime = timeSec;
                    pLocalEpics->epicsOutput.startgpstime = timeinfo.startGpsTime;
                }
                cycle_gps_time = timeSec;
            }
#ifdef NO_CPU_SHUTDOWN
#ifndef RUN_WO_IO_MODULES
            // If core not locked, need occasional delays to keep Linux happy
            if ( ( cycleNum % 2048 ) == 0 )
            {
                usleep_range( 1, 3 );
            }
#endif
#endif

            // **********************************************************************
            // Read ADC data
            // **********************************************************************
            // If sync to 1pps, first cycle 0 data hase already been read by the
            // sync21pps routine, so don't read again.
            if ( !sync21pps )
                status = iop_adc_read( padcinfo, cpuClock );

            // Clear sync21pps so reads now continue every cycle
            sync21pps = 0;
            adcinfo.startup_sync = 0;
            // Check for an ADC timeout error return
            // This indicates one or more ADC modules in not clocking properly
            if ( status == ADC_TO_ERROR )
            {
                feStatus |= adc_status_update( &adcinfo );
                pLocalEpics->epicsOutput.stateWord = FE_ERROR_ADC;
                pLocalEpics->epicsOutput.diagWord |= ADC_TIMEOUT_ERR;
                pLocalEpics->epicsOutput.fe_status = ADC_TO_ERROR;
                RTSLOG_ERROR("An ADC timeout error has been detected on ADC %d, waiting for an exit signal.\n", fe_status_return_subcode);
                pLocalEpics->epicsOutput.epicsSync++;
                vmeDone = 1;
                goto error_out;
            }
            // If ADC channels not where they should be, we have no option
            // but to exit from the RT code ie loops would be working with
            // wrong input data.
            if ( status == CHAN_HOP_ERROR )
            {
                feStatus |= adc_status_update( &adcinfo );
                pLocalEpics->epicsOutput.stateWord = FE_ERROR_ADC;
                pLocalEpics->epicsOutput.fe_status = CHAN_HOP_ERROR;
                RTSLOG_ERROR("A channel hop error has been detected on ADC %d, waiting for an exit signal.\n", fe_status_return_subcode);
                vmeDone = 1;
                pLocalEpics->epicsOutput.epicsSync++;
                goto error_out;
            }

            // Following captures time after ADC reads
            timer_start( &cpuClock[ CPU_TIME_USR_START ] );
            commData3SetCycleStartTime( cpuClock[ CPU_TIME_USR_START ] );


            // In normal operation, the following for loop runs only once per IOP
            // code cycle. This for loop runs > once per cycle if ADC is clocking
            // faster then IOP is running ie
            // specifically added for fast 18bit ADC running at 128KS/sec and
            // greater.
            for ( usloop = 0; usloop < UNDERSAMPLE; usloop++ )
            {
                // Move ADC data from read buffer to local buffer for passing to
                // user code.
                for ( ii = 0; ii < cdsPciModules.adcCount; ii++ )
                {
                    for ( jj = 0; jj < cdsPciModules.adcChannels[ ii ]; jj++ )
                    {
                        adcval[ ii ][ jj ] = dWord[ ii ][ jj ][ usloop ];
                    }
                }
                // *****************************************************************
                /// \> Call the front end specific application  ******************\n
                /// - -- This is where the user application produced by RCG gets
                /// called and executed. \n\n
                //
                iopDacEnable = feCode( cycleNum,
                        adcval,
                        dacOut,
                        dspPtr[ 0 ],
                        &dspCoeff[ 0 ],
                        (struct CDS_EPICS*)pLocalEpics,
                        FE_CODE_PROCESS );
                timer_start( &cpuClock[ CPU_TIME_USR_END ] );
                // ******************************************************************
#if UNDERSAMPLE > 4
                if ( usloop == 0 )
#endif // UNDERSAMPLE > 4
                {
                    // ********************************************************************
                    /// WRITE DAC OUTPUTS *****************************************
                    /// \n
                    // ********************************************************************
#ifndef DAC_WD_OVERRIDE
                    // If a DAC module has bad timing then quit writing outputs
                    // COMMENT OUT NEXT LINE FOR TEST STAND w/bad DAC cards.
                    if ( dacTimingError )
                        iopDacEnable = 0;
#endif
                    // Write out data to DAC modules
                    dac_write_error = iop_dac_write( cpuClock );
                    if( dac_write_error ) {
                        dac_write_err_occured = 1;
                        feStatus |= FE_ERROR_DAC;
                    }
                }
                odcStateWord = 0;

                // ***********************************************************************
                /// BEGIN HOUSEKEEPING
                /// ************************************************ \n
                // ***********************************************************************


                /// \> Update duotone diag information
#if UNDERSAMPLE > 4
                if ( (syncSource == SYNC_SRC_TDS ||
                            syncSource == SYNC_SRC_LTC) && (usloop == duotone_usloop_offset)  )
                {
                    dt_diag.dac[ ( hkp_cycle + dt_dac_data_offset - duotone_full_cycle_offset ) % IOP_IO_RATE ] =
                        dWord[ ADC_DUOTONE_BRD ][ DAC_DUOTONE_CHAN ][ usloop ];
                    dt_diag.totalDac +=
                        dWord[ ADC_DUOTONE_BRD ][ DAC_DUOTONE_CHAN ][ usloop ];
                    dt_diag.adc[ ( hkp_cycle + dt_adc_data_offset - duotone_full_cycle_offset ) % IOP_IO_RATE ] =
                        dWord[ ADC_DUOTONE_BRD ][ ADC_DUOTONE_CHAN ][ usloop ];
                    dt_diag.totalAdc +=
                        dWord[ ADC_DUOTONE_BRD ][ ADC_DUOTONE_CHAN ][ usloop ];
                }
#else
                if ( (syncSource == SYNC_SRC_TDS ||
                            syncSource == SYNC_SRC_LTC))
                {
                    dt_diag.dac[ ( cycleNum + dt_dac_data_offset - duotone_full_cycle_offset ) % IOP_IO_RATE ] =
                        dWord[ ADC_DUOTONE_BRD ][ DAC_DUOTONE_CHAN ][ usloop ];
                    dt_diag.totalDac +=
                        dWord[ ADC_DUOTONE_BRD ][ DAC_DUOTONE_CHAN ][ usloop ];
                    dt_diag.adc[ ( cycleNum + dt_adc_data_offset - duotone_full_cycle_offset ) % IOP_IO_RATE ] =
                        dWord[ ADC_DUOTONE_BRD ][ ADC_DUOTONE_CHAN ][ usloop ];
                    dt_diag.totalAdc +=
                        dWord[ ADC_DUOTONE_BRD ][ ADC_DUOTONE_CHAN ][ usloop ];
                }
#endif // UNDERSMPLE > 4



                // FOR DIAGNOSTIC TESTING ONLY
                // Following is only used on automated test system
                // These signals are looped back from other test models
                // to measure phase delays
#ifdef DIAG_TEST
                for ( ii = 0; ii < 15; ii++ )
                {
                    if ( ii < 5 )
                        loopback.value = adcinfo.adcData[ 0 ][ ii ];
                    if ( ( ii > 4 ) && ( ii < 10 ) )
                        loopback.value = adcinfo.adcData[ 1 ][ ii - 5 ];
                    if ( ii > 9 )
                        loopback.value = adcinfo.adcData[ 0 ][ ( ii - 2 ) ];
                    if ( ( loopback.value > 400 ) && ( loopback.signalHi[ ii ] == 0 ) )
                    {
                        loopback.time[ ii ] = cycleNum;
                        loopback.signalHi[ ii ] = 1;
                        if ( ( ii % 5 ) == 0 )
                            pLocalEpics->epicsOutput.timingTest[ ii ] = 1 +
                                (cycleNum * 15.26 ) / UNDERSAMPLE;
                        // Control apps do not see 1pps until after IOP signal loops
                        // around and back into ADC channel 0, therefore, need to
                        // subtract IOP loop time.
                        else
                            pLocalEpics->epicsOutput.timingTest[ ii ] =
                                ( cycleNum * 15.26 /UNDERSAMPLE) -
                                pLocalEpics->epicsOutput.timingTest[ 0 ];
                    }


                    // Reset the diagnostic for next cycle
                    if ( cycleNum > 2000 )
                        loopback.signalHi[ ii ] = 0;
                }
#endif

                // *****************************************************************
                /// \>  Write data to DAQ.
                // *****************************************************************
#ifndef NO_DAQ

                // Call daqLib
                pLocalEpics->epicsOutput.daqByteCnt =
                    daqWrite( 1,
                            dcuId,
                            daq,
                            DAQ_RATE,
                            testpoint,
                            dspPtr[ 0 ],
                            0,
                            (int*)( pLocalEpics->epicsOutput.gdsMon ),
                            xExc,
                            pEpicsDaq );
                // Send the current DAQ block size to the awgtpman for TP number
                // checking
                pEpicsComms->padSpace.feDaqBlockSize = curDaqBlockSize;
                pLocalEpics->epicsOutput.tpCnt = tpPtr->count & 0xff;
                feStatus |= ( FE_ERROR_EXC_SET & tpPtr->count );
                if ( FE_ERROR_EXC_SET & tpPtr->count )
                    odcStateWord |= ODC_EXC_SET;
                else
                    odcStateWord &= ~( ODC_EXC_SET );
                if ( pLocalEpics->epicsOutput.daqByteCnt > DAQ_DCU_RATE_WARNING )
                    feStatus |= FE_ERROR_DAQ;
#endif


                // *****************************************************************
                /// \> 16 Hz, Update latest DAC output values to EPICS
                // *****************************************************************
                if ( cycle_16Hz_reset == DAC_EPICS_UPDATES_16HZ )
                {
                    // Send DAC output values at 16Hzfb
                    for ( jj = 0; jj < cdsPciModules.dacCount; jj++ )
                    {
                        for ( ii = 0; ii < MAX_DAC_CHN_PER_MOD; ii++ )
                        {
                            pLocalEpics->epicsOutput.dacValue[ jj ][ ii ] =
                                dacOutEpics[ jj ][ ii ];
                        }
                    }
                }

                // *****************************************************************
                /// \> Check for requests for filter module clear history requests.
                /// This is spread out over a number of cycles.
                // Spread out filter coeff update, but keep updates at 16 Hz
                // here we are rounding up:
                //   x/y rounded up equals (x + y - 1) / y
                //
                // *****************************************************************
                {
                    static const unsigned int mpc =
                        ( MAX_MODULES + ( MODEL_RATE_HZ / 16 ) - 1 ) /
                        ( MODEL_RATE_HZ / 16 ); // Modules per cycle
                    unsigned int smpc = mpc * cycle_16Hz_reset; // Start module counter
                    unsigned int empc = smpc + mpc; // End module counter
                    unsigned int i;
                    for ( i = smpc; i < MAX_MODULES && i < empc; i++ ) 
                        feStatus |= checkFiltReset( i, dspPtr[ 0 ], pDsp[ 0 ], 
                                        &dspCoeff[ 0 ], MAX_MODULES, pCoeff[ 0 ] );
                }



                /// \> Update internal cycle counters
                cycleNum += 1;


                // FOR DIAGNOSTIC TESTING ONLY
                // Introduces clocking errors for verification that code will catch
                // and report these errors.
#ifdef DIAG_TEST
                if ( pLocalEpics->epicsInput.bumpCycle != 0 )
                {
                    cycleNum += pLocalEpics->epicsInput.bumpCycle;
                    pLocalEpics->epicsInput.bumpCycle = 0;
                }
#endif
                cycleNum %= MODEL_RATE_HZ;
                pLocalEpics->epicsOutput.cycle = cycleNum;
                if ( cycle_16Hz_reset == DAQ_CYCLE_CHANGE )
                {
                    daqCycle = ( daqCycle + 1 ) % DAQ_NUM_DATA_BLOCKS_PER_SECOND;
                    if ( !( daqCycle % 2 ) )
                        pLocalEpics->epicsOutput.epicsSync = daqCycle;
                }
                if ( cycle_16Hz_reset ==
                        END_OF_DAQ_BLOCK ) /*we have reached the 16Hz second barrier*/
                {
                    /* Reset the data cycle counter */
                    cycle_16Hz_reset = 0;
                }
                else
                {
                    /* Increment the internal cycle counter */
                    cycle_16Hz_reset++;
                }


            } //End of usloop for undersampling IOPs


            //******************************************************************
            //
            // Start 64K Housekeeping
            //
            //******************************************************************

            int num_actions;
            for (num_actions=0;
                 num_actions < IOP_IO_RATE/(MODEL_RATE_HZ/UNDERSAMPLE);
                 ++num_actions) //More filter actions for longer cycles
            {
                if ( !doFilterLoadWork( dspPtr[ 0 ], pDsp[ 0 ], &dspCoeff[ 0 ], pCoeff[ 0 ] ) )
                    break; //No more work to do
            }


            // *****************************************************************
            // If synced to 1PPS on startup, continue to check that code
            // is still in sync with 1PPS.
            // This is NOT normal aLIGO mode, but still used at AEI and ANU.
            // This is using cycleNum, which ticks at the model rate Hz but we know
            // PPS signal must be high long enough for 64K IOPs so we can check at 64K
#ifdef TEST_1PPS
            if ( syncSource == SYNC_SRC_LTC )
#else
                if ( syncSource == SYNC_SRC_1PPS )
#endif
                {
                    // Check if front end continues to be in sync with 1pps
                    // If not, set sync error flag
                    if ( sync21pps_check( &onePps, padcinfo, hkp_cycle ) != 0 ) 
                    {
                        diagWord |= TIME_ERR_1PPS;
                        feStatus |= FE_ERROR_TIMING;
                    }
                }



            //******************************************************************
            //
            //Start of 1Hz housekeeping
            //
            //******************************************************************

            // *****************************************************************
            /// \> Cycle 0: \n
            /// - ---- Read IRIGB time if symmetricom card (this is not
            /// standard, but supported for earlier cards. \n
            // *****************************************************************
            if ( hkp_cycle == HKP_READ_SYMCOM_IRIGB && 
                    ( syncSource == SYNC_SRC_TDS || syncSource == SYNC_SRC_LTC ) )
            {
                if ( cdsPciModules.gpsType == SYMCOM_RCVR )
                {
                    // Retrieve time set in at adc read and report offset from
                    // 1PPS
                    gps_receiver_locked = getGpsTime( &cdsPciModules, &timeSec, &usec );
                    usec -= TIME0_DELAY_US;
                    pLocalEpics->epicsOutput.irigbTime = usec;
                    if ( ( usec > MAX_IRIGB_SKEW || usec < MIN_IRIGB_SKEW ) &&
                            cdsPciModules.gpsType != 0 )
                    {
                        diagWord |= TIME_ERR_IRIGB;
                        feStatus |= FE_ERROR_TIMING;
                    }
                }
                /// - ---- Calc duotone diagnostic mean values for past second
                /// and reset.
                dt_diag.meanAdc = dt_diag.totalAdc / MODEL_RATE_HZ;
                dt_diag.totalAdc = 0.0;
                dt_diag.meanDac = dt_diag.totalDac / MODEL_RATE_HZ;
                dt_diag.totalDac = 0.0;

            }

            // *****************************************************************
            /// \> Cycle 1: Get cycle 0 time offset from  1 sec mark in usec.
            /// If Spectricom IRIGB, get IRIG-B usec information.
            /// If LIGO PCIe timing rcvr, get timing card usec information.
            /// In both cases, usec register should be held by having read
            /// the seconds register on cycle 0.
            // *****************************************************************
            if ( hkp_cycle == HKP_READ_TSYNC_IRIBB && 
                    ( syncSource == SYNC_SRC_TDS || syncSource == SYNC_SRC_LTC ) )
            {

                //The Spectracom (TSYNC) timing card locks the us counter when the integer time is read
                //in iop_adc_read(). The us value we read here was set then, this automatically does 
                //the adjustment we are doing below on the LIGO timing card
                if ( cdsPciModules.gpsType == TSYNC_RCVR ) {
                    gps_receiver_locked = getGpsuSecTsync( &cdsPciModules, &usec );
                }

                //ADC read waited for UNDERSAMPLE samples, so we subtract off USEC_PER_CYCLE
                //to get back to the time of the first ADC sample read
                if ( syncSource == SYNC_SRC_LTC ) { 
                    time_from_ADC_read_us = timer_tock_ns( &cpuClock[ CPU_TIME_CYCLE_START ] ) / 1000;
                    usec = lptc_get_gps_usec( &cdsPciModules ) - USEC_PER_CYCLE - time_from_ADC_read_us;
                }

                usec -= TIME0_DELAY_US;
                pLocalEpics->epicsOutput.irigbTime = usec;
                /// // - ---- If not in acceptable range, generate error.
                if ( ( usec > MAX_IRIGB_SKEW  ||usec < MIN_IRIGB_SKEW ) &&
                     cdsPciModules.gpsType != 0 )
                {
                    feStatus |= FE_ERROR_TIMING;
                    diagWord |= TIME_ERR_IRIGB;
                }
            }

            // *****************************************************************
            /// \> Cycle 16, perform duotone diag calcs.
            // *****************************************************************
            if ( hkp_cycle == HKP_DT_CALC &&
                    ( syncSource == SYNC_SRC_TDS || syncSource == SYNC_SRC_LTC ) )
            {
                duotoneTime =
                    duotime( dt_data_count, dt_adc_data_offset, dt_diag.meanAdc, dt_diag.adc );

                pLocalEpics->SYSDEF.dtTimeFloat = (double)duotoneTime;
                pLocalEpics->epicsOutput.dtTime = (int)duotoneTime;

                if ( ( ( duotoneTime < min_dt_diag_val ) ||
                            ( duotoneTime > max_dt_diag_val ) ) )
                    feStatus |= FE_ERROR_TIMING;

                if ( dt_diag.dacDuoEnable )
                {
                    duotoneTimeDac = duotime( dt_data_count,
                            dt_dac_data_offset,
                            dt_diag.meanDac,
                            dt_diag.dac );
                }
                else {
                    duotoneTimeDac = -1000;
                }


                pLocalEpics->epicsOutput.dacDtTime = (int)duotoneTimeDac;
                pLocalEpics->SYSDEF.dacDtTimeFloat = duotoneTimeDac;
            }


            // *****************************************************************
            /// \> Cycle 17, set/reset DAC duotone switch if request has
            /// changed.
            // *****************************************************************
            if ( hkp_cycle == HKP_DAC_DT_SWITCH && syncSource == SYNC_SRC_TDS )
            {
                if ( dt_diag.dacDuoEnable != pLocalEpics->epicsOutput.dacDuoSet )
                {
                    dt_diag.dacDuoEnable = pLocalEpics->epicsOutput.dacDuoSet;
                    if ( dt_diag.dacDuoEnable )
                        CDIO1616Output[ 0 ] = TDS_START_ADC_NEG_DAC_POS;
                    else
                        CDIO1616Output[ 0 ] =
                            TDS_START_ADC_NEG_DAC_POS | TDS_NO_DAC_DUOTONE;
                    CDIO1616Input[ 0 ] = contec1616WriteOutputRegister(
                            &cdsPciModules, tdsControl[ 0 ], CDIO1616Output[ 0 ] );
                }
            }
            if ( hkp_cycle == HKP_DAC_DT_SWITCH && syncSource == SYNC_SRC_LTC )
            {
                if ( dt_diag.dacDuoEnable != pLocalEpics->epicsOutput.dacDuoSet )
                {
                    dt_diag.dacDuoEnable = pLocalEpics->epicsOutput.dacDuoSet;
                    lptc_dac_duotone( &cdsPciModules, dt_diag.dacDuoEnable );
                }
            }

            // *****************************************************************
            /// \> Cycle 18, Send timing info to EPICS at 1Hz
            // *****************************************************************
            if ( hkp_cycle == HKP_TIMING_UPDATES )
            {
                //sendTimingDiags2Epics() will clear cycleTimeSec, so we set it here
                commData3SetCycleMax( timeinfo.cycleTimeSec );
                
                sendTimingDiags2Epics( pLocalEpics, &timeinfo, &adcinfo );

                pLocalEpics->epicsOutput.dacEnable = dacEnable;

                if ( ( adcinfo.adcHoldTime > CYCLE_TIME_ALRM_HI ) ||
                        ( adcinfo.adcHoldTime < CYCLE_TIME_ALRM_LO ) )
                {
                    diagWord |= FE_ADC_HOLD_ERR;
                    feStatus |= FE_ERROR_TIMING;
                }
                if ( timeinfo.cycleTimeMax > CYCLE_TIME_ALRM )
                {
                    diagWord |= FE_PROC_TIME_ERR;
                    feStatus |= FE_ERROR_TIMING;
                }
                pLocalEpics->epicsOutput.stateWord = feStatus;
                feStatus = 0;
                if ( pLocalEpics->epicsInput.diagReset || initialDiagReset )
                {
                    initialDiagReset = 0;
                    pLocalEpics->epicsInput.diagReset = 0;
                    pLocalEpics->epicsInput.ipcDiagReset = 1;
                    timeinfo.cycleTimeMax = 0;
                    diagWord = 0;
                    ipcErrBits = 0;
                    for ( jj = 0; jj < cdsPciModules.adcCount; jj++ )
                        adcinfo.adcRdTimeMax[ jj ] = 0;
                    clear_fpu_exceptions();
                    clearFilterHistoryChecks();
                    feStatus &= ~FE_ERROR_FPU;
                    dac_write_err_occured = 0; //Clear sticky DAC bit from missed sample
                    ipc_subsys_diag_reset(pLocalEpics);
                }
                pLocalEpics->epicsOutput.diagWord = diagWord;
            }

            // *****************************************************************
            /// \> Cycle 19, write updated diag info to EPICS
            // *****************************************************************
            if ( hkp_cycle == HKP_DIAG_UPDATES )
            {
                pLocalEpics->epicsOutput.ipcStat = ipcErrBits;
                if ( ipcErrBits & 0xf )
                    feStatus |= FE_ERROR_IPC;
                // Create FB status word for return to EPICS
                mxStat = 0;
                mxDiagR = daqPtr->reqAck;
                if ( ( mxDiag & 1 ) != ( mxDiagR & 1 ) )
                    mxStat = 1;
                if ( ( mxDiag & 2 ) != ( mxDiagR & 2 ) )
                    mxStat += 2;
                pLocalEpics->epicsOutput.fbNetStat = mxStat;
                mxDiag = mxDiagR;
                if ( mxStat != MX_OK )
                    feStatus |= FE_ERROR_DAQ;
                ;
                if ( pLocalEpics->epicsInput.overflowReset )
                {
                    if ( pLocalEpics->epicsInput.overflowReset )
                    {
                        for ( ii = 0; ii < MAX_ADC_CHN_PER_MOD; ii++ )
                        {
                            for ( jj = 0; jj < cdsPciModules.adcCount; jj++ )
                            {
                                adcinfo.overflowAdc[ jj ][ ii ] = 0;
                                pLocalEpics->epicsOutput
                                    .overflowAdcAcc[ jj ][ ii ] = 0;
                            }
                        }
                        for ( ii = 0; ii < MAX_DAC_CHN_PER_MOD; ii++ ) 
                        {
                            for ( jj = 0; jj < cdsPciModules.dacCount; jj++ )
                            {
                                pLocalEpics->epicsOutput
                                    .overflowDacAcc[ jj ][ ii ] = 0;
                            }
                        }
                    }
                }
                if ( ( pLocalEpics->epicsInput.overflowReset ) ||
                        ( overflowAcc > OVERFLOW_CNTR_LIMIT ) )
                {
                    pLocalEpics->epicsInput.overflowReset = 0;
                    pLocalEpics->epicsOutput.ovAccum = 0;
                    overflowAcc = 0;
                }
            }


            // *****************************************************************
            /// \> Cycle 21, Update ADC/DAC status to EPICS.
            // *****************************************************************
            if ( hkp_cycle == HKP_ADC_DAC_STAT_UPDATES )
            {
                pLocalEpics->epicsOutput.ovAccum = overflowAcc;
                feStatus |= adc_status_update( &adcinfo );
                feStatus |= dac_status_update( &dacinfo );
                // pLocalEpics->epicsOutput.fe_status = NORMAL_RUN;
            }



            // *****************************************************************
            // Cycle 22
            /// \> Check if we need to disable dolphin IPCs or code exit is requested
            if ( hkp_cycle == HKP_CHECK_EXIT )
            {
                if ( pLocalEpics->epicsOutput.disableRemoteIpc != 0 ) {
                    ioMemData->stop_dolphin_ipcs = 1;
                }
                else {
                    ioMemData->stop_dolphin_ipcs = 0;
                }

                vmeDone = atomic_read(&g_atom_should_exit) |
                    checkEpicsReset( (struct CDS_EPICS*)pLocalEpics );
            }

            //
            // Cycle 23 - Check FPU exceptions
            //
            if ( hkp_cycle == HKP_MATH_EXCEPTION_CHECK)
            {
                feStatus |= check_fpu_exceptions();
            }

            //
            // Cycle 25 - Update IPC Wait Time Stats
            //
            if( hkp_cycle == HKP_IPC_STATS_UPDATES)
            {
                ipc_subsys_1Hz_stats_update( pLocalEpics );
            }

#if !defined( RUN_WO_IO_MODULES ) && !defined( USE_DOLPHIN_TIMING )
            // Following read I/O
            // In systems > 64KHz, do not want to read multiple times
            // within an ADC read period.
            // *****************************************************************
            /// \> Cycle 400 to 400 + numDacModules, write DAC heartbeat to
            /// AI chassis (only for 18 and 20bit DAC modules), via DAC DIO port.
            // Only one write per code cycle to reduce cycle time
            // Lower 4 bits of 8 bit DAC DIO set as outputs.
            // *****************************************************************
            if ( hkp_cycle >= HKP_DAC_WD_CLK &&
                    hkp_cycle < ( HKP_DAC_WD_CLK + cdsPciModules.dacCount ) )
            {
                dacnum = hkp_cycle - HKP_DAC_WD_CLK;
                // AI WD only used with 18 and 20bit DAC modules
                if ( cdsPciModules.dac_info[ dacnum ].card_type == GSC_18AO8 ||
                        cdsPciModules.dac_info[ dacnum ].card_type == GSC_20AO8 )
                {
                    // Only send to DAC if IOP is writing normally
                    if ( iopDacEnable && !dacChanErr[ dacnum ] )
                        _dacPtr[ dacnum ]->CSR_DIO =
                            ( (timeSec & 1) | DAC_DIO_L4B_OUTPUT );
                }

                if (cdsPciModules.dac_info[ dacnum ].card_type == LIGO_28AO32)
                {
                    ligo28ao32_reset_watchdog( cdsPciModules.dacDrivers[ dacnum ] );
                }
            }

            // *****************************************************************
            /// \> Cycle 500 to 500 + numDacModules, read back watchdog from
            /// AI chassis (18 and 20bit DAC only)
            // Check once per second on code cycle HKP_DAC_WD_CHK to DAC
            // count Only one read per code cycle to reduce cycle time
            // Upper 4 bits of 8 bit DAC DIO set as inputs.
            // *****************************************************************
            if ( hkp_cycle >= HKP_DAC_WD_CHK &&
                    hkp_cycle < ( HKP_DAC_WD_CHK + cdsPciModules.dacCount ) )
            {
                dacnum = hkp_cycle - HKP_DAC_WD_CHK;
                if ( cdsPciModules.dac_info[ dacnum ].card_type == GSC_18AO8 ||
                        cdsPciModules.dac_info[ dacnum ].card_type == GSC_20AO8 )
                {
                    // Read the DAC DIO register
                    dacWDread = _dacPtr[ dacnum ]->CSR_DIO;
#ifdef DIAG_TEST
                    if( (dacWDread & 1) == lastWDread[ dacnum ] )
                        pLocalEpics->epicsOutput.statDac[ dacnum ] &=
                            ~( DAC_WD_BIT );
                    else
                        pLocalEpics->epicsOutput.statDac[ dacnum ] |=
                            DAC_WD_BIT;
                    lastWDread[ dacnum ] = (dacWDread & 1);
#else
                    // Bit 8 of DAC DIO is AI chassis enable status
                    // This bit should be high if AIC enabled
                    if ( ( ( dacWDread >> 8 ) & 1 ) > 0 )
                    {
                        pLocalEpics->epicsOutput.statDac[ dacnum ] &=
                            ~( DAC_WD_BIT );
                    }
                    else
                        pLocalEpics->epicsOutput.statDac[ dacnum ] |=
                            DAC_WD_BIT;
#endif //DIAG_TEST
                }

                if (cdsPciModules.dac_info[ dacnum ].card_type == LIGO_28AO32)
                {
                    uint32_t h = ligo28ao32_get_watchdog(cdsPciModules.dacDrivers[ dacnum ]);
                    //RTSLOG_INFO("WD reg: 0x%x\n", h);
                    if ( ( h & 0x2 ) == 0 ) //If bit 2 is not set
                        pLocalEpics->epicsOutput.statDac[ dacnum ] |= DAC_WD_BIT; //statDac 1 -> green
                    else
                        pLocalEpics->epicsOutput.statDac[ dacnum ] &= ~( DAC_WD_BIT );
                }

            }

            // *****************************************************************
            /// \> Cycle 600 to 600 + numDacModules, Check DAC FIFO Sizes to
            /// determine if DAC modules are synched to code
            /// - ---- 18bit DAC reads out FIFO size dirrectly, but 16bit
            /// module only has a 4 bit register area for FIFO empty,
            /// quarter full, etc. So, to make these bits useful in 16 bit
            /// module, code must set a proper FIFO size in map.c code.
            // This code runs once per second.
            // *****************************************************************
            if ( hkp_cycle >= HKP_DAC_FIFO_CHK &&
                    hkp_cycle < ( HKP_DAC_FIFO_CHK + cdsPciModules.dacCount ) )
            {
                dacnum = hkp_cycle - HKP_DAC_FIFO_CHK;
                status = check_dac_buffers( dacnum, dac_fault_armed );
                if ( dacTimingError )
                    feStatus |= FE_ERROR_DAC;
            }

            // *****************************************************************
            /// \> Cycle 700, Get timing card temps
            if ( hkp_cycle == HKP_TIME_CARD_TEMP_CHK && syncSource == SYNC_SRC_LTC ) {
                pLocalEpics->epicsOutput.timing_card_temp_c = lptc_get_temp_c(&cdsPciModules);
            }

            // *****************************************************************
            /// \> Cycle 701 to 701 + numDacModules, Get DAC temps
            if ( hkp_cycle >= HKP_CARD_TEMP_CHK &&
                 hkp_cycle < ( HKP_CARD_TEMP_CHK + cdsPciModules.dacCount ) )
            {
                dacnum = hkp_cycle - HKP_CARD_TEMP_CHK;
                if (cdsPciModules.dac_info[ dacnum ].card_type == LIGO_28AO32) {
                    pLocalEpics->epicsOutput.dac_temp_c[dacnum] = 
                    ligo28ao32_get_temp_c( cdsPciModules.dacDrivers[ dacnum ] );
                }
            }

            // Check LIGO timing card and backplane status

            if (syncSource == SYNC_SRC_LTC )
            {
                if ( hkp_cycle >= HKP_IOC_SLOT_STATUS &&
                        hkp_cycle < ( HKP_IOC_SLOT_STATUS + LPTC_IOC_SLOTS ) )
                {
                    jj = hkp_cycle - HKP_IOC_SLOT_STATUS;
                    pLocalEpics->epicsOutput.lptMon[ jj ] =
                        lptc_get_slot_status( &cdsPciModules, jj );
                }

                if ( hkp_cycle >= HKP_IOC_SLOT_CONFIG &&
                        hkp_cycle < ( HKP_IOC_SLOT_CONFIG + LPTC_IOC_SLOTS ) )
                {
                    jj = hkp_cycle - HKP_IOC_SLOT_CONFIG;
                    pLocalEpics->epicsOutput.lptMon[ ( jj + LPTC_IOC_SLOTS ) ] =
                        lptc_get_slot_config( &cdsPciModules, jj );
                }

                if ( hkp_cycle == HKP_IOC_BP_CONFIG )
                {
                    pLocalEpics->epicsOutput.lpt_bp_config =
                        lptc_get_bp_config( &cdsPciModules );
                }

                if ( hkp_cycle == HKP_IOC_BP_STATUS )
                {
                    pLocalEpics->epicsOutput.lpt_bp_status =
                        lptc_get_bp_status( &cdsPciModules );
                }

                if ( hkp_cycle == HKP_IOC_LPTC_STATUS )
                {
                    pLocalEpics->epicsOutput.lpt_status =
                        lptc_get_lptc_status( &cdsPciModules );

                    //Set timing state word if we unlock
                    if( (pLocalEpics->epicsOutput.lpt_status & LPTC_STATUS_OK) == 0 ||
                            (pLocalEpics->epicsOutput.lpt_status & LPTC_STATUS_UPLINK_OK ) == 0  )
                    {
                        pLocalEpics->epicsOutput.stateWord |= FE_ERROR_TIMING;
                    }

                }
            }
#endif //!defined( RUN_WO_IO_MODULES ) && !defined( USE_DOLPHIN_TIMING )


            // *****************************************************************
            // Update end of cycle information
            // *****************************************************************
            // Capture end of cycle time.
            timer_start( &cpuClock[ CPU_TIME_CYCLE_END ] );

            /// \> Compute code cycle time diag information. (in us)
            timeinfo.cycleTime =  timer_tock_ns( &cpuClock[ CPU_TIME_CYCLE_START ] ) / 1000;

            captureEocTiming(
                    hkp_cycle, cycle_gps_time, &timeinfo, &adcinfo );


            //Get the elapsed time between the last cycle start (adcinfo.adcTime) and this
            //cycles start (CPU_TIME_CYCLE_START [Set when the first ADC returns data] ) (in us)
            adcinfo.adcHoldTime = timer_duration_ns( &adcinfo.adcTime, &cpuClock[ CPU_TIME_CYCLE_START ]) / 1000;

            adcinfo.adcTime = cpuClock[ CPU_TIME_CYCLE_START ];

            //This is calculating the time from when the ADC returned its first sample
            //and when we started the model code. Result in us.
            timeinfo.usrTime = timer_duration_ns( &cpuClock[ CPU_TIME_CYCLE_START ], 
                                                  &cpuClock[ CPU_TIME_USR_START ] ) / 1000;
            // Keep the max IO time for the second, this is what is displayed as "IO" time
            // on the GPS_TP screen and that is a more apt than "usrTime"
            if ( timeinfo.usrTime > timeinfo.usrTimeMax )
                timeinfo.usrTimeMax = timeinfo.usrTime;



            //hkp_cycle is incremented every UNDERSAMPLE cycleNums
            if ( cycleNum == 0 )
                hkp_cycle = 0;
            else
                hkp_cycle++;

            /// \> If not exit request, then continue INFINITE LOOP  *******
        }

        pLocalEpics->epicsOutput.cpuMeter = 0;

        /* System reset command received */
        iop_dac_cleanup();
        atomic_set(&g_atom_has_exited, 1);
        return;



        error_out:
        iop_dac_cleanup();
        wait_for_exit_signal();
        atomic_set(&g_atom_has_exited, 1);
        return;
    }
