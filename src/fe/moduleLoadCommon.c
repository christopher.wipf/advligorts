///     @file moduleLoadCommon.c
///     @brief File contains common routines for moduleLoad.c
///     for both IOP and User apps.`

#include "moduleLoadCommon.h"
#include "controller.h" //FILT_INIT_ERROR, etc
#include "feComms.h" //RFM_FE_COMMS
#include "controllerko.h" //tdsControl, tdsCount
#include "util/formatting.h" //snprintf
#include "mbuf/mbuf_kernel.h" //MBUF_ERROR_MSG_ALLOC_LEN, MBUF_KERNEL_CODE_OK
#include "../drv/mbuf/mbuf.h"
#include "drv/rts-logger.h"

#include <linux/string.h>

void
print_exit_messages( int error_type, int error_sub, const char * sysname)
{
    RTSLOG_INFO( "Brought the CPU back up\n");
    switch ( error_type )
    {
    case FILT_INIT_ERROR:
        RTSLOG_ERROR( "exited on filter initiialization error\n" );
        break;
    case DAQ_INIT_ERROR:
        RTSLOG_ERROR( "%s FE error: %s\n",
                sysname, "exited on DAQ initiialization error\n");
        break;
    case CHAN_HOP_ERROR:
        RTSLOG_ERROR( "exited on ADC Channel Hopping error, "
        	      "detected on ADC %d\n", error_sub );
        break;
    case BURT_RESTORE_ERROR:
        RTSLOG_ERROR( "exited on BURT restore error\n");
        break;
    case DAC_INIT_ERROR:
        RTSLOG_ERROR( "exited on DAC module initialization error\n" );
        break;
    case ADC_TO_ERROR:
        RTSLOG_ERROR( "Exited on ADC module timeout error, "
		      "detected on ADC %d\n", error_sub );
        break;
    default:
        RTSLOG_INFO( "Returning from cleanup_module\n");
        break;
    }
}

#ifndef USER_SPACE
int
detach_shared_memory( )
{
    mbuf_release_area( SYSTEM_NAME_STRING_LOWER);
    mbuf_release_area( SYSTEM_NAME_STRING_LOWER SHMEM_TESTPOINT_SUFFIX);
    mbuf_release_area( SYSTEM_NAME_STRING_LOWER SHMEM_AWG_SUFFIX );
    mbuf_release_area( "ipc" );
    mbuf_release_area( "shmipc" );
    mbuf_release_area( SYSTEM_NAME_STRING_LOWER "_daq");
    return 0;
}

int
attach_shared_memory( )
{
    enum MBUF_KERNEL_CODE  ret;
    char fname[ MBUF_NAME_LEN ];
    char err_msg[MBUF_ERROR_MSG_ALLOC_LEN];

    /// Allocate EPICS memory area
    snprintf( fname, MBUF_NAME_LEN, "%s", SYSTEM_NAME_STRING_LOWER );
    ret = mbuf_allocate_area( fname, SHMEM_EPICS_SIZE, (volatile void **)&_epics_shm );
    if ( ret != MBUF_KERNEL_CODE_OK ) goto attach_shared_memory_error;

    // Set pointer to EPICS area
    pLocalEpics = (CDS_EPICS*)&( (RFM_FE_COMMS*)_epics_shm )->epicsSpace;
    pLocalEpics->epicsOutput.fe_status = 0;

    /// Allocate TP config memory area
    snprintf( fname, MBUF_NAME_LEN, "%s", SYSTEM_NAME_STRING_LOWER SHMEM_TESTPOINT_SUFFIX );
    ret = mbuf_allocate_area(fname, SHMEM_TESTPOINT_SIZE, (volatile void **)&_tp_shm );
    if ( ret != MBUF_KERNEL_CODE_OK ) goto attach_shared_memory_error;


    /// Allocate AWG data memory area, and set pointer to AWG area
    snprintf( fname, MBUF_NAME_LEN, "%s", SYSTEM_NAME_STRING_LOWER SHMEM_AWG_SUFFIX);
    ret = mbuf_allocate_area(fname, SHMEM_AWG_SIZE, (volatile void **)&_awg_shm );
    if ( ret != MBUF_KERNEL_CODE_OK ) goto attach_shared_memory_error;


    /// Allocate IPC memory area, ans assign pointer
    snprintf( fname, MBUF_NAME_LEN, "%s", SHMEM_IOMEM_NAME);
    ret = mbuf_allocate_area( fname, SHMEM_IOMEM_SIZE, (volatile void **) &_ipc_shm );
    if ( ret != MBUF_KERNEL_CODE_OK ) goto attach_shared_memory_error;

    // Assign pointer to IOP/USER app comms space
    ioMemData = (IO_MEM_DATA*)( _ipc_shm + 0x4000 );

    /// Allocate Shared memory IPC comms memory area
    snprintf( fname, MBUF_NAME_LEN, "%s", SHMEM_IPCCOMMS_NAME);
    ret = mbuf_allocate_area( fname, SHMEM_IPCCOMMS_SIZE, (volatile void **)&_shmipc_shm );
    if ( ret != MBUF_KERNEL_CODE_OK ) goto attach_shared_memory_error;

    /// Allocate DAQ memory area
    snprintf( fname, MBUF_NAME_LEN, "%s_" SHMEM_MODEL_DAQ_COMMS_NAME, SYSTEM_NAME_STRING_LOWER );
    ret = mbuf_allocate_area( fname, SHMEM_MODEL_DAQ_COMMS_SIZE, (volatile void **)&_daq_shm );
    if ( ret != MBUF_KERNEL_CODE_OK ) goto attach_shared_memory_error;

    daqPtr = (struct rmIpcStr*)_daq_shm;

    return 0; //No error return

    attach_shared_memory_error:
    mbuf_lookup_error_msg(ret, err_msg);
    RTSLOG_ERROR( "mbuf_allocate_area(%s) failed - %s ", fname, err_msg );
    return -12;
}


#ifdef IOP_MODEL
// This function writes the IO_MEM_DATA struct in shared memory
// so user apps have their needed information from the IOP model.
// Apps read this data back out in mapApp.c:mapPciModules()
void send_io_info_to_mbuf( int totalcards, CDS_HARDWARE* pCds )
{
    int ii, kk;

    // Write IOP data copy rate.  Needed by awgtpman
    ioMemData->mem_data_rate_hz = IOP_IO_RATE;

    // Write the version of the struct we are using 
    ioMemData->struct_version = IO_MEM_DATA_VERSION;

    /// Wirte PCIe card info to mbuf for use by userapp models
    // Clear out card model info in IO_MEM
    for ( ii = 0; ii < MAX_IO_MODULES; ii++ )
    {
        ioMemData->model[ ii ] = -1;
    }

    /// Master send module counts to control model via ipc shm
    ioMemData->totalCards = totalcards;
    ioMemData->adcCount = pCds->adcCount;
    ioMemData->dacCount = pCds->dacCount;
    ioMemData->bioCount = pCds->doCount;
    // kk will act as ioMem location counter for mapping modules
    kk = pCds->adcCount;
    for ( ii = 0; ii < pCds->adcCount; ii++ )
    {
        // MASTER maps ADC modules first in ipc shm for control models
        ioMemData->model[ ii ] = pCds->adcType[ ii ];
        ioMemData->card[ ii ] = pCds->adcInstance[ ii ];
        ioMemData->ipc[ ii ] =
            ii; // ioData memory buffer location for control model to use
    }
    for ( ii = 0; ii < pCds->dacCount; ii++ )
    {
        // Pass DAC info to control processes
        ioMemData->model[ kk ] = pCds->dac_info[ ii ].card_type;
        ioMemData->card[ kk ] = pCds->dacInstance[ ii ];
        ioMemData->ipc[ kk ] = kk;
        //Copy over the full DAC info
        memcpy((void*)&ioMemData->dac_info[kk], (void*)&pCds->dac_info[ii], sizeof(ioMemData->dac_info[0]));
        // Following used by MASTER to point to ipc memory for inputting DAC
        // data from control models
        pCds->dacConfig[ ii ] = kk;
        kk++;
    }
    // MASTER sends DIO module information to control models
    // Note that for DIO, control modules will perform the I/O directly and
    // therefore need to know the PCIe address of these modules.
    ioMemData->bioCount = pCds->doCount;
    for ( ii = 0; ii < pCds->doCount; ii++ )
    {
        // MASTER needs to find Contec 1616 I/O card to control timing card.
        if ( pCds->doType[ ii ] == CON_1616DIO )
        {
            tdsControl[ tdsCount ] = ii;
            tdsCount++;
        }
        ioMemData->model[ kk ] = pCds->doType[ ii ];
        ioMemData->card[ kk ] = pCds->doInstance[ ii ];
        // Unlike ADC and DAC, where a memory buffer number is passed, a PCIe
        // address is passed for DIO cards.
        ioMemData->ipc[ kk ] = pCds->pci_do[ ii ];
        kk++;
    }

    // Following section maps Reflected Memory, both VMIC hardware style and
    // Dolphin PCIe network style. Control units will perform I/O transactions
    // with RFM directly ie MASTER does not do RFM I/O. Master unit only maps
    // the RFM I/O space and passes pointers to control models.

    /// Map VMIC RFM cards, if any
    ioMemData->rfmCount = pCds->rfmCount;
    for ( ii = 0; ii < pCds->rfmCount; ii++ )
    {
        // Master sends RFM memory pointers to control models
        ioMemData->pci_rfm[ ii ] = pCds->pci_rfm[ ii ];
        ioMemData->pci_rfm_dma[ ii ] = pCds->pci_rfm_dma[ ii ];
    }
    for ( ii = 0; ii < 10; ii++ )
    {
        pLocalEpics->epicsOutput.pcieInfo[ ii ] = 
            pCds->ioc_config[ ii ] + ( pCds->ioc_instance [ ii ] * 32 );
    }
#ifdef DOLPHIN_TEST
    /// Send Dolphin addresses to user app processes
    // dolphinCount is number of segments
    ioMemData->dolphinCount = pCds->dolphinCount;
    // dolphin read/write 0 is for local PCIe network traffic
    ioMemData->dolphinPcieReadPtr = pCds->dolphinPcieReadPtr;
    ioMemData->dolphinPcieWritePtr = pCds->dolphinPcieWritePtr;
    // dolphin read/write 1 is for long range PCIe (RFM) traffic
    ioMemData->dolphinRfmReadPtr = pCds->dolphinRfmReadPtr;
    ioMemData->dolphinRfmWritePtr = pCds->dolphinRfmWritePtr;

#else
    // Clear Dolphin pointers so the controller process sees NULLs
    ioMemData->dolphinCount = 0;
    ioMemData->dolphinPcieReadPtr = 0;
    ioMemData->dolphinPcieWritePtr = 0;
    ioMemData->dolphinRfmReadPtr = 0;
    ioMemData->dolphinRfmWritePtr = 0;
#endif
    ioMemData->stop_dolphin_ipcs = 0;

}
#endif //IOP_MODEL
#endif //ndef USER_SPACE


