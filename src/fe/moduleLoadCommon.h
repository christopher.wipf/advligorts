#ifndef LIGO_MODULELOADCOMMON_H
#define LIGO_MODULELOADCOMMON_H

#include "drv/cdsHardware.h"

#ifdef __cplusplus
extern "C" {
#endif

void print_exit_messages( int error_type, int error_sub, const char* sysname );

#ifndef USER_SPACE
int detach_shared_memory( void );
int attach_shared_memory( void );
#ifdef IOP_MODEL
void send_io_info_to_mbuf( int totalcards, CDS_HARDWARE* pCds );
#endif //IOP_MODEL
#endif //USER_SPACE

#ifdef __cplusplus
}
#endif


#endif //LIGO_MODULELOADCOMMON_H
