#include "part_headers/statespace/stateSpacePart.h"
#include "part_headers/statespace/stateSpaceCtrl_ioctl.h"


#include "drv/rts-logger.h"

#ifdef __KERNEL__
#include <linux/vmalloc.h>
#else
#include <string.h>
#include <stdlib.h>
#define vmalloc malloc
#define vfree free
#endif

#include "util/ligo_atomic.h"


typedef struct stateSpaceModelConfig_t {
    unsigned num_parts; //Total number of parts in model

    struct partConfig_t {
        char name [STATE_SPACE_ALLOC_NAME_LEN];
        unsigned model_inputs;
        unsigned model_outputs;
        struct StateSpaceCtx_t context; 
    } partConfig_t;
    struct partConfig_t * ss_parts_ptr;

} stateSpaceModelConfig_t;

static stateSpaceModelConfig_t g_model_config = {0, };

/**
 * The g_part_index_to_load variable is used to signal
 * to a part that the configuration should be reloaded.
 * After the commended part loads the configuration, 
 * it will signal that it has completed by setting the
 * value back to -1.
 */
static ligo_atomic_t g_part_index_to_load;

static struct StateSpaceCtx_t g_config_hold = {0,};


int globStateSpaceInit( unsigned num_parts )
{

    ligo_atomic_set(&g_part_index_to_load, -1);

    g_model_config.num_parts = num_parts;
    
    if ( num_parts > 0 ) { //Don't try a 0 byte allocation

        g_model_config.ss_parts_ptr = vmalloc(sizeof(*g_model_config.ss_parts_ptr) * num_parts);
        if ( g_model_config.ss_parts_ptr == NULL ) {
            RTSLOG_ERROR("stateSpace - Failed to allocate memory for state space parts.\n");
            return -1;
        }

        for(int i=0; i < num_parts; ++i)
        {
            //Zero out the ctx
            memset((void*)&g_model_config.ss_parts_ptr[i].context, 0, sizeof(StateSpaceCtx_t));
        }
    }

    return 0;
}

int globStateSpaceConfigPart( unsigned part_index, const char name [], unsigned num_in, unsigned num_out )
{
    if( part_index >=  g_model_config.num_parts) {
        RTSLOG_ERROR("stateSpace - index: %u is too large, expected max index: %u\n", part_index, g_model_config.num_parts-1); 
        return -1;
    }

    if ( strlen(name) > STATE_SPACE_MAX_NAME_LEN ) {
        RTSLOG_ERROR("stateSpace - The name: %s , is too long.\n", name);
        return -1;
    }

    g_model_config.ss_parts_ptr[part_index].name[0] = '\0';
    strncat(g_model_config.ss_parts_ptr[part_index].name, name, STATE_SPACE_MAX_NAME_LEN);

    g_model_config.ss_parts_ptr[part_index].model_inputs = num_in;
    g_model_config.ss_parts_ptr[part_index].model_outputs = num_out;

    return 0;
}

void globStateSpaceFree( void )
{
    for(int i=0; i < g_model_config.num_parts; ++i)
    {
        if (g_model_config.ss_parts_ptr[i].context.mem_block_ptr != NULL)
            vfree( g_model_config.ss_parts_ptr[i].context.mem_block_ptr);    
    }

    if( g_config_hold.mem_block_ptr != NULL ) {
        vfree( g_config_hold.mem_block_ptr );
    }


    if( g_model_config.ss_parts_ptr != NULL ) {
        vfree(g_model_config.ss_parts_ptr);
        g_model_config.ss_parts_ptr = NULL;
    }
}


void _cleanUpStateSpace(StateSpaceCtx_t *  ctx)
{
    if ( ctx->mem_block_ptr != NULL) {
        vfree( ctx->mem_block_ptr );
    }
    memset((void*)ctx, 0, sizeof(*ctx));
}


/**
 *
 */
int matrixConfigureStateSpace(
                          unsigned part_index, 
                          unsigned input_vec_len, unsigned output_vec_len, unsigned state_vec_len,
                          const double * initial_state_vec,
                          const double * state_matrix, const double * input_matrix,  
                          const double * output_matrix, const double * feedthrough_matrix)
{
    unsigned count, i, j;

    RTSLOG_DEBUG("part_index: %u, input_vec_len: %u, output_vec_len: %u, state_vec_len: %u\n"
                "initial_state_vec: %lx, state_matrix: %lx, input_matrix: %lx, "
                "output_matrix: %lx, feedthrough_matrix: %lx\n",
                part_index, input_vec_len, output_vec_len, state_vec_len,
                (uint64_t)initial_state_vec, (uint64_t)state_matrix, (uint64_t)input_matrix, 
                (uint64_t)output_matrix, (uint64_t)feedthrough_matrix);

    if( ligo_atomic_read(&g_part_index_to_load) != -1) {
        RTSLOG_ERROR("matrixConfigureStateSpace() - There is still a pending configuration " \
                     "to be loaded by the real-time model, returning without setting new config. \n");
        return -1;
    }


    //Verify we aren't trying to change the input/output number
    if( input_vec_len != g_model_config.ss_parts_ptr[part_index].model_inputs ||
        output_vec_len != g_model_config.ss_parts_ptr[part_index].model_outputs) {
            RTSLOG_ERROR("The requested statespace configuration has a bad number of inputs or outputs.\n");
            RTSLOG_ERROR("Inputs: %d, expected %d, Outputs: %d, expected: %d\n", 
            input_vec_len, g_model_config.ss_parts_ptr[part_index].model_inputs,
            output_vec_len, g_model_config.ss_parts_ptr[part_index].model_outputs);
            return -1;
        }

    //Allocate space for matrices, based on inputs
    //Internally we store a temp state_vector so we add that on here
    uint64_t total_sz       = calcDataBlockSzStateSpace(input_vec_len, 
                                                       output_vec_len, 
                                                       state_vec_len) 
                              + state_vec_len * sizeof(double);


    //First we need to check if this is a re-init, and we need to cleanup the old allocation
    if(g_config_hold.mem_block_ptr != NULL && total_sz > g_config_hold.buffer_sz) {
        vfree( g_config_hold.mem_block_ptr );
        g_config_hold.mem_block_ptr = NULL;
        RTSLOG_DEBUG("Free for new allocation, old sz: %u, new sz: %u\n", g_config_hold.buffer_sz, total_sz);
    }

    //Check if we need to re-allocate
    if( g_config_hold.mem_block_ptr == NULL ) {

        RTSLOG_DEBUG("Allocating new buffer for part data, sz: %u\n", total_sz);
        g_config_hold.mem_block_ptr = (double*)vmalloc(total_sz);
        if( g_config_hold.mem_block_ptr == NULL)
        {
            RTSLOG_ERROR("Failed to vmalloc() %lu bytes for the StateSpace part.", total_sz);
            return -1;
        }
        g_config_hold.buffer_sz = total_sz;
    }


    g_config_hold.in_len = input_vec_len;
    g_config_hold.out_len = output_vec_len;
    g_config_hold.state_len = state_vec_len;

    //Save pointers to vector and matrix offsets in mem block
    unsigned offset = 0;
    g_config_hold.state_vec  = &g_config_hold.mem_block_ptr[offset];
    offset += state_vec_len;
    g_config_hold.state_vec_new  = &g_config_hold.mem_block_ptr[offset];

    offset += state_vec_len;
    g_config_hold.state_matrix = &g_config_hold.mem_block_ptr[offset];
    offset += state_vec_len*state_vec_len;
    g_config_hold.input_matrix = &g_config_hold.mem_block_ptr[offset];
    offset += state_vec_len*input_vec_len;
    g_config_hold.output_matrix = &g_config_hold.mem_block_ptr[offset];
    offset += output_vec_len*state_vec_len;
    g_config_hold.feedthrough_matrix = &g_config_hold.mem_block_ptr[offset];


    //
    // Read matrices into memory allocation
    //
    for(i=0; i < g_config_hold.state_len; ++i)
        g_config_hold.state_vec[i] = initial_state_vec[i];

    for(i=0,count=0; i < g_config_hold.state_len; ++i)
        for(j=0; j < g_config_hold.state_len; ++j)
            g_config_hold.state_matrix[i * g_config_hold.state_len + j] = state_matrix[count++];

    for(i=0,count=0; i < g_config_hold.state_len; ++i)
        for(j=0; j < g_config_hold.in_len; ++j) 
            g_config_hold.input_matrix[i * g_config_hold.in_len + j] = input_matrix[count++];
        
    
    for(i=0,count=0; i < g_config_hold.out_len; ++i)
        for(j=0; j < g_config_hold.state_len; ++j) {
            g_config_hold.output_matrix[i * g_config_hold.state_len + j] = output_matrix[count++];
        }
        

    for(i=0,count=0; i < g_config_hold.out_len; ++i)
        for(j=0; j < g_config_hold.in_len; ++j)
            g_config_hold.feedthrough_matrix[i * g_config_hold.in_len + j] = feedthrough_matrix[count++];


    //asm volatile("":::"memory");

    ligo_atomic_set(&g_part_index_to_load, part_index);


    return 0;
}
 
int check_pending_config( void )
{
    return ligo_atomic_read(&g_part_index_to_load);
}

void loadNewMatrixNowStateSpace( unsigned part_index ) 
{
    StateSpaceCtx_t save_old_config;
    StateSpaceCtx_t * ctx = &g_model_config.ss_parts_ptr[part_index].context;

    //Save old config
    memcpy((void*)&save_old_config, ctx, sizeof(save_old_config));
    //Load new config
    memcpy(ctx, (void*)&g_config_hold, sizeof(*ctx));
    //Store old config
    memcpy((void*)&g_config_hold, (void*)&save_old_config, sizeof(save_old_config));



    ligo_atomic_set(&g_part_index_to_load, -1); //Let people know we are done with load
}



//TODO: vactorize with x86 primatives?
void calcStateSpace(unsigned part_index, int reset, double * input_vec, double * out_vec)
{
    int i,j;

    //Check to see if we need to reload our configuration
    if ( ligo_atomic_read(&g_part_index_to_load) == part_index ) {
        //RTSLOG_INFO("About to load a new config for SS part %d\n", part_index);
        loadNewMatrixNowStateSpace(  part_index );
    }


    StateSpaceCtx_t * ctx = &g_model_config.ss_parts_ptr[part_index].context;

    if( reset > 0 ) {
        for(i=0; i < ctx->state_len; ++i)
            ctx->state_vec[i] = 0.0;
    }


    //compute outputs 
    //out_vec(k) += OutputMatrix * state_vec(k) 
    memset(out_vec, 0, sizeof(double)* ctx->out_len);
    for (i=0; i < ctx->out_len; ++i) {
        for(j=0; j < ctx->state_len; ++j) {
            out_vec[i] += ctx->output_matrix[i * ctx->state_len + j] * ctx->state_vec[j];
        }
    }

    //out_vec(k) += FeedMatrix * input_vec(k)
    for (i=0; i < ctx->out_len; ++i) {
        for(j=0; j < ctx->in_len; ++j) {
            out_vec[i] += ctx->feedthrough_matrix[i * ctx->in_len + j] * input_vec[j];
        }
    }

    //build new state
    //state_vec(k+1) += StateMatrix * state_vec(k) 
    memset(ctx->state_vec_new, 0, sizeof(double)* ctx->state_len);
    for(i=0; i < ctx->state_len; ++i) {
        for(j=0; j < ctx->state_len; ++j) {
            ctx->state_vec_new[i] += ctx->state_matrix[i * ctx->state_len + j] * ctx->state_vec[j];
        }
    }

    //state_vec(k+1) += InputMatrix * input_vec[k]
    for(i=0; i < ctx->state_len; ++i) {
        for(j=0; j < ctx->in_len; ++j) {
            ctx->state_vec_new[i] += ctx->input_matrix[i * ctx->in_len + j] * input_vec[j];
        }
    }

    //store state
    for(i=0; i < ctx->state_len; ++i) {
        ctx->state_vec[i] = ctx->state_vec_new[i];
    }
}

void setStateVecStateSpace(unsigned part_index, double * state_vec)
{
    StateSpaceCtx_t * ctx = &g_model_config.ss_parts_ptr[part_index].context;
    for(int i=0; i < ctx->state_len; ++i)
        ctx->state_vec[i] = state_vec[i];
}



//
// Start IOCLT servicing code
//

unsigned getNumPartsStateSpace( void )
{
    return g_model_config.num_parts;
}

void getPartNamePtrStateSpace( unsigned part_index, char ** name_ptr )
{
    *name_ptr = g_model_config.ss_parts_ptr[part_index].name;
}

void getConfigOfPartStateSpace(unsigned part_index, struct stateSpace_part_config_t * fill_me)
{
    fill_me->part_index = part_index;
    fill_me->input_vec_len = g_model_config.ss_parts_ptr[part_index].model_inputs;
    fill_me->output_vec_len = g_model_config.ss_parts_ptr[part_index].model_outputs;
    fill_me->state_vec_len = g_model_config.ss_parts_ptr[part_index].context.state_len;
}

int getConfigAndDataOfPartStateSpace(unsigned part_index, unsigned data_sz, struct stateSpace_part_config_t * fill_me)
{
    if ( g_model_config.ss_parts_ptr[part_index].context.mem_block_ptr == NULL) {
         //mem_block_ptr will be NULL after the model starts until matrixInitStateSpace() 
         //through the IOCTL interface is called.
            return -1;
    
    }
    fill_me->part_index = part_index;
    fill_me->input_vec_len = g_model_config.ss_parts_ptr[part_index].model_inputs;
    fill_me->output_vec_len = g_model_config.ss_parts_ptr[part_index].model_outputs;
    fill_me->state_vec_len = g_model_config.ss_parts_ptr[part_index].context.state_len;
    memcpy( &fill_me->data_block[0], g_model_config.ss_parts_ptr[part_index].context.mem_block_ptr, fill_me->state_vec_len * 8);
    memcpy( &fill_me->data_block[fill_me->state_vec_len], 
            g_model_config.ss_parts_ptr[part_index].context.state_matrix, 
            data_sz - (fill_me->state_vec_len * 8));
            
    return 0;
}

