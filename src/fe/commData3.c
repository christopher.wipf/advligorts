/// @file commData3.c
///	@brief This is the generic software for communicating realtime data
/// between CDS applications.
///	@detail This software supports data communication via: \n
///		1) Shared memory, between two processes on the same computer \n
///		2) Dolphinics Reflected Memory over a local PCIe network.
///     3) Dolphinics Reflected Memory over a bridged PCIe network. (RFM)
/// @author R.Bork, A.Ivanov
/// @copyright Copyright (C) 2014 LIGO Project
/// California Institute of Technology	
///	Massachusetts Institute of Technology
///
/// @license This program is free software: you can redistribute it and/or
/// modify it under the terms of the GNU General Public License as published 
/// by the Free Software Foundation, version 3 of the License. This program
/// is distributed in the hope that it will be useful, but WITHOUT ANY
/// WARRANTY; without even the implied warranty of MERCHANTABILITY or
/// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
/// for more details.


/// Information on Waiting for Late IPCs
///
/// When an IPC is late, we would like to wait some additional time for
/// the IPC to arrive. The simplest approtch might be to to wait a fixed 
/// amount of time, based on model cycle rate. But because commData3Receive()
/// is called first thing in the model's feCode() we don't know how much time
/// we need for the model logic to execute. A better approtch would be 
/// to take into account an estimate of how long the model has been taking
/// and use that to limit how long we can wait. This way models with lots of
/// headroom (lots of models like this) can wait for longer and models with
/// no headroom won't be impaired.
///
/// The g_max_cycle_time_us variable will store the maximum time the model
/// has taken in the last second, and will be used to estimate the 
/// headroom we have to wait for IPCs. It is updated by calling 
/// commData3SetCycleMax(), the controller will do this once a second. 
///
/// The g_cycle_start_tsc variable will store the start time TSC counter.
/// This will be used to measure how long we have been waiting and when
/// we should abort out wait. This should be updated by the 
/// commData3SetCycleStartTime() function every cycle before calling feCode().
///
/// Because waiting for IPCs will in turn make this model send its IPCs later
/// and thus make its receivers get their data later, we could start a chain 
/// reaction where IPC errors become more common and then the IPC waiting 
/// becomes the main reason for missed IPCs. So this solution will assume 
/// mostly good IPCs with the occasional late IPC. That means we should only
/// wait when the last IPC was good, and once a IPC times out, we shouldn't
/// wait for IPCs until they start coming in on time again. 
///

#include "commData3.h"
#include "controller.h" //cdsPciModules, _shmipc_shm
#include "drv/rts-logger.h"
#include "util/timing.h"

#ifdef __KERNEL__
#include <asm/cacheflush.h>
#include <linux/delay.h>
#else
#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#endif


/**  We will wait for 75% of the expected headroom.
 *
 */
#define MULT_HEADROOM_TO_WAIT 0.75

/** For slow models, we have a TON of potential headroom.
 *  However, it really dosn't make sense to wait that long because
 *  after enough time has passed, the IPC is not likely to come through.
 *  This varirable controls that max wait, where we just assume the IPC
 *  is never going to come in. 
 */ 
#define MAX_ABS_WAIT_TIME_NS 20000

/** This is calculated in the init function, and stores a multiplier that will 
 *  convert cycle numbers from models of different rates to the equivalent
 *  count for a 2^16 Hz model. 2048Hz(190) -> 65536Hz(6080)
 */
static int g_localCycleTo65K;

/** The g_max_cycle_time_us variable stores the model's max execution time 
 * (over the last second) and is updated by the controller once a second. 
 * This is used to calculate the expected headroom that we have to wait for
 * late IPCs.
 */
static uint32_t g_max_cycle_time_us;

/** The g_cycle_start_tsc is a captured monotonic clk from the start of the 
 * models latest cycle. This is used to measure how long we have been waiting
 * so we can timeout.
 */
static LIGO_TIMER_t g_cycle_start_tsc;

/** The g_in_error_state variable is used to mark when one or more IPCs have
 *  timed out. This is used to prevent us from timing out over and over again.
 *  After we go into this error state, the system will wait until all IPCs 
 *  are received as expected (in a cycle) before starting to wait for 
 *  late IPCs again.
 */
static bool g_in_error_state;

/** Really commData3Init() should be able to return an error, but because this
 *  is called in feCode() that is not currently feasible. So for now, we will 
 *  store the result of the init, and check it in the controller logic, after
 *  the first init call to feCode().
 */
static bool g_init_failed;

/** This structure stores stats about how long the model has waited for 
 *  late IPC data. 
 */
static CDS_IPC_STATS_t g_ipc_wait_time_stats = {0, };

///	This function is called from the user application to initialize
/// communications structures 	and pointers.
///	@param[in] connects = total number of IPC connections in the application
///	@param[in] rate = Sample rate of the calling application eg 2048
///	@param[in,out] ipcInfo[] = Stucture to hold information about each IPC
void commData3Init(
    int connects, // total number of IPC connections in the application
    int model_rate_hz,
    int send_rate, // Sample rate of the calling application eg 2048, 16384, etc.
    CDS_IPC_INFO ipcInfo[] // IPC information structure
)
{
    int           ii;
    //Set ipcMemOffset to default value, will re-set below
    unsigned long ipcMemOffset = IPC_PCIE_BASE_OFFSET + RFM0_OFFSET;

    //We will default to a very short wait time, until commData3SetCycleMax()
    //can update it. 
    g_max_cycle_time_us = 1; 
    g_in_error_state = false;
    g_init_failed = false;

    //For now all cycle numbers passed are in terms of a 65K or less
    //so we don't need to support a divisor
    if (model_rate_hz >= IPC_MAX_RATE)
    {
        g_localCycleTo65K = 1;
    }
    else
    {
        g_localCycleTo65K = IPC_MAX_RATE / model_rate_hz;
    }

    for ( ii = 0; ii < connects; ii++ )
    {
        // Set the sendCycle field, used by all send/rcv to determine IPC data
        // block to write/read
        //      All cycle counts sent as part of data sync word are based on max
        //      supported rate of 65536 cycles/sec, regardless of sender/receiver 
        //      native cycle rate.
        ipcInfo[ ii ].sendCycle = IPC_MAX_RATE / send_rate;

        // Sender always sends data at his native rate, or a configured lower ipc_rate. 
        // It is the responsiblity of the reciever to sync to this rate, 
        // regardless of the rate of the receiver application.
        if ( ipcInfo[ ii ].mode == IRCV ) // RCVR
        {

            if ( ipcInfo[ ii ].sendRate >= model_rate_hz )
            {
                ipcInfo[ ii ].rcvCycle65k = 1;
            }
            else //Model is faster than IPC
            {
                ipcInfo[ ii ].rcvCycle65k = IPC_MAX_RATE / ipcInfo[ ii ].sendRate;
            }
            RTSLOG_DEBUG("New Recever : sendRate: %d, rcvCycle65k: %d, localCycleTo65K: %d\n",
                         ipcInfo[ ii ].sendRate,
                         ipcInfo[ii].rcvCycle65k,
                         g_localCycleTo65K);
        }
        // Clear the data point
        ipcInfo[ ii ].data = 0.0;
        ipcInfo[ ii ].pIpcDataRead = NULL;
        ipcInfo[ ii ].pIpcDataWrite = NULL;

        // Save pointers to the IPC communications memory locations.
        if ( ipcInfo[ ii ].netType == IRFM0 )
            ipcMemOffset = IPC_PCIE_BASE_OFFSET + RFM0_OFFSET;
        else if ( ipcInfo[ ii ].netType == IRFM1 )
            ipcMemOffset = IPC_PCIE_BASE_OFFSET + RFM1_OFFSET;

        if ( ( ipcInfo[ ii ].netType == IRFM0 ||
               ipcInfo[ ii ].netType == IRFM1 ) &&
             ( ipcInfo[ ii ].mode == ISND ) &&
             ( cdsPciModules.dolphinRfmWritePtr ) )
        {
            ipcInfo[ ii ].pIpcDataWrite =
                (CDS_IPC_COMMS*)( 
                    (volatile char*)( cdsPciModules.dolphinRfmWritePtr ) + ipcMemOffset 
                );
        }
        else if ( ( ipcInfo[ ii ].netType == IRFM0 ||
               ipcInfo[ ii ].netType == IRFM1 ) &&
             ( ipcInfo[ ii ].mode == IRCV ) &&
             ( cdsPciModules.dolphinRfmReadPtr ) )
        {
            ipcInfo[ ii ].pIpcDataRead =
                (CDS_IPC_COMMS*)( 
                    (volatile char*)( cdsPciModules.dolphinRfmReadPtr ) + ipcMemOffset 
                );
        }
        else if ( ipcInfo[ ii ].netType ==
             ISHME ) // Computer shared memory ******************************
        {
            if ( ipcInfo[ ii ].mode == ISND )
                ipcInfo[ ii ].pIpcDataWrite =
                    (CDS_IPC_COMMS*)( _shmipc_shm + IPC_BASE_OFFSET );
            else
                ipcInfo[ ii ].pIpcDataRead =
                    (CDS_IPC_COMMS*)( _shmipc_shm + IPC_BASE_OFFSET );
        }
        // PCIe communications requires one pointer for sending data and a
        // second one for receiving data.
        else if ( ( ipcInfo[ ii ].netType == IPCIE ) &&
             ( ipcInfo[ ii ].mode == IRCV ) &&
             ( cdsPciModules.dolphinPcieReadPtr ) )
        {
            ipcInfo[ ii ].pIpcDataRead =
                (CDS_IPC_COMMS*)( 
                    (volatile char*)( cdsPciModules.dolphinPcieReadPtr ) + IPC_PCIE_BASE_OFFSET 
                );
        }
        else if ( ( ipcInfo[ ii ].netType == IPCIE ) &&
             ( ipcInfo[ ii ].mode == ISND ) &&
             ( cdsPciModules.dolphinPcieWritePtr ) )
        {
            ipcInfo[ ii ].pIpcDataWrite =
                (CDS_IPC_COMMS*)( 
                    (volatile char*)( cdsPciModules.dolphinPcieWritePtr ) + IPC_PCIE_BASE_OFFSET 
                );

        }
        else //Error case, no match
        {
            RTSLOG_ERROR("commData3Init() - IPC index %d, was not matched as a supported type. "
            "netType: %d, mode: %d",ii , ipcInfo[ ii ].netType, ipcInfo[ ii ].mode );
            if( ipcInfo[ ii ].netType == IPCIE && 
                (!cdsPciModules.dolphinPcieReadPtr || 
                !cdsPciModules.dolphinPcieWritePtr)) {
                RTSLOG_ERROR("commData3Init() - This model contains a Dolphin PCIE IPC but this system is not a dolphin node, "
                             "or has not been configured to initalize Dolphin (pciRfm=1). If you want this model to run on a "
                             "test system without Dolphin, you can add the (use_shm_ipc=1) cds parameter to convert all IPCs to "
                             "SHMEM during the build.\n");
            }
            if( (ipcInfo[ ii ].netType == IRFM0 || ipcInfo[ ii ].netType == IRFM1) &&
                (!cdsPciModules.dolphinRfmReadPtr ||
                !cdsPciModules.dolphinRfmWritePtr)) {
                RTSLOG_ERROR("commData3Init() - This model contains a Dolphin RFMX IPC but this system is not a dolphin node, "
                             "or has not been configured to initalize Dolphin (pciRfm=1). If you want this model to run on a "
                             "test system without Dolphin, you can add the (use_shm_ipc=1) cds parameter to convert all IPCs to "
                             "SHMEM during the build.\n");
            }
            g_init_failed = true;
        }
#if 0
	// Following for diags, if desired. Otherwise, leave out as it fills dmesg
	if(ipcInfo[ii].mode == ISND && ipcInfo[ii].netType != ISHME) {
        RTSLOG_DEBUG("IPC Number = %d\n",ipcInfo[ii].ipcNum);
        RTSLOG_DEBUG("IPC Name = %s\n",ipcInfo[ii].name);
        RTSLOG_DEBUG("Sender Model Name = %s\n",ipcInfo[ii].senderModelName);
        RTSLOG_DEBUG("RCV Rate  = %d\n",ipcInfo[ii].rcvRate);
        RTSLOG_DEBUG("Send Computer Number  = %d\n",ipcInfo[ii].sendNode);
        RTSLOG_DEBUG("Send address  = %lx\n",(unsigned long)&ipcInfo[ii].pIpcDataWrite->dBlock[0][ipcInfo[ii].ipcNum].data);
	}
#endif
    }
}

int commData3GetInitStatus( void )
{
    return g_init_failed;
}

// *************************************************************************************************
/// This function is called once on FE code init and returns the number of
/// IPC senders and receivers so they can be reported in a data channel
/// @param[in]  totalIPCs The total number of IPC in the model
/// @param[in]  ipcInfo[] The struct containing all IPC info for the model
/// @param[out] numSenders The number of IPCs sent by this model
/// @param[out] numReceiver The number of IPC receivers in this model
void commData3GetIpcNums(int totalIPCs, CDS_IPC_INFO ipcInfo[], int * numSenders, int * numReceiver)
{
    int i;

    *numSenders = 0;
    *numReceiver = 0;

    for ( i = 0; i < totalIPCs; ++i )
    {
        if ( ipcInfo[ i ].mode == ISND ) 
        {
            ++(*numSenders);
        }
        if ( ipcInfo[ i ].mode == IRCV ) 
        {
            ++(*numReceiver);
        }
    }
}


// *************************************************************************************************
///	@brief This function is called from the user application to send data via IPC
/// connections.
///
/// @detail This routine sends out all IPC data marked as a send (SND) channel in the IPC
///         INFO list. Data is sent at the native rate of the calling model. Data
///         sent is of type double, with timestamp and 65536 cycle count combined into
///         long. 
///	@param[in] connects = total number of IPC connections in the application
///	@param[in,out] ipcInfo[] = Structure to hold information about each IPC
///	@param[in] timeSec = Present GPS time in GPS seconds
///	@param[in] cycle = Present IPC cycle of the user application making this
/// call.
void commData3Send(
    int          connects, // Total number of IPC connections in the application
    CDS_IPC_INFO ipcInfo[], // IPC information structure
    int          timeSec, // Present GPS Second
    int          cycle ) // Application IPC write cycle count (0 to IPC_RATE)
{
    unsigned long
        syncWord; ///	\param syncWord Combined GPS timestamp and cycle counter
    int ipcIndex; ///	\param ipcIndex Pointer to next IPC data buffer
    int dataCycle; ///	\param dataCycle Cycle counter 0-65535
    int ii = 0; ///	\param ii Loop counter
    int chan; ///	\param chan Local ipc number
    int sendBlock; ///	\param sendBlock Data block data is to be sent to
    int lastPcie = -1;
    int dolphinEnabled = 1;
    LIGO_TIMER_t send_timer;

    timer_start( &send_timer );

#ifdef RFM_DELAY
    // Need to write ahead one extra block
    int mycycle = ( cycle + RFM_DELAY );
    sendBlock = ( ( mycycle + 1 ) * ( IPC_MAX_RATE / IPC_RATE ) ) % IPC_BLOCKS;
    dataCycle = ( ( mycycle + 1 ) * ipcInfo[ 0 ].sendCycle ) % IPC_MAX_RATE;
    if ( dataCycle == 0 || dataCycle == ipcInfo[ 0 ].sendCycle )
        syncWord = timeSec + 1;
    else
        syncWord = timeSec;
    syncWord = ( syncWord << 32 ) + dataCycle;
#else


    sendBlock = ( ( cycle + 1 ) * ( IPC_MAX_RATE / IPC_RATE ) ) % IPC_BLOCKS;
    // Calculate the SYNC word to be sent with all data.
    // Determine the cycle count to be sent with the data
    dataCycle = ( ( cycle + 1 ) * ipcInfo[ 0 ].sendCycle ) % IPC_MAX_RATE;
    // Since this is write ahead, need to increment the GPS second if
    // writing to the first block of a new second.
    if ( dataCycle == 0 )
        syncWord = timeSec + 1;
    else
        syncWord = timeSec;
    // Combine GPS seconds and cycle counter into long word.
    syncWord = ( syncWord << 32 ) + dataCycle;
#endif

    //If the IOP model has signaled us to not send remote IPCsn
    if (ioMemData->stop_dolphin_ipcs != 0)
        dolphinEnabled = 0;


    // Want to send out RFM signals first to allow maximum time for data
    // delivery.
    for ( ii = 0; dolphinEnabled && ii < connects; ii++ )
    {
        // If IPC Sender on RFM Network:
        // RFM network has highest latency, so want to get these signals out
        // first.
        if ( ( ipcInfo[ ii ].mode == ISND ) &&
             ( ( ipcInfo[ ii ].netType == IRFM0 ) ||
               ( ipcInfo[ ii ].netType == IRFM1 ) ) )
        {
            chan = ipcInfo[ ii ].ipcNum;
            // Determine next block to write in IPC_BLOCKS block buffer
            ipcIndex = ipcInfo[ ii ].ipcNum;

            // Write Data
            ipcInfo[ ii ]
                .pIpcDataWrite
                ->dBlock[ sendBlock ][ ipcIndex ]
                .data = ipcInfo[ ii ].data;
            // Write timestamp/cycle counter word
            ipcInfo[ ii ]
                .pIpcDataWrite
                ->dBlock[ sendBlock ][ ipcIndex ]
                .timestamp = syncWord;
            lastPcie = ii;
            
        }
    }

#ifdef __KERNEL__
    // Flush out the last PCIe transmission
    if ( lastPcie >= 0 )
    {
        clflush_cache_range(
            (void*)&( ipcInfo[ lastPcie ]
                          .pIpcDataWrite
                          ->dBlock[ sendBlock ][ ipcInfo[ lastPcie ].ipcNum ]
                          .data ),
            16 );
    }
    lastPcie = -1;
#endif

//If RFM_DELAY is defined we messed up sendBlock and dataCycle above so we
//recalculate them for the below non-rfm IPCs
#ifdef RFM_DELAY
    // We don't want to delay SHMEM or PCIe writes, so calc block as usual,
    // so need to recalc send block and syncWord.
    sendBlock = ( ( cycle + 1 ) * ( IPC_MAX_RATE / IPC_RATE ) ) % IPC_BLOCKS;
    // Calculate the SYNC word to be sent with all data.
    // Determine the cycle count to be sent with the data
    dataCycle = ( ( cycle + 1 ) * ipcInfo[ 0 ].sendCycle ) % IPC_MAX_RATE;
    // Since this is write ahead, need to increment the GPS second if
    // writing to the first block of a new second.
    if ( dataCycle == 0 )
        syncWord = timeSec + 1;
    else
        syncWord = timeSec;
    // Combine GPS seconds and cycle counter into long word.
    syncWord = ( syncWord << 32 ) + dataCycle;
#endif

    for ( ii = 0; ii < connects; ii++ )
    {
        // If IPC Sender on PCIE Network or via Shared Memory:
        if ( ( ipcInfo[ ii ].mode == ISND ) &&
             ( ( ipcInfo[ ii ].netType == ISHME ) ||
               ( dolphinEnabled && ipcInfo[ ii ].netType == IPCIE ) ) ) //Don't send dolphin
        {
            chan = ipcInfo[ ii ].ipcNum;
            // Determine next block to write in IPC_BLOCKS block buffer
            ipcIndex = ipcInfo[ ii ].ipcNum;

            // Write Data
            ipcInfo[ ii ]
                .pIpcDataWrite
                ->dBlock[ sendBlock ][ ipcIndex ]
                .data = ipcInfo[ ii ].data;
            // Write timestamp/cycle counter word
            ipcInfo[ ii ]
                .pIpcDataWrite
                ->dBlock[ sendBlock ][ ipcIndex ]
                .timestamp = syncWord;
            if ( ipcInfo[ ii ].netType == IPCIE )
            {
                lastPcie = ii;
            }
            
        }
    }

#ifdef __KERNEL__
    // Flush out the last PCIe transmission
    if ( lastPcie >= 0 )
    {
        clflush_cache_range(
            (void*)&( ipcInfo[ lastPcie ]
                          .pIpcDataWrite
                          ->dBlock[ sendBlock ][ ipcInfo[ lastPcie ].ipcNum ]
                          .data ),
            16 );
    }
#endif

    uint64_t total_time_ns = timer_end_ns( &send_timer );
    g_ipc_wait_time_stats.ipcTotalSendTime_ns += total_time_ns;
    if( total_time_ns > g_ipc_wait_time_stats.ipcMaxSendTime_ns )
        g_ipc_wait_time_stats.ipcMaxSendTime_ns = total_time_ns;


}

// *************************************************************************************************
///	This function is called from the user application to receive data via
/// IPC connections. It receives all IPC data marked as a read (RCV) channel
/// in the IPC INFO list.
///
///	@param[in] connects = total number of IPC connections in the application
///	@param[in,out] ipcInfo[] = Stucture to hold information about each IPC
///	@param[in] timeSec = Present GPS time in GPS seconds
///	@param[in] cycle = Present cycle of the user application making this
/// call.
///
/// @return The number of IPCs that had at least one error during the read
///
int commData3Receive(
    int          connects, // Total number of IPC connections in the application
    CDS_IPC_INFO ipcInfo[], // IPC information structure
    int          timeSec, // Present GPS Second
    int          cycle ) // Application cycle count (0 to FE_CODE_RATE)

{
    unsigned long syncWord; // Combined GPS timestamp and cycle counter word
                            // received with data
    unsigned long expectedSyncWord; // Local version of syncWord for comparison and
                              // error detection
    int ipcIndex; // Pointer to next IPC data buffer
    int cycle65k; // All data sent with 64K cycle count; need to convert local
                  // app cycle count to match
    int    ii;
    int    rcvBlock; // Which of the IPC_BLOCKS IPC data blocks to read from
    double tmp; // Temp location for data for checking NaN
    int numInError=0;
    uint64_t max_time_to_wait_ns;
    int ipc_good = false, ipc_num_in_list=0, slow_ipc_num=0;

    int temp_sleep_count=0;

    //g_max_cycle_time_us is always set by the controller before it is reset for the second
    if ( g_max_cycle_time_us >= ((1.0/MODEL_RATE_HZ)*1000000)) { //Max is larger than cycle time (no headroom)
        max_time_to_wait_ns = 0;
    }
    else {
        max_time_to_wait_ns = ((((1.0/MODEL_RATE_HZ)*1000000) - g_max_cycle_time_us) * 1000) * MULT_HEADROOM_TO_WAIT;
    }
    //Never wait longer than MAX_ABS_WAIT_TIME_NS
    if ( max_time_to_wait_ns > MAX_ABS_WAIT_TIME_NS) max_time_to_wait_ns = MAX_ABS_WAIT_TIME_NS;

    // Create local 65K cycle count
    cycle65k = (  cycle * g_localCycleTo65K );
    // Calculate the block where the next data point is at
    rcvBlock = (cycle65k) % IPC_BLOCKS;
    // Create local GPS time/cycle word for comparison to ipc
    expectedSyncWord = ( (uint64_t)timeSec << 32 ) + cycle65k;


    for ( ii = 0; ii < connects; ii++ )
    {
        if ( ipcInfo[ ii ].mode == IRCV ) // Zero = Rcv and One = Send
        {
            ++ipc_num_in_list;
            if ( (cycle65k % ipcInfo[ ii ].rcvCycle65k ) == 0 ) // Time to rcv
            {

                do {

                    ipcIndex = ipcInfo[ ii ].ipcNum;
                    // Read GPS time/cycle count
                    tmp = ipcInfo[ ii ]
                            .pIpcDataRead
                            ->dBlock[ rcvBlock ][ ipcIndex ]
                            .data;
                    syncWord = ipcInfo[ ii ]
                                .pIpcDataRead
                                ->dBlock[ rcvBlock ][ ipcIndex ]
                                .timestamp;
                    // If IPC syncword = local syncword, data is good
                    if ( syncWord == expectedSyncWord )
                    {
                        ipcInfo[ ii ].data = tmp;
                        ipc_good = true;
                        break; //Got latest data, exit wait loop
                    }
                    else
                    {
                        //wait, in case IPC is almost here
                        if ( g_in_error_state == false )
                        {
                            udelay(1);
                            ++temp_sleep_count;
                            slow_ipc_num = ipc_num_in_list;
                        }
                    }

                } while ( timer_tock_ns(&g_cycle_start_tsc) < max_time_to_wait_ns 
                          && g_in_error_state == false );

                //We timed out
                if( ipc_good == false ) {
                    ipcInfo[ ii ].errFlag++;
                    g_in_error_state = true;
                    ++numInError;
                }
                ipc_good = false; //Reset for next IPC

            }
        }
    }

    if( temp_sleep_count > 0 && g_in_error_state == false)
    {
        int time_waited_ns = (int)timer_tock_ns(&g_cycle_start_tsc);
        ++g_ipc_wait_time_stats.numWaitsInSec;
        g_ipc_wait_time_stats.slowIpcListIndex = slow_ipc_num;
        if( time_waited_ns > g_ipc_wait_time_stats.ipcMaxWait_ns ) 
            g_ipc_wait_time_stats.ipcMaxWait_ns = time_waited_ns;
    }


    // On cycle 0, set error flags to send back to EPICS
    if ( cycle == 0 )
    {
        ipcErrBits = ipcErrBits & 0xf0;
        for ( ii = 0; ii < connects; ii++ )
        {
            if ( ipcInfo[ ii ].mode == IRCV ) // Zero = Rcv and One = Send
            {
                ipcInfo[ ii ].errTotal = ipcInfo[ ii ].errFlag;
                if ( ipcInfo[ ii ].errFlag )
                {
                    ipcErrBits |= 1 << ( ipcInfo[ ii ].netType );
                    ipcErrBits |= 16 << ( ipcInfo[ ii ].netType );
                }
                ipcInfo[ ii ].errFlag = 0;
            }
        }
    } //if ( cycle == 0 )

    if( numInError == 0) {
        //We got all our IPCs, so we are in a good state
        //start waiting for IPCs again
        g_in_error_state = false;
    }

    return numInError;
}

void commData3SetCycleMax(unsigned measured_max_us)
{
    g_max_cycle_time_us = measured_max_us;
}

void commData3SetCycleStartTime(LIGO_TIMER_t cycle_start_tsc)
{
    g_cycle_start_tsc = cycle_start_tsc;
}


void ipc_subsys_1Hz_stats_update( volatile CDS_EPICS* epics_ptr )
{

    //Calc Recv Stats
    epics_ptr->epicsOutput.ipcNumWaitsInSec = g_ipc_wait_time_stats.numWaitsInSec;
    epics_ptr->epicsOutput.ipcMaxWait_ns = g_ipc_wait_time_stats.ipcMaxWait_ns;
    if( g_ipc_wait_time_stats.slowIpcListIndex != 0) 
        epics_ptr->epicsOutput.slowIpcInList = g_ipc_wait_time_stats.slowIpcListIndex;
    if( g_ipc_wait_time_stats.ipcMaxWait_ns > epics_ptr->epicsOutput.ipcMaxWaitHold_ns)
        epics_ptr->epicsOutput.ipcMaxWaitHold_ns = g_ipc_wait_time_stats.ipcMaxWait_ns;

    //Calc Send Stats
    epics_ptr->epicsOutput.ipcMaxSendTime_ns = g_ipc_wait_time_stats.ipcMaxSendTime_ns;
    epics_ptr->epicsOutput.ipcAvgSendTime_ns = 
        g_ipc_wait_time_stats.ipcTotalSendTime_ns / (MODEL_RATE_HZ / UNDERSAMPLE);

    if( g_ipc_wait_time_stats.ipcMaxSendTime_ns > epics_ptr->epicsOutput.ipcMaxSendTimeHold_ns)
    {
        epics_ptr->epicsOutput.ipcMaxSendTimeHold_ns = g_ipc_wait_time_stats.ipcMaxSendTime_ns;
    }

    if( g_ipc_wait_time_stats.ipcMaxSendTime_ns  > IPC_MAX_SEND_TIME_NS )
    {
        RTSLOG_WARN("A commData3Send() (IPC) send call took %d ns, in the last second...\n", 
                    g_ipc_wait_time_stats.ipcMaxSendTime_ns);
    }

    //Clear stats for next second
    memset(&g_ipc_wait_time_stats, 0, sizeof(g_ipc_wait_time_stats));


    return;
}

void ipc_subsys_diag_reset( volatile CDS_EPICS* epics_ptr )
{
    epics_ptr->epicsOutput.ipcMaxWaitHold_ns = 0;
    epics_ptr->epicsOutput.slowIpcInList = 0;
    epics_ptr->epicsOutput.ipcMaxSendTimeHold_ns = 0;
}
