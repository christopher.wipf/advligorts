//
// Created by jonathan.hanks on 8/24/22.
//
#include "catch.hpp"
#include <vector>

#include "epics_channel_common.hh"

#include <iostream>

TEST_CASE( "Test to_upper_ascii" )
{
    typedef struct TestCase
    {
        epics::channel_name input;
        epics::channel_name output;
    } TestCase;

    std::vector< TestCase > test_cases = {
        TestCase{ epics::channel_name( "" ), epics::channel_name( "" ) },
        TestCase{ epics::channel_name( "abc" ), epics::channel_name( "ABC" ) },
        TestCase{ epics::channel_name( "c1b" ), epics::channel_name( "C1B" ) },
        TestCase{ epics::channel_name( "c1b DeF 2^7*\\#" ),
                  epics::channel_name( "C1B DEF 2^7*\\#" ) },
    };

    for ( const auto& test_case : test_cases )
    {
        auto out = epics::to_upper_ascii( test_case.input );
        REQUIRE( out == test_case.output );
    }
}

TEST_CASE( "Test model_to_system" )
{
    typedef struct TestCase
    {
        const char*         modelname;
        epics::channel_name output;
    } TestCase;

    std::vector< TestCase > test_cases = {
        TestCase{ "", epics::channel_name( "" ) },
        TestCase{ "abc", epics::channel_name( "AB:C" ) },
        TestCase{ "c1b", epics::channel_name( "C1:B" ) },
        TestCase{ "abcd", epics::channel_name( "AB:CD" ) },
        TestCase{ "abcde", epics::channel_name( "AB:CDE" ) },
        TestCase{ "abcdef", epics::channel_name( "AB:CDE-F" ) },
        TestCase{ "h1syscstcssdf", epics::channel_name( "H1:SYS-CSTCSSDF" ) }
    };

    for ( const auto& test_case : test_cases )
    {
        auto out = epics::model_to_system( test_case.modelname );
        REQUIRE( out == test_case.output );
    }
}

TEST_CASE( "Test make_name" )
{
    typedef struct TestCase
    {
        const char* prefix;
        int         output;
    } TestCase;

    std::vector< TestCase > test_cases = {
        TestCase{ nullptr, 0 },
        TestCase{ "", 0 },
        TestCase{ "abc", 0 },
        TestCase{ "abc1234", 0 },
        TestCase{ "abc-1234", 1234 },
        TestCase{ "abc-1234-2", 1234 },
        TestCase{ "L1:FEC-1234", 1234 },
        TestCase{ "L1:FEC-1234abc", 1234 },
        TestCase{ "L1:FEC-1234-", 1234 },
        TestCase{ "L1:FEC-1234-2", 1234 },
    };

    for ( const auto& test_case : test_cases )
    {
        auto out = epics::extract_dcu_from_prefix( test_case.prefix );
        REQUIRE( out == test_case.output );
    }
}

TEST_CASE( "Test extract_dcu_from_prefix" )
{
    typedef struct TestCase
    {
        const char*         prefix;
        const char*         name;
        epics::channel_name output;
    } TestCase;

    std::vector< TestCase > test_cases = {
        TestCase{ nullptr, nullptr, epics::channel_name( "_" ) },
        TestCase{ "", "", epics::channel_name( "_" ) },
        TestCase{ "abc", "def", epics::channel_name( "abc_def" ) },
    };

    for ( const auto& test_case : test_cases )
    {
        auto out = epics::make_name( test_case.prefix, test_case.name );
        REQUIRE( out == test_case.output );

        epics::channel_name fixed_prefix{ test_case.prefix };
        auto out_prefix = epics::make_name( fixed_prefix, test_case.name );
        REQUIRE( out_prefix == test_case.output );
    }
}
