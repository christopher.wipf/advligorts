//
// Created by jonathan.hanks on 11/17/20.
//

#ifndef DAQD_TRUNK_SIMPLE_RANGE_HH
#define DAQD_TRUNK_SIMPLE_RANGE_HH

#include <type_traits>
#include <utility>

namespace embedded
{
    /*!
     * @brief a very simple range class, just tracks two iterators in one unit, [start, end)
     * @tparam It the iterator type, which must be noexcept on copy and move
     */
    template <typename It>
    class range
    {
        static_assert(std::is_nothrow_copy_constructible<It>::value, "It must be no throw copy constructable");
        static_assert(std::is_nothrow_copy_assignable<It>::value, "It must be no throw copyable");
        static_assert(std::is_nothrow_move_constructible<It>::value, "It must be no throw move constructable");
        static_assert(std::is_nothrow_move_assignable<It>::value, "It must be no throw movable");
    public:
        using iterator = It;

        range() noexcept: start_{}, end_{} {}
        range(const range&) noexcept = default;
        range(range&&) noexcept = default;
        range(It it1, It it2) noexcept: start_{std::move(it1)}, end_{std::move(it2)} {}
        range& operator=(const range&) noexcept = default;
        range& operator=(range&&) noexcept = default;

        iterator
        begin() noexcept
        {
            return start_;
        }

        iterator
        end() noexcept
        {
            return end_;
        }

        const iterator
        begin() const noexcept
        {
            return start_;
        }

        const iterator
        end() const noexcept
        {
            return end_;
        }

    private:
        It start_;
        It end_;
    };

    /*!
     * @brief Given two iterators of type It construct a range
     * @tparam It
     * @param start
     * @param end
     * @return the resulting range
     */
    template <typename It>
    range<It>
    make_range(It start, It end)
    {
        return range<It>(std::move(start), std::move(end));
    }
}

#endif //DAQD_TRUNK_SIMPLE_RANGE_HH
