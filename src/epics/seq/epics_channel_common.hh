//
// Created by jonathan.hanks on 3/21/22.
//

#ifndef DAQD_TRUNK_EPICS_CHANNEL_TYPES_HH
#define DAQD_TRUNK_EPICS_CHANNEL_TYPES_HH

#include <cctype>
#include "fixed_size_string.hh"

namespace epics
{
    using channel_name = embedded::fixed_string< 256 >;

    inline channel_name
    to_upper_ascii( const channel_name& input )
    {
        channel_name out{ };
        auto         buf = out.get_buffer( );
        auto         dest = buf.data( );
        for ( const auto ch : input )
        {
            *dest = std::toupper( static_cast< int >( ch ) );
            ++dest;
        }
        return out;
    }

    inline channel_name
    model_to_system( const char* modelname )
    {
        channel_name dest{ };
        channel_name name( modelname );
        channel_name upper = to_upper_ascii( name );
        auto         count = 0;
        for ( const auto& ch : upper )
        {
            switch ( count )
            {
            case 2:
                dest += ":";
                break;
            case 5:
                dest += "-";
                break;
            }
            dest += ch;
            ++count;
        }
        return dest;
    }

    inline channel_name
    make_name( const char* prefix, const char* name )
    {
        channel_name output{ };
        output.printf(
            "%s_%s", ( prefix ? prefix : "" ), ( name ? name : "" ) );
        return output;
    }
    inline channel_name
    make_name( const channel_name& prefix, const char* name )
    {
        return make_name( prefix.c_str( ), name );
    }

    inline int
    extract_dcu_from_prefix( const char* prefix )
    {
        if ( !prefix )
        {
            return 0;
        }
        auto dash = std::strstr( prefix, "-" );
        if ( !dash )
        {
            return 0;
        }
        return std::atoi( dash + 1 );
    }

    enum class PVType
    {
        UInt16,
        Int32,
        UInt32,
        Float64,
        String,
    };

    template < PVType T >
    struct type_lookup
    {
    };

    template <>
    struct type_lookup< PVType::UInt16 >
    {
        using type = std::uint16_t;
    };

    template <>
    struct type_lookup< PVType::Int32 >
    {
        using type = std::int32_t;
    };

    template <>
    struct type_lookup< PVType::UInt32 >
    {
        using type = std::uint32_t;
    };

    template <>
    struct type_lookup< PVType::Float64 >
    {
        using type = double;
    };

    template <>
    struct type_lookup< PVType::String >
    {
        using type = embedded::fixed_string< 256 >;
    };
} // namespace epics

#endif // DAQD_TRUNK_EPICS_CHANNEL_TYPES_HH
