import os
import os.path
import time
import epics
import typing

import integration
import sys

integration.Executable(name="epics_test.py", hints=[os.path.dirname(sys.argv[0])], description=["EPICS EDC test ioc"])
integration.Executable(name="standalone_edc", hints=[], description=["Standalone edc application"])
integration.Executable(name="local_dc", hints=["../../local_dc",], description=["local data concentrator"])
integration.Executable(name="daqd", hints=["../../daqd", ], description=["daqd executable"])
integration.Executable(name="fe_stream_check_edcu_nds", hints=["../../fe_stream_test", ],
                       description=["fe stream check specialized for the edcu"])

ini_dir = integration.state.temp_dir('ini_files')
ini_filename = os.path.join(ini_dir, 'edcu.ini')
master_file = os.path.join(ini_dir, "master")

with open(master_file, "wt") as f:
    f.write("{0}\n".format(ini_filename))

testpoint_file = ""
daqdrc_file = os.path.join(ini_dir, "daqdrc")

integration.transform_text_file(input="daqdrc_standalone_edc_live_test",
                                output=daqdrc_file,
                                substitutions=[('MASTER', master_file),
                                               ('TESTPOINT', testpoint_file)])

ioc = integration.Process("epics_test.py",
                          ["--ini", ini_filename])
edcu = integration.Process("standalone_edc",
                           ["-b", "edc",
                            "-i", ini_filename,
                            "-p", "X6:EDCU_",
                            "-l", "127.0.0.1:9000",
                            ])
local_dc = integration.Process("local_dc",
                               ["-b", "local_dc",
                                "-m", "100",
                                "-s", "edc:12",
                                "-d", ini_dir])
daqd = integration.Process("daqd",
                           ["-c", daqdrc_file])
fe_check = integration.Process("fe_stream_check_edcu_nds",
                               ["-c", "100",
                                "-start", "0",
                                "-stop", "5"])

def wait_for_ini():
    timeout = time.time() + 5
    while time.time() < timeout:
        if os.path.exists(ini_filename):
            return
        time.sleep(0.25)
    raise RuntimeError("ini file did not appear in time")


def check_ok(timeout:float):
    end_time = time.time() + timeout

    def do_check():
        global fe_check
        fe_check.run()
        while True:
            state = fe_check.state()
            if state == integration.Process.STOPPED:
                fe_check.ignore()
                return
            elif state == integration.Process.FAILED:
                raise RuntimeError('Data check failed')
            print("state is {0}".format(state))

            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for nds check to complete')
            time.sleep(0.5)

    return do_check


def check_epics_ge(name: str, ref: int):

    def do_check():
        val = epics.caget(name)
        print("check_epics_ge {0} >= {1} got {2}".format(name, ref, val))
        if val is None:
            raise RuntimeError("Unable to retrieve {0}".format(name))
        if val < ref:
            raise RuntimeError("{0} is less than {1}".format(name, ref))

    return do_check


def check_epics_eq(name: str, ref: int):

    def do_check():
        val = epics.caget(name)
        print("check_epics_eq {0} == {1} got {2}".format(name, ref, val))
        if val is None:
            raise RuntimeError("Unable to retrieve {0}".format(name))
        if val != ref:
            raise RuntimeError("{0} is not equal to {1}".format(name, ref))

    return do_check

def check_epics_ne(name: str, ref: int):

    def do_check():
        val = epics.caget(name)
        print("check_epics_ne {0} != {1} got {2}".format(name, ref, val))
        if val is None:
            raise RuntimeError("Unable to retrieve {0}".format(name))
        if val == ref:
            raise RuntimeError("{0} is equal to {1}".format(name, ref))

    return do_check


integration.Sequence(
    [
        # integration.state.preserve_files,
        integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
        ioc.run,
        wait_for_ini,
        edcu.run,
        integration.wait(2),
        local_dc.run,
        integration.wait(2),
        daqd.run,
        integration.wait_tcp_server(8088, timeout=30),
        check_ok(30),
        check_epics_ge("X6:EDCU_CHAN_CONN", 1),
        check_epics_eq("X6:EDCU_CHAN_NOCON", 0),
        check_epics_ne("X6:EDCU_CHAN_CNT", 0),
        check_epics_ge("X6:EDCU_UPTIME_SECONDS", 1),
        check_epics_ne("X6:EDCU_GPS", 0),
        check_epics_ge("X6:EDCU_DATA_RATE_KB_PER_S", 1),
    ]
)
