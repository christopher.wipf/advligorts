#!/usr/bin/env python3

import sys
import configparser
import argparse
import logging
import collections
from io import StringIO

TP_CONFIG_PREFIX = "-node"

def build_section_name(site_letter, dcuid):
    return str(site_letter) + TP_CONFIG_PREFIX + str(dcuid)

def has_duplicate(name_to_config, new_system, new_config_name):

    # Collect all system names in dict
    system_names = {}
    for key, value in name_to_config.items():
        system_names[value['system']] = key

    if new_system in system_names and system_names[new_system] != new_config_name:
        return True
    return False


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-par_file", help = "The input testpoint.par file to build from", required=True)
    parser.add_argument("-gds_node", help = "The dcuid on the new system we are adding", required=True, type=int )
    parser.add_argument("-site_letter", help = "The lowercase first letter of the site, SITE=LHO -> L", required=True)
    parser.add_argument("-system", help = "The system name (model name)", required=True)
    parser.add_argument("-host", help = "The hostname of the machine the system will be run on", required=True)
    args = parser.parse_args()

    config = configparser.ConfigParser({}, collections.OrderedDict)
    config.read(args.par_file)

    name_to_config = {}

    for section in config.sections():
        dcuid = int(section.split(TP_CONFIG_PREFIX, 1)[1])
        hostname = config.get(section, 'hostname')
        system   = config.get(section, 'system') 
        site_letter = section[0]
        name_to_config[section] = {'hostname':hostname, 'system':system, 'site_letter':site_letter}


    new_system_sec_name = build_section_name(args.site_letter, args.gds_node)

    # Check for dupliacte dcuid
    if new_system_sec_name in name_to_config:

        #If this is not the same system/host
        if args.system != name_to_config[new_system_sec_name]['system'] or args.host != name_to_config[new_system_sec_name]['hostname']:
            print((
                    f"\nError: A node with the dcuid {args.gds_node} is already installed as:\n"
                    f"[{new_system_sec_name}]\n"
                    f"hostname={name_to_config[new_system_sec_name]['hostname']}\n"
                    f"system={name_to_config[new_system_sec_name]['system']}\n\n"
                    f"The new entry you are trying to write is as follows:\n"
                    f"[{new_system_sec_name}]\n"
                    f"hostname={args.host}\n"
                    f"system={args.system}\n\n"
                    f"This script will not overwrite existing entries in {args.par_file}\n"
                    f"If this is an attempt to move an existing system from one host to another,\n"
                    f"please remove conflicting entry from the testpoint.par file.\n"), file=sys.stderr)
            exit(1)

    # Check for duplicate system name
    if has_duplicate(name_to_config, args.system, new_system_sec_name) == True:
        print((f"\nError: A system with the name {args.system} is already installed in {args.par_file}\n"
                "Each system name must be unique. If this is an attempt to replace an existing system with a new system with the same name,\n"
                "please remove conflicting entry from the testpoint.par file and try again, otherwise you may need to rename your model.\n"), file=sys.stderr)
        exit(1)

    # Add the new system
    config[new_system_sec_name] =  {'hostname':args.host, 'system':args.system}

    # Order the sections alphabetically
    config._sections = collections.OrderedDict(sorted(config._sections.items(), key=lambda t: t[0] ))

    with StringIO() as ss:
        config.write(ss)
        ss.seek(0) # rewind
        for line in ss.getvalue().splitlines():
            if line != "":
                print(line.replace(" ", ""))






