package CDS::Parameters;
use Exporter;
use Env qw(RCG_HOST);
use Scalar::Util qw(looks_like_number);
@ISA = ('Exporter');


#//     \page Parameters Parameter.pm
#//     Documentation for Parameters.pm
#//
#// \n

# Print Epics communication structure into a header file
# Current part number is passed as first argument
sub partType {
	return Parameters;
}

sub parseParams {
        my ($i) = @_;
	my @sp = split(/\\n/, $i);
	#print "Model Parameters are $i;\n";
	#print "Split array is @sp\n";
	for (@sp) {
		@spp = split(/=/);
        # Find and convert params due for deprecation in later releases
		if (@spp == 2) {
            if ($spp[0] eq "site" ) {
                $::sitedepwarning = 1;
                $spp[0] = "ifo";
            }
            if($spp[0] eq "adcMaster") {
                $::adcmasterdepwarning = 1;
                $spp[0] = "iop_model";
            }
            if($spp[0] eq "time_master") {
                $::timemasterdepwarning = 1;
                $spp[0] = "dolphin_time_xmit";
            }
            for($spp[0])
            {
                if($_ eq "ifo")
                {
                    print "PARAM ifo set to $spp[1]\n";
                    $spp[1] =~ s/,/ /g;
                    $spp[1] = uc($spp[1]); #Force the IFO to be caps

                    #Make sure the ifo and filename match
                    if (lc($spp[1]) ne $::ifo_from_mdl_name) {
                        $errmsg = "***ERROR: Model cdsParameter ifo= does not match filename (first two characters): ";
                        $errmsg .=  "ifo=" . $spp[1] . ", filename[0:2]: ". $::ifo_from_mdl_name . "\n";
                        die $errmsg;
                    }
                    $::ifo = $spp[1];
                }
                elsif($_ eq "rate")
                {
                    print "PARAM Rate set to $spp[1]\n";
                    my $param_speed = $spp[1];

                    $good_model_rate = 0;
                    %supported_model_rates = ( '256' => 256, 
                                               '2048' => 2048, '4096' => 4096,
                                               '16384' => 16384, '32768' => 32768,
                                               '65536' => 65536, '131072' => 131072,
                                               '262144' => 262144, '524288' => 524288,
                                               '2K' => 2048, '4K' => 4096, 
                                               '16K' => 16384, '32K' => 32768, 
                                               '64K' => 65536, '128K' => 131072,
                                               '256K' => 262144, '512K' => 524288);

                    if( exists($supported_model_rates{$param_speed}) ) {
                        $::modelrateHz = $supported_model_rates{$param_speed};
                        $::perCycle_us = sprintf ("%.0f", (1/$::modelrateHz * 1000000.0));
                        $::fastAdcSampleRate_2power = sprintf "%.0f" , (log($::modelrateHz) / log(2)); 
                    }
                    else {
                        $errmsg = "\n************\n";
                        $errmsg .= "***ERROR: Invalid Parameter Block Entry: rate = $param_speed \n";
                        $errmsg .= "************\n\n";
                        die $errmsg;
                    }




                }
                elsif($_ eq "dcuid")
                {
				    print "PARAM Dcu Id is set to $spp[1]\n";
				    $::dcuId = $spp[1];
				    print "GDS node id is set to $::dcuId \n";
				    $::gdsNodeId = $::dcuId;
                }
                elsif($_ eq "host")
                {
				    print "PARAM Target host name is set to $spp[1]\n";
				    $::targetHost = $spp[1];
				    if ($RCG_HOST) {
					    $::targetHost = $RCG_HOST;
				    }
                }
                elsif($_ eq "plant_name")
                {
				    print "Plant name is set to $spp[1]\n";
				    $::plantName = $spp[1];
                }
                elsif($_ eq "daq_prefix")
                {
				    $::daq_prefix = $spp[1];
                }
                elsif($_ eq "no_sync")
                {
                    # This essentially set up IOP for a Cymac
				    print "Will not sync up to 1PPS\n";
				    $::no_sync = $spp[1];
                }
                elsif($_ eq "test1pps")
                {
                    # This forces sync to 1pps for testing
				    print "Force sync up to 1PPS\n";
				    $::test1pps = $spp[1];
                }
                elsif($_ eq "no_daq")
                {
                    # Will compile code not to use DAQ
				    print "Will not connect to DAQ\n";
				    $::no_daq = 1;
                }
                elsif($_ eq "enable_fir")
                {
                    # Will compile code to use FIR filtes
				    print "Will use FIR filters\n";
				    $::useFIRs = 1;
                }
                elsif($_ eq "no_oversampling")
                {
				    print "Will not oversample\n";
				    $::no_oversampling = 1;
                }
                elsif($_ eq "no_dac_interpolation")
                {
				    print "Will not interpolate DAC\n";
				    $::no_dac_interpolation = 1;
                }
                elsif($_ eq "specific_cpu")
                {
                }
                elsif($_ eq "iop_model")
                {
				    print "PARAM FE is IOP\n";
				    print "FE will run as IOP\n";
				    $::iopModel = $spp[1];
                }
                elsif($_ eq "diagTest")
                {
				    print "PARAM FE Compiles as DIAG TEST CODE\n";
				    $::diagTest = $spp[1];
                }
                elsif($_ eq "dacwdoverride")
                {
				    print "FE Compiles with override of bad DAC error\n";
				    $::dacWdOverride = $spp[1];
                }
                elsif($_ eq "dolphin_time_xmit")
                {
				    $::dolphin_time_xmit = $spp[1];
                }
                elsif($_ eq "dolphin_time_rcvr")
                {
				    $::dolphinTiming = $spp[1];
                }
                elsif($_ eq "no_cpu_shutdown")
                {
				    $::no_cpu_shutdown = $spp[1];
                }
                elsif($_ eq "pciRfm")
                {
				    print "PARAM FE will run with PCIE RFM Network\n";
				    $::pciNet = $spp[1];
                }
                elsif($_ eq "dolphingen")
                {
				    print "PARAM FE will run with PCIE RFM Network\n";
                    # Set Dolphin Gen to run with; default=2
				    $::dolphinGen = $spp[1];
                }
                elsif($_ eq "remoteGPS")
                {
				    print "FE will run with EPICS for GPS Time\n";
				    $::remoteGPS = $spp[1];
                }
                elsif($_ eq "rfm_delay")
                {
                    if ( not looks_like_number($spp[1]) or int($spp[1]) < 0 or int($spp[1]) > 1028)
                    {
                        die "The value passed to rfm_delay=$spp[1] must be an integer greator than or equal to 0 and less than 1028.\n";
                    }
                    
				    $::rfmDelay = int($spp[1]);
                }
                elsif($_ eq "flip_signals")
                {
				    $::flipSignals = $spp[1];
                }
                elsif($_ eq "sdf")
                {
				    $::globalsdf = $spp[1];
                }
                elsif($_ eq "casdf")
                {
				    $::casdf = $spp[1];
                }
                elsif($_ eq "requireIOcnt")
                {
				    $::requireIOcnt = $spp[1];
                }

                elsif($_ eq "vectorization")
                {
                		    $::vectorization = $spp[1];
                }
                elsif($_ eq "noiseGeneratorSeed")
                {
                    $::noiseGeneratorSeed = $spp[1];
                }
                elsif($_ eq "gaussNoiseGeneratorSeed")
                {
                    $::gaussNoiseGeneratorSeed = $spp[1];
                }
                elsif($_ eq "virtualIOP")
                {
				    $::virtualiop = $spp[1];
                }
                elsif($_ eq "use_shm_ipc")
                {
				    $::force_shm_ipc = $spp[1];
                }
                elsif($_ eq "adcclock")
                {
				    $::adcclockHz = $spp[1] * 1024;
                }
                elsif($_ eq "clock_div")
                {
				    $::clock_div = $spp[1];
                }
                elsif($_ eq "sync")
                {
				    $::edcusync = $spp[1];
                }
                elsif($_ eq "bio_test")
                {
				    $::biotest = $spp[1];
                }
                elsif($_ eq "optimizeIO")
                {
				    $::optimizeIO = $spp[1];
                }
                elsif($_ eq "internalclk")
                {
				    $::internalclk = $spp[1];
                }
                elsif($_ eq "no_zero_pad")
                {
				    $::noZeroPad = $spp[1];
                }
                elsif($_ eq "lhomid")
                {
				    $::lhomid = $spp[1];
                }
                elsif($_ eq "ipc_rate")
                {
                    # Specify IPC rate if lower than model rate
				    $::ipcrateHz = $spp[1];
                }
                elsif($_ eq "time0_delay_us")
                {
                    $::time0_delay_us = $spp[1];
                }
                elsif($_ eq "userspacegps")
                {
                    $::userspacegps = 1;
                }
                elsif($_ eq "extra_includes" )
                {
                    $::extra_includes = $spp[1];
                }
                elsif($_ eq "extra_cflags" )
                {
                    $::extra_cflags = $spp[1];
                }
                elsif($_ eq "extra_usp_linkoptions" )
                {
                    $::extra_usp_linkoptions = $spp[1];
                }
                # Following are old options that are no longer required
                elsif($_ eq "biquad")
                {
				    $nolongerused = 1;
                }
                elsif($_ eq "adcSlave")
                {
				    $nolongerused = 2;
                }
                elsif($_ eq "accum_overflow")
                {
				    $nolongerused = 2;
                }
                elsif($_ eq "shmem_daq")
                {
				    $nolongerused = 2;
                }
                elsif($_ eq "rfm_dma")
                {
				    $nolongerused = 2;
                }
			    else {
                $errmsg = "***ERROR: Unknown Parameter Block Entry: ";
                $errmsg .=  $spp[0] . "\n";
                die $errmsg;
                }
		    }
        }
	}
    # Check that all required Parameter block entries have been set
    if($::targetHost eq "dummy")
    {
        $errmsg = "\n************\n";
        $errmsg .= "***ERROR: Missing Required Parameter Block Entry: host\n";
        $errmsg .= "********: Please add host=targetname, where: \n";
        $errmsg .= "********: \ttargetname is name of computer on which code will run\n";
        $errmsg .= "************\n\n";
        die $errmsg;
    }
    if($::dcuId eq 0)
    {
        $errmsg = "\n************\n";
        $errmsg .= "***ERROR: Missing Required Parameter Block Entry: dcuid\n";
        $errmsg .= "************\n\n";
        die $errmsg;
    }

    # For model rates faster than 65536, the ipcrate must be passed in as a parameter
    # as 65536 is the fastest IPC rate we currently support
    if ($::modelrateHz > 65536 && $::ipcrateHz <= 0) {
        die "ERROR: IPCx : Models with a rate higher than 64K must have the ipc_rate=... parameter set in the cdsParameters block in the model file. Terminating.";
    }

    #Make sure IPC rate is lower or 

    # Check that the IPC rate is valid
    if ( $::ipcrateHz > 0 )
    {
        if ( $::ipcrateHz > $::modelrateHz )
        {
            die "ERROR: ipc_rate : The given ipc_rate (", $::ipcrateHz , ") must be less than or equal to the model rate ", $::modelrateHz;
        }
    }
    else
    {
	# by default set $ipcrateHz to model rate in units of cycles per second
    	$::ipcrateHz = $::modelrateHz;
    }

    $good_ipc_rate = 0;
    @supported_ipc_rates = ( 256, 2048, 4096, 8192, 16384, 32768, 65536 );

    for( @supported_ipc_rates ) {
	if( $::ipcrateHz eq $_ ) {
	    $good_ipc_rate = 1;
	    last;
	}
    }
    if ( ! $good_ipc_rate ) {
	die "ERROR: ipc_rate : The ipc_rate cdsParameter must be specified in full power of two form. Accepted rates: 2048, 4096, 16384, 32768, 65536";
    }
}

sub printHeaderStruct {
}

# Print Epics variable definitions
# Current part number is passed as first argument
sub printEpics {
        my ($i) = @_;
}


# Print variable declarations int front-end file
# Current part number is passed as first argument
sub printFrontEndVars  {
        my ($i) = @_;
}

# Check inputs are connected
sub checkInputConnect {
        my ($i) = @_;
				    print "PARAM doing connect check\n";
        return "";
}

# Figure out part input code
# Argument 1 is the part number
# Argument 2 is the input number
# Returns calculated input code
sub fromExp {
        my ($i, $j) = @_;
        return "";
}

# Return front end initialization code
# Argument 1 is the part number
# Returns calculated code string
sub frontEndInitCode {
	my ($i) = @_;
	return "";
}


# Return front end code
# Argument 1 is the part number
# Returns calculated code string
sub frontEndCode {
	my ($i) = @_;
	return "";
}
