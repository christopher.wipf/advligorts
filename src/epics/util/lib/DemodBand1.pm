package CDS::DemodBand1;
use Exporter;
@ISA = ('Exporter');

$bands = 1;

# I and Q outputs for (n-1) bands + a 0 band and a nyquist band
$num_outs = $bands * 2;

# size of history needed
# 3 second order sections
# two values per section
# two entries per band
$num_hist = 3*2*2*$bands;

sub partType {
	return DemodBand1;
}

# Print Epics communication structure into a header file
# Current part number is passed as first argument
sub printHeaderStruct {
	my ($i) = @_;
}

# Print Epics variable definitions
# Current part number is passed as first argument
sub printEpics {
        my ($i) = @_;
}

# Print variable declarations int front-end file
# Current part number is passed as first argument
sub printFrontEndVars  {
        my ($i) = @_;
        print ::OUT "double \L$::xpartName[$i]_in;\n";
        print ::OUT "double \L$::xpartName[$i]_out\[$num_outs\];\n";
        print ::OUT "double \L$::xpartName[$i]_hist\[$num_hist\];\n";
}

# Check inputs are connected
sub checkInputConnect {
        my ($i) = @_;
        return "";
}


# Return front end initialization code
# Argument 1 is the part number
# Returns calculated code string
sub frontEndInitCode {
        my ($i) = @_;
        return "for (int i=0;i<$num_hist;++i) \L$::xpartName[$i]_hist\[i\]=0.0;\n";
}

# Figure out part input code
# Argument 1 is the part number
# Argument 2 is the input number
# Returns calculated input code
sub fromExp {
        my ($i, $j) = @_;
        my $from = $::partInNum[$i][$j];
        my $fromPort = $::partInputPort[$i][$j];
        return "\L$::xpartName[$from]_out" . "\[" . $fromPort . "\]";
}

# Return front end code
# Argument 1 is the part number
# Returns calculated code string

sub frontEndCode {
	my ($i) = @_;

	my $name = "\L$::xpartName[$i]";
    my $calcExp = "// DemodBand:  $::xpartName[$i]\n";

    $calcExp .= $name . "_in = $::fromExp[0];\n";

	$calcExp .= "demodulation_decimation_band1_section3($name" . "_in, ";
	$calcExp .= $name . "_out, feCoeff8x, $name" . "_hist, cycle);\n";
        return $calcExp;
}
