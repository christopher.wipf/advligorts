package CDS::USP_Cmake_Utils;
use Exporter;
@ISA = ('Exporter');

#// \b sub \b createUSP_CmakeFiles \n
#// Move the standard userspace cmake file into the build dir
#// and generate UserspaceVars.cmake build env file.  \n\n
sub createUSP_CmakeFiles{
my ($makefileDir) = @_;

    system("/bin/cp Userspace_CMakeLists.cmake  $::modelCodeUspDir/CMakeLists.txt");


    open(OUTM,">".$makefileDir."/UserspaceVars.cmake") || die "cannot open UserspaceVars.txt file for writing";

    print OUTM "# User Space Linux\n";
    if ($::iopModel > -1) {
        print OUTM "set(IOP_MODEL YES)\n";
    }
    if($::userspacegps)
    {
        print OUTM "set(BUILD_USP_GPSCLOCK YES)\n";
    }
    if ($::dolphinTiming > -1 or $::virtualiop == 2) {
        print OUTM "set(USE_DOLPHIN_TIMING YES)\n";
    }
    if ($::dolphin_time_xmit > -1) {
        if ( $::pciNet < 0 )
        {
            die "Error: Model is configured to transmit dolphin time (dolphin_time_xmit) but is not configured to be a dolphin node (pciRfm).";
        }
        print OUTM "set(XMIT_DOLPHIN_TIME YES)\n";
    }

    print OUTM "list(APPEND CFLAGS \"-O2 -fPIC\")\n";
    print OUTM "list(APPEND CFLAGS \"$::servoflag\")\n";

    if ($::iopModel > -1) {  #************ SETUP FOR IOP ***************
        #Following used for IOP running at 128K 
        if($::adcclockHz == 131072) {
            print OUTM "list(APPEND CFLAGS \"-DIOP_IO_RATE=131072\")\n";
        } else {
            print OUTM "list(APPEND CFLAGS \"-DIOP_IO_RATE=65536\")\n";
        }
        # Invoked if IOP cycle rate slower than ADC clock rate
        print OUTM "list(APPEND CFLAGS \"-DUNDERSAMPLE=$::clock_div\")\n";
        $adccopyrate = $::modelrateHz / $::adcclockHz;
        print OUTM "list(APPEND CFLAGS \"-DADC_MEMCPY_RATE=$adccopyrate\")\n";
        print OUTM "list(APPEND CFLAGS \"-DSLOW_ADC_SAMPLE_RATE_POW_2=16\")\n";
        print OUTM "list(APPEND CFLAGS \"-DFAST_ADC_SAMPLE_RATE_POW_2=$::fastAdcSampleRate_2power\")\n";


        #Non-zero in cases where there is a large delay in querying the time
        print OUTM "list(APPEND CFLAGS \"-DTIME0_DELAY_US=$::time0_delay_us\")\n";

    } 

    print OUTM "list(APPEND CFLAGS \"-D";
    print OUTM "\U$::skeleton";
    print OUTM "_CODE\")\n";

    print OUTM "list(APPEND CFLAGS \"-DFE_SRC=\\\\\\\"\L$::skeleton/\L$::skeleton.c\\\\\\\"\")\n";
    print OUTM "list(APPEND CFLAGS \"-DFE_HEADER=\\\\\\\"\L$::skeleton.h\\\\\\\"\")\n";

    print OUTM "list(APPEND CFLAGS \"-g\")\n";

    # set vectorization level
    if($::vectorization eq "avx512")
    {
        print "vectorization set to avx512\n";
        print OUTM "list(APPEND CFLAGS \" -march=native -mavx512f -mavx512bw -mavx512cd -mavx512dq -mfma \") \n";
    }
    elsif($::vectorization eq "avx2")
    {
        print "vectorization set to avx2\n";
        print OUTM "list(APPEND CFLAGS \" -march=native -mavx2 -mfma -DNO_AVX512\") \n";
    }
    elsif($::vectorization eq "sse3")
    {
        print "vectorization set to sse3\n";
        print OUTM "list(APPEND CFLAGS \" -march=native -msse3 -mfma -DNO_AVX512 -DNO_AVX2\") \n";
    }
    elsif($::vectorization eq "none")
    {
        print "no vectorization used\n";
        #don't add anything if no vectorization
    }
    else
    {
        die "Unknown vectorization level: $::vectorization\n";
    }



    if ($::remoteGPS) {
        print OUTM "list(APPEND CFLAGS \"-DREMOTE_GPS\")\n";
    }
    if($::useFIRs)
    {
        print OUTM "list(APPEND CFLAGS \"-DFIR_FILTERS\")\n";
    }
    if ($::no_sync) {
        print OUTM "#Comment out to enable 1PPS synchronization\n";
        print OUTM "list(APPEND CFLAGS \"-DNO_SYNC\")\n";
    } else {
        print OUTM "#Uncomment to disable 1PPS signal sinchronization (channel 31 (last), ADC 0)\n";
        print OUTM "#list(APPEND CFLAGS \"-DNO_SYNC\")\n";
    }
    if (0 == $::dac_testpoint_names && 0 == $::extraTestPoints && 0 == $::filtCnt) {
        print "Not compiling DAQ into the front-end\n";
        $::no_daq = 1;
    }
    if ($::no_daq) {
        print OUTM "#Comment out to enable DAQ\n";
        print OUTM "list(APPEND CFLAGS \"-DNO_DAQ\")\n";
    }

    # Use oversampling code if not 64K system
    #if($::modelrateHz < 65536) {
    #  if ($::no_oversampling) {
    #    print OUTM "#Uncomment to oversample A/D inputs\n";
    #    print OUTM "#CFLAGS += -DOVERSAMPLE\n";
    #    print OUTM "#Uncomment to interpolate D/A outputs\n";
    #    print OUTM "#CFLAGS += -DOVERSAMPLE_DAC\n";
    #  } else {
    #    print OUTM "#Comment out to stop A/D oversampling\n";
    #    print OUTM "CFLAGS += -DOVERSAMPLE\n";
    #    if ($::no_dac_interpolation) {
    #    } else {
    #      print OUTM "#Comment out to stop interpolating D/A outputs\n";
    #      print OUTM "CFLAGS += -DOVERSAMPLE_DAC\n";
    #    }
    #  }
    #}

    if ($::iopModel < 1) {
        print OUTM "list(APPEND CFLAGS \"-DUNDERSAMPLE=1\")\n";
        print OUTM "list(APPEND CFLAGS \"-DADC_MEMCPY_RATE=1\")\n";
    }

    print OUTM "list(APPEND CFLAGS \"-DMODEL_RATE_HZ=$::modelrateHz\")\n";

    #Following used with IOP running at 64K (NORMAL)
    if($::adcclockHz >= 65536) {

        #All user models currently view the IOP as running at 65536 even when they are faster
        print OUTM "list(APPEND CFLAGS \"-DIOP_IO_RATE=65536\")\n";


        if($::modelrateHz < 65536) {
            my $drate = 65536/$::modelrateHz;
            if($drate == 8 ) {
                die "RCG does not support a user model rate $::modelrateHz" . " Hz with IOP data at $::adcclockHz" ." Hz\n"  ;
            }
            print OUTM "list(APPEND CFLAGS \"-DOVERSAMPLE\")\n";
            print OUTM "list(APPEND CFLAGS \"-DOVERSAMPLE_DAC\")\n";
            print OUTM "list(APPEND CFLAGS \"-DOVERSAMPLE_TIMES=$drate\")\n";
            print OUTM "list(APPEND CFLAGS \"-DFE_OVERSAMPLE_COEFF=feCoeff$drate"."x\")\n";
            print OUTM "list(APPEND CFLAGS \"-DADC_MEMCPY_RATE=1\")\n";
        }
    }
    else
    {
        die "Unsupported ADC clock rate: ". $::adcclockHz . "\n";
    }

    print OUTM "list(APPEND CFLAGS \"-DUNDERSAMPLE=1\")\n";

    if ($::iopModel > -1) {
        $::modelType = "IOP";
        if($::diagTest > -1) {
            print OUTM "list(APPEND CFLAGS \"-DDIAG_TEST\")\n";
        }
        if($::dacWdOverride > -1) {
            print OUTM "list(APPEND CFLAGS \"-DDAC_WD_OVERRIDE\")\n";
        }
        # ADD DAC_AUTOCAL to IOPs
        print OUTM "list(APPEND CFLAGS \"-DDAC_AUTOCAL\")\n";
    } else {
        print OUTM "#Uncomment to run on an I/O Master \n";
        print OUTM "#list(APPEND CFLAGS \"-DIOP_MODEL\")\n";
    }
    if ($::iopModel < 1) {
        print OUTM "list(APPEND CFLAGS \"-DCONTROL_MODEL\")\n";
        $::modelType = "CONTROL";
    } 
    if ($::dolphin_time_xmit > -1) {
        if ( $::pciNet < 0 )
        {
            die "Error: Model is configured to transmit dolphin time (dolphin_time_xmit) but is not configured to be a dolphin node (pciRfm)."
        }
        print OUTM "list(APPEND CFLAGS \"-DXMIT_DOLPHIN_TIME=1\")\n";
    } 
    if ($::dolphinTiming > -1) {
        print OUTM "list(APPEND CFLAGS \"-DUSE_DOLPHIN_TIMING=1\")\n";
    } 
    if ($::flipSignals) {
        print OUTM "list(APPEND CFLAGS \"-DFLIP_SIGNALS=1\")\n";
    }

    if ($::virtualiop != 1) {
        if ($::pciNet > 0) {
            print OUTM "#Enable use of PCIe RFM Network Gen 2\n";
            print OUTM "set(DOLPHIN_PATH \"/opt/srcdis\")\n";
            print OUTM "list(APPEND CFLAGS \"-DHAVE_CONFIG_H -DOS_IS_LINUX=196616 -DLINUX -DUNIX " .
            "-DLITTLE_ENDIAN -DDIS_LITTLE_ENDIAN -DCPU_WORD_IS_64_BIT -DCPU_ADDR_IS_64_BIT ".
            "-DCPU_WORD_SIZE=64 -DCPU_ADDR_SIZE=64 -DCPU_ARCH_IS_X86_64 -DADAPTER_IS_IX ".
            "-m64 -D_REENTRANT\")\n"; 
        }
    }

    # Set BIQUAD as default starting RCG V2.8
    print OUTM "#Comment out to go back to old iir_filter calculation form\n";
    print OUTM "list(APPEND CFLAGS \"-DALL_BIQUAD=1 -DCORE_BIQUAD=1\")\n";

    if ($::directDacWrite) {
        print OUTM "list(APPEND CFLAGS \"DDIRECT_DAC_WRITE=1\")\n";
    } else {
        print OUTM "#list(APPEND CFLAGS \"DDIRECT_DAC_WRITE=1\")\n";
    }

    if ($::noZeroPad) {
        print OUTM "CFLAGS += -DNO_ZERO_PAD=1\n";
        print OUTM "list(APPEND CFLAGS \"-DNO_ZERO_PAD=1\")\n";
    } else {
        print OUTM "#CFLAGS += -DNO_ZERO_PAD=1\n";
        print OUTM "#list(APPEND CFLAGS \"-DNO_ZERO_PAD=1\")\n";
    }

    if ($::optimizeIO) {
        print OUTM "list(APPEND CFLAGS \"-DNO_DAC_PRELOAD=1\")\n";
    } else {
        print OUTM "#list(APPEND CFLAGS \"-DNO_DAC_PRELOAD=1\")\n";
    }

    if ($::rfmDelay >= 0) {
        print OUTM "list(APPEND CFLAGS \"-DRFM_DELAY=$::rfmDelay\")\n";
    }

    print OUTM "list(APPEND CFLAGS \"-DUSER_SPACE=1 -fno-builtin-sincos \")\n";

    if ($::userspacegps) {
        print OUTM "list(APPEND CFLAGS \"-DUSE_GPSCLOCK\")\n";
    }

    print OUTM "set(ENV{LD_LIBRARY_PATH} \"\$ENV{LD_LIBRARY_PATH}:/opt/DIS/lib64 \")\n";
    print OUTM "set(API_LIB_PATH \"/opt/DIS/lib64\")\n";
    print OUTM "\n\n";

    print OUTM "\n";
    print OUTM "\n";
    print OUTM "\n";

    #
    # Start Include Dirs
    if ($::virtualiop != 1) {
        if ($::pciNet > 0) {
            print OUTM "list(APPEND INC_DIRS \"\${DOLPHIN_PATH}/src/include/dis \${DOLPHIN_PATH}/src/include ".
            "\${DOLPHIN_PATH}/src/SISCI/cmd/test/lib \${DOLPHIN_PATH}/src/SISCI/src \${DOLPHIN_PATH}/src/SISCI/api ".
            "\${DOLPHIN_PATH}/src/SISCI/cmd/include \${DOLPHIN_PATH}/src/IRM_GX/drv/src ".
            "\${DOLPHIN_PATH}/src/IRM_GX/drv/src/LINUX \")\n";
        }
    }

    print OUTM "list(APPEND INC_DIRS \"\${PROJECT_SOURCE_DIR}/../../../models/" . "\L$::skeleton" ."/include\"  \"$::rcg_src_dir\/src/include\")\n";

    if ($::extra_cflags) {
        print OUTM "# Extra CFLAGS from cdsParameters in the model:\n";
        print OUTM "list(APPEND CFLAGS \"$::extra_cflags\")\n";
    }

    # add in demodulation includes
    print OUTM "list(APPEND INC_DIRS \"$::rcg_src_dir/src/fe/demod\")\n";

    if ($::pciNet > 0 && $::virtualiop != 1) {
        print OUTM "list(APPEND LDFLAGS \"-L \${API_LIB_PATH} -lsisci \")\n";
    } else {
        print OUTM "list(APPEND LDFLAGS \"-L \${API_LIB_PATH}\")\n";
    }

    if ($::extra_usp_linkoptions) {
        print OUTM "list(APPEND LDFLAGS \"$::extra_usp_linkoptions\")\n";
    }


    print OUTM "\n";
    close OUTM;
}
