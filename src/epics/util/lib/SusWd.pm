package CDS::SusWd;
use Exporter;
@ISA = ('Exporter');

sub partType {
	return SusWd;
}

# Print Epics communication structure into a header file
# Current part number is passed as first argument
sub printHeaderStruct {
    die "printHeaderStruct called in part with EpicsVariable interface.";
}

# Print Epics variable definitions
# Current part number is passed as first argument
sub printEpics {
    die "printEpics called in part with EpicsVariable interface.";
}

# return an array of EpicsVariable objects
# this function should be idempotent: calling it over and over should give the same
# result and have no side effects.
sub getEpicsVariables {
    my ($i) = @_;

    @epics_vars = ();


    push @epics_vars, CDS::EpicsVariable::new("$::xpartName[$i]", "$::xpartName[$i]", "int", "ao");
    push @epics_vars, CDS::EpicsVariable::new("$::xpartName[$i]\_MAX", "$::xpartName[$i]\_MAX", "int", "ai", 1, 0, {"PREC" => 0});

    @list_of_epics = ("$::xpartName[$i]\_VAR_M0_F1", "$::xpartName[$i]\_VAR_M0_F2", "$::xpartName[$i]\_VAR_M0_F3",
                                               "$::xpartName[$i]\_VAR_M0_L", "$::xpartName[$i]\_VAR_M0_R", "$::xpartName[$i]\_VAR_M0_S",
                                               "$::xpartName[$i]\_VAR_R0_F1", "$::xpartName[$i]\_VAR_R0_F2", "$::xpartName[$i]\_VAR_R0_F3",
                                               "$::xpartName[$i]\_VAR_R0_L", "$::xpartName[$i]\_VAR_R0_R", "$::xpartName[$i]\_VAR_R0_S",
                                               "$::xpartName[$i]\_VAR_L1_UL", "$::xpartName[$i]\_VAR_L1_LL", "$::xpartName[$i]\_VAR_L1_UR",
                                               "$::xpartName[$i]\_VAR_L1_LR", "$::xpartName[$i]\_VAR_L2_UL", "$::xpartName[$i]\_VAR_L2_LL",
                                               "$::xpartName[$i]\_VAR_L2_UR", "$::xpartName[$i]\_VAR_L2_LR");

    push @epics_vars, CDS::EpicsVariable::new(\@list_of_epics,
                                               "$::xpartName[$i]\_VAR", "double", "ao", 0, 0, {"PREC" => 1});

    return @epics_vars;
}


# Print variable declarations int front-end file
# Current part number is passed as first argument
sub printFrontEndVars  {
    my ($i) = @_;
    print ::OUT "double \L$::xpartName[$i];\n";
    print ::OUT "static double \L$::xpartName[$i]\_avg\[20\];\n";
    print ::OUT "static double \L$::xpartName[$i]\_var\[20\];\n";
    print ::OUT "double vabs;\n";
}

# Check inputs are connected
sub checkInputConnect {
    my ($i) = @_;
    return "";
}

# Return front end initialization code
# Argument 1 is the part number
# Returns calculated code string
sub frontEndInitCode {
    my ($i) = @_;
    return "for\(ii=0;ii<20;ii++\) {\n"
       . "\t\L$::xpartName[$i]\_avg\[ii\] = 0.0;\n"
       . "\t\L$::xpartName[$i]\_var\[ii\] = 0.0;\n"
       . "}\n";
}

# Figure out part input code
# Argument 1 is the part number
# Argument 2 is the input number
# Returns calculated input code
sub fromExp {
    my ($i, $j) = @_;
    return "";
}

# Return front end code
# Argument 1 is the part number
# Returns calculated code string

sub frontEndCode {
    my ($mm) = @_;
    my $calcExp = "// SusWd Module\n";
    $calcExp .= "if((cycle \% 16) == 0) {\n";
    $calcExp .= "\L$::xpartName[$mm] = 16384;\n";
    $calcExp .= "   for\(ii=0;ii<20;ii++\) {\n";
    $calcExp .= "\tint jj;\n";
    $calcExp .= "\tif\(ii\<16\) jj = ii;\n";
    $calcExp .= "\telse jj = ii+2;\n";
    $calcExp .= "\t\L$::xpartName[$mm]\_avg\[ii\]";
    $calcExp .= " = dWord\[0\]\[jj\] * \.00005 + ";
    $calcExp .= "\L$::xpartName[$mm]\_avg\[ii\] * 0\.99995;\n";
    $calcExp .= "\tvabs = dWord\[0\]\[jj\] - \L$::xpartName[$mm]\_avg\[ii\];\n";
    $calcExp .= "\tif\(vabs < 0) vabs *= -1.0;\n";
    $calcExp .= "\t\L$::xpartName[$mm]\_var\[ii\] = vabs * \.00005 + ";
    $calcExp .= "\L$::xpartName[$mm]\_var\[ii\] * 0\.99995;\n";
    $calcExp .= "\tpLocalEpics->$::systemName\.";
    $calcExp .= $::xpartName[$mm];
    $calcExp .= "_VAR\[ii\] = ";
    $calcExp .= "\L$::xpartName[$mm]\_var\[ii\];\n";

    $calcExp .= "\tif(\L$::xpartName[$mm]\_var\[ii\] \> ";
    $calcExp .= "pLocalEpics->$::systemName\.";
    $calcExp .= $::xpartName[$mm];
    $calcExp .= "_MAX\) ";
    $calcExp .= "\L$::xpartName[$mm] = 0;\n";
    $calcExp .= "   }\n";
    $calcExp .= "\tpLocalEpics->$::systemName\.";
    $calcExp .= $::xpartName[$mm];
    $calcExp .= " = \L$::xpartName[$mm] \/ 16384;\n";
    $calcExp .= "}\n";

    return $calcExp;
}
