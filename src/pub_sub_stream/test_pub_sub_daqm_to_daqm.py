import os
import os.path
import time
import typing
import integration

integration.Executable(name="cds_pub_sub", hints=[], description="cds_pub_sub")
integration.Executable(name="fe_stream_check", hints=["../fe_stream_test",], description="verification program")
integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")

ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")
mod5ini = os.path.join(ini_dir, "mod5.ini")
mod5par = os.path.join(ini_dir, "tpchn_mod5.par")
mod6ini = os.path.join(ini_dir, "mod6.ini")
mod6par = os.path.join(ini_dir, "tpchn_mod6.par")
mod255ini = os.path.join(ini_dir, "mod255.ini")
mod255par = os.path.join(ini_dir, "tpchn_mod255.par")

env = os.environ.copy()
env["GDS_TP_DIR"] = ini_dir

fe_simulated_streams = integration.Process("fe_simulated_streams",
                                           [
                                               "-b", "local_dc",
                                               "-m", "100",
                                               "-i", ini_dir,
                                               "-M", master_file,
                                               "-k", "2048",
                                               "-D", "5,6,255"
                                           ])

pub_sub = integration.Process("cds_pub_sub",
                              [
                                  "-i", "daqm://local_dc:100",
                                  "-o", "daqm://pub_sub_test:100"
                              ], env = env)

fe_stream_check = integration.Process("fe_stream_check",
                                      [
                                          "-m", "pub_sub_test",
                                          "-s", "100",
                                          "-v",
                                          "-c", mod5ini, mod5par,
                                          "-c", mod6ini, mod6par,
                                          "-c", mod255ini, mod255par
                                      ])
#functions...

def check_ok(timeout:float):
    end_time = time.time() + timeout
    def do_check():
        global fe_stream_check
        while True:
            state = fe_stream_check.state()
            if state == integration.Process.STOPPED:
                fe_stream_check.ignore()
                
                print("the check finished in a stopped state")
                return
            elif state == integration.Process.FAILED:
                raise RuntimeError('Data check failed')
            print("state is {0}".format(state))

            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for nds check to complete')
            time.sleep(0.5)

    return do_check


def echo(msg):
    def wrapper():
        print(msg)
    return wrapper




integration.Sequence(
    [
        integration.state.preserve_files,
        integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
        fe_simulated_streams.run,
        integration.wait(),
        pub_sub.run,
        integration.wait(.5),
        #integration.wait_tcp_server(hostname="daqm://pub_sub_test", port=100),
        echo("pub_sub running"),
        fe_stream_check.run,
        check_ok(20)



    ]
)