import os
import os.path
import time
import typing
import integration

#
#                      Not Working. can't create files in Shared memory with the intergration modue yet
#

integration.Executable(name="cds_pub_sub", hints=[], description="cds_pub_sub")
integration.Executable(name="cps_recv", hints=[], description="cps_recv")
integration.Executable(name="fe_stream_check", hints=["../fe_stream_test",], description="verification program")
integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")

ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")
mod5ini = os.path.join(ini_dir, "mod5.ini")
mod5par = os.path.join(ini_dir, "tpchn_mod5.par")
mod6ini = os.path.join(ini_dir, "mod6.ini")
mod6par = os.path.join(ini_dir, "tpchn_mod6.par")
mod255ini = os.path.join(ini_dir, "mod255.ini")
mod255par = os.path.join(ini_dir, "tpchn_mod255.par")

env = os.environ.copy()
env["GDS_TP_DIR"] = ini_dir


fe_simulated_streams = integration.Process("fe_simulated_streams",
                                           [
                                               "-S",
                                               "-i", ini_dir,
                                               "-M", master_file,
                                               "-k", "2048",
                                               "-D", "5,6,255",
                                               "-s", "shm://"
                                           ])

pub_sub = integration.Process("cds_pub_sub",
                              [
                                  "-i", "rmipc://shm://mod5,shm://mod6,shm://mod255",
                                  "-o", "tcp://127.0.0.1:9000"
                              ], env = env)

sub_recv = integration.Process("cps_recv",
                               [
                                   "-b", "shm://sub_recv",
                                    "-m", "100",
                                   "-s", "tcp://127.0.0.1:9000"
                               ])

fe_stream_check = integration.Process("fe_stream_check",
                                      [
                                          "-m", "shm://sub_recv",
                                          "-s", "100",
                                          "-v",
                                          "-c", mod5ini, mod5par,
                                          "-c", mod6ini, mod6par,
                                          "-c", mod255ini, mod255par
                                      ])



#functions...

def check_ok(timeout:float):
    end_time = time.time() + timeout
    def do_check():
        global fe_stream_check
        while True:
            state = fe_stream_check.state()
            if state == integration.Process.STOPPED:
                fe_stream_check.ignore()
                print("the check finished in a stopped state")
                return
            elif state == integration.Process.FAILED:
                raise RuntimeError('Data check failed')
            print("state is {0}".format(state))

            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for nds check to complete')
            time.sleep(0.5)

    return do_check


def echo(msg):
    def wrapper():
        print(msg)
    return wrapper






integration.Sequence(
    [
        integration.state.preserve_files,
        integration.require_readable(files=["/dev/gpstime"], description="required device files"),
        fe_simulated_streams.run,
        integration.wait(),
        pub_sub.run,
        integration.wait_tcp_server(port=9000, timeout=20),
        echo("pub_sub running"),
        integration.wait(5),
        sub_recv.run,
        integration.wait_tcp_server(port=9000, timeout=20),
        echo("sub_recv is running"),
        integration.wait(5),
        fe_stream_check.run,
        check_ok(20)








    ]
)

