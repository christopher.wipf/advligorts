import os
import os.path
import time
import typing
import integration

integration.Executable(name="cps_xmit", hints=["./cps_xmit"], description="cps_xmit")
integration.Executable(name="cps_recv", hints=["./cps_recv"], description="cps_recv")
integration.Executable(name="local_dc", hints=["../local_dc",], description="local_dc")
integration.Executable(name="fe_stream_check", hints=["../fe_stream_test",], description="verification program")
integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")

ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")


mod5ini = os.path.join(ini_dir, "mod5.ini")
mod5par = os.path.join(ini_dir, "tpchn_mod5.par")
mod6ini = os.path.join(ini_dir, "mod6.ini")
mod6par = os.path.join(ini_dir, "tpchn_mod6.par")
mod255ini = os.path.join(ini_dir, "mod255.ini")
mod255par = os.path.join(ini_dir, "tpchn_mod255.par")


fe_simulated_streams = integration.Process("fe_simulated_streams",
                                           [
                                               "-S",
                                               "-i", ini_dir,
                                               "-M", master_file,
                                               "-k", "2048",
                                               "-D", "5,6,255"
                                           ])

local_dc = integration.Process("local_dc", [
                                            "-m", "140",
                                            "-s", "mod5 mod6 mod255",
                                            "-d", ini_dir,
                                            "-b", "local_dc"
                                            ])
cps_xmit = integration.Process("cps_xmit", [
                                               "-b", "local_dc",
                                                "-m", "140",
                                                "-p", "zstd://9|tcp://127.0.0.1:9000"
                                            ])
cps_recv = integration.Process("cps_recv", [
                                               "-b", "sub_recv",
                                                "-m", "140",
                                                "-s", "tcp://127.0.0.1:9000|zstd://"
                                            ])



fe_stream_check = integration.Process("fe_stream_check",
                                      [
                                          "-m", "sub_recv",
                                          "-s", "140",
                                          "-v",
                                          "-c", mod5ini, mod5par,
                                          "-c", mod6ini, mod6par,
                                          "-c", mod255ini, mod255par
                                      ])


def check_ok(timeout: float):
    end_time = time.time() + timeout

    def do_check():
        global fe_stream_check
        while True:
            state = fe_stream_check.state()
            if state == integration.Process.STOPPED:
                fe_stream_check.ignore()

                print("the check finished in a stopped state")
                return
            elif state == integration.Process.FAILED:
                raise RuntimeError('Data check failed')
            print("state is {0}".format(state))

            if time.time() > end_time:
                raise RuntimeError('Timed out while waiting for nds check to complete')
            time.sleep(0.5)

    return do_check


def echo(msg):
    def wrapper():
        print(msg)

    return wrapper


integration.Sequence(
    [
        integration.state.preserve_files,
        integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
        fe_simulated_streams.run,
        integration.wait(),
        echo("Simualted streams running"),
        local_dc.run,
        integration.wait(),
        echo("local dc running"),
        cps_xmit.run,
        integration.wait_tcp_server(port=9000, timeout=20),
        echo("cps_xmit running"),
        cps_recv.run,
        integration.wait_tcp_server(port=9000, timeout=20),
        echo("cps_recv running"),
        fe_stream_check.run,
        check_ok(20)


    ]
)



































