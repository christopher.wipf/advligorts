//
// Created by jonathan.hanks on 3/14/22.
//
#include "simple_epics.hh"
#include <atomic>
#include <csignal>

#include "fdManager.h"

std::atomic< bool > done{ false };

void
signal_handler( int dummy )
{
    done = true;
}

int
main( int argc, char* argv[] )
{
    using pvInt = simple_epics::pvIntAttributes;
    simple_epics::Server server{ };

    std::atomic< int >               int1val{ 0 };
    pvInt                            int1{ "int1",
                &int1val,
                std::make_pair( -1, 100 ),
                std::make_pair( -1, 100 ),
                simple_epics::PVMode::ReadWrite };
    std::atomic< double >            float1val{ 0.0 };
    simple_epics::pvDoubleAttributes float1{ "float1",
                                             &float1val,
                                             std::make_pair( -1, 100 ),
                                             std::make_pair( -1, 100 ),
                                             simple_epics::PVMode::ReadWrite };

    server.addPV( int1 );
    server.addPV( float1 );

    std::signal( SIGINT, signal_handler );
    server.update( );
    while ( !done )
    {
        if ( int1val == 42 )
        {
            done = true;
        }
        server.update( );
        fileDescriptorManager.process( 0. );
    }
    return 0;
}