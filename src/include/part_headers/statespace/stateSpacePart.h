#ifndef STATESPACEPART_H
#define STATESPACEPART_H

#include "part_headers/statespace/stateSpaceCtrl_ioctl.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct StateSpaceCtx_t
{
    unsigned in_len;
    unsigned out_len;
    unsigned state_len;

    //Stores the size of the data block allocation below, in bytes.
    unsigned buffer_sz; 

    //Start of memory block pointers, all pointers point to different 
    //offsets in the same data block.
    double *mem_block_ptr;
    double *state_vec;  //[state_len]
    double *state_vec_new;  //[state_len]

    //TODO: Make sure these dim are ordered 
    //for the iteration we do over them
    double *state_matrix; // [state_len][state_len]
    double *input_matrix; // [state_len][in_len]
    double *output_matrix;// [out_len][state_len]
    double *feedthrough_matrix; //Zero or [out_len][in_vec]
} StateSpaceCtx_t;

//
// Functions to be called during model setup, before isolation
//

/**
* @brief Initializes the global data structures for statespace parts
*        in the model.
* 
* @param num_parts The number of statespace parts the model has.
*
* @return 0 if the data structures were initialized successfully,
*        -1 if an error occurred.
*/
int globStateSpaceInit( unsigned num_parts );

/**
* @brief Frees all statespace part allocations.
*
* This should be called after the RT logic has been stopped,
* and before model removal, it will free all parts and temp
* allocations.        
*/
void globStateSpaceFree( void );

//
// Functions called in RT Code
//

/**
* @brief Initializes the specific part (by index), 
*        and gives it the passed name and num ins/outs.
*
* AUTO-GENERATED call: This function is generated once for each 
* statespace part in the model, and is inserted into the feCode() init call.
* 
* @param part_index The index of the statespace part to configure.
* @param name The name the part will have. (Used to configure by name)
* @param num_in The numer of inputs the part has.
* @param num_out The number of outputs the part has.
*
* @return 0 if the configuration was set successfully,
*        -1 if an error occurred.
*/
int globStateSpaceConfigPart( unsigned part_index, const char name [], unsigned num_in, unsigned num_out );


/**
* @brief Calculates the next timestep of data with the given inputs,
*        internal state, and producing outputs.
*
* AUTO-GENERATED call:
* 
* @param part_index The index of the part to process.
*
* @param reset If >0 the state vector is cleared (0ed out) 
*              before the cycle's calculation.
*
* @param input_vec A pointer to a double buffer where inputs will be read from.
*                  The buffer length must match the num_in parameter passed 
*                  to globStateSpaceConfigPart()
*
* @param out_vec   A pointer to a double buffer where outputs will be written to.
*                  The buffer length must match the num_out parameter passed 
*                  to globStateSpaceConfigPart()
* 
* @return none
*/
void calcStateSpace(unsigned part_index, int reset, double * input_vec, double * out_vec);

/**
* @brief Sets the identified part to the given state vector.
*
* @param part_index The index of the statespace part to reset.
* @param state_vec A pointer to double data to load on reset.
*
* @return none
*/
void setStateVecStateSpace(unsigned part_index, double * state_vec);

//
// IOCTL Servicing Functions
//

/**
* @brief Gets the name of the part with the given index.
*
* @param none
*
* @return The number of statespace parts in the model.
*/
unsigned getNumPartsStateSpace( void );

/**
* @brief Gets the current configuration of the part with the given index.
*
* @param part_index The index of the part that should be (re)configured. 
* @param input_vec_len The number of inputs (fixed).
* @param output_vec_len The number of outputs (fixed).
* @param state_vec_len The number of states
* @param initial_state_vec A ptr to a buffer of doubles (state_vec_len long)
*                          that will be used as the initial state for the part. 
* @param state_matrix A ptr to a buffer of doubles for the flattened (row-major) 
*                     state matrix. Length must be state_vec_len * state_vec_len.
* @param input_matrix A ptr to a buffer of doubles for the flattened (row-major) 
*                     input matrix. Length must be input_vec_len * state_vec_len.
* @param output_matrix A ptr to a buffer of doubles for the flattened (row-major) 
*                     output matrix. Length must be output_vec_len * state_vec_len.
* @param feedthrough_matrix A ptr to a buffer of doubles for the flattened (row-major) 
*                     feedthrough matrix. Length is input_vec_len * output_vec_len 
*                     or NULL if the feedthrough is 0 for this configuration.
*
* @return 0 if the new configuration was loaded, -1 if the lengths were wrong etc.
*/
int matrixConfigureStateSpace(unsigned part_index, 
                          unsigned input_vec_len, unsigned output_vec_len, unsigned state_vec_len,
                          const double * initial_state_vec,
                          const double * state_matrix, const double * input_matrix,  
                          const double * output_matrix, const double * feedthrough_matrix);


/**
* @brief Gets the name of the part with the given index.
*
* @param part_index The index of the part we want the name of. 
* @param name_ptr A pointer to the name of the part. This is for read only. 
*
* @return none
*/
void getPartNamePtrStateSpace( unsigned part_index, char ** name_ptr );

/**
* @brief Gets the current configuration of the part with the given index.
*
* @param part_index The index of the part that should be loaded. 
* @param config A pointer to a buffer to copy the configuration.
*
* @return none
*/
void getConfigOfPartStateSpace(unsigned part_index, struct stateSpace_part_config_t * config );

/**
* @brief Copies the part config and data block into the buffer pointed to
*        by fill_me.
*
* @param part_index The index of the statespace part to read.
* @param data_sz The size of the buffer pointed to by fill_me.
* @param fill_me A pointer to a buffer where the part config should be copied.
*
* @return -1 if data for part is not available
*/
int getConfigAndDataOfPartStateSpace(unsigned part_index, unsigned data_sz, struct stateSpace_part_config_t * fill_me);

/**
* @brief Loads the new configuration (now). 
*
* This is used by librts to load swap the pointers and load 
* the part config immediately.
*
* @param part_index The index of the part that should be loaded. 
*
* @return none
*/
void loadNewMatrixNowStateSpace( unsigned part_index );

/**
* @brief Checks if a new configuration is queued and waiting for the RT
*        code to load.
*
* Once a new configuration is loaded by matrixConfigureStateSpace(), 
* the new data is stored and the RT code is responsible to sawp out
* the pointers before its next cycle. This is used to signal, when 
* that swap is complete.
*
* @param none
*
* @return -1 if nothing is queued, the part index we are waiting on otherwise.
*/
int check_pending_config( void );



#ifdef __cplusplus
}
#endif


#endif //STATESPACEPART_H