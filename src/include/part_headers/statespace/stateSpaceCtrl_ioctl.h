#ifndef LIGO_STATE_SPACE_CTRL_IOCTL_H
#define LIGO_STATE_SPACE_CTRL_IOCTL_H


#ifdef __KERNEL__
#include <linux/types.h>
#include <linux/kernel.h>
#else
#include <stdint.h> //size_t
#include <stdio.h>
#endif


#define STATE_SPACE_IOCTL_V0 0xA0000000

#define STATE_SPACE_MAX_BUFFER (1<<23) //8 MB max for state-space matrix/etc data


#define STATE_SPACE_MAX_NAME_LEN 255
#define STATE_SPACE_ALLOC_NAME_LEN (STATE_SPACE_MAX_NAME_LEN+1)
#define STATE_SPACE_ERROR_MSG_LEN STATE_SPACE_ALLOC_NAME_LEN



#ifdef __cplusplus
extern "C" {
#endif

// Returns the number of stateSpace parts in the model
struct stateSpace_info_t {
    uint32_t num_parts;
};

struct stateSpace_get_name_req_t {
    uint32_t part_index; //Input param
    char part_name[STATE_SPACE_ALLOC_NAME_LEN]; //Returned result
};


struct stateSpace_part_config_t {
    uint32_t ioctl_version; 
    uint32_t part_index;
    uint32_t input_vec_len;
    uint32_t output_vec_len;
    uint32_t state_vec_len;
    double data_block[]; //Expected size based on above parameters
    //initial/current state vector [state_vec_len]
    //state_matrix                 [state_vec_len][state_vec_len]
    //input_matrix                 [state_vec_len][input_vec_len]
    //output_matrix                [output_vec_len][state_vec_len]
    //feedthrough_matrix           [output_vec_len][input_vec_len]
}; 

/**
* @brief Calculates the total size a stateSpace_part_config_t.data_block 
*        would be, with the given in/out/state lengths.
* 
* @param input_vec_len The number of inputs the part has.
* @param output_vec_len The number of outputs the part has.
* @param state_vec_len The number of states the part has.
*
* @return The total size (in bytes) of the data_block
*/
static inline unsigned calcDataBlockSzStateSpace( uint32_t input_vec_len, 
                                                  uint32_t output_vec_len, 
                                                  uint32_t state_vec_len)
{
    unsigned num_ele = state_vec_len + 
                       (state_vec_len * state_vec_len) +
                       (state_vec_len * input_vec_len) + 
                       (output_vec_len * state_vec_len) +
                       (output_vec_len * input_vec_len);

    return num_ele * sizeof(double);
}


/**
* @brief Verifies the length of the vectors/matrices for
*        the "SQUARE" configuration format
* 
* @param state_vec_len The number of states the part has.
* @param state_vec_actual_len The length of the state vector, should match state_vec_len
* @param state_mat_len The length of the flattened state matrix.
* @param input_vec_len The number of inputs the part has.
* @param input_mat_len The length of the flattened input matrix.
* @param output_vec_len The number of outputs the part has.
* @param output_mat_len The length of the flattened output matrix.
* @param feed_mat_len The length of the flattened feedthrough matrix.
* @param error_msg If NULL, error msg not set, otherwise expect 
*                  STATE_SPACE_ERROR_MSG_LEN bytes to be allocated.
*
* @return 1 if the lengths are as expected, 0 if one or more of the 
*         lengths are invalid for the configuration format
*/
static inline int verify_square_config_lengths( uint32_t state_vec_len, uint32_t state_vec_actual_len, 
                                                uint32_t state_mat_len,
                                                uint32_t input_vec_len, uint32_t input_mat_len,
                                                uint32_t output_vec_len, uint32_t output_mat_len,
                                                uint32_t feed_mat_len, char * error_msg)
{

    if( state_vec_len != state_vec_actual_len) {
        if( error_msg ) {
            snprintf(error_msg, STATE_SPACE_ERROR_MSG_LEN,
                     "The state vector should be %u, but actual is %u.", 
                     state_vec_len, state_vec_actual_len);
        }
        return 0;
    }

    if( state_mat_len != state_vec_len * state_vec_len) {
        if( error_msg ) {
            snprintf(error_msg, STATE_SPACE_ERROR_MSG_LEN,
                     "The state matrix size was %u, but we expected %u.", 
                     state_mat_len, state_vec_len * state_vec_len);
        }
        return 0;
    }

    if( input_mat_len != state_vec_len * input_vec_len) {
        if( error_msg ) {
            snprintf(error_msg, STATE_SPACE_ERROR_MSG_LEN,
                     "The input matrix size was %u, but we expected %u.", 
                     input_mat_len, state_vec_len * input_vec_len);
        }
        return 0;
    }

    if( output_mat_len != state_vec_len * output_vec_len) {
        if( error_msg ) {
            snprintf(error_msg, STATE_SPACE_ERROR_MSG_LEN,
                     "The output matrix size was %u, but we expected %u.", 
                     output_mat_len, state_vec_len * output_vec_len);
        }
        return 0;
    }

    if( (feed_mat_len != input_vec_len * output_vec_len) && feed_mat_len != 0 )
    {
        if( error_msg ) {
            snprintf(error_msg, STATE_SPACE_ERROR_MSG_LEN,
                     "The feedthrough matrix size was %u, but we expected %u or 0", 
                     feed_mat_len, input_vec_len * output_vec_len);
        }
        return 0;
    }

    return 1;
}


//
// Define IOCTL Commands
//
#define STATE_SPACE_IOCTL_MAGIC 0xC0

/**
* @brief Querys the number of statespace parts the model has.
* 
* @param A pointer to a stateSpace_info_t structure that will be filled.
*/
#define STATE_SPACE_GET_NUM_PARTS   _IOR(STATE_SPACE_IOCTL_MAGIC, 1, struct stateSpace_info_t)

/**
* @brief Querys the name of the part identified with the given index.
* 
* @param A pointer to a stateSpace_get_name_req_t structure that will be filled.
*/
#define STATE_SPACE_GET_PART_NAME   _IOWR(STATE_SPACE_IOCTL_MAGIC, 2, struct stateSpace_get_name_req_t)

/**
* @brief Requests that the given configuration be applied to the part with the given index.
* 
* @param A pointer to a stateSpace_part_config_t that contains the requested configuration.
*/
#define STATE_SPACE_CONFIGURE_PART  _IOW(STATE_SPACE_IOCTL_MAGIC, 3, struct stateSpace_part_config_t)

/**
* @brief Reads out the part with the given index's configuration (ioctl_version through 
*        state_vec_len) in the stateSpace_part_config_t struct.
*        
*        stateSpace_part_config_t.part_index must be set to the requested part. 
* 
* @param A pointer to a stateSpace_part_config_t that will be filled.
*/
#define STATE_SPACE_GET_PART_CONFIG _IOR(STATE_SPACE_IOCTL_MAGIC, 4, struct stateSpace_part_config_t)

/**
* @brief Reads out the part with the given index's configuration and data
*        (ioctl_version through data_block) in the stateSpace_part_config_t struct.
*        
*        stateSpace_part_config_t.part_index must be set to the requested part. 
* 
* @param A pointer to a stateSpace_part_config_t that will be filled. The size of the struct
*        should be calculated with the calcDataBlockSzStateSpace(...) function.
*/
#define STATE_SPACE_GET_PART_DATA   _IOR(STATE_SPACE_IOCTL_MAGIC, 5, struct stateSpace_part_config_t)


#ifdef __cplusplus
}
#endif

#endif //LIGO_STATE_SPACE_CTRL_IOCTL_H