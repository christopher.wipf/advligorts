#ifndef LIGO_FILTER_COEFFS_HH
#define LIGO_FILTER_COEFFS_HH

// Up/Down Sampling Filter Coeffs
// All systems not running at 64K require up/down sampling to communicate I/O
// data with IOP, which is running at 64K. Following defines the filter coeffs
// for these up/down filters.


/* Coeffs for the 256x downsampling filter (256 Hz system)
 * from feCoeff2k with zero/pole freqs rescaled by 1/8 */
extern const double feCoeff256x[ 9 ];


/* Coeffs for the 2x downsampling (32K system) filter */
extern const double feCoeff2x[ 9 ];

/*
 * DCC docs of note
 * T1600066 - New RCG 3.0 Decimation (IOP Upsampling / Downsampling) Filter for 16 kHz models
 * T1500075 - A Note on the RCG Decimation (IOP Upsampling / Downsampling) Filters (Prior to RCG 3.0)
 */

/* RCG V3.0 Coeffs for the 4x downsampling (16K system) filter */
extern const double feCoeff4x[ 9 ];


/*
 * Filter design link: https://dcc.ligo.org/LIGO-G2202011
 * These taps are used in decimation filters for 524k ADC date down to 65K
 * and in demodulation parts.
 *
 */
extern const double feCoeff8x[ 13 ];

//
// New Brian Lantz 4k decimation filter
extern const double feCoeff16x[ 9 ];


/* Coeffs for the 32x downsampling filter (2K system) per Brian Lantz May 5,
 * 2009 */
extern const double feCoeff32x[ 9 ];

#endif //LIGO_FILTER_COEFFS_HH
