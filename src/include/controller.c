
#include "controller.h"

adcInfo_t     adcinfo;
dacInfo_t     dacinfo;
timing_diag_t timeinfo;

/// Maintains present cycle count within a one second period.
int          cycleNum = 0;
unsigned int odcStateWord = 0xffff;
/// Value of readback from DAC FIFO size registers; used in diags for FIFO
/// overflow/underflow.
int          out_buf_size = 0; // test checking DAC buffer size
unsigned int cycle_gps_time = 0; // Time at which ADCs triggered
unsigned int cycle_gps_event_time = 0; // Time at which ADCs triggered
unsigned int cycle_gps_ns = 0;
unsigned int cycle_gps_event_ns = 0;
unsigned int gps_receiver_locked = 0; // Lock/unlock flag for GPS time card
/// GPS time in GPS seconds
unsigned int     timeSec = 0;
unsigned int     timeSecDiag = 0;
unsigned int     ipcErrBits = 0;
int              cardCountErr = 0;
struct rmIpcStr* daqPtr;
int dacOF[ MAX_DAC_MODULES ]; /// @param dacOF[]  DAC overrange counters

char daqArea[ DAQ_DCU_SIZE ]; // Space allocation for daqLib buffers
int  cpuId = 1;

// Initial diag reset flag
int initialDiagReset = 1;

// Cache flushing mumbo jumbo suggested by Thomas Gleixner, it is probably
// useless Did not see any effect
char fp[ 64 * 1024 ];



char*          build_date = __DATE__ " " __TIME__;

//
// Pointers to shared memory blocks
//
shmem_handle g_epics_shm_handle;
volatile char* _epics_shm; ///< Ptr to EPICS shared memory area

shmem_handle g_tp_shm_handle;
volatile TESTPOINT_CFG  *_tp_shm;

shmem_handle g_awg_shm_handle;
volatile AWG_DATA *_awg_shm;

shmem_handle g_ipc_shm_handle;
volatile char*          _ipc_shm; ///< Ptr to inter-process communication area

shmem_handle g_daq_shm_handle;
volatile char*          _daq_shm; ///< Ptr to frame builder comm shared mem area

shmem_handle g_shmipc_shm_handle;
volatile char*          _shmipc_shm; ///< Ptr to IOP I/O data to/from User app shared mem area

shmem_handle g_io_shm_handle;
volatile char*          _io_shm; ///< Ptr to user space I/O area

int            daq_fd; ///< File descriptor to share memory file

long                      daqBuffer; // Address for daq dual buffers in daqLib.c
CDS_HARDWARE              cdsPciModules; // Structure of PCI hardware addresses
volatile IO_MEM_DATA*     ioMemData;
volatile IO_MEM_DATA_IOP* ioMemDataIop;
volatile int              vmeDone = 0; // Code kill command
volatile int              fe_status_return = 0; // fe code status return to module_exit
volatile int              fe_status_return_subcode = 0; // fe code status return to module_exit
volatile int              stop_working_threads = 0;


// Following define string names of above modules
// Used to print info to syslog
//#define CDS_CARD_TYPES  17
char _cdscardtypename[ CDS_CARD_TYPES ][ 40 ] = {
        "GSC_16AI64SSA", "GSC_18AISS6C", "GSC_16AO16",  "GSC_20AO8",
        "CON_32DO",      "ACS_16DIO",    "ACS_8DIO",    "GSC_18AI32SSC1M",
        "GSC_18AO8",     "ACS_24DIO",    "CON_1616DIO", "CON_6464DIO",
        "CDO64",         "CDI64",        "LPTC",        "GSC_18AI64SSC",
        "LIGO28AO32"
};

/// Testpoints which are not part of filter modules
double* testpoint[ GDS_MAX_NFM_TP ];
#ifndef NO_DAQ
DAQ_RANGE daq; // Range settings for daqLib.c
int       numFb = 0;
int       fbStat[ 2 ] = { 0, 0 }; // Status of DAQ backend computer
/// Excitation points which are not part of filter modules
double xExc[ GDS_MAX_NFM_EXC ]; // GDS EXC not associated with filter modules
#endif
/// See header for info on cycle_16Hz_reset
int cycle_16Hz_reset = 0; // Internal cycle counter
/// DAQ cycle counter (0-15)
unsigned int daqCycle; // DAQS cycle counter

// Sharded memory discriptors
int                 wfd, ipc_fd;
volatile CDS_EPICS* pLocalEpics; // Local mem ptr to EPICS control data
volatile char*      pEpicsDaq; // Local mem ptr to EPICS daq data


// Filter module variables
/// Standard Filter Module Structure
FILT_MOD dsp[ NUM_SYSTEMS ]; // SFM structure.
/// Pointer to local memory SFM Structure
FILT_MOD* dspPtr[ NUM_SYSTEMS ]; // SFM structure pointer.
/// Pointer to SFM in shared memory.
FILT_MOD* pDsp[ NUM_SYSTEMS ]; // Ptr to SFM in shmem.
/// Pointer to filter coeffs local memory.
COEF dspCoeff[ NUM_SYSTEMS ]; // Local mem for SFM coeffs.
/// Pointer to filter coeffs shared memory.
VME_COEF* pCoeff[ NUM_SYSTEMS ]; // Ptr to SFM coeffs in shmem


// ADC Variables
/// Array of ADC values
#ifdef IOP_MODEL
double dWord[ MAX_ADC_MODULES ][ MAX_ADC_CHN_PER_MOD ][ 16 ]; // ADC read values
#else
double dWord[ MAX_ADC_MODULES ][ MAX_ADC_CHN_PER_MOD ]; // ADC read values
#endif
/// List of ADC channels used by this app. Used to determine if downsampling
/// required.
unsigned int dWordUsed[ MAX_ADC_MODULES ]
                      [ MAX_ADC_CHN_PER_MOD ]; // ADC chans used by app code

// DAC Variables
/// Enables writing of DAC values; Used with DACKILL parts..
int iopDacEnable; // Returned by feCode to allow writing values or zeros to DAC
                  // modules
int dacChanErr[ MAX_DAC_MODULES ];
#ifdef IOP_MODEL
int dacOutBufSize[ MAX_DAC_MODULES ];
#endif
/// Array of DAC output values.
double dacOut[ MAX_DAC_MODULES ][ MAX_DAC_CHN_PER_MOD ]; // DAC output values
/// DAC output values returned to EPICS
int dacOutEpics[ MAX_DAC_MODULES ]
               [ MAX_DAC_CHN_PER_MOD ]; // DAC outputs reported back to EPICS
/// DAC channels used by an app.; determines up sampling required.
unsigned int dacOutUsed[ MAX_DAC_MODULES ]
                       [ MAX_DAC_CHN_PER_MOD ]; // DAC chans used by app code
/// Array of DAC overflow (overrange) counters.
int overflowDac[ MAX_DAC_MODULES ]
               [ MAX_DAC_CHN_PER_MOD ]; // DAC overflow diagnostics
/// DAC outputs stored as floats, to be picked up as test points
double floatDacOut[ MAX_DAC_CHN_PER_MOD * 10 ]; // DAC outputs stored as floats, to be picked up as
                           // test points

/// Counter for total ADC/DAC overflows
int overflowAcc = 0; // Total ADC/DAC overflow counter


#ifndef IOP_MODEL
// Variables for Digital I/O board values
// DIO board I/O is handled in control (user) applications for timing reasons
// (longer I/O access times)
/// Read value from Acces I/O 24bit module
int dioInput[ MAX_DIO_MODULES ];
/// Write value to Acces I/O 24bit module
int dioOutput[ MAX_DIO_MODULES ];
/// Last value written to Acces I/O 24bit module
int dioOutputHold[ MAX_DIO_MODULES ];

int rioInput1[ MAX_DIO_MODULES ];
int rioInputInput[ MAX_DIO_MODULES ];
int rioInputOutput[ MAX_DIO_MODULES ];
int rioOutput[ MAX_DIO_MODULES ];
int rioOutputHold[ MAX_DIO_MODULES ];
int rioOutput1[ MAX_DIO_MODULES ];
int rioOutputHold1[ MAX_DIO_MODULES ];

// Contec 32 bit output modules
/// Read value from Contec 32bit I/O module
unsigned int CDO32Input[ MAX_DIO_MODULES ];
/// Write value to Contec 32bit I/O module
unsigned int CDO32Output[ MAX_DIO_MODULES ];

#endif

#ifdef OVERSAMPLE

// History buffers for oversampling filters
double dHistory[ ( MAX_ADC_MODULES * MAX_ADC_CHN_PER_MOD) ][ MAX_HISTRY ];
double dDacHistory[ ( MAX_DAC_MODULES * MAX_DAC_CHN_PER_MOD ) ][ MAX_HISTRY ];

#else

#define OVERSAMPLE_TIMES 1

#endif //OVERSAMPLE

