#ifndef LIGO_PORTABLE_INLINE_H
#define LIGO_PORTABLE_INLINE_H

#ifndef LIGO_INLINE
# ifdef __KERNEL__
#  define LIGO_INLINE extern inline
# elif defined(__GNUC__)
#  if !defined(__GNUC_STDC_INLINE__)
#    define LIGO_INLINE extern inline
#  else
#    define LIGO_INLINE static inline
#  endif
# else
#  define LIGO_INLINE inline 
# endif
#endif //INC_FROM_SOURCE


#endif //LIGO_PORTABLE_INLINE_H
