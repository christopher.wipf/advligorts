#ifndef LIGO_FE_STATE_WORD
#define LIGO_FE_STATE_WORD
/// @file fe_state_word.h
/// @brief Contains prototype defs for EPICS state word diagnostic
//         used to signal errors to operator screens 


#ifdef __cplusplus
extern "C" {
#endif

// The following bits define the EPICS STATE_WORD, this is displayed as the 
// STATE WORD boxes on the GDS_TP screen. 

// The kernel-space RT code controls these bits of the state word until noted otherwise
#define FE_ERROR_TIMING 0x2 // Timing Error (TIM)
#define FE_ERROR_ADC 0x4 // Analog-to-Digital Converter Error (ADC)
#define FE_ERROR_DAC 0x8 // Digital-to-analog Converter Error (DAC)
#define FE_ERROR_DAQ 0x10 // Data Acquisition Error (DAQ)
#define FE_ERROR_IPC 0x20 // Inter-process Communication Error (IPC)
#define FE_ERROR_AWG 0x40 // Arbitrary Waveform Generator Error (AWG)
#define FE_ERROR_DAC_KILLED 0x80 // DAC KILL Active (DK) 
#define FE_ERROR_EXC_SET 0x100 // Excitation Active (EXC)
// FE_ERROR_EXC_SET is not an error, just a signal that at least one excitation is active.
#define FE_ERROR_OVERFLOW 0x200 // ADC/DAC Overflow Detected (OVF)
// Below are the bits that the user-space sequencer has control over. 
// Because it can do things like read files from disk, it is beter suited 
// to do the CFC, FLT, etc checks.
#define FE_ERROR_CFC 0x400 // Configuration File Checksum Mismatch (CFC)
#define FE_ERROR_FLT 0x800 // Filter load error (FLT)
// Below this point the kernel-space RT code controls these bits again.
#define FE_ERROR_FPU 0x1000 // Floating point unit (FPU) error detected

#ifdef __cplusplus
}
#endif

#endif //LIGO_FE_STATE_WORD

