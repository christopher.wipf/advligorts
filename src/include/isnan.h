/*
 * ====================================================
 * Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
 *
 * Developed at SunPro, a Sun Microsystems, Inc. business.
 * Permission to use, copy, modify, and distribute this
 * software is freely granted, provided that this notice
 * is preserved.
 * ====================================================
 */

#ifndef LIGO_ISNAN_H
#define LIGO_ISNAN_H 

#include <asm/types.h>

#include "util/fixed_width_types.h"

/* A union which permits us to convert between a double and two 32 bit
   ints.  */

typedef union
{
  double value;
  struct
  {
    uint32_t lsw;
    uint32_t msw;
  } parts;
  uint64_t word;
} ieee_double_shape_type;



/* Get two 64 bit ints from a long double.  */

#define GET_LDOUBLE_WORDS64(ix0,ix1,d)              \
do {                                \
  ieee854_long_double_shape_type qw_u;              \
  qw_u.value = (d);                     \
  (ix0) = qw_u.parts64.msw;                 \
  (ix1) = qw_u.parts64.lsw;                 \
} while (0)


# define EXTRACT_WORDS64(i,d)                   \
do {                                \
  ieee_double_shape_type gh_u;                  \
  gh_u.value = (d);                     \
  (i) = gh_u.word;                      \
} while (0)


#ifndef UINT64_C
# if BITS_PER_LONG == 64
#  define UINT64_C(c)   c ## UL
# else
#  define UINT64_C(c)   c ## ULL
# endif
#endif

/*
 * isnan(x) returns 1 is x is nan, else 0;
 * no branching!
 */

static int inline isnan( double x)
{
    int64_t hx;
    EXTRACT_WORDS64 (hx, x);
    hx &= UINT64_C (0x7fffffffffffffff);
    hx = UINT64_C (0x7ff0000000000000) - hx;
    return (int)(((uint64_t)hx)>>63);
}

/*
 * isinf(x) returns 1 is x is inf, -1 if x is -inf, else 0;
 * no branching!
 */

static int inline isinf (double x)
{
  int64_t ix;
  EXTRACT_WORDS64 (ix, x);
  int64_t t = ix & UINT64_C (0x7fffffffffffffff);
  t ^= UINT64_C (0x7ff0000000000000);
  t |= -t;
  return ~(t >> 63) & (ix >> 62);
}




#endif //LIGO_ISNAN_H
