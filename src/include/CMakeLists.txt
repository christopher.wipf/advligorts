
install(FILES
            daq_core.h
            daq_core_defs.h
            daq_data_types.h
            portableInline.h
            daqmap.h
        DESTINATION
            include/advligorts
        )


add_subdirectory(drv)
add_subdirectory(util)