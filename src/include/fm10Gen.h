/*-----------------------------------------------------------------------------
 */
/*                                                                              */
/*                      ------------------- */
/*                                                                              */
/*                             LIGO */
/*                                                                              */
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY. */
/*                                                                              */
/*                     (C) The LIGO Project, 2005. */
/*                                                                              */
/*                                                                              */
/* File: fm10Gen.h */
/* Description: */
/*      CDS generic code for calculating IIR filters. */
/*                                                                              */
/* California Institute of Technology */
/* LIGO Project MS 18-34 */
/* Pasadena CA 91125 */
/*                                                                              */
/* Massachusetts Institute of Technology */
/* LIGO Project MS 20B-145 */
/* Cambridge MA 01239 */
/*                                                                              */
/*-----------------------------------------------------------------------------
 */

///	@file fm10Gen.h
///	@brief This file contains the routines for performing the real-time
///		IIR/FIR filter calculations. \n
///	Further information is provided in the LIGO DCC
///	<a
///href="https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=7687">T0900606
///CDS Standard IIR Filter Module Software</a>

#ifndef LIGO_FM10GEN_H
#define LIGO_FM10GEN_H

#include "fm10Gen_types.h"
#include "fm10Gen_bits.h"
#include "tRamp.h"
#include "portableInline.h" //LIGO_INLINE


#ifdef __cplusplus
extern "C" {
#endif


//
// Function Prototypes for c file implementation
//
int
readCoefVme2( COEF* filtC, volatile FILT_MOD* fmt, int modNum1, int filtNum,
              int cycle, volatile VME_COEF* pRfmCoeff, int* changed );
double
filterModuleD2( FILT_MOD* pFilt, COEF* pC, int modNum, double filterInput, 
                int fltrCtrlVal, int mask, double offset_in, double gain_in,
                double ramp_in );

/* Convert opSwitchE bits into the 16-bit FiltCtrl2 Ctrl output format */
unsigned int filtCtrlBitConvert( unsigned int v );

int doFilterLoadWork( volatile FILT_MOD* pL,
                      volatile FILT_MOD* dspVme, COEF* pC,
                      volatile VME_COEF* pRfmCoeff);

int checkFiltResetId( int bankNum, FILT_MOD* pL, volatile FILT_MOD* dspVme,
                       COEF* pC, int totMod, volatile VME_COEF* pRfmCoeff);

int initVarsId( FILT_MOD* pL, volatile FILT_MOD* pV, COEF* pC, int totMod,
                volatile VME_COEF* pRfmCoeff, int id );

void clearFilterHistoryChecks( void );


//
// Inline functions provided by this header
//



/// @brief Perform IIR filtering sample by sample on doubles.
/// Implements cascaded direct form II second order sections.
///	@param[in] input New input sample
///	@param[in] *coef Pointer to filter coefficient data with size 4*n + 1
///(gain)
///	@param[in] n Number of second order sections in filter definition
///	@param[in,out] *history Pointer to filter history data of size 2*n
///	@return Result of filter calculation
LIGO_INLINE double
iir_filter( double input, double* coef, int n, double* history )
{

    int     i;
    double* coef_ptr;
    double *hist1_ptr, *hist2_ptr;
    double  output, new_hist, history1, history2;

    coef_ptr = coef; /* coefficient pointer */

    hist1_ptr = history; /* first history */
    hist2_ptr = hist1_ptr + 1; /* next history */

    output = input * ( *coef_ptr++ ); /* overall input scale factor */

    for ( i = 0; i < n; i++ )
    {

        history1 = *hist1_ptr; /* history values */
        history2 = *hist2_ptr;

        output = output - history1 * ( *coef_ptr++ );
        new_hist = output - history2 * ( *coef_ptr++ ); /* poles */

        output = new_hist + history1 * ( *coef_ptr++ );
        output = output + history2 * ( *coef_ptr++ ); /* zeros */

        *hist2_ptr++ = *hist1_ptr;
        *hist1_ptr++ = new_hist;
        hist1_ptr++;
        hist2_ptr++;
    }

    return ( output );
}

/* Biquad form IIR */
/// @brief Perform IIR filtering sample by sample on doubles.
/// Implements Biquad form calculations.
///	@param[in] input New input sample
///	@param[in] *coef Pointer to filter coefficient data with size 4*n + 1
///(gain)
///	@param[in] n Number of second order sections in filter definition
///	@param[in,out] *history Pointer to filter history data of size 2*n
///	@return Result of filter calculation
LIGO_INLINE double
iir_filter_biquad( double input, const double * coef, int n, double* history )
{

    int     i;
    const double* coef_ptr;
    double *hist1_ptr, *hist2_ptr;
    double  output, new_w, new_u, w, u, a11, a12, c1, c2;

    coef_ptr = coef; /* coefficient pointer */

    hist1_ptr = history; /* first history */
    hist2_ptr = hist1_ptr + 1; /* next history */

    output = input * ( *coef_ptr++ ); /* overall input scale factor */

    for ( i = 0; i < n; i++ )
    {

        w = *hist1_ptr;
        u = *hist2_ptr;

        a11 = *coef_ptr++;
        a12 = *coef_ptr++;
        c1 = *coef_ptr++;
        c2 = *coef_ptr++;

        new_w = output + a11 * w + a12 * u;
        output = output + w * c1 + u * c2;
        new_u = w + u;

        *hist1_ptr++ = new_w;
        *hist2_ptr++ = new_u;
        hist1_ptr++;
        hist2_ptr++;
    }

    // if((output < 1e-28) && (output > -1e-28)) output = 0.0;
    return ( output );
}

#ifdef FIR_FILTERS
// *************************************************************************/
///	@brief Perform FIR filter calculations.
///	@param[in] input New input sample
///	@param[in] *coef Pointer to filter coefficients
///	@param[in] n Number of taps
///	@param[in,out] *history Pointer to filter history data
///	@return Result of FIR filter calculation
LIGO_INLINE double
fir_filter( double input, double* coef, int n, double* history )
{
    int     i;
    double *hist_ptr, *hist1_ptr, *coef_ptr;
    double  output;

    hist_ptr = history;
    hist1_ptr = hist_ptr; /* use for history update */
    coef_ptr = coef + n - 1; /* point to last coef */

    /* form output accumulation */
    output = *hist_ptr++ * ( *coef_ptr-- );
    for ( i = 2; i < n; i++ )
    {
        *hist1_ptr++ = *hist_ptr; /* update history array */
        output += ( *hist_ptr++ ) * ( *coef_ptr-- );
    }
    output += input * ( *coef_ptr ); /* input tap */
    *hist1_ptr = input; /* last history */

    return ( output );
}
#endif



/************************************************************************/
/************************************************************************/
/// @brief Read in filter coeffs from shared memory on code initialization.
///	@param[in,out] *filtC Pointer to coeffs in local memory
///	@param[in] *fmt Pointer to filter data in local memory
///	@param[in] bF	Start filter number
///	@param[in] sF	End filter number
///	@param[in] *pRfmCoeff Pointer to coeffs in shared memory
LIGO_INLINE int
readCoefVme(
    COEF* filtC, FILT_MOD* fmt, int bF, int sF, volatile VME_COEF* pRfmCoeff )
{
    int ii, jj, kk;

#ifdef FIR_FILTERS
    int l;
    for ( jj = 0; jj < MAX_FIR_MODULES; jj++ )
        for ( kk = 0; kk < FILTERS; kk++ )
            for ( l = 0; l < MAX_FIR_COEFFS; l++ )
                filtC->firFiltCoeff[ jj ][ kk ][ l ] =
                    pRfmCoeff->firFiltCoeff[ jj ][ kk ][ l ];
#endif
    for ( ii = bF; ii < sF; ii++ )
    {
        filtC->coeffs[ ii ].biquad = pRfmCoeff->vmeCoeffs[ ii ].biquad;
        // if(filtC->coeffs[ii].biquad) RTSLOG_INFO("Found a BQF filter %d\n",ii);
        for ( jj = 0; jj < FILTERS; jj++ )
        {
            if ( pRfmCoeff->vmeCoeffs[ ii ].filtSections[ jj ] )
            {
                filtC->coeffs[ ii ].filterType[ jj ] =
                    pRfmCoeff->vmeCoeffs[ ii ].filterType[ jj ];
#ifdef FIR_FILTERS
                if ( filtC->coeffs[ ii ].filterType[ jj ] < 0 ||
                     filtC->coeffs[ ii ].filterType[ jj ] > MAX_FIR_MODULES )
                {
                    filtC->coeffs[ ii ].filterType[ jj ] = 0;
                    // RTSLOG_ERROR("Corrupted data coming from Epics: module=%d
                    // filter=%d filterType=%d\n",
                    //	ii, jj, pRfmCoeff->vmeCoeffs[ii].filterType[jj]);
                    return 1;
                }
#endif
                filtC->coeffs[ ii ].filtSections[ jj ] =
                    pRfmCoeff->vmeCoeffs[ ii ].filtSections[ jj ];
                filtC->coeffs[ ii ].sType[ jj ] =
                    pRfmCoeff->vmeCoeffs[ ii ].sType[ jj ];
                fmt->inputs[ ii ].rmpcmp[ jj ] =
                    pRfmCoeff->vmeCoeffs[ ii ].ramp[ jj ];
                fmt->inputs[ ii ].timeout[ jj ] =
                    pRfmCoeff->vmeCoeffs[ ii ].timout[ jj ];

                if ( filtC->coeffs[ ii ].filterType[ jj ] == 0 )
                {
                    if ( filtC->coeffs[ ii ].filtSections[ jj ] > 10 )
                    {
                        // RTSLOG_ERROR("Corrupted Epics data:  module=%d filter=%d
                        // filterType=%d filtSections=%d\n", ii, jj,
                        // pRfmCoeff->vmeCoeffs[ii].filterType[jj],
                        // filtC->coeffs[ii].filtSections[jj]);
                        return 1;
                    }
                    for ( kk = 0;
                          kk < filtC->coeffs[ ii ].filtSections[ jj ] * 4 + 1;
                          kk++ )
                    {
                        filtC->coeffs[ ii ].filtCoeff[ jj ][ kk ] =
                            pRfmCoeff->vmeCoeffs[ ii ].filtCoeff[ jj ][ kk ];
                    }
                }
            }
        }
    }
    return 0;
}



///	@brief Calls checkFiltResetId
///	@param[in] bankNum	Filter module ID number
///	@param[in,out] *pL	Pointer to filter module data in local memory.
///	@param[in] *dspVme	Pointer to filter module data in shared memory.
///	@param[in,out] *pC	Pointer to filter coefs in local memory.
///	@param[in] totMod	Total number of filter modules
///	@param[in] *pRfmCoeff	Pointer to filter coefs in shared memory.
LIGO_INLINE int
checkFiltReset( int                bankNum,
                FILT_MOD*          pL,
                volatile FILT_MOD* dspVme,
                COEF*              pC,
                int                totMod,
                volatile VME_COEF* pRfmCoeff )
{
    return checkFiltResetId( bankNum, pL, dspVme, pC, totMod, pRfmCoeff);
}


///	@brief Calls initVarsId with dummy sys id of 0
///	@param[in,out] *pL	Pointer to filter module data in local memory
///	@param[in] *pV		Pointer to filter module data in shared memory
///	@param[in,out] *pC	Pointer to filter coeffs in local memory
///	@param[in] totMod	Total number of filter modules
///	@param[in] *pRfmCoeff	Pointer to filter coeffs in shared memory
LIGO_INLINE int
initVars( FILT_MOD*          pL,
          volatile FILT_MOD* pV,
          COEF*              pC,
          int                totMod,
          volatile VME_COEF* pRfmCoeff )
{
    return initVarsId( pL, pV, pC, totMod, pRfmCoeff, 0 );
}

/* This module added to hanle all input, calculations and outputs as doubles.
   This module also incorporates the input module, done separately above for
   older systems. */




/// 	@brief This function is called by user apps using standard IIR/FIR
/// filters..
///< This function in turn calls filterModuleD2 to actually perform the calcs,
///< with dummy vars added.
///	@param[in,out] *pFilt Filter Module Data
///	@param[in] *pC Filter module coefficients
///	@param[in] modNum Filter module ID number
///	@param[in] filterInput Input data sample
///	@param[in] fltrCtrlVal Filter control value
///	@param[in] mask Control mask
///	@return Output of IIR/FIR filter calculations.
LIGO_INLINE double
filterModuleD( FILT_MOD* pFilt, /* Filter module data  */
               COEF*     pC, /* Filter coefficients */
               int       modNum, /* Filter module number */
               double    filterInput, /* Input data sample (output from funtcion
                                         inputModule()) */
               int fltrCtrlVal, /* Filter control value */
               int mask ) /* Mask of bits to act upon */
{
    /* Limit control to the 10 bits */
    return filterModuleD2( pFilt,
                           pC,
                           modNum,
                           filterInput,
                           fltrCtrlVal & 0x3ff,
                           mask & 0x3ff,
                           0.,
                           0.,
                           0. );
}

#ifdef __cplusplus
}
#endif


#endif //LIGO_FM10GEN_H
