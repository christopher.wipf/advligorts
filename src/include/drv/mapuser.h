#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED

#include <drv/cdsHardware.h>
#include <commData3.h>

//In userspace the linux/pci.h header does not have
//the pci_dev struct defined, so we forward declare it
//here, so we can include these headers without a warning
struct pci_dev;

#include "gsc16ai64.h"
#include "gsc16ao16.h"
#include "gsc18ao8.h"
#include "gsc20ao8.h"
#include <drv/gsc18ai32.h>

#include <drv/accesIIRO8.h>
#include <drv/accesIIRO16.h>
#include <drv/contec6464.h>
#include <drv/contec1616.h>
#include <drv/vmic5565.h>
#include <drv/symmetricomGps.h>
#include <drv/spectracomGPS.h>





#endif
