#ifndef LIGO_APP_DAC_FUNCTIONS_H
#define LIGO_APP_DAC_FUNCTIONS_H

#include "portableInline.h"
#include "cds_types.h"
#include "controller.h" //cdsPciModules
#include "fm10Gen.h"
#include "drv/rts-logger.h"
#include "drv/gsc16ao16.h" //GSAO_16BIT_MASK
#include "drv/gsc18ao8.h" //GSAO_18BIT_MASK
#include "drv/gsc20ao8.h" //GSAO_20BIT_MASK
#include "util/ligo_mfence.h"




#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Verifies that DAC channels we are going to write to
 *        are not already being used by other models.
 *
 * This function first searches through all DAC channels and verifies
 * none of the channels we plan on using are already used, it then loops
 * over the channels again and marks each one we are going to use in the 
 * IOPs shared memory. 
 *
 */
LIGO_INLINE int app_dac_init( void )
{
    int cur_dac, chan;
    int pd; //physical DAC number

    // See if my DAC channel map overlaps with already running models
    for ( cur_dac = 0; cur_dac < cdsPciModules.dacCount; cur_dac++ )
    {
        if ( cdsPciModules.dacConfig[ cur_dac ] )
        {
            pd = cdsPciModules.dacConfig[ cur_dac ] -
                ioMemData->adcCount; // physical DAC number
            for ( chan = 0; chan < MAX_DAC_CHN_PER_MOD; chan++ )
            {
                if ( dacOutUsed[ cur_dac ][ chan ] )
                {
                    if ( ioMemData->dacOutUsed[ pd ][ chan ] )
                    {
                        RTSLOG_ERROR("Failed to reserve DAC channel for model.\n");
                        RTSLOG_ERROR("DAC local index %d, global index %d, channel %d is already allocated.\n", 
                                     cur_dac, pd, chan);
                        return 1;
                    }
                }
            }
        }
    }

    //Loop over the all DACs channels and mark the ones we are going to use
    for ( cur_dac = 0; cur_dac < cdsPciModules.dacCount; cur_dac++ )
    {
        if ( cdsPciModules.dacConfig[ cur_dac ] )
        {
            pLocalEpics->epicsOutput.statDac[ cur_dac ] = DAC_FOUND_BIT;
            pd = cdsPciModules.dacConfig[ cur_dac ] -
                ioMemData->adcCount; // physical DAC number
            for ( chan = 0; chan < MAX_DAC_CHN_PER_MOD; chan++ )
            {
                if ( dacOutUsed[ cur_dac ][ chan ] )
                {
                    ioMemData->dacOutUsed[ pd ][ chan ] = 1;
                }
            }
        }
    }
    return 0;
}

// Clear the used DAC channel shared memory map,
// used to keep track of non-overlapping DAC channels among control models
LIGO_INLINE void deallocate_dac_channels( void )
{
    int cur_dac, chan;
    for ( cur_dac = 0; cur_dac < MAX_DAC_MODULES; cur_dac++ )
    {
        if ( cdsPciModules.dacConfig[ cur_dac ] )
        {
            int pd = cdsPciModules.dacConfig[ cur_dac ] - ioMemData->adcCount;
            for ( chan = 0; chan < MAX_DAC_CHN_PER_MOD; chan++ )
                if ( dacOutUsed[ cur_dac ][ chan ] ) {
                    //We need to clear our local copy, so we stop writing and marking them "written"
                    dacOutUsed[ cur_dac ][ chan ] = 0; 

                    //Clear the IOP shared mem chans so other models can use them again
                    ioMemData->dacOutUsed[ pd ][ chan ] = 0;
                    ioMemData->dacChanWritten[ pd ][ chan ] = 0;
                }
        }
    }
}

LIGO_INLINE int app_dac_write( int ioMemCtrDac, int ioClkDac, dacInfo_t* dacinfo )
{
    int    chan, card, mm, kk;
    double dac_in = 0.0;
    int    dac_out = 0;
    int    memCtr = 0;


    /// \> Loop thru all DAC modules
    for ( card = 0; card < cdsPciModules.dacCount; card++ )
    {
        /// - -- locate the proper DAC memory block
        mm = cdsPciModules.dacConfig[ card ];

        if ( mm )
        {

            /// - -- If user app < 64k rate (typical), need to upsample from
            /// code rate to IOP rate
            for ( kk = 0; kk < OVERSAMPLE_TIMES; kk++ )
            {
                /// - -- For each DAC channel
                for ( chan = 0; chan < cdsPciModules.dac_info[ card ].num_chans; chan++ )
                {
#ifdef FLIP_SIGNALS
                    dacOut[ card ][ chan ] *= -1;
#endif
                    /// - ---- Read in DAC value, if channel is used by the
                    /// application,  and run thru upsample filter\n
                    if ( dacOutUsed[ card ][ chan ] )
                    {
#ifdef OVERSAMPLE_DAC
#ifdef NO_ZERO_PAD
                        /// - --------- If set to NO_ZERO_PAD (not standard
                        /// setting), then DAC output value is repeatedly run
                        /// thru the filter OVERSAMPE_TIMES.\n
                        dac_in = dacOut[ card ][ chan ];
                        dac_in = iir_filter_biquad(
                            dac_in,
                            FE_OVERSAMPLE_COEFF,
                            2,
                            &dDacHistory[ chan + card * MAX_DAC_CHN_PER_MOD ]
                                        [ 0 ] );
#else //NO_ZERO_PAD
                        /// - --------- Otherwise zero padding is used
                        /// (standard), ie DAC output value is applied to filter
                        /// on first interation, thereafter zeros are applied.
                        dac_in = kk == 0 ? (double)dacOut[ card ][ chan ] : 0.0;
                        dac_in = iir_filter_biquad(
                            dac_in,
                            FE_OVERSAMPLE_COEFF,
                            2,
                            &dDacHistory[ chan + card * MAX_DAC_CHN_PER_MOD ]
                                        [ 0 ] );
                        dac_in *= OVERSAMPLE_TIMES;
#endif //NO_ZERO_PAD
#else //OVERSAMPLE_DAC
                        dac_in = dacOut[ card ][ chan ];
#endif //OVERSAMPLE_DAC
                    }
                    else /// - ---- If channel is not used, then set value to zero.
                    {
                        dac_in = 0.0;
                    }

                    /// - ---- Smooth out some of the double > short roundoff
                    /// errors
                    if ( dac_in > 0.0 )
                        dac_in += 0.5;
                    else
                        dac_in -= 0.5;
                    dac_out = dac_in;

                    /// - ---- Check output values are within range of DAC \n
                    /// - --------- If overflow, clip at DAC limits and report
                    /// errors
                    if ( dac_out > cdsPciModules.dac_info[ card ].sample_limit || 
                         dac_out < -cdsPciModules.dac_info[ card ].sample_limit )
                    {
                        dacinfo->overflowDac[ card ][ chan ]++;
                        pLocalEpics->epicsOutput.overflowDacAcc[ card ][ chan ]++;
                        overflowAcc++;
                        dacinfo->dacOF[ card ] = 1;
                        odcStateWord |= ODC_DAC_OVF;
                        
                        if ( dac_out > cdsPciModules.dac_info[ card ].sample_limit )
                            dac_out = cdsPciModules.dac_info[ card ].sample_limit;
                        else
                            dac_out = -cdsPciModules.dac_info[ card ].sample_limit;
                    }
                    /// - ---- If DAQKILL tripped, set output to zero.
                    if ( !iopDacEnable )
                        dac_out = 0;
                    /// - ---- Load last values to EPICS channels for monitoring
                    /// on GDS_TP screen.
                    dacinfo->dacOutEpics[ card ][ chan ] = dac_out;

                    /// - ---- Load DAC testpoints
                    floatDacOut[ MAX_DAC_CHN_PER_MOD * card + chan ] = dac_out;
                    /// - ---- Determine shared memory location for new DAC
                    /// output data
                    memCtr = ( ioMemCtrDac + kk ) % IO_MEMORY_SLOTS;
                    /// - ---- Write DAC output to shared memory. \n
                    /// - --------- Only write to DAC channels being used to
                    /// allow two or more control models to write to same DAC
                    /// module.
                    if ( dacOutUsed[ card ][ chan ] ) {
                        ioMemData->outputData[ mm ][ memCtr ].channel_set[ chan ].data = dac_out;
                        if ( iopDacEnable ) {
                            ligo_smp_wmb(); //HW memory barrier, that guarantees write to .data will be seen before cycle
                            ioMemData->outputData[ mm ][ memCtr ].channel_set[ chan ].cycle_65k = 
                            ( ioClkDac + kk ) % IOP_IO_RATE;
                        }

                    }
                } //For each DAC chan

            } //For each OVERSAMPLE_TIMES
        }
    } //For each DAC card
    
    return 0;
}

#ifdef __cplusplus
}
#endif


#endif
