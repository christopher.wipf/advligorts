//	\file gsc18ai32.h 
///	\brief GSC 18bit, 32 channel ADC Module Definitions. See  
///<	<a href="http://www.generalstandards.com/view-products2.php?BD_family=18ai32ssc">GSC 16AI64SSC Manual</a>
///< for more info on board registers.
#ifndef LIGO_GSC18AI32_H
#define LIGO_GSC18AI32_H

#include <linux/pci.h>

#define ADC_18AI32_SS_ID       0x3431  ///< Subsystem ID to identify and locate module on PCI bus

#ifdef __cplusplus
extern "C" {
#endif

// Function Prototypes
int gsc18ai32Init(CDS_HARDWARE *, struct pci_dev *);

#ifdef __cplusplus
}
#endif


// Board Control Register (BCR) bits
#define GSAF_FULL_DIFFERENTIAL  0x200
#define GSAF_AUTO_CAL           0x2000
#define GSAF_AUTO_CAL_PASS      0x4000
#define GSAF_RESET              0x8000

// Scan and Sync Control (SSC) register bits
#define GSAF_32_CHANNEL         0x5
#define GSAF_16_CHANNEL         0x4
#define GSAF_8_CHANNEL          0x3
#define GSAF_ENABLE_CLOCK       0x20
#define GSAF_ISR_ON_SAMPLE      0x3
#define GSAF_SAMPLE_START       0x10000
#define GSAF_USE_RAG_CLK        0x8

// IDBC register bits
#define GSAF_18BIT_DATA         0x100000

// ADC Read info
#define GSAF_DATA_CODE_OFFSET   0x8000
#define GSAF_DATA_MASK          0x3ffff
#define GSAF_FIRST_SAMPLE_MARK      0x80000000

#define GSC18AI32_OSC_FREQ      36000000

#endif //LIGO_GSC18AI32_H
