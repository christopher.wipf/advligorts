#ifndef LIGO_RTS_LOGGER_H_INCLUDED
#define LIGO_RTS_LOGGER_H_INCLUDED
/**
 *  @section kernel_space
 *  In kernel space the functions are exported by the rts-logger module.
 *  Real-time kernel modules only need to copy their messages into the logger
 *  module's buffer, and then can return to their work. The logger module does 
 *  the interruptible work of calling prink, handling the high latancy/jittery 
 *  portion of printing messages on a non-isolated core.
 *
 *  @section user_space
 *  In user space these are defined in rts-logger/userspace/usp-rts-logger.c
 *  We want to provide the same logging interface to user space that kernel space
 *  gets, so we define these functions in user space. The user space implementation
 *  is very simple, calling printf with the message. It is important to note that 
 *  this implementation is slower than kernel space, because the user space version
 *  actually calls printf in the model's thread.
 *
 */

#ifndef RTS_LOG_PREFIX

#if defined(FE_HEADER)
#include FE_HEADER //SYSTEM_NAME_STRING_LOWER
#define RTS_LOG_PREFIX SYSTEM_NAME_STRING_LOWER

#else //FE_HEADER not defined, so define prefix as "undefined"

#define RTS_LOG_PREFIX "undefined"

#endif //defined(FE_HEADER)

#endif //ifndef RTS_LOG_PREFIX

enum RTSLOG_LOG_LEVEL
{
    RTSLOG_LOG_LEVEL_DEBUG = 0,
    RTSLOG_LOG_LEVEL_INFO = 1, //Default log level
    RTSLOG_LOG_LEVEL_WARN = 2,
    RTSLOG_LOG_LEVEL_ERROR = 3,
    RTSLOG_LOG_LEVEL_RAW = 5
};

//
// Start user functions
//

/**
 * @usage This is the main interface that should be used to log messages
 *        from front end code infrastructure. The default log level is INFO,
 *        so you must set the logger level to DEBUG before you will see debug
 *        level messages. 
 *
 * These macros can be used just like prink or printf:
 *
 * RTSLOG_INFO("A string: %s and an int: %d\n", "Hello", 5);
 *
 */
#define RTSLOG_RAW(fmt, ...) rtslog_print(RTSLOG_LOG_LEVEL_RAW, fmt, ##__VA_ARGS__)
#define RTSLOG_DEBUG(fmt, ...) rtslog_print (RTSLOG_LOG_LEVEL_DEBUG, RTS_LOG_PREFIX ": DEBUG - " fmt, ##__VA_ARGS__)
#define RTSLOG_INFO(fmt, ...) rtslog_print (RTSLOG_LOG_LEVEL_INFO, RTS_LOG_PREFIX ": INFO - " fmt, ##__VA_ARGS__)
#define RTSLOG_WARN(fmt, ...) rtslog_print (RTSLOG_LOG_LEVEL_WARN, RTS_LOG_PREFIX ": WARN - " fmt, ##__VA_ARGS__)
#define RTSLOG_ERROR(fmt, ...) rtslog_print (RTSLOG_LOG_LEVEL_ERROR, RTS_LOG_PREFIX ": ERROR - " fmt, ##__VA_ARGS__)


//
// Debugging/Stats functions
//

#ifdef __cplusplus
extern "C" {
#endif

/**
* @brief Requests the new log level be set in the logger.
* 
* When the kernel module version is being used the sysfs interface
* can be used to set this value. 
*
* cat /sys/kernel/rts_logger/debug_level
*
* @param new_level  The new log level to set (a RTSLOG_LOG_LEVEL)
*
* @return 1 if the log level was set, 0 if the log level was invalid        
*/
int rtslog_set_log_level(int new_level);

/**
* @brief Requests the new log level be set in the logger
*
* @return The level set (a RTSLOG_LOG_LEVEL)
*/
int rtslog_get_log_level( void );

/**
* @brief Requests number of messages that have been dropped
*        due to a full buffer. This should't happen, but can
*        if too many modules are spamming messages faster than
*        they can be printed. This is an accumulative count 
*        from module insertion (kernel space), or process creation (usermode)
*
* @return The number of messages that have been dropped 
*/
int rtslog_get_num_dropped_no_space ( void );

/**
* @brief Requests number of messages that are currently 
*        queued, waiting for printing.
*
* @return The number of messages that are queued for print
*/
int rtslog_get_num_ready_to_print( void );











//
// Internal functions
//

/**
* @brief Don't call this function directly, use the macros above
*
* Passes the message to the logger for logging
*
* @param level The log level of this message
* @param fmt   The format string of the message
* @param ...   Arguments for the format string
*
* @return none
*/

void rtslog_print(int level, const char * fmt, ...);

#ifdef __cplusplus
}
#endif


#endif //LIGO_RTS_LOGGER_H_INCLUDED
