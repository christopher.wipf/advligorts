#ifndef LIGO_EPICS_XFER_H
#define LIGO_EPICS_XFER_H

/// @file epicsXfer.h
/// @brief File contains routines for:
///<        - Exchanging filter module data with EPICS shared memory
///<        - Checking for process kill command from EPICS
///<        - Ramping algorithm, used by Product.pm part.
/************************************************************************/


#include "portableInline.h" //LIGO_INLINE
#include "fm10Gen_types.h" 
#include "fm10Gen.h" //checkFiltReset()
//
// Start function declarations
//
int gainRamp( float gainReq, int rampTime, int id, float* gain, int gainRate);






//
// Start Inline functions
//



///	Function to exchange filter module data with EPICS via shared memory.
///	@param[in] subcycle		Filter Module ID for data transfer.
///	@param[in] *dsp			Pointer to process memory fm data.
///	@param[in] *pDsp		Pointer to shared memory fm data.
///	@param[in] *dspCoeff		Pointer to process memory fm coeff data.
///	@param[in] *pCoeff		Pointer to shared memory fm coeff data.
/************************************************************************/
LIGO_INLINE void
updateEpics( int       subcycle,
             FILT_MOD* dsp,
             FILT_MOD* pDsp,
             COEF*     dspCoeff,
             VME_COEF* pCoeff )
{
    int ii;

    ii = subcycle;
    if ( ( ii >= 0 ) && ( ii < MAX_MODULES ) )
    {
        checkFiltReset( ii, dsp, pDsp, dspCoeff, MAX_MODULES, pCoeff );
        // dsp->inputs[ii].opSwitchE = pDsp->inputs[ii].opSwitchE;
        pDsp->data[ ii ].filterInput = dsp->data[ ii ].filterInput;
        pDsp->data[ ii ].exciteInput = dsp->data[ ii ].exciteInput;
        pDsp->data[ ii ].output16Hz = dsp->data[ ii ].output16Hz;
        pDsp->data[ ii ].output = dsp->data[ ii ].output;
        pDsp->data[ ii ].testpoint = dsp->data[ ii ].testpoint;
        pDsp->data[ ii ].swStatus = dsp->data[ ii ].swStatus;
        pDsp->inputs[ ii ].opSwitchP = dsp->inputs[ ii ].opSwitchP;
        // dsp->inputs[ii].limiter = pDsp->inputs[ii].limiter;
        pDsp->inputs[ ii ].mask = dsp->inputs[ ii ].mask;
        pDsp->inputs[ ii ].control = dsp->inputs[ ii ].control;
    }
}


LIGO_INLINE void
updateFmSetpoints( FILT_MOD* dsp,
                   FILT_MOD* pDsp,
                   COEF*     dspCoeff,
                   VME_COEF* pCoeff )
{
    int ii;

    for ( ii = 0; ii < MAX_MODULES; ii++ )
    {
        dsp->inputs[ ii ].opSwitchE = pDsp->inputs[ ii ].opSwitchE;
        dsp->inputs[ ii ].limiter = pDsp->inputs[ ii ].limiter;
        if ( dsp->inputs[ ii ].mask & 0x20000000 )
        { /* Offset controlled by the FE */
            pDsp->inputs[ ii ].offset = dsp->inputs[ ii ].offset;
        }
        else
        {
            dsp->inputs[ ii ].offset = pDsp->inputs[ ii ].offset;
        }
        if ( dsp->inputs[ ii ].mask & 0x40000000 )
        { /* Gain controlled by the FE */
            pDsp->inputs[ ii ].outgain = dsp->inputs[ ii ].outgain;
        }
        else
        {
            dsp->inputs[ ii ].outgain = pDsp->inputs[ ii ].outgain;
        }
        if ( dsp->inputs[ ii ].mask & 0x80000000 )
        { /* Ramp time controlled by the FE */
            pDsp->inputs[ ii ].gain_ramp_time =
                dsp->inputs[ ii ].gain_ramp_time;
        }
        else
        {
            dsp->inputs[ ii ].gain_ramp_time =
                pDsp->inputs[ ii ].gain_ramp_time;
        }
    }
}

/// Check for process stop command from EPICS
///	@param[in] subcycle	Present code cycle
///	@param[in] *plocalEpics	Pointer to EPICS data in shared memory
LIGO_INLINE int
checkEpicsReset( CDS_EPICS* plocalEpics )
{
    if ( plocalEpics->epicsInput.vmeReset )
    {
        return ( 1 );
    }

    return ( 0 );
}


#endif //LIGO_EPICS_XFER_H

