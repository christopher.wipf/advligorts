#ifndef GPSCLOCK_INCLUDED
#define GPSCLOCK_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

// gpsclock_init: initialize library (return 0 if successful)
// currently this just calls gpsclock_offset
int gpsclock_init(void);

// gpsclock_offset: store difference between GPS and system time in offset (return 0 if successful)
int gpsclock_offset(long *offset);

// gpsclock_timestring: print gps time into string s with length size (return s)
// calls gpsclock_init if it has not already been called
char *gpsclock_timestring(char *s, int size);

// gpsclock_time: store gps time in req
// req[0] : seconds
// req[1] : microseconds
// req[2] : nanoseconds
// calls gpsclock_init if it has not already been called
void gpsclock_time(unsigned long *req);

#ifdef __cplusplus
}
#endif

#endif // GPSCLOCK_INCLUDED
