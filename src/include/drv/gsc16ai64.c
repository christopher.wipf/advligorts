///	\file gsc16ai64.c
///	\brief File contains the initialization routine and various register
/// read/write
///<		operations for the General Standards 16bit, 32 channel ADC
///< modules. \n
///< For board info, see
///<    <a
///<    href="http://www.generalstandards.com/view-products2.php?BD_family=16ai64ssc">GSC
///<    16AI64SSC Manual</a>

#include "gsc16ai64.h"
#include "gsc_adc_common.h"
#include "ioremap_selection.h"
#include "drv/plx_9056.h" //adcDma
#include "drv/rts-logger.h"

#include <linux/pci.h> //PCI_BASE_ADDRESS_0
#include <linux/delay.h> //udelay()


// *****************************************************************************
/// \brief Routine to initialize GSC 16bit, 32 channel ADC modules
///     @param[in,out] *pHardware Pointer to global data structure for storing
///     I/O
///<            register mapping information.
///     @param[in] *adcdev PCI address information passed by the mapping code in
///     map.c
///	@return Status from board enable command.
// *****************************************************************************
int
gsc16ai64Init( CDS_HARDWARE* pHardware, struct pci_dev* adcdev )
{
    static unsigned int pci_io_addr; /// @param pci_io_addr Bus address of PCI
                                     /// card I/O register.
    int devNum; /// @param devNum Index into CDS_HARDWARE struct for adding
                /// board info.
    char* _adc_add; /// @param *_adc_add ADC register address space
    int   pedStatus; /// @param pedStatus Status return from call to enable
                   /// device.
    int autocal = 0;

    /// Get index into CDS_HARDWARE struct based on total number of ADC cards
    /// found by mapping routine in map.c
    devNum = pHardware->adcCount;
    /// Enable the module.
    pedStatus = pci_enable_device( adcdev );
    /// Enable device to be DMA master.
    pci_set_master( adcdev );
    /// Get the PLX chip address
    pedStatus = pci_read_config_dword( adcdev, PCI_BASE_ADDRESS_0, &pci_io_addr );
#ifdef MAP_TEST
    if (devNum == 2)
        pedStatus = -22;
#endif
    if(pedStatus != 0)
        return -1;
    RTSLOG_INFO( "pci0 = 0x%x\n", pci_io_addr );
    /// Map module DMA space directly to computer memory space.
    _adc_add = IOREMAP( (unsigned long)pci_io_addr, 0x200 );
    /// Map the module DMA control registers via PLX chip registers
    adcDma[ devNum ] = (PLX_9056_DMA*)_adc_add;

    /// Get the ADC register address
    pedStatus = pci_read_config_dword( adcdev, PCI_BASE_ADDRESS_2, &pci_io_addr );
    if(pedStatus != 0)
        return -1;
    RTSLOG_INFO( "pci2 = 0x%x\n", pci_io_addr );
    /// Map the module control register so local memory space.
    _adc_add = IOREMAP( (unsigned long)pci_io_addr, 0x200 );
    RTSLOG_INFO( "ADC I/O address=0x%x  0x%lx\n", pci_io_addr, (long)_adc_add );
    /// Set global ptr to control register memory space.
    _adcPtr[ devNum ] = (GSA_ADC_REG*)_adc_add;

    RTSLOG_INFO( "BCR = 0x%x\n", _adcPtr[ devNum ]->BCR );
    /// Reset the ADC board
    _adcPtr[ devNum ]->BCR |= GSAI_RESET;
    do
    {
    } while ( ( _adcPtr[ devNum ]->BCR & GSAI_RESET ) != 0 );

    /// Write in a sync word
    _adcPtr[ devNum ]->SMUW = 0x0000;
    _adcPtr[ devNum ]->SMLW = 0x0000;

    /// Set ADC to 64 channel = 32 differential channels
    _adcPtr[ devNum ]->BCR |= ( GSAI_FULL_DIFFERENTIAL );

#ifdef ADC_EXTERNAL_SYNC
    _adcPtr[ devNum ]->BCR |= ( GSAI_ENABLE_X_SYNC );
    _adcPtr[ devNum ]->AUX_SIO |= 0x80;
#endif

    /// Set ADC internal sample rate generator
    /// This is used for Cymac using ADC internal clock
    _adcPtr[ devNum ]->RAG =
        (unsigned int)( GSC16AI64_OSC_FREQ / ( UNDERSAMPLE * IOP_IO_RATE ) );
    RTSLOG_INFO( "RAG = %d \n", _adcPtr[ devNum ]->RAG );
    RTSLOG_INFO( "BCR = 0x%x\n", _adcPtr[ devNum ]->BCR );
    _adcPtr[ devNum ]->RAG &= ~( GSAI_SAMPLE_START );
    /// Initiate board calibration
    _adcPtr[ devNum ]->BCR |= GSAI_AUTO_CAL;
    /// Wait for internal calibration to complete.
    do
    {
        autocal++;
        udelay( 100 );
    } while ( ( _adcPtr[ devNum ]->BCR & GSAI_AUTO_CAL ) != 0 );
    if ( ( _adcPtr[ devNum ]->BCR & GSAI_AUTO_CAL_PASS ) == 0 )
    {
        RTSLOG_ERROR( "GSC_16AI64SSA : devNum %d : Took %d ms : ADC AUTOCAL FAIL\n", 
                      devNum, autocal/10 );
        autocal = 0;
    }
    else
    {
        RTSLOG_INFO( "GSC_16AI64SSA : devNum %d : Took %d ms : ADC AUTOCAL PASS\n", 
                     devNum, autocal/10 );
        autocal = GSAI_AUTO_CAL_PASS;
    }
    _adcPtr[ devNum ]->RAG |= GSAI_SAMPLE_START;
    _adcPtr[ devNum ]->IDBC = ( GSAI_CLEAR_BUFFER | GSAI_THRESHOLD );
#ifdef USE_ADC_CLOCK
    _adcPtr[ devNum ]->SSC = ( GSAI_64_CHANNEL );
#else
    _adcPtr[ devNum ]->SSC = ( GSAI_64_CHANNEL | GSAI_EXTERNAL_SYNC );
#endif
    RTSLOG_INFO( "SSC = 0x%x\n", _adcPtr[ devNum ]->SSC );
    RTSLOG_INFO( "IDBC = 0x%x\n", _adcPtr[ devNum ]->IDBC );
    /// Fill in CDS_HARDWARE structure with ADC information.
    pHardware->pci_adc[ devNum ] =
        (volatile int *)dma_alloc_coherent( &adcdev->dev, 0x2000, &adc_dma_handle[ devNum ], GFP_ATOMIC );
    pHardware->adcType[ devNum ] = GSC_16AI64SSA;
    pHardware->adcDuoToneDivisor[ devNum ] = 1;
    pHardware->adcInstance[ devNum ] = pHardware->card_count[ GSC_16AI64SSA ];
    pHardware->card_count[ GSC_16AI64SSA ] ++;
    pHardware->adcChannels[ devNum ] = GSAI_CHAN_COUNT;
    pHardware->adcConfig[ devNum ] = _adcPtr[ devNum ]->ASSC;
    pHardware->adcConfig[ devNum ] |= autocal;
    pHardware->adcCount++;
    /// Return board enable status.
    return ( pedStatus );
}

// *****************************************************************************
/// \brief Function stops ADC acquisition by removing the clocking signal.
// *****************************************************************************
void gsc16ai64AdcStop( int modNum )
{
    _adcPtr[ modNum ]->BCR &= ~( GSAI_ENABLE_X_SYNC );
}

#ifdef DIAG_TEST
// *****************************************************************************
/// \brief Test Code
///< For DIAGS ONLY !!!!!!!!
///< This will change ADC DMA BYTE count

///< -- Greater than normal will result in channel hopping.

///< -- Less than normal will result in ADC timeout.

///< In both cases, real-time kernel code should exit with errors to dmesg
// *****************************************************************************
void
gsc16ai64DmaBump( int modNum, int bump )
{
    adcDma[ modNum ]->DMA0_BTC = GSAI_DMA_BYTE_COUNT + bump;
}
#endif
