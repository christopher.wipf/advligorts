#ifndef LIGO_CONTEC32O_H
#define LIGO_CONTEC32O_H

#include "cdsHardware.h"

#include <linux/pci.h>

// Contec card model ID
#define C_DO_32L_PE 0x86E2


#ifdef __cplusplus
extern "C" {
#endif

int contec32OutInit( CDS_HARDWARE* pHardware, struct pci_dev* diodev );
unsigned int contec32WriteOutputRegister( CDS_HARDWARE* pHardware, int modNum, unsigned int data );
unsigned int contec32ReadOutputRegister( CDS_HARDWARE* pHardware, int modNum );

#ifdef __cplusplus
}
#endif




#endif //LIGO_CONTEC32O_H
