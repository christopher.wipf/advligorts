#ifndef __GDS_ADC_COMMON_H__
#define __GDS_ADC_COMMON_H__

/// @file gsc_adc_common.h
/// @brief Defines standard ADC card register map and defines.

#include "drv/cdsHardware.h" //CDS_HARDWARE
#include "util/fixed_width_types.h"

#ifdef __cplusplus
extern "C" {
#endif

// Prototypes for routines found in gsc_adc_common.c
int gscCheckAdcBuffer( int );
void gscDisableAdcClk( CDS_HARDWARE* , int );
void gscEnableAdcModule( CDS_HARDWARE* , int , int );

#ifdef __cplusplus
}
#endif


// Standard ADC register map
typedef struct GSA_ADC_REG{
        u32 BCR;       ///< Board Control Register at 0x0  
        u32 INTCR;     ///< Interrupt Control Register at 0x4
        u32 IDB;       ///< Input Data Buffer at 0x8 
        u32 IDBC;      ///< Input Buffer Control at 0xc */
        u32 RAG;       ///< Rate A Generator at 0x10 
        u32 RBG;       ///< Rate B Generator at 0x14 
        u32 BUF_SIZE;  ///< Buffer Size at 0x18
        u32 BRT_SIZE;  ///< Burst Size at 0x1c 
        u32 SSC;       ///< Scan and Sync Control at 0x20 
        u32 ACA;       ///< Active Channel Assignment Register at 0x24
        u32 ASSC;      ///< Board Configuration Register at 0x28
        u32 AC_VAL;    ///< Autocal Values register at 0x2c
        u32 AUX_RWR;   ///< Auxillary Read/Write Register at 0x30
        u32 AUX_SIO;   ///< Auxillary Sync Control Register at 0x34
        u32 SMUW;      ///< Scan Marker Upper Word 0x38
        u32 SMLW;      ///< Scan Maker Lower Word at 0x3c
}GSA_ADC_REG;

// Global pointer to ADC register map
extern volatile GSA_ADC_REG* _adcPtr[ MAX_ADC_MODULES ]; ///< Ptr to ADC registers */

#define GSAI_CLEAR_BUFFER   0x40000
#define GSAI_THRESHOLD      0x001f
#define GSAI_ALL_CARDS      0xf
#define GSAI_DMA_DEMAND_MODE 0x80000
#define GSAI_NO_DMA         0x0
#define GSAI_DMA_MODE         0x1
#define GSAI_SAMPLE_START 0x10000
// #define GSAF_FIRST_SAMPLE_MARK      0x80000000
#define GSAI_FIRST_SAMPLE_MARK 0x10000


#define GSAI_18BIT_DATA         0xf
#define GSAI_ENABLE_X_SYNC      0x80
#define GSAI_CLOCK_ENABLE       0x800
#define GSAI_ENABLE_CLOCK       0x20


#define GSAI_SAMPLE_SZ       4


#endif // __GDS_ADC_COMMON_H__

