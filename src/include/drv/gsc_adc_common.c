/// @file gsc_adc_common.c
/// @brief File contains routines common to all ADC modules

#include "gsc_adc_common.h"
#include "drv/plx_9056.h"

//
// Global Data
// This is used by common ADC code (gsc_adc_common.c) and a lot 
// of specific drivers. (gsc18ai64.c, gsc16ai64.c etc...) 
//
volatile GSA_ADC_REG* _adcPtr[ MAX_ADC_MODULES ]; ///< Ptr to ADC registers */

// *****************************************************************************
/// \brief Routine reads number of samples in ADC FIFO.
/// @param[in] numAdc The ID number of the ADC module to read.
/// @return The number of samples presently in the ADC FIFO.
// *****************************************************************************
int
gscCheckAdcBuffer( int numAdc )
{
    int dataCount;

    dataCount = _adcPtr[ numAdc ]->BUF_SIZE;
    return ( dataCount );
}

// *****************************************************************************
/// \brief Routine disables external clock mode on selected ADC 
/// @param[in] card The ID number of the ADC module to write.
// *****************************************************************************
void
gscDisableAdcClk( CDS_HARDWARE* pHardware, int card )
{
    int ii;
    int fc, lc;

    if ( card == GSAI_ALL_CARDS )
    {
        fc = 0;
        lc = pHardware->adcCount;
    }
    else
    {
        fc = card;
        lc = card + 1;
    }
    for ( ii = fc; ii < lc; ii++ )
    {
        if ( pHardware->adcType[ ii ] == GSC_16AI64SSA )
            _adcPtr[ ii ]->BCR &= ~( GSAI_ENABLE_X_SYNC );
        if ( pHardware->adcType[ ii ] == GSC_18AI32SSC1M )
            _adcPtr[ ii ]->SSC &= ~( GSAI_ENABLE_CLOCK );
        if ( pHardware->adcType[ ii ] == GSC_18AI64SSC )
            _adcPtr[ ii ]->SSC &= ~( GSAI_CLOCK_ENABLE );
        _adcPtr[ ii ]->IDBC |= ( GSAI_CLEAR_BUFFER | GSAI_THRESHOLD );
    }
}

// *****************************************************************************
/// \brief Function clears ADC buffer and starts acquisition via external clock.
///< Also sets up ADC for Demand DMA mode and set GO bit in DMA Mode Register
///< if dmamode != 0;
///< from the timing receiver are turned OFF ie during initialization process.
/// @param[in] card Id number of card to set. 
/// @dmamode[in] Enable DMA mode?. 
// *****************************************************************************
void
gscEnableAdcModule( CDS_HARDWARE* pHardware, int card, int dmamode )
{
    int ii;
    int fc, lc;

    if ( card == GSAI_ALL_CARDS )
    {
        fc = 0;
        lc = pHardware->adcCount;
    }
    else
    {
        fc = card;
        lc = card + 1;
    }

    for ( ii = fc; ii < lc; ii++ )
    {
        /// Clear the FIFO and set demand DMA to start after all 32 channels
        /// have 1 sample
        _adcPtr[ ii ]->IDBC |= ( GSAI_CLEAR_BUFFER | GSAI_THRESHOLD );
        // Normal operation is to enable DMA mode. 
        // Not setting DMA mode is used in some test routines.
        if ( dmamode )
        {
            /// Enable demand DMA mode ie auto DMA data to computer memory when
            ///< GSAI_THRESHOLD data points in ADC FIFO.
            if ( pHardware->adcType[ ii ] == GSC_16AI64SSA )
                _adcPtr[ ii ]->BCR &= ~( GSAI_DMA_DEMAND_MODE );
            /// Set DMA mode and direction in PLX controller chip on module.
            /// Enable DMA
            plx9056_adc_dma_enable( ii );
        }
        /// Do ADC model type specific setup
        if ( pHardware->adcType[ ii ] == GSC_18AI32SSC1M )
        {
            /// Enable 18bit data
            _adcPtr[ ii ]->IDBC |= GSAI_18BIT_DATA;
#ifdef USE_ADC_CLOCK
            // Start internal clock generator
            _adcPtr[ ii ]->RAG &= ~( GSAI_SAMPLE_START ); //  0x10000
#endif
            /// Enable sync via external clock input.
            _adcPtr[ ii ]->SSC |= GSAI_ENABLE_CLOCK;
        }
        // 
        if ( pHardware->adcType[ ii ] == GSC_16AI64SSA )
        {
#ifdef USE_ADC_CLOCK
            // Start the internal clock generator
            _adcPtr[ ii ]->RAG &= ~( GSAI_SAMPLE_START );
#else
            /// Enable sync via external clock input.
            _adcPtr[ ii ]->BCR |= GSAI_ENABLE_X_SYNC;
#endif
        }
        //
        if ( pHardware->adcType[ ii ] == GSC_18AI64SSC )
            /// Enable sync via external clock input.
            _adcPtr[ ii ]->SSC |= GSAI_CLOCK_ENABLE;
    }
}
