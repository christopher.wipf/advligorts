#ifndef LIGO_FW_UTILS_H
#define LIGO_FW_UTILS_H

#include <linux/io.h>
#include <linux/slab.h>


#define COMMON_CPURATE_KHZ ((uint64_t)(cpu_khz))

// Convert a number of ticks to the equivalent duration in ns.
//Math here is: (ticks * 1e9) / 2^32 reduced.
// 2^32 * 1953125 cannot overflow uint64_t
#define convert_4GHz_to_ns( ticks ) \
    (((uint64_t)(ticks)) * 1953125) / (1ULL<<23)

#define convert_ns_to_4GHz( ns ) \
    ((((uint64_t)(ns)) * (1ULL<<23)) / 1953125)

#define GET_NANO_FROM_GPS_NS( gps_time_ns ) \
    (gps_time_ns % 1000000000)

#define GET_SEC_FROM_GPS_NS( gps_time_ns ) \
    (gps_time_ns / 1000000000)


// Returns the nth byte in the num passed as the first parameter
// The byte parameter is 0 indexed so requesting the 0th byte 
// will return the least significant byte, 1 the next more 
// significant byte and so on.
#define GET_BYTE(num, byte) \
    ( (num >> (byte*8)) & 0xFF )

#define GET_SHORT(num, short_p) \
    (  (num >> (short_p*16)) & 0xFFFF )

#define BIT_TO_MASK(index) \
    ( 1<<index )

#define GET_BIT(num, index) \
    ( (num & BIT_TO_MASK(index)) >> index )

#define BAR0_INDX 0
#define BAR2_INDX 2

static inline void setBits(uint32_t bits, void __iomem * mem )
{
    uint32_t hold = ioread32(mem);
    iowrite32(hold | bits, mem);
}

static inline void setBitIndex(uint32_t index, void __iomem * mem )
{
    return setBits( BIT_TO_MASK(index), mem );
}

static inline void clearBits(uint32_t bits, void __iomem * mem )
{
    uint32_t hold = ioread32(mem);
    iowrite32(hold & (~bits), mem);
}

static inline void clearBitIndex(uint32_t index, void __iomem * mem )
{
    return clearBits( BIT_TO_MASK(index), mem );
}

// Round to next line
static inline uint32_t round_to_line(uint32_t size, uint32_t line)
{
    return line * ((size + line - 1) / line);
}



#endif //LIGO_FW_UTILS_H