/// 	\file accesIIRO8.h
///	\brief ACCESS PCI-IIRO-8 relay digital input and output defs. See 
///<	<a href="http://accesio.com/go.cgi?p=../pcie/pcie-iiro-8.html">PCIe-IIRO-8 Manual</a>
///< for more info on board registers.
#ifndef LIGO_ACCESIIRO8_H
#define LIGO_ACCESIIRO8_H


#include "drv/cdsHardware.h" //CDS_HARDWARE

#include <linux/pci.h> // struct pci_dev 


#define ACC_IIRO_TID_OLD 0x0f00
#define ACC_IIRO_TID 0x0f02

#define IIRO_DIO8_INPUT  0x1
#define IIRO_DIO8_OUTPUT 0x0

#ifdef __cplusplus
extern "C" {
#endif

int accesIiro8Init( CDS_HARDWARE* pHardware, struct pci_dev* diodev );
unsigned int accesIiro8ReadInputRegister( CDS_HARDWARE* pHardware, int modNum );
unsigned int accesIiro8ReadOutputRegister( CDS_HARDWARE* pHardware, int modNum );
void accesIiro8WriteOutputRegister( CDS_HARDWARE* pHardware, int modNum, int data );

#ifdef __cplusplus
}
#endif


#endif //LIGO_ACCESIIRO8_H
