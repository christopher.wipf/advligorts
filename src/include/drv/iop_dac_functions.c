#include "drv/iop_dac_functions.h" 
#include "drv/gsc_adc_common.h"
#include "gsc_dac_common.h" //_dacPtr
#include "drv/gsc20ao8.h" //GSAO_20BIT_PRELOAD
#include "drv/gsc16ao16.h" //GSAO_16BIT_PRELOAD
#include "drv/gsc18ao8.h" //GSAO_18BIT_PRELOAD
#include "controller.h"
#include "drv/plx_9056.h"
#include "drv/ligo28ao32/ligo28ao32.h"
#include "drv/rts-logger.h"
#include "drv/ligoPcieTiming.h"
#include "util/timing.h"
#include "util/ligo_mfence.h"

#include "../fe/controllerIop.h"//ioMemCntrDac

#include <linux/delay.h> //udelay()

//
// This parameter controls how long an IOP should wait for slow
// user model DAC data, as a ratio of the IOP's processing time.
//
// As an example, a 65536 Hz IOP (~15us processing window) would
// wait up until 0.7 * 15258 ns = 10681 ns for a slow user model's
// DAC data. 
//
// The logic takes into account how long the IOPs cycle has already
// taken, and won't wait longer than the allocated cycle time. In
// other words, this allows fast IOPs to wait longer into their
// allocated cycle time for data.
//
#define MULT_HEADROOM_TO_WAIT 0.7
#define MAX_TIME_TO_WAIT_NS (((((1.0/MODEL_RATE_HZ)*1000000)) * 1000) * MULT_HEADROOM_TO_WAIT)


// This parameter controls the number of cycles (at the IOP rate) that 
// the IOP will repeat the last sample, waiting for the control model
// to start writing the channel again. 
// The IOP will still report an error when any sample is missed, and a 
// sticky DAC error will be set, this just controls how many cycles we 
// should wait until we set the DAC chan output to 0.0
#define NUM_CYCLES_GRACE_PERIOD 2

static double last_dac_out_cache[MAX_IO_MODULES][MAX_DAC_CHN_PER_MOD];
static double last_tx_was_good[MAX_IO_MODULES][MAX_DAC_CHN_PER_MOD] = {{0,},{0,},};


int
iop_dac_init( int errorPend[] )
{
    int ii, jj;
    int status;

    /// \> Zero out DAC outputs
    for ( ii = 0; ii < MAX_DAC_MODULES; ii++ )
    {
        errorPend[ ii ] = 0;
        for ( jj = 0; jj < MAX_DAC_CHN_PER_MOD; jj++ )
        {
            dacOut[ ii ][ jj ] = 0.0;
            dacOutUsed[ ii ][ jj ] = 0;
            dacOutBufSize[ ii ] = 0;
            // Zero out DAC channel map in the shared memory
            // to be used to check on control apps' channel allocation
            ioMemData->dacOutUsed[ ii ][ jj ] = 0;
            ioMemData->dacChanWritten[ ii ][ jj ] = 0;
        }
    }

    for ( jj = 0; jj < cdsPciModules.dacCount; jj++ )
    {
        pLocalEpics->epicsOutput.statDac[ jj ] = DAC_FOUND_BIT;
        status = cdsPciModules.dacAcr[ jj ] & DAC_CAL_PASS;
        if(status)  pLocalEpics->epicsOutput.statDac[ jj ] |= DAC_AUTOCAL_BIT;
        // Arm DAC DMA for full data size
        if ( cdsPciModules.dac_info[ jj ].card_type == GSC_16AO16 )
        {
            plx9056_dac_16_dma_setup( jj );
        }
        else if ( cdsPciModules.dac_info[ jj ].card_type == LIGO_28AO32 ) 
        {
            ligo28ao32_dma_stop_dacs(cdsPciModules.dacDrivers[jj]);
            ligo28ao32_dma_clear_dac_error_ctr(cdsPciModules.dacDrivers[jj]);

            //Configure the DAC DMA and check the return status
            ligo28ao32_dma_config(cdsPciModules.dacDrivers[jj], 65536, 65536);

            if ( !ligo28ao32_is_dac_config_valid(cdsPciModules.dacDrivers[jj]) ) 
            { 
                RTSLOG_ERROR("DAC config: (is_valid: %u).\n",
                ligo28ao32_is_dac_config_valid(cdsPciModules.dacDrivers[jj]));
                return -1;
            }

        }
        else
        {
            plx9056_dac_1820_dma_setup( jj );
        }
    }

    return 0;
}

void
iop_dac_cleanup( void ) 
{
    //Clean up the drivers we have for any DACs
    for ( unsigned cur_dac = 0; cur_dac < cdsPciModules.dacCount; ++cur_dac )
    {
        if( cdsPciModules.dacDrivers[ cur_dac ] != NULL )
        {
            ligo28ao32_dma_stop_dacs( cdsPciModules.dacDrivers[ cur_dac ] );
            udelay(MAX_UDELAY);
            udelay(MAX_UDELAY);
        }
    }
}

void
iop_dac_preload( int card )
{
    int fc = 0;
    int lc = 0;
    int                         ii, jj;

     if ( card == GSAI_ALL_CARDS )
    {
        fc = 0;
        lc = cdsPciModules.dacCount;
    }
    else
    {
        fc = card;
        lc = card + 1;
    }

    for ( jj = fc; jj < lc; jj++ )
    {
        if ( cdsPciModules.dac_info[ jj ].card_type == GSC_18AO8 )
        {
            for ( ii = 0; ii < GSAO_18BIT_PRELOAD; ii++ )
                _dacPtr[ jj ]->OUTPUT_BUF = 0;
        }
        else if ( cdsPciModules.dac_info[ jj ].card_type == GSC_20AO8 )
        {
            for ( ii = 0; ii < GSAO_20BIT_PRELOAD; ii++ )
                _dacPtr[ jj ]->OUTPUT_BUF = 0;
        }
        else if ( cdsPciModules.dac_info[ jj ].card_type == LIGO_28AO32 ) 
        {
            //Not preload for this DAC type
        }
        else
        {
            for ( ii = 0; ii < GSAO_16BIT_PRELOAD; ii++ )
                _dacPtr[ jj ]->ODB = 0;
        }
    }
}

/**
 * @param cpuClock[] Passes in the timing state of the model, so this function can know
 *                   how long we are able to wait for late DAC data from control models.
 *
 * Static Vars  
 * dacWriteEnable - Used to skip the DMA calls on the first DAC_START_CYCLE+1 calls
 *                  to this function, because the cycles can be slow when first starting. 
 *
 * Global Inputs
 * cycleNum - The model's cycle counter. 
 * timeSec  - The current GPS seconds of the model.
 * iopDacEnable - When > 0 signifies that the DACs are enabled. When 0 all DAC output data will
 *                be zeroed. 
 *                
 *
 *
 * Global Outputs
 * dacEnable - Is used to set a <FEC-NUM>_DAC_MASTER_STAT EPICS channel. DACs are mapped into bits
 *             [DAC31,...,DAC1,DAC0]] -> [bit31,...,bit1,bit0], when the bit is set the DAC has at
 *             least one channel being driven by a control model. (Does not appear to be used on any MEDMs)
 * dacChanErr - Stores the sum of the number od DAC TX errors, per card. Reset once a second.
 *
 * @return 0 if all expected DAC data was ready from user models and transmitted to the DAC cards.
 *         1 if any of the expected data was not transmitted to the DAC cards.
 *         When 1 the RT model will have a sticky DAC error set by the controller.
 *
*/

int iop_dac_write( uint64_t cpuClock[] )
{
    static uint32_t dacWriteEnable = 0; //Counts up to DAC_START_CYCLE before tr
    volatile uint32_t* pDacData;
    int           mm;
    int           dac_write_error = 0;
    int           card = 0;
    int           chan = 0;
    int           dac_out = 0;


    /// START OF IOP DAC WRITE ***************************************** \n
    /// \> If DAC FIFO error, always output zero to DAC modules. \n
    /// - -- Code will require restart to clear.
    // COMMENT OUT NEX LINE FOR TEST STAND w/bad DAC cards.
    /// \> Loop thru all DAC modules
    
    if ( dacWriteEnable > DAC_START_CYCLE )
    {
        for ( card = 0; card < cdsPciModules.dacCount; card++ )
        {
            if ( cdsPciModules.dacDrivers[ card ] != NULL) {

                pDacData = ligo28ao32_get_next_dac_dma_queue(cdsPciModules.dacDrivers[ card ], 
                           timeSec, cycleNum/UNDERSAMPLE); 
            }
            else {
                /// - -- Point to DAC memory buffer
                pDacData = (volatile unsigned int*)( cdsPciModules.pci_dac[ card ] );
            }

            /// - -- locate the proper DAC memory block
            mm = cdsPciModules.dacConfig[ card ];


            /// - -- For each DAC channel
            for ( chan = 0; chan < cdsPciModules.dac_info[ card ].num_chans; chan++ )
            {

                /// Wait for the channels data to be set with the correct cycle count by control app.
                while ( last_tx_was_good[ mm ][ chan ] == NUM_CYCLES_GRACE_PERIOD && //Only wait if last cycle was good
                        ioMemData->dacChanWritten[ card ][ chan ] && //Only if this chan is being driven
                        ioMemData->outputData[ mm ][ ioMemCntrDac ].channel_set[chan].cycle_65k != ioClockDac &&
                        timer_tock_ns(&cpuClock[CPU_TIME_USR_START]) < MAX_TIME_TO_WAIT_NS) 
                {} 



                /// - ---- Read DAC output value from shared memory and reset
                /// memory to zero
                if( ioMemData->outputData[ mm ][ ioMemCntrDac ].channel_set[chan].cycle_65k == ioClockDac )
                {
                    ligo_smp_rmb(); //HW memory barrier, that guarantees read of cycle will be done before we read data
                    dac_out = ioMemData->outputData[ mm ][ ioMemCntrDac ].channel_set[ chan ].data;
                    //Save last good TXed value
                    last_dac_out_cache[ mm ][ chan ] = 
                        ioMemData->outputData[ mm ][ ioMemCntrDac ].channel_set[ chan ].data;


                    last_tx_was_good[ mm ][ chan ] = NUM_CYCLES_GRACE_PERIOD;
                    dacEnable |= pBits[ card ];

                }
                else if (last_tx_was_good[ mm ][ chan ]) {

                    //We hit this else in two cases:
                    //1. The control app driving this chan is exiting
                    //2. The control app was so late we missed its sample
                    
                    if ( ioMemData->dacChanWritten[ card ][ chan ] ) { //The CM is still writing this channel, but its data did not get here
                        //RTSLOG_WARN("Missed an expected DAC sample, DAC card: %d, chan %d, time_sec: %d, cycle: %d\n", 
                        //card, chan, timeSec, ioClockDac);
                        --last_tx_was_good[ mm ][ chan ];
                        dac_out = last_dac_out_cache[ mm ][ chan ];

                        dac_write_error = 1;
                        dacChanErr[ card ] += 1; //Causes the watchdog on the DAC mon to go red
                        dacEnable &= ~( pBits[ card ] );
                    }
                    else { //CM is/has exited, just zero out the output
                        last_tx_was_good[ mm ][ chan ] = 0;
                        dac_out = 0;
                    }
                }
                else { //Case where chan is not being driven by anyone
                    dac_out = 0; 
                }


                /// - ----  Write out ADC duotone signal to first DAC, last
                /// channel, if DAC duotone is enabled.
                if ( ( dt_diag.dacDuoEnable ) && ( chan == ( cdsPciModules.dac_info[ card ].num_chans - 1 ) ) &&
                     ( card == 0 ) )
                {
                    dac_out = adcinfo.adcData[ 0 ][ ADC_DUOTONE_CHAN ]
                        * cdsPciModules.dac_info[0].duoToneMultiplier / cdsPciModules.adcDuoToneDivisor[0];
                }
// Code below is only for use in DAC test system.
#ifdef DIAG_TEST
                if ( ( chan == 0 ) && ( card < 3 ) )
                {
                    if ( cycleNum < 800 )
                        dac_out = cdsPciModules.dac_info[ card ].sample_limit / 20;
                    else
                        dac_out = 0;
                }
#endif
                /// - ---- Check output values are within range of DAC \n
                /// - --------- If overflow, clip at DAC limits and report
                /// errors
                if ( dac_out >  cdsPciModules.dac_info[ card ].sample_limit  || 
                     dac_out < - cdsPciModules.dac_info[ card ].sample_limit  )
                {
                    dacinfo.overflowDac[ card ][ chan ]++;
                    pLocalEpics->epicsOutput.overflowDacAcc[ card ][ chan ]++;
                    overflowAcc++;
                    dacinfo.dacOF[ card ] = 1;
                    odcStateWord |= ODC_DAC_OVF;
                    
                    if ( dac_out >  cdsPciModules.dac_info[ card ].sample_limit  )
                        dac_out =  cdsPciModules.dac_info[ card ].sample_limit ;
                    else
                        dac_out = - cdsPciModules.dac_info[ card ].sample_limit ;
                }
                /// - ---- If DACKILL tripped, set output to zero.
                if ( !iopDacEnable )
                    dac_out = 0;
                /// - ---- Load last values to EPICS channels for monitoring on
                /// GDS_TP screen.
                dacOutEpics[ card ][ chan ] = dac_out;

                /// - ---- Load DAC testpoints
                floatDacOut[ MAX_DAC_CHN_PER_MOD * card + chan ] = dac_out;

                /// - ---- Write to DAC local memory area, for later xmit to DAC
                /// module
                *pDacData = (unsigned int)( dac_out & cdsPciModules.dac_info[ card ].sample_mask  );
                pDacData++;

                /// Forces control apps to mark this cycle or the IOP will be able
                /// to detect the channel is no longer being written to.
                ioMemData->outputData[ mm ][ ioMemCntrDac ].channel_set[ chan ].cycle_65k = -1;

            } // for ( each DAC chan )



            // For all non LIGO DACs, trigger the DMA
            if ( cdsPciModules.dacDrivers[ card ] == NULL) { 
                plx9056_dac_dma_start( card );
            }

        } //for (each DAC)
    }



#ifdef USE_ADC_CLOCK
    //TODO: What to do about LIGO DAC case
    gscDacSoftClock( &cdsPciModules, GSAI_ALL_CARDS );
#endif
    /// \> Increment DAC memory block pointers for next cycle
    ioClockDac = ( ioClockDac + 1 ) & (IOP_IO_RATE-1);
    ioMemCntrDac = ( ioMemCntrDac + 1 ) & (IO_MEMORY_SLOTS-1);
    if ( dacWriteEnable < 10 )
        ++dacWriteEnable;

    /// END OF IOP DAC WRITE *************************************************
    return dac_write_error;
}

int
check_dac_buffers( int cardNum, int report_all_faults )
{
    // if report_all_faults not set, then will only set
    // dacTimingError on FIFO full.
    int                         out_buf_size = 0;
    int                         status = 0;

    if ( cdsPciModules.dac_info[ cardNum ].card_type == GSC_18AO8 )
    {
        out_buf_size = _dacPtr[ cardNum ]->OUT_BUF_SIZE;
        dacOutBufSize[ cardNum ] = out_buf_size;
        pLocalEpics->epicsOutput.buffDac[ cardNum ] = out_buf_size;
        if ( report_all_faults )
        {
            if ( ( out_buf_size < 8 ) || ( out_buf_size > 24 ) )
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
                if (report_all_faults && ( dacTimingErrorPending[ cardNum ] > DAC_WD_TRIP_SET))
                    dacTimingError = 1;
                else dacTimingErrorPending[ cardNum ] ++;
            }
            else
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
                dacTimingErrorPending[ cardNum ] = 0;
            }

            // Set/unset FIFO empty,hi qtr, full diags
            if ( out_buf_size < 8 )
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_EMPTY;
            else
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_EMPTY );
            if ( out_buf_size > 24 )
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_HI_QTR;
            else
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
            if ( out_buf_size > 320 )
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_FULL;
                dacTimingError = 1;
            }
            else
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_FULL );
            }
        }
        else
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_EMPTY );
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
            // Check only for FIFO FULL
            if ( out_buf_size > 320 )
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_FULL;
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
                dacTimingError = 1;
            }
            else
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
            }
        }
    }

    if ( cdsPciModules.dac_info[ cardNum ].card_type == GSC_20AO8 )
    {
        out_buf_size = _dacPtr[ cardNum ]->OUT_BUF_SIZE;
        dacOutBufSize[ cardNum ] = out_buf_size;
        pLocalEpics->epicsOutput.buffDac[ cardNum ] = out_buf_size;
        if ( ( out_buf_size > 24 ) )
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
            if ( (dacTimingErrorPending[ cardNum ] > DAC_WD_TRIP_SET) && report_all_faults )
                dacTimingError = 1;
            else dacTimingErrorPending[ cardNum ] ++;
        }
        else
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
            dacTimingErrorPending[ cardNum ] = 0;
        }

        // Set/unset FIFO empty,hi qtr, full diags
        if ( out_buf_size > 24 )
            pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_HI_QTR;
        else
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
        if ( out_buf_size > 500 )
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_FULL;
            dacTimingError = 1;
        }
        else
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_FULL );
        }
    }

    if ( cdsPciModules.dac_info[ cardNum ].card_type == GSC_16AO16 )
    {
        status = gsc16ao16CheckDacBuffer( cardNum );
        dacOutBufSize[ cardNum ] = status;
        pLocalEpics->epicsOutput.buffDac[ cardNum ] = out_buf_size;
        if ( report_all_faults )
        {
            if ( !dacTimingError )
            {
                if ( status != 2 )
                {
                    pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
                    if ( dacTimingErrorPending[ cardNum ] > DAC_WD_TRIP_SET)
                        dacTimingError = 1;
                    else dacTimingErrorPending[ cardNum ] ++;
                }
                else
                {
                    pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
                    dacTimingErrorPending[ cardNum ] = 0;
                }
            }

            // Set/unset FIFO empty,hi qtr, full diags
            if ( status & 1 )
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_EMPTY;
            else
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_EMPTY );
            if ( status & 8 )
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_FULL;
                dacTimingError = 1;
            }
            else
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_FULL );
            }
            if ( status & 4 )
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_HI_QTR;
            else
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
        }
        else
        {
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_EMPTY );
            pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
            // Check only for FIFO FULL
            if ( status & 8 )
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_FULL;
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_BIT );
                dacTimingError = 1;
            }
            else
            {
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_FULL );
            }
        }
    }

    if ( cdsPciModules.dac_info[ cardNum ].card_type == LIGO_28AO32 )
    {
        uint32_t status = ligo28ao32_dma_read_common_errors( cdsPciModules.dacDrivers[ cardNum ] );
        if ( (status & 0x3F000000) != 0 || ligo28ao32_samp_stat_is_timing_ok(status) != 1 ) { //TODO: const here
            pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_TIMING_BIT;
            RTSLOG_ERROR("LIGO_28AO32(%d) DAC error detected, 0x%x, DAC error counter: %u, is_locked: %d\n",
            cardNum,
            status, 
            ligo28ao32_dma_query_dac_error_ctr(cdsPciModules.dacDrivers[ cardNum ]), 
            ligo28ao32_samp_stat_is_timing_ok(status) );
        }
        else {
                //Don't use these for this DAC type
                pLocalEpics->epicsOutput.statDac[ cardNum ] |= DAC_FIFO_BIT;
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_EMPTY );
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_HI_QTR );
                pLocalEpics->epicsOutput.statDac[ cardNum ] &= ~( DAC_FIFO_FULL );

                //pLocalEpics->epicsOutput.statDac[ cardNum ] |= ~(DAC_TIMING_BIT);
        }
        //RTSLOG_INFO("LIGO DAC temp: %d c\n", ligo28ao32_get_temp_c(cdsPciModules.dacDrivers[ cardNum ]));
    }

    return 0;
}
