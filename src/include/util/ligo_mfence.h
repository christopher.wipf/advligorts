#ifndef LIGO_MFENCE_H
#define LIGO_MFENCE_H



#ifdef __KERNEL__

//SW memory barrier
#define ligo_barrier() barrier()

//"HW" memory barrier, some eval to only SW for x86
#define ligo_mb()	mb()
#define ligo_rmb()	rmb()
#define ligo_wmb()	wmb()
#define ligo_smp_rmb() smp_rmb()
#define ligo_smp_wmb() smp_wmb()
#define ligo_smp_mb()  smp_mb()

#else //!__KERNEL

//SW memory barrier
#define ligo_barrier() __asm__ __volatile__("": : :"memory") 

//"HW" memory barrier, some eval to only SW for x86
#define ligo_mb()	asm volatile("mfence" ::: "memory")
#define ligo_rmb()	asm volatile("lfence" ::: "memory")
#define ligo_wmb()	asm volatile("sfence" ::: "memory")
#define ligo_smp_rmb() ligo_barrier()
#define ligo_smp_wmb() ligo_barrier()
#define ligo_smp_mb()  asm volatile("lock; addl $0,-132(%%rsp)" ::: "memory", "cc")

#endif 


#endif //LIGO_MFENCE_H