#ifndef LIGO_FIXED_WIDTH_TYPES_H
#define LIGO_FIXED_WIDTH_TYPES_H

#if __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#include <stddef.h>
typedef uint64_t    u64;
typedef int64_t     s64;
typedef uint32_t    u32;
typedef int32_t     s32;
#endif

#endif //LIGO_FIXED_WIDTH_TYPES_H
