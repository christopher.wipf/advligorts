#ifndef LIGO_ALIGNMENT_H
#define LIGO_ALIGNMENT_H

#ifndef __cplusplus
#ifdef __KERNEL__
# if __STDC_VERSION__ >= 201112L
#  define alignas(x) _Alignas(x)
# else
#  include <stdalign.h>
# endif
#else
#include <stdalign.h>
#endif
#endif

#endif //LIGO_ALIGNMENT_H