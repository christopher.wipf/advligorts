#ifndef LIGO_EXIT_SIGNALING_H
#define LIGO_EXIT_SIGNALING_H

#include <linux/types.h>
#include <linux/delay.h>
#include <linux/atomic.h>

// Used to signal the model to exit, defined in fe/moduleLoad.c
extern atomic_t g_atom_should_exit;
// Used by fe_start_controller() (The model) to signal it has exited,
// defined in fe/moduleLoad.c
extern atomic_t g_atom_has_exited; 

// When a model experiences a nonrecoverable error and needs to exit
// we block execution with this function until the model is rmmod-ed
static void wait_for_exit_signal(void)
{
    while(atomic_read(&g_atom_should_exit) == 0)
    {
    }
}


#endif //LIGO_EXIT_SIGNALING_H
