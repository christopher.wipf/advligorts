#ifndef LIGO_ATOMIC_HELPER_HH
#define LIGO_ATOMIC_HELPER_HH

/** 
 * This header can be used for a kernel/user space compatible 
 * atimic interface.
 */

#if defined(__KERNEL__)

#include <linux/atomic.h>

#define ligo_atomic_set atomic_set
#define ligo_atomic_read atomic_read
typedef atomic_t ligo_atomic_t;

#else

#include <stdatomic.h>

#define ligo_atomic_set atomic_store
#define ligo_atomic_read atomic_load
typedef atomic_int ligo_atomic_t;

#endif // NOT __KERNEL__

#endif //LIGO_ATOMIC_HELPER_HH