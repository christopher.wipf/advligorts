/* CVS VERSION: $Id: inlineMath.h,v 1.4 2009/08/18 16:25:21 aivanov Exp $ */

#ifndef LIGO_INLINE_MATH_H
#define LIGO_INLINE_MATH_H

#if defined(USE_STDLIB_MATH) 

#if defined(__KERNEL__) //Make sure we aren't trying to use the stdlib in kernelspace
#error "USE_STDLIB_MATH is defined, but the standard lib CANNOT be used in a kernel module. Invalid build options."
#endif

#include "util/user/inlineMath_stdlib.h"
#else

//The inlineMath_x86_asm version can be used in kernel and user space, 
//but the inline assembly makes it dependent on system architecture
#include "util/inlineMath_x86_asm.h"

#include "isnan.h" //isnan() implementation that works for kernel space

#endif


#endif //LIGO_INLINE_MATH_H
