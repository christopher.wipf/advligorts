package fe_simulation

/*
#cgo pkg-config: fe_simulation

#include <stdlib.h>
#include "cds-shmem.h"
#include <daqmap.h>
#include <daq_core.h>
#include <shmem_all.h>
*/
import "C"
import (
	"errors"
	"fmt"
	"git.ligo.org/cds/advligorts/src/fe_stream_test/fe_simulation/gps"
	"git.ligo.org/cds/advligorts/src/fe_stream_test/fe_simulation/ligo_crc"
	"log"
	"reflect"
	"sync/atomic"
	"time"
	"unsafe"
)

const (
	SimulationNotStarted = iota
	SimulationRunning    = iota
	SimulationStopped    = iota
)

const (
	ModelRunning = iota
	ModelStopped = iota
)

// SimulationDcuStatus holds a brief report on the state of a given dcu
type SimulationDcuStatus struct {
	DcuId          int
	Status         int
	ModelRate      int
	DataRate       int
	ModelName      string
	TestPointTable []int
}

func (s *SimulationDcuStatus) IsRunning() bool {
	return s.Status == ModelRunning
}

func (s *SimulationDcuStatus) IsStopped() bool {
	return s.Status == ModelStopped
}

func createSimulationStatus(models []*Model) []SimulationDcuStatus {
	status := make([]SimulationDcuStatus, 0, len(models))
	for _, model := range models {
		tpCount := model.ActiveTP()
		testPoints := make([]int, tpCount)
		for i := 0; i < tpCount; i++ {
			testPoints[i] = model.TPTable[i]
		}
		status = append(status, SimulationDcuStatus{
			DcuId:          model.DcuId,
			Status:         model.Status,
			ModelRate:      model.ModelRate.Rate,
			DataRate:       model.DataRate.Rate,
			ModelName:      model.Name,
			TestPointTable: testPoints,
		})
	}
	return status
}

// ModelParams defines the rough parameters that
// define a simulated model, name, model rate, output buffer size, ...
type ModelParams struct {
	Name       string
	ModelRate  int
	DCUId      uint
	DataRate   int
	TPList     []int
	Status     int
	LinkTPList []TPLink
}

type InitialOptions struct {
	Models         []ModelParams
	IniRoot        string
	IFOPrefix      string
	MasterPath     string
	ShmemPrefix    string
	MBufName       string
	MBufSizeMB     int
	Concentrate    bool
	AdminIface     string
	CloseDaqOnStop bool
	UseAWG         bool
	NoiseAmp       float64
}

type MutationRequest struct {
	DcuId             int
	RemoveCount       int
	AddCount          int
	ProtectedChannels []string
	RemoveChannels    []string
	AddChannels       []string
}

type ModificationRequest struct {
	DcuId          int
	RemoveChannels []string
	AddChannels    []string
}

type TimingGlitchRequest struct {
	DeltaCycles int
	Dcus        []int
}

const (
	commandSimulationStop           = iota
	commandSimulationStatus         = iota
	commandSimulationStopDcu        = iota
	commandSimulationStartDcu       = iota
	commandSimulationMutateChannels = iota
	commandSimulationTimeGlitch     = iota
)

// simulationMessages are used to send request to a running simulation.
type simulationMessage interface {
	Command() int
}

type stopCommand struct{}

func (s *stopCommand) Command() int {
	return commandSimulationStop
}

type statusCommand struct {
	response chan []SimulationDcuStatus
}

func (s *statusCommand) Command() int {
	return commandSimulationStatus
}

type startStopCommand struct {
	command int
	dcuId   int
}

func (s *startStopCommand) Command() int {
	return s.command
}

type mutateChannelsCommand struct {
	params MutationRequest
}

func (s *mutateChannelsCommand) Command() int {
	return commandSimulationMutateChannels
}

type SimulationLoop struct {
	opts  InitialOptions
	queue chan simulationMessage
	state int32
}

type timingGlitchCommand struct {
	params TimingGlitchRequest
}

func (t *timingGlitchCommand) Command() int {
	return commandSimulationTimeGlitch
}

func NewSimulationLoop(opts InitialOptions) (*SimulationLoop, error) {
	return &SimulationLoop{opts: opts, queue: make(chan simulationMessage, 10)}, nil
}

func (s *SimulationLoop) State() int {
	return int(atomic.LoadInt32(&s.state))
}

// Status returns the overall status of the simulation, the models, test points, ...
func (s *SimulationLoop) Status() []SimulationDcuStatus {
	resp := make(chan []SimulationDcuStatus)
	s.queue <- &statusCommand{response: resp}
	return <-resp
}

func (s *SimulationLoop) RequestMutation(req MutationRequest) {
	s.queue <- &mutateChannelsCommand{params: req}
}

func (s *SimulationLoop) RequestTimingGlitch(req TimingGlitchRequest) {
	s.queue <- &timingGlitchCommand{params: req}
}

// Run is the main simluation loop.  It takes the configuration, creates models, and ensures
// that the models output data.
func (s *SimulationLoop) Run(clock gps.Clock) error {
	var err error
	iniManager := NewIniManager(s.opts.IniRoot, s.opts.MasterPath, s.opts.IFOPrefix)
	models := make([]*Model, len(s.opts.Models))
	for i, params := range s.opts.Models {
		models[i], err = NewModel(&iniManager, params.Name,
			int(params.DCUId), NewRateHertz(params.ModelRate), NewRateBytes(params.DataRate), params.TPList,
			params.Status, s.opts.ShmemPrefix, params.LinkTPList)
		if err != nil {
			return err
		}
	}
	defer CloseModels(models)

	totalChans := 0
	for _, model := range models {
		totalChans += len(model.Generators)
	}
	fmt.Printf("Model count: %d\nChannel count: %d\n", len(models), totalChans)

	var shmem *Shmem

	var ifoHeader *daqMultiCycleHeader
	var ifoData uintptr
	var dataSize uintptr

	if s.opts.UseAWG {
		// open AWG ipcs
		for _, m := range models {
			err = m.OpenTPShmem()
			if err != nil {
				log.Printf("Unable to open testpoint buffer %s_tp %v", m.Name, err)
				return err
			}
			err = m.OpenAWGShmem()
			if err != nil {
				log.Printf("Unable to open awg buffer %s_awg %v", m.Name, err)
				return err
			}
		}

	}

	if s.opts.Concentrate {
		shmemName := s.opts.ShmemPrefix + s.opts.MBufName
		bufferSizeMB := s.opts.MBufSizeMB
		shmem, err = NewShmem(shmemName, bufferSizeMB)
		if err != nil {
			log.Printf("Unable to open buffer %s %v", shmemName, err)
			return err
		}
		defer shmem.Close()
		base := shmem.Mapping()

		ifoHeader = (*daqMultiCycleHeader)(unsafe.Pointer(base))
		ifoHeader.maxCycle = C.uint(16)

		dataSize = uintptr(bufferSizeMB*1024*1024) - unsafe.Sizeof(daqMultiCycleHeader{})
		dataSize /= 16
		dataSize -= dataSize % 8
		ifoHeader.cycleDataSize = C.uint(dataSize)
		ifoData = base + unsafe.Sizeof(daqMultiCycleHeader{})
		_ = ifoData
	} else {
		for _, m := range models {
			err = m.OpenRMIPC()
			if err != nil {
				log.Printf("Unable to open model buffer %s %v", m.Name, err)
				return err
			}
		}
	}

	timeStep := time.Nanosecond * 1000000000 / 16
	// start transmission at the start of the next second
	transmitTime := time.Unix(clock.Now().Unix()+1, 0)

	delayMultiplier := time.Duration(0)
	cycle := 0
	done := false
	atomic.StoreInt32(&s.state, SimulationRunning)
	defer func() {
		atomic.StoreInt32(&s.state, SimulationStopped)
	}()
	for !done {
		select {
		case msg := <-s.queue:
			s.handleMessage(msg, &done, models, &iniManager)
		default:
		}

		now := clock.Now()
		for ; now.Before(transmitTime); now = clock.Now() {
			time.Sleep(2 * time.Millisecond)
			select {
			case msg := <-s.queue:
				s.handleMessage(msg, &done, models, &iniManager)
			default:
			}
		}

		time.Sleep(time.Millisecond * delayMultiplier)
		if s.opts.Concentrate {
			dest := ifoData + (uintptr(cycle) * dataSize)
			if err := generateConcentrated(models, dest, dataSize, cycle, transmitTime, s.opts.UseAWG); err != nil {
				return err
			}
			ifoHeader.curCycle = C.uint(cycle)
		} else {
			if err := generateIndividual(models, cycle, transmitTime); err != nil {
				return err
			}
		}
		if cycle == 0 {
			if s.opts.UseAWG {
				for _, m := range models {
					m.CheckTestpoints(s.opts.NoiseAmp)
				}

			}
			//end := clock.Now()
			//delta := end.Sub(now)
			//fmt.Printf("Cycle took %vms\n", delta.Milliseconds())
			//fmt.Printf("start %d:%d\nend %d:%d\n", now.Unix(), now.Nanosecond(), end.Unix(), end.Nanosecond())
		}

		if s.opts.UseAWG {
			//setIoMemTime(ioShmem, transmitTime, cycle)
		}
		cycle = (cycle + 1) % 16
		transmitTime = transmitTime.Add(timeStep)
	}
	return nil
}

func (s *SimulationLoop) handleMessage(msg simulationMessage, done *bool, models []*Model, iniManager *IniManager) {
	findModel := func(dcuId int) *Model {
		for _, model := range models {
			if model != nil && model.DcuId == dcuId {
				return model
			}
		}
		return nil
	}
	switch msg.Command() {
	case commandSimulationStop:
		*done = true
	case commandSimulationStatus:
		{
			statusMsg := msg.(*statusCommand)
			statusMsg.response <- createSimulationStatus(models)
		}
	case commandSimulationStopDcu:
		{
			startStopMsg := msg.(*startStopCommand)
			dcuId := startStopMsg.dcuId
			for i := 0; i < len(models); i++ {
				if models[i].DcuId == dcuId {
					models[i].Status = ModelStopped
					if s.opts.CloseDaqOnStop {
						_ = models[i].ShmemObj.Close()
						models[i].ShmemObj = nil
						models[i].rmipc = uintptr(0)
					}
				}
			}
		}
	case commandSimulationStartDcu:
		{
			startStopMsg := msg.(*startStopCommand)
			dcuId := startStopMsg.dcuId
			for i := 0; i < len(models); i++ {
				if models[i].DcuId == dcuId {
					if models[i].ShmemObj == nil {
						oldState := models[i].Status
						models[i].Status = ModelRunning
						if err := models[i].OpenRMIPC(); err != nil {
							models[i].Status = oldState
							log.Printf("Unable to open the rmipc for model, %v", err)
							continue
						}
					}
					models[i].Status = ModelRunning
				}
			}
		}
	case commandSimulationMutateChannels:
		{
			mutateMsg := msg.(*mutateChannelsCommand)
			if model := findModel(mutateMsg.params.DcuId); model != nil {
				model.Mutate(mutateMsg.params, iniManager)
			}
		}
	case commandSimulationTimeGlitch:
		{
			timingMsg := msg.(*timingGlitchCommand)
			for _, dcu := range timingMsg.params.Dcus {
				if model := findModel(dcu); model != nil {
					model.SetTimingGlitchDelta(timingMsg.params.DeltaCycles)
				}
			}
		}
	}
}

// Stop is used to reqeust that a running simulation stop
func (s *SimulationLoop) Stop() {
	s.queue <- &stopCommand{}
}

// StopDcu is used to signal that the given dcu (model) should be stopped.
// Invalid dcuIds are ignored.  This requires the simulation to be running.
func (s *SimulationLoop) StopDcu(dcuId int) {
	s.queue <- &startStopCommand{
		command: commandSimulationStopDcu,
		dcuId:   dcuId,
	}
}

// StartDcu is used to signal that the given dcu (model) should be started.
// Invalid dcuIds are ignored.  This requires the simulation to be running.
func (s *SimulationLoop) StartDcu(dcuId int) {
	s.queue <- &startStopCommand{
		command: commandSimulationStartDcu,
		dcuId:   dcuId,
	}
}

// generateConcentrated runs the given set of models and outputs their data to a daq_dc_data_t buffer
func generateConcentrated(models []*Model, destPtr, maxDatasize uintptr, cycle int, curTime time.Time, useAWG bool) error {
	dest := (*daqDCData)(unsafe.Pointer(destPtr))
	dest.header.dcuTotalModels = 0
	dest.header.fullDataBlockSize = 0
	data := uintptr(unsafe.Pointer(&(dest.dataBlock[0])))

	gpsSec := int(curTime.Unix())
	gpsNano := int(curTime.Nanosecond())

	curHeader := 0
	for _, model := range models {
		start := data
		if model.Status == ModelStopped || curHeader >= C.DAQ_TRANSIT_MAX_DCU {
			continue
		}
		dest.header.dcuTotalModels++
		dcuHeader := &dest.header.dcuheader[curHeader]
		curHeader++

		glitchCycle := cycle + model.GetTimingGlitchDelta()
		headerGps := gpsSec
		headerNano := gpsNano
		if glitchCycle < 0 {
			glitchCycle = 15
			headerGps--
		}
		if glitchCycle > 15 {
			headerGps += glitchCycle / 16
			glitchCycle %= 16
		}
		headerNano = glitchCycle

		if glitchCycle != cycle {
			log.Printf("Sending Glitch dcuid: %d - %d:%d %d", model.DcuId, headerGps, headerNano, cycle)
		}

		model.SetTimingGlitchDelta(0)

		dcuHeader.dcuId = C.uint(model.DcuId)
		dcuHeader.fileCrc = C.uint(model.ConfigCrc)
		dcuHeader.status = 2
		dcuHeader.cycle = C.uint(cycle)
		dcuHeader.timeSec = C.uint(headerGps)
		dcuHeader.timeNSec = C.uint(headerNano)
		dcuHeader.dataCrc = 0
		dcuHeader.dataBlockSize = 0
		dcuHeader.tpBlockSize = C.uint(model.TpDataRatePerSec())
		dcuHeader.tpCount = C.uint(model.ActiveTP())
		for j := 0; j < int(dcuHeader.tpCount); j++ {
			dcuHeader.tpNum[j] = C.uint(model.TPTable[j])
		}
		for j := int(dcuHeader.tpCount); j < C.DAQ_GDS_MAX_TP_NUM; j++ {
			dcuHeader.tpNum[j] = 0
		}

		for _, generator := range model.Generators {
			nextData := generator.Generate(gpsSec, gpsNano, data)
			dest.header.fullDataBlockSize += C.uint(nextData - data)
			data = nextData
		}
		dcuHeader.dataBlockSize = C.uint(data - start)

		dataBlockHeader := reflect.SliceHeader{
			Data: start,
			Len:  int(data - start),
			Cap:  int(data - start),
		}
		dataSlice := *(*[]byte)(unsafe.Pointer(&dataBlockHeader))
		crc := ligo_crc.LigoCrc32{}
		_, _ = crc.Write(dataSlice)

		dcuHeader.dataCrc = C.uint(crc.Sum32())
		tpStart := data
		for tp := 0; tp < int(dcuHeader.tpCount); tp++ {
			var nextData uintptr
			tpIndex := int(model.TPTable[tp])
			if tpIndex == 0 {
				nextData = model.NullTP.Generate(gpsSec, gpsNano, data)
			} else {
				if useAWG {
					nextData = model.TPGenerators[tp].Generate(gpsSec, gpsNano, data)
				} else {
					nextData = model.TPGenerators[tpIndex-1].Generate(gpsSec, gpsNano, data)
				}
			}
			data = nextData
		}
		dcuHeader.tpBlockSize = C.uint(data - tpStart)
		dest.header.fullDataBlockSize += dcuHeader.tpBlockSize
	}
	return nil
}

// generateIndividual takes the given set of models and sends their output to the
// local rmIpcStr buffers (1 per model).
func generateIndividual(models []*Model, cycle int, transmitTime time.Time) error {
	gpsSec := int(transmitTime.Unix())
	gpsNano := transmitTime.Nanosecond()
	for _, model := range models {
		if model == nil || model.Status == ModelStopped || model.rmipc == uintptr(0) {
			continue
		}
		ipc := (*C.struct_rmIpcStr)(unsafe.Pointer(model.rmipc + C.CDS_DAQ_NET_IPC_OFFSET))
		data := model.rmipc + C.CDS_DAQ_NET_DATA_OFFSET
		curData := data + (uintptr(cycle) * C.DAQ_DCU_BLOCK_SIZE)
		endData := curData + C.DAQ_DCU_BLOCK_SIZE

		modelDataStart := curData
		for _, generator := range model.Generators {
			curData = generator.Generate(gpsSec, gpsNano, curData)
			if curData > endData {
				log.Printf("%s %s has overflowed its data buffer", model.Name, generator.FullChannelName())
				return errors.New("a generator has overflowed the data area")
			}
		}
		dataBlockSize := curData - modelDataStart
		tpCount := model.ActiveTP()
		for i := 0; i < tpCount; i++ {
			tpIndex := model.TPTable[i]
			generator := model.NullTP
			if tpIndex != 0 {
				generator = model.TPGenerators[tpIndex-1]
			}
			curData = generator.Generate(gpsSec, gpsNano, curData)
			if curData > endData {
				log.Printf("%s %s has overflowed its data buffer", model.Name, generator.FullChannelName())
				return errors.New("a generator has overflowed the data area")
			}
		}
		ipc.status = 0
		ipc.crc = C.uint(model.ConfigCrc)

		headerCycle := cycle + model.GetTimingGlitchDelta()
		headerGps := gpsSec
		headerNano := gpsNano
		if headerCycle < 0 {
			headerCycle = 15
			headerGps--
		}
		if headerCycle > 15 {
			headerGps += headerCycle / 16
			headerCycle %= 16
		}
		headerNano = headerCycle
		//model.SetTimingGlitchDelta(0)

		ipc.channelCount = C.uint(len(model.Generators))
		ipc.dataBlockSize = C.uint(dataBlockSize)
		ipc.bp[cycle].status = 2
		ipc.bp[cycle].timeSec = C.uint(headerGps)
		ipc.bp[cycle].timeNSec = C.uint(headerNano)
		ipc.bp[cycle].cycle = C.uint(cycle)
		ipc.cycle = C.uint(cycle)
	}
	return nil
}

// create io shared memory, aka IOP shared memory
// The Io shared memory functions are no longer used.
// they were created to support awg timing, but awg can now
// handle its own timing which will be in sync with the fe_streams
// These will be left in case we want to add IO Mem back
// in at a later time.
func openIoShmem(shmem_prefix string) (*Shmem, error) {
	shmem_name := shmem_prefix + C.SHMEM_IOMEM_NAME
	shmem, err := NewShmem(shmem_name, C.SHMEM_IOMEM_SIZE_MB)
	if err != nil {
		return nil, err
	}

	iomem_ptr := (*C.struct_IO_MEM_DATA)(unsafe.Pointer(shmem.Mapping() + C.IO_MEM_DATA_OFFSET))
	iomem_ptr.struct_version = C.int(C.IO_MEM_DATA_VERSION)
	iomem_ptr.mem_data_rate_hz = C.int(65536)

	return shmem, nil
}

// call each run loop to set the IOP timing info for awgtpman
func setIoMemTime(ioShmem *Shmem, transmit_time time.Time, cycle int) {
	iomem_ptr := (*C.struct_IO_MEM_DATA)(unsafe.Pointer(ioShmem.Mapping() + C.IO_MEM_DATA_OFFSET))
	gps_sec := transmit_time.Unix()
	clock_ticks := cycle * 4096 // 2^12 IO counts per cycle
	iomem_ptr.gpsSecond = C.int(gps_sec)
	iomem_ptr.inputData[0][0].timeSec = C.int(gps_sec)
	iomem_ptr.inputData[0][0].cycle = C.int(clock_ticks)
}
