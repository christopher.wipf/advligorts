//
// Created by erik.vonreis on 5/21/21.
//

#include "string.h"
#include "testpoint_structs.h"
#include "dtt/rmorg.h"
#include "rtestpoint.h"
#include "dtt/gdsstring.h"
#include "dtt/gdserr.h"
#include "dtt/gdserrmsg.h"
#include "tconv.h"
#include "testpoint_interface_v3.h"
#include "testpoint.h"

#if defined (_CONFIG_DYNAMIC)
#include "confinfo.h"
#endif

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Types: tpNode_t - node type for rpc client				*/
/*        tpIndex_t - test point index chache				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
struct tpNode_t {
    int		valid;
    int		duplicate;
    int		id;
    char		hostname[80];
    unsigned long	prognum;
    unsigned long	progver;
};
typedef struct tpNode_t tpNode_t;

#define _HELP_TEXT	"Test point interface commands:\n" \
			"  show 'node': show active test points\n" \
			"  set 'node' 'number': set a test point\n" \
			"  clear 'node' 'number': clear a test point, " \
			" use * for wildcards\n"

#define _NETID			"tcp"

static int			tp_init = 0;
static tpNode_t		tpNode[TP_MAX_NODE];
static int			tpNum = 0;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: tpSetHostAddress				*/
/*                                                         		*/
/* Procedure Description: cleans up test point interface		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 if successful						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int tpSetHostAddress (int node, const char* hostname,
                     unsigned long prognum, unsigned long progver)
   {
      int		k;

      if ((node < 0) || (node >= TP_MAX_NODE)) {
         return -1;
      }
      /* set node parameters */
      tpNode[node].valid = 1;
      strncpy (tpNode[node].hostname, hostname,
              sizeof (tpNode[node].hostname));
      tpNode[node].hostname[sizeof(tpNode[node].hostname)-1] = 0;
      tpNode[node].prognum = (prognum > 0) ?
                           prognum : RPC_PROGNUM_TESTPOINT;
      tpNode[node].progver = (progver > 0) ?
                           progver : RPC_PROGVER_TESTPOINT;

      /* look for identical nodes */
      for (k = node - 1; k >= 0; k--) {
         if ((tpNode[k].valid) &&
            (gds_strcasecmp (tpNode[k].hostname,
            tpNode[node].hostname) == 0) &&
            (tpNode[k].prognum == tpNode[node].prognum) &&
            (tpNode[k].progver == tpNode[node].progver)) {
            break;
         }
      }
      tpNode[node].duplicate = (k >= 0);
      if (tpNode[node].duplicate) {
         tpNode[node].id = k;
      }
//      printf ("TP: node = %i, host = %s, dup = %i, prog = 0x%x, vers = %i\n",
//              node, tpNode[node].hostname, tpNode[node].duplicate,
//              (int)tpNode[node].prognum, (int)tpNode[node].progver);
      return 0;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: initTestpoint				*/
/*                                                         		*/
/* Procedure Description: initializes test point interface		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void initTestpoint (void)
{

    int		node;		/* node id */

    /* test if already initialized */
    if (tp_init != 0) {
        return;
    }

    tpNum = 0;

    for (node = 0; node < TP_MAX_NODE; node++) {
        /* make section header */
        tpNode[node].valid = 0;
        /*#if (_TESTPOINT_DIRECT != 0)
           if ((_TESTPOINT_DIRECT & (1 << node)) != 0) {
              tpNode[node].valid = 1;*/

    }

    /* intialized */
    tp_init = 1;

}



/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: testpoint_client				*/
/*                                                         		*/
/* Procedure Description: installs test point client interface		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int testpoint_client (void)
{
    int		node;		/* node id */
    struct timeval	timeout;	/* timeout for probe */
    CLIENT*		clnt;		/* client rpc handle */
    int		status;		/* rpc status */


#if defined (_CONFIG_DYNAMIC)
    const char* const* cinfo;		/* configuration info */
    confinfo_t	crec;		/* conf. info record */
#endif

    /* already initialized */
    if (tp_init == 2) {
        return tpNum;
    }

    gdsDebug ("start test point client");

    /* intialize interface first */
    if (tp_init == 0) {
        initTestpoint();
        if (tp_init == 0) {
            gdsError (GDS_ERR_MEM, "failed to initialze test points");
            return -1;
        }
        /* Log the version ID */
        printf("testpoint_client %s\n", _TP_CLIENT_VERSION) ;
    }

#if defined (_CONFIG_DYNAMIC)
    for (cinfo = getConfInfo (0, 0); cinfo && *cinfo; cinfo++) {
        if ((parseConfInfo (*cinfo, &crec) == 0) &&
             (gds_strcasecmp (crec.interface,
                               CONFIG_SERVICE_TP) == 0) &&
             (crec.ifo >= 0) && (crec.ifo < TP_MAX_NODE) &&
             (crec.port_prognum > 0) && (crec.progver > 0)) {

            tpSetHostAddress (crec.ifo, crec.host,
                              crec.port_prognum, crec.progver);
        }
    }
#endif

    timeout.tv_sec = RPC_PROBE_WAIT;
    timeout.tv_usec = 0;
    for (node = 0; node < TP_MAX_NODE; node++) {
        if ((tpNode[node].valid) &&
            (rpcProbe (tpNode[node].hostname, tpNode[node].prognum,
                       tpNode[node].progver, _NETID, &timeout, NULL))) {
            tpNum++;
        }
        else {
            tpNum++;
        }
    }



    tp_init = 2;
    return tpNum;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* internal Procedure Name: tpMakeHandle				*/
/*                                                         		*/
/* Procedure Description: makes a rpc client handle for a TP node	*/
/*                                                         		*/
/* Procedure Arguments: node ID						*/
/*                                                         		*/
/* Procedure Returns: client handle if successful, NULL when failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static CLIENT* tpMakeHandle (int node)
{
    CLIENT*		clnt;		/* client handle */

    /* printf("making handle tp node %d, max is %d\n", node, TP_MAX_NODE); */

    /* check node */
    if ((node < 0) || (node >= TP_MAX_NODE)) {
        printf("tp node %d not in valid range: 0 to %d\n",
               node, TP_MAX_NODE);
        return NULL;
    }
    /* check validity */
    if (!tpNode[node].valid) {
        printf("tp node %d invalid\n", node);
        return NULL;
    }

    /* create handle */
    clnt = clnt_create (tpNode[node].hostname, tpNode[node].prognum,
                        tpNode[node].progver, _NETID);
    if (clnt == NULL) {
        printf("couldn't create test point handle\n");
        printf("hostname=%s, prognum=%d, progver=%d\n",
               tpNode[node].hostname, (int)tpNode[node].prognum,
               (int)tpNode[node].progver);
        gdsError (GDS_ERR_MEM,
                  "couldn't create test point handle");
    }

    return clnt;
}


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: tpClear					*/
/*                                                         		*/
/* Procedure Description: clears a test point				*/
/*                                                         		*/
/* Procedure Arguments: request ID, test point list			*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int tpClear (int node, const testpoint_t tp[], int tplen)
{
    static testpoint_t 	all = _TP_CLEAR_ALL;
    TP_r		testpoints;	/* test point list */
    int		result;		/* result of rpc call */
    CLIENT*		clnt;		/* client rpc handle */
    const testpoint_t *target;

    gdsDebug ("clear test point");

    /* intialize interface */
    if (testpoint_client() < 0) {
        return -2;
    }

    /* make test point list */
    if (tp == NULL) {
        testpoints.TP_r_len = 1;
        target = &all;
    }
    else if (tplen == 0) {
        return 0;
    }
    else {
        testpoints.TP_r_len = tplen;
        target = tp;
    }

    testpoints.TP_r_val = malloc(testpoints.TP_r_len * sizeof(testpoints.TP_r_val[0]));
    if(testpoints.TP_r_val == NULL)
    {
        gdsError(GDS_ERR_MEM, "unable to allocate memory for RPC struct");
        return -5;
    }

    int i;
    for(i=0; i < testpoints.TP_r_len; ++i)
    {
        testpoints.TP_r_val[i] = target[i];
    }

    /* make client handle */
    clnt = tpMakeHandle (node);
    if (clnt == NULL) {
        free(testpoints.TP_r_val);
        return -3;
    }

    /* call remote procedure */
    if ((cleartp_1 (tpNode[node].id, node, testpoints, &result,
                    clnt) != RPC_SUCCESS) || (result < 0)) {
        gdsError (GDS_ERR_PROG, "unable to clear test points");
        result = -4;
    }

    /* free handle */
    clnt_destroy (clnt);
    free(testpoints.TP_r_val);
    return result;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: tpClearName					*/
/*                                                         		*/
/* Procedure Description: clears a test point by name			*/
/*                                                         		*/
/* Procedure Arguments: test point names				*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int tpClearName (const char* tpNames)
{
    int		node;		/* node ID */
    int		result;		/* result of rpc call */
    int		retval;		/* return value */
    CLIENT*		clnt;		/* client rpc handle */

    gdsDebug ("clear test point by name");

    /* intialize interface */
    if (testpoint_client() < 0) {
        return -2;
    }

    /* send to all nodes which are not duplicates */
    retval = 0;
    for (node = 0; node < TP_MAX_NODE; node++) {
        if (!tpNode[node].valid || tpNode[node].duplicate) {
            continue;
        }

        /* make client handle */
        clnt = tpMakeHandle (node);
        if (clnt == NULL) {
            return -3;
        }

        /* call remote procedure */
        if ((cleartpname_1 (tpNode[node].id, (char*) tpNames, &result,
                            clnt) != RPC_SUCCESS) || (result < 0)) {
            gdsError (GDS_ERR_PROG, "unable to clear test points");
            result = -4;
        }

        /* free handle */
        clnt_destroy (clnt);
        if (result < 0) {
            retval = result;
        }
    }

    return retval;
}





/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: tpRequest					*/
/*                                                         		*/
/* Procedure Description: requests a test point				*/
/*                                                         		*/
/* Procedure Arguments: node ID, test point list & length, timeout,	*/
/*                      active time & epoch				*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int tpRequest (int node, const testpoint_t tp[], int tplen,
               tainsec_t timeout, taisec_t* time, int* epoch)
{
    TP_r		testpoints;	/* test point list */
    resultRequestTP_r	result;		/* result of rpc call */
    int		retval;		/* return value */
    CLIENT*		clnt;		/* client rpc handle */

    gdsDebug ("request test point");

    /* intialize interface */
    if (testpoint_client() < 0) {
        return -2;
    }

    /* check test point list */
    if ((tp == NULL) || (tplen == 0)) {
        return 0;
    }

    /* make test point list */
    testpoints.TP_r_len = tplen;
    testpoints.TP_r_val = malloc(tplen * sizeof( testpoints.TP_r_val[0] ));
    if(testpoints.TP_r_val == NULL)
    {
        gdsError(GDS_ERR_MEM, "could not allocate buffer for testpoints");
        return -5;
    }

    int i;
    for(i=0; i < tplen; ++i)
    {
        testpoints.TP_r_val[i] = tp[i];
    }

    /* make client handle */
    clnt = tpMakeHandle (node);
    if (clnt == NULL) {
        free(testpoints.TP_r_val);
        return -3;
    }

    /* call remote procedure */
    memset (&result, 0, sizeof (resultRequestTP_r));
    if ((requesttp_1 (tpNode[node].id, node, testpoints, timeout,
                      &result, clnt) == RPC_SUCCESS) && (result.status >= 0)) {
        /* set return arguments */
        if (time != NULL) {
            *time = result.time;
        }
        if (epoch != NULL) {
            *epoch = result.epoch;
        }
        retval = result.status;
    }
    else {
        gdsError (GDS_ERR_PROG, "unable to set test points");
        retval = -4;
    }

    /* free handle and memory of return argument */
    xdr_free ((xdrproc_t)xdr_resultRequestTP_r, (char*) &result);
    clnt_destroy (clnt);
    free(testpoints.TP_r_val);
    return retval;
}


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: tpRequestName				*/
/*                                                         		*/
/* Procedure Description: requests a test point	by name			*/
/*                                                         		*/
/* Procedure Arguments: test point name(s), timeout,			*/
/*                      active time & epoch				*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int tpRequestName (const char* tpNames,
                   tainsec_t timeout, taisec_t* time, int* epoch)
{
    int		node;		/* test point node */
    resultRequestTP_r	result;		/* result of rpc call */
    int		retval;		/* return value */
    int		k;		/* node index */
    int		temp;		/* temporary var. */
    CLIENT*		clnt;		/* client rpc handle */

    gdsDebug ("request test point by name");

    /* intialize interface */
    if (testpoint_client() < 0) {
        return -2;
    }

    /* check test point list */
    if (tpNames == NULL) {
        return 0;
    }

    /* send to all nodes which aren't duplicates */
    for (node = 0; node < TP_MAX_NODE; node++) {
        if (!tpNode[node].valid || tpNode[node].duplicate) {
            continue;
        }

        /* make client handle */
        clnt = tpMakeHandle (node);
        if (clnt == NULL) {
            continue;
            /* return -3; */
        }

        /* call remote procedure */
        memset (&result, 0, sizeof (resultRequestTP_r));
        if ((requesttpname_1 (tpNode[node].id, (char*) tpNames, timeout,
                              &result, clnt) == RPC_SUCCESS) && (result.status >= 0)) {
            /* set return arguments */
            if (time != NULL) {
                *time = result.time;
            }
            if (epoch != NULL) {
                *epoch = result.epoch;
            }
            retval = result.status;
        }
        else {
            gdsError (GDS_ERR_PROG, "unable to set test points");
            retval = -4;
        }

        /* free handle and memory of return argument */
        xdr_free ((xdrproc_t)xdr_resultRequestTP_r, (char*) &result);
        clnt_destroy (clnt);

        /* cleanup on error */
        if (retval < 0) {
            for (k = node - 1; k >= 0; k--) {
                if (!tpNode[k].valid || tpNode[k].duplicate) {
                    continue;
                }
                /* make client handle */
                clnt = tpMakeHandle (k);
                if (clnt == NULL) {
                    return -3;
                }
                /* call remote procedure */
                cleartpname_1 (tpNode[k].id, (char*) tpNames, &temp,
                               clnt);
                /* free handle */
                clnt_destroy (clnt);
            }
            return retval;
        }
    }

    return 0;
}








/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: tpQuery					*/
/*                                                         		*/
/* Procedure Description: queries the test point interface		*/
/*                                                         		*/
/* Procedure Arguments: node ID, test point list, max. length, 		*/
/*                      time and epoch of query request     		*/
/*                                                         		*/
/* Procedure Returns: # of entries, if successful, <0 when failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int tpQuery (int node, int tpinterface, testpoint_t tp[], int tplen,
             taisec_t time, int epoch)
{


    resultQueryTP_r	result;		/* result of rpc call */
    int		retval;		/* return value */
    CLIENT*		clnt;		/* client rpc handle */
    int		i;		/* index into test point list */

    gdsDebug ("query test point");

    /* intialize interface */
    if (testpoint_client() < 0) {
        return -2;
    }

    /* check node */
    if ((node < 0) || (node >= TP_MAX_NODE)) {
        return -2;
    }

    int max_interface = (testAwgTpInterfaceVersion(node) > 3)
        ? TP_MAX_INTERFACE : TP_MAX_INTERFACE_V3;

    /* check interface */
    if ((tpinterface < 0) || (tpinterface >= max_interface)) {
        return -2;
    }

    /* check test point list */
    if (tplen < 0) {
        return -2;
    }

    /* do remote procedure call */

    /* make client handle */
    clnt = tpMakeHandle (node);
    if (clnt == NULL) {
        return -3;
    }

    /* call remote procedure */
    memset (&result, 0, sizeof (resultQueryTP_r));
    if ((querytp_1 (tpNode[node].id, node, tpinterface, tplen, time,
                    epoch, &result, clnt) == RPC_SUCCESS) && (result.status >= 0)) {
        /* copy result */
        if (tp != NULL) {
            for (i = 0; i < result.tp.TP_r_len; i++) {
                tp[i] = result.tp.TP_r_val[i];
            }
        }
        retval = result.tp.TP_r_len;
    }
    else {
        retval = -4;
    }

    /* free handle and memory of return array */
    xdr_free ((xdrproc_t)xdr_resultQueryTP_r, (char*) &result);
    clnt_destroy (clnt);
    return retval;

}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: cmdreply					*/
/*                                                         		*/
/* Procedure Description: command reply					*/
/*                                                         		*/
/* Procedure Arguments: string						*/
/*                                                         		*/
/* Procedure Returns: newly allocated char*				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static char* cmdreply (const char* m)
{
    if (m == 0) {
        return 0;
    }
    else {
        char* p = (char*) malloc (strlen (m));
        if (p != 0) {
            strcpy (p, m);
        }
        else /* JCB */
        {
            gdsDebug("cmdreply malloc(strlen(m) + 1) failed.") ;
        }
        return p;
    }
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: queryCmd					*/
/*                                                         		*/
/* Procedure Description: queries tp's and returns a description	*/
/*                                                         		*/
/* Procedure Arguments: buffer, node					*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void queryCmd_v3 (char* buf, int node)
{
    int		i;
    char*		p;
    testpoint_t	tp[TP_MAX_INDEX]; /* test points */
    int		num;	/* number of test points */

    sprintf (buf, "Test points for node %i\n", node);
    /* query lsc exc */
    num = tpQuery (node, TP_LSC_EX_INTERFACE, tp,
                   TP_MAX_INDEX, 0, 0);
    p = strend (buf);
    sprintf (p, "LSC EX:");
    p = strend (p);
    if (num < 0) {
        sprintf (p, " invalid\n");
        return;
    }
    for (i = 0; i < num; i++) {
        sprintf (p, " %i", tp[i]);
        p = strend (p);
    }
    sprintf (p++, "\n");
    /* query lsc tp */
    num = tpQuery (node, TP_LSC_TP_INTERFACE, tp,
                   TP_MAX_INDEX, 0, 0);
    p = strend (buf);
    sprintf (p, "LSC TP:");
    p = strend (p);
    if (num < 0) {
        sprintf (p, " invalid\n");
        return;
    }
    for (i = 0; i < num; i++) {
        sprintf (p, " %i", tp[i]);
        p = strend (p);
    }
    sprintf (p++, "\n");
    /* query asc exc */
    num = tpQuery (node, TP_ASC_EX_INTERFACE, tp,
                   TP_MAX_INDEX, 0, 0);
    p = strend (buf);
    sprintf (p, "ASC EX:");
    p = strend (p);
    if (num < 0) {
        sprintf (p, " invalid\n");
        return;
    }
    for (i = 0; i < num; i++) {
        sprintf (p, " %i", tp[i]);
        p = strend (p);
    }
    sprintf (p++, "\n");
    /* query asc tp */
    num = tpQuery (node, TP_ASC_TP_INTERFACE, tp,
                   TP_MAX_INDEX, 0, 0);
    p = strend (buf);
    sprintf (p, "ASC TP:");
    p = strend (p);
    if (num < 0) {
        sprintf (p, " invalid\n");
        return;
    }
    for (i = 0; i < num; i++) {
        sprintf (p, " %i", tp[i]);
        p = strend (p);
    }
    sprintf (p++, "\n");
}

static void queryCmd_v4 (char* buf, int node)
{
    int		i;
    char*		p;
    testpoint_t	tp[DAQ_GDS_MAX_TP_NUM]; /* test points */
    int		num;	/* number of test points */

    sprintf (buf, "Test points for node %i\n", node);
    /* query exc */
    num = tpQuery (node, TP_EX_INTERFACE, tp,
                   DAQ_GDS_MAX_TP_NUM, 0, 0);
    p = strend (buf);
    sprintf (p, "EX:");
    p = strend (p);
    if (num < 0) {
        sprintf (p, " invalid\n");
        return;
    }
    for (i = 0; i < num; i++) {
        sprintf (p, " %i", tp[i]);
        p = strend (p);
    }
    sprintf (p++, "\n");
    /* query tp */
    num = tpQuery (node, TP_TP_INTERFACE, tp,
                   DAQ_GDS_MAX_TP_NUM, 0, 0);
    p = strend (buf);
    sprintf (p, "TP:");
    p = strend (p);
    if (num < 0) {
        sprintf (p, " invalid\n");
        return;
    }
    for (i = 0; i < num; i++) {
        sprintf (p, " %i", tp[i]);
        p = strend (p);
    }
    sprintf (p++, "\n");
}

static void queryCmd (char* buf, int node)
{
    int interface_version = testAwgTpInterfaceVersion(node);
    switch (interface_version)
    {
    case 3:
        queryCmd_v3(buf, node);
        break;
    case 4:
        queryCmd_v4(buf, node);
        break;
    default:
        printf("unrecognized awgtp interface version %d\n", interface_version);
        break;
    }
}


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: tpCommand					*/
/*                                                         		*/
/* Procedure Description: command line interface			*/
/*                                                         		*/
/* Procedure Arguments: command string					*/
/*                                                         		*/
/* Procedure Returns: reply string					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
char* tpCommand (const char* cmd)
{
    int		i;
    char*		p;
    int		node;	/* node */
    char*		buf;
    testpoint_t	tp[DAQ_GDS_MAX_TP_NUM];

    /* help */
    if (gds_strncasecmp (cmd, "help", 4) == 0) {
        return cmdreply (_HELP_TEXT);
    }
    /* show */
    else if (gds_strncasecmp (cmd, "show", 4) == 0) {
        p = (char*) (cmd + 4);
        while (*p == ' ') {
            p++;
        }
        if (*p == '*') {
            buf = malloc (TP_MAX_NODE * 2000);
            if (!buf) /* JCB */
            {
                gdsDebug("tpCommand malloc (TP_MAX_NODE * 2000) failed.") ;
                return NULL ;
            }
            p = buf;
            for (node = 0; node < TP_MAX_NODE; node++) {
                if (tpNode[node].valid) {
                    queryCmd (p, node);
                    p = strend (p);
                }
            }
        }
        else {
            node = atoi(p);
            if ((node < 0) || (node >= TP_MAX_NODE)) {
                printf("invalid node number: 0 < node <= %d\n", TP_MAX_NODE);
                return cmdreply ("error: node number out of range");
            }
            if ((!tpNode[node].valid)) {
                return cmdreply ("error: node number invalid");
            }
            buf = malloc (2000);
            if (!buf) /* JCB */
            {
                gdsDebug("tpCommand malloc (2000) failed.") ;
                return NULL ;
            }
            queryCmd (buf, node);
        }
        buf = realloc (buf, strlen (buf) + 1);
        return buf;
    }
    /* set */
    else if (gds_strncasecmp (cmd, "set", 3) == 0) {
        p = (char*) (cmd + 3);
        while (*p == ' ') {
            p++;
        }
        char *endptr;
        node = strtol(p, &endptr, 10);

        if (endptr == p) {
            /* assume channel names are specified */
            if (tpRequestName (endptr, -1, NULL, NULL) < 0) {
                return cmdreply ("error: unable to set test point");
            }
            else {
                return cmdreply ("test point set");
            }
        }
        else {
            p = endptr;
            if ((node < 0) || (node >= TP_MAX_NODE)) {
                printf("invalid node number: 0 < node <= %d\n", TP_MAX_NODE);
                return cmdreply ("error: node number out of range");
            }
            /* assume test point numbers are specified */
            if (!tpNode[node].valid) {
                return cmdreply ("error: invalid node number");
            }
            /* read testpoint numbers */
            i = 0;
            do {
                p++;
                while (*p == ' ') {
                    p++;
                }
                tp [i++] = strtol (p, &p, 10);
            } while ((tp[i-1] != 0) && (i < DAQ_GDS_MAX_TP_NUM));
            /* set test point */
            if (tpRequest (node, tp, i, -1, NULL, NULL) < 0) {
                return cmdreply ("error: unable to set test point");
            }
            else {
                return cmdreply ("test point set");
            }
        }
    }
    /* clear */
    else if (gds_strncasecmp (cmd, "clear", 5) == 0) {
        p = (char*) (cmd + 5);
        while (*p == ' ') {
            p++;
        }
        /* read node */
        if (*p == '*') {
#ifdef CLEAR_ALL_TP_ALLOWED
            for (node = 0; node < TP_MAX_NODE; node++) {
                if (tpNode[node].valid) {
                    tpClear (node, NULL, 0);
                }
            }
            return cmdreply ("test point cleared");
#else
            return cmdreply ("Clearing all test points on all nodes is not allowed") ;
#endif
        }
        /* try reading node */
        char *endptr;
        node = strtol(p, &endptr, 10);

        if (endptr == p) {
            /* assume channel names are specified */
            if (tpClearName (p) < 0) {
                return cmdreply ("error: unable to clear test point");
            }
            else {
                return cmdreply ("test point cleared");
            }
        }
        else {
            p = endptr;
            if ((node < 0) || (node >= TP_MAX_NODE)) {
                printf("invalid node number: 0 < node <= %d\n", TP_MAX_NODE);
                return cmdreply ("error: node number out of range");
            }
            /* assume test point numbers are specified */
            if (!tpNode[node].valid) {
                return cmdreply ("error: invalid node number");
            }
            /* read testpoint numbers */
            i = 0;
            do {
                p++;
                while (*p == ' ') {
                    p++;
                }
                if (*p == '*') {
                    tp[i++] = _TP_CLEAR_ALL;
                }
                else {
                    tp [i++] = strtol (p, &p, 10);
                }
            } while ((tp[i-1] != 0) && (i < DAQ_GDS_MAX_TP_NUM));
            /* clear test point */
            if (tpClear (node, tp, i) < 0) {
                return cmdreply ("error: unable to clear test point");
            }
            else {
                return cmdreply ("test point cleared");
            }
        }
    }
    else {
        return cmdreply ("error: unrecognized command\n"
                         "use help for further information");
    }
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: tpcmdline					*/
/*                                                         		*/
/* Procedure Description: command line interface			*/
/*                                                         		*/
/* Procedure Arguments: command string					*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 on error			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int tpcmdline (const char* cmd)
{
    char* 		p;
    int		ret;

    p = tpCommand (cmd);
    if (p == NULL) {
        printf ("error: testpoints not supported\n");
        return -2;
    }
    ret = (strncmp (p, "error:", 6) == 0) ? -1 : 0;
    printf ("%s\n", p);

    free (p);
    return ret;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: keepAlive					*/
/*                                                         		*/
/* Procedure Description: sends keep alive				*/
/*                                                         		*/
/* Procedure Arguments: scheduler task, time, epoch, argument		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if not			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int keepAlive (int node)
{
    CLIENT*		clnt;		/* client rpc handle */
    int		ret = 0;		/* return value */

    /* make client handle */
    clnt = tpMakeHandle (node);
    if (clnt == NULL) {
        return -3;
    }



    if((keepalive_1(tpNode[node].id, &ret, clnt) != RPC_SUCCESS) ||
         ret != TRUE)
    {
        gdsError (GDS_ERR_PROG, "unable to send keep alive");
        ret = -4;
    }

    /* free handle */
    clnt_destroy (clnt);
    return ret;
}


/// Find RPC interface version used by awgtpman
/// version 4 was introduced about RCG version 4.1.0
/// all previous interface versions, including for RCG 4.0.1,
/// are considered version 3.
/// Version 4 has only two testpoint interfaces: one for excitations and one for
/// other test points.
/// Version 3 has 4 testpoint interfaces: the two interfaces of version 4 are
/// divided between LSC and ASC test points.
///
/// \param node the node number to identify
/// \return the version number, 0 on error
int testAwgTpInterfaceVersion(int node)
{
    // memoize
    static int node_version[ TP_MAX_NODE ];

    if ( node_version[ node ] )
    {
        return node_version[ node ];
    }

    resultQueryTP_r result;
    memset (&result, 0, sizeof (resultQueryTP_r));

    int chk = testpoint_client( );

    if (chk < 0) {
        printf("testAwgTpInterfaceVersion: testpoint_client() failed with return code %d\n", chk);
        return 0;
    }

    CLIENT * clnt = tpMakeHandle (node);
    if(clnt == NULL)
    {
        return 0;
    }

    // Test if TP_ASC_TP_INTERFACE is accessible
    // if so, we must be older version 3, otherwise, something newer.
    if ((querytp_1 (tpNode[node].id, node, TP_ASC_TP_INTERFACE, 128, 0,
                    0, &result, clnt) == RPC_SUCCESS) && (result.status >= 0)) {
        printf("found version 3 or older test point interface\n");
        node_version[node] = 3;
    }
    else
    {
        printf("found version 4 or newer test point interface\n");
        node_version[node] = 4;
    }

    xdr_free ((xdrproc_t)xdr_resultQueryTP_r, (char*) &result);
    clnt_destroy (clnt);
    return node_version[node];
}