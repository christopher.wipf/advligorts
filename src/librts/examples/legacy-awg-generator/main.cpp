#include "Model.hh"

#include "RampGenerator.hh"


//AWG
#include "awgstandalone.h"


#include <iostream>
#include <fstream>
#include <cstring>


int main(int argc, char** argv)
{


    std::unique_ptr<rts::Model> model_ptr = rts::Model::create_instance();
    if(model_ptr == nullptr)
    {
        std::cout << "Error coult not create Model. Exiting..."  << std::endl;
        return 2;
    }


    //Set inputs 
    //
    model_ptr->set_adc_channel_generator( 0, 30, std::make_unique<rts::RampGenerator>(0, 1) ); 

    //Configure modules
    //
    model_ptr->get_awg_slot("TEST_EXCITATION");


    //Configure record points
    //
    model_ptr->record_model_var("EXCITATION_EPICS_OUT", model_ptr->get_model_rate_Hz());

    int slot = model_ptr->get_awg_slot("TEST_EXCITATION");
  
    std::cout << "Slot returned was: " << slot << "\n";

    //Awg lib call 

    AWG_Component awg_com;
    std::memset(&awg_com, 0, sizeof(awg_com));
    awg_com.wtype = awgSine;
    awg_com.start = (model_ptr->get_gps_time()+1) * 1e9;
    awg_com.duration = -1;
    awg_com.restart = -1;
    awg_com.par[0] = 1;
    awg_com.par[1] = 1.1;

    addWaveformAWG(slot, &awg_com, 1);


    //Run Model
    //
    model_ptr->run_model(model_ptr->get_model_rate_Hz()*3);

    //Examine Recorded Outputs
    std::optional< std::vector<double> > epics_out = model_ptr->get_recorded_var<double>("EXCITATION_EPICS_OUT");
    for (auto val : epics_out.value() )
    {
        std::cout << val << ", ";
    }
    std::cout << "\n";

    model_ptr->free_awg_slot( slot );

    return 0;
}
