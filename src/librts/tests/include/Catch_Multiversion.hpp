#ifndef LIGO_CATCH_MULTIVERSION_HPP
#define LIGO_CATCH_MULTIVERSION_HPP


// Handle the differences between versions
// V1: USE_OLD_CATCH_HEADER_ONLY is defined by cmake, header only
// V2: Only "catch2/catch.hpp" header, link with Catch2::Catch2WithMain
// V3: We now have "catch2/catch_all.hpp" header, and code in name spaces. 
//     Link with Catch2::Catch2WithMain, and use namespace.
//
#ifdef USE_OLD_CATCH_HEADER_ONLY
//V1
#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#else

#if __has_include (<catch2/catch_all.hpp>)
//V3
#include <catch2/catch_all.hpp>
using namespace Catch;
#else

//V2
#include <catch2/catch.hpp>
#endif

#endif


#endif //LIGO_CATCH_MULTIVERSION_HPP
