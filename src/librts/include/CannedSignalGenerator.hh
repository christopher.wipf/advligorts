#ifndef LIGO_CANNED_SIGNAL_GENERATOR_HH
#define LIGO_CANNED_SIGNAL_GENERATOR_HH

#include "Generator.hh"

#include <vector>
#include <stdexcept>

namespace rts
{
    class CannedSignalGenerator : public Generator
    {
        public:
            CannedSignalGenerator(const std::vector<double>& canned_data, bool repeat = true, uint64_t wait_cycles = 0, double gain = 1.0, bool hold_zero = false)
                : _cur_inx(0), _repeat(repeat), _canned_data(canned_data), _wait_cycles(wait_cycles), _current_gain(gain), _delta_gain(0.0), _requested_gain(gain), _hold_zero(hold_zero)
            {
            }
            virtual double get_next_sample()
            {
                double gain_diff = _current_gain - _requested_gain;
                if (abs(gain_diff) > abs(gain_diff + _delta_gain))
                {
                    //Continue ramping gain
                    _current_gain += _delta_gain;
                }
                else
                {
                    //Done ramping
                    _current_gain = _requested_gain;
                    _delta_gain = 0.0;
                }
                //No output until preset start time
                if (_wait_cycles > 0)
                {
                    _wait_cycles--;
                    return 0.0;
                }
                //When we aren't repeating, we just return the last value, or zero
                if(_repeat == false && _cur_inx == _canned_data.size() - 1)
                {
                    if (_hold_zero)
                    {
                        return 0.0;
                    }
                    else
                    {
                        return _canned_data[_cur_inx] * _current_gain;
                    }
                }

                double hold = _canned_data[_cur_inx];
                _cur_inx = (_cur_inx + 1) %  _canned_data.size();
                return hold * _current_gain;
            }
            virtual void reset()
            {
                _cur_inx = 0;
            }
            virtual void set_gain(double gain, uint64_t ramp_cycles = 0)
            {
                _requested_gain = gain;
                if (ramp_cycles > 0)
                {
                    _delta_gain = (_requested_gain - _current_gain)/ramp_cycles;
                }
                else
                {
                    _current_gain = gain;
                    _delta_gain = 0.0;
                }
            }
            virtual void append_data(std::vector<double>& data)
            {
                _canned_data.insert(_canned_data.end(), data.begin(), data.end());
            }
            virtual ~CannedSignalGenerator() {};
        private:

            uint64_t _cur_inx;
            bool _repeat;
            std::vector<double> _canned_data;
            uint64_t _wait_cycles;
            double _current_gain;
            double _delta_gain;
            double _requested_gain;
            bool _hold_zero;

    };
}




#endif //LIGO_CANNED_SIGNAL_GENERATOR_HH
