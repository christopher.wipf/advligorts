#ifndef UNIFORM_REAL_GENERATOR_HH
#define UNIFORM_REAL_GENERATOR_HH

#include "Generator.hh"

#include <random>

namespace rts
{
    /**
    * @brief Generates uniform random numbers in the range [min, max)
    */
    class UniformRealGenerator : public Generator
    {
        public:
            UniformRealGenerator(double min, double max) : _dist(min, max)
            {
            }
            virtual double get_next_sample()
            {
                return _dist(_generator);
            }
            virtual void reset() {}
            virtual ~UniformRealGenerator() {};
        private:

            std::default_random_engine _generator;
            std::uniform_real_distribution<double> _dist;
    }; 
}

#endif //UNIFORM_REAL_GENERATOR_HH

