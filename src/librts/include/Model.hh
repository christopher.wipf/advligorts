#ifndef LIGO_MODEL_HH
#define LIGO_MODEL_HH

#include "Generator.hh"

#include "LIGO_FilterModule.hh"

#include <memory>
#include <string>
#include <vector>
#include <optional>
#include <cstdint>

namespace rts
{

    class Model
    {
        public:

            //
            // Constructor/Destructor
            //

            /**
             * @brief This static method must be used to create instances of the model
             *
             * The default log level is info and higher, you can change it with the
             * log_level parameter, or with Model::set_log_level() after the 
             * model has been created/initialized.
             *
             * @return The newly constructed model class or nullptr if there was 
             *         an nonrecoverable error.
             */
            static std::unique_ptr<Model> create_instance( int log_level = 1 );

            virtual ~Model();

            //
            // General model methods
            //
            /**
             * @brief Returns the cycle rate of the model
             *
             * The model rate is configured in the cdsParameters block
             * in the model file.
             * 
             * @return The model rate in Hz
             */
            int get_model_rate_Hz();


            /**
             * @brief Returns the model name on the built model
             *
             * The model name is from the built mdl file. The model
             * name returned will be the MODEL_NAME in <MODEL_NAME>.mdl
             *
             * @return The model name as a string
             */
            std::string get_model_name();


            /**
             * @brief Returns the model's cycle number
             *
             * @return Number of cycles the model has run in the current gps second
             */
            int get_cycle_num();


            /**
             * @brief Returns the gps time of the model
             *
             * @return The gps time in seconds
             */
            uint64_t get_gps_time();


            /**
             * @brief Sets the gps time of the model
             */
            void set_gps_time(uint64_t gps);


            /**
             * @brief Sets the log level for the rts logger in the underlying model
             *
             * Log Levels Are:
             * debug: 0
             * info:  1
             * warn:  2
             * error: 3
             *
             * The log level set will filter out any messages lower than it.
             *
             */
            void set_log_level(int level);

            /**
             * @brief Returns the number of filter modules in the model
             *
             * The number of filter modules is based on model design
             * and depends of the number of filter modules placed in 
             * the model file
             * 
             * @return The number of filter module
             */
            int get_num_filter_modules();

            //
            // Model variable utilities
            //
           /**
             * @brief Returns the names of all input types supported by the model
             *
             * The model rate is configured in the cdsParameters block
             * in the model file.
             * 
             * @param none
             * @return A vector of strings containing all names of input types
             */
            std::vector<std::string> get_all_input_names();

           /**
             * @brief Returns the names of all set/get able model vars
             *
             * The model rate is configured in the cdsParameters block
             * in the model file.
             * 
             * @param none
             * @return A vector of strings containing all names of usable
             *         model variables
             */
            std::vector<std::string> get_all_model_var_names();

            /**
             * @brief Sets the requested variable name to the given value
             *
             * @param name The name of the model var you are trying to set
             * @param val The var value you would like to set
             * @return True if the variable was set, false if the variable name could not be found
             */
            bool set_var(const std::string& name, double val);
            bool set_var(const std::string& name, float val);
            bool set_var(const std::string& name, int val);

            /**
             * @brief Marks a variable by name, so that on the next cycle (N) the model
             *        is run, the variable with the name is set to the requested value, 
             *        and on the next cycle (N+1) it is set back to its original value.
             *
             * @param name The name of the model var you are trying to set momentarily
             * @param val The var value you would like to set momentarily
             * @return True if the variable was found and marked, false if the variable was not found
             */
            bool set_var_momentary(const std::string& name, double val);

            /**
             * @brief Retrieves a model var by name
             *
             * @param name The name of the model var you are retrieving
             * @return True if the variable was set, false if the variable name could not be found
             * @note This is a template function and can get the val as a int, double, float or char
             */
            template<class T>
            std::optional<T> get_var(const std::string& name) const;
            
          /**
             * @brief Marks a model variable by name for recording
             *
             * After marking a variable to be recorded with record_model_var(), running
             * the model with run_model(num_cycles), the samples (for all num_cycles) can
             * be retrieved with get_recorded_var()
             * 
             * @param name The name of the var you are recording
             * @param record_rate_hz The rate at which to record the variable
             * 
             * @return True if the variable name was found and marked for recording
             *         False if the variable name was not found
             * @note You can only record a variable at up to to model rate
             */
            bool record_model_var(const std::string & name, int record_rate_hz);

          /**
             * @brief Deselects a model variable by name for recording
             *
             * @param name The name of the var you are no longer recording
             *
             * @return True if the variable name was found and deselected
             *         False if the variable name was not found
             */
            bool stop_recording_model_var(const std::string & name);

            /**
             * @brief Returns all samples recorded during the last call to run_model()
             *
             * After marking a variable to be recorded with record_model_var(), running
             * the model with run_model(num_cycles), the samples (for all num_cycles) can
             * be retrieved with this call.
             * 
             * @param name The name of the var you are retrieving 
             * @return nothing if the var was not selected for recording 
             *         prior to the run_model() call, or if the name does not match 
             *         a recorded var, and the samples recorded values if they were recorded
             * @note You only get samples generated during the last call to run_model()
             */
            template<class T>
            std::optional<std::vector<T>> get_recorded_var(const std::string & name) const;

            /**
             * @brief This method controls how recorded DAC and variable data
             *        is treated between calls to run_model()
             *
             *        When recording DAC and variable data the default behavior is to 
             *        clear the buffers at the start of each call to run_model(). This 
             *        means that if you make multiple calls to run_model() and call
             *        get_recorded_var() after the last call, it will only contain the data
             *        generated by the model during the last call to run_model(). You can 
             *        call get_recorded_var() between calls to run_model() to get intermediate
             *        data. This is the default behavior because buffering all recorded data 
             *        can take a lot of memory. 
             *
             *        When this option is enabled, true passed as a parameter, the data
             *        returned by get_recorded_var() and get_dac_output_by_id() 
             *        won't be cleared between calls to run_model(). So when you configure 
             *        a variable to be recorded, run  
             *
             *
             *
             * @param enable true: Record data from multiple runs, false: only record data
             *               from the last run.
             *
             * @return None
             */
            void multicycle_record_control( bool enable );

            /**
             * @brief Loads variables from a .snap file into the model
             *
             * @param pathname The pathname of the .snap file to load
             *
             * @return -1 if there was an issued opening or parsing the snap file,
             *         if parse was successful the number of variables loaded 
             *         from the given snap file is returned. 
             */
            int load_snap_file(const std::string & pathname);


            //
            // Filter module control
            //

           /**
             * @brief Returns the names of all filter modules
             * 
             * @param none
             * @return A vector of strings containing all names of filter
             *         modules
             */
            std::vector<std::string> get_all_filter_names();


            /**
             * @brief Retrieves a filter module by name
             *
             * @param filter_name The name of the filter module you would like to retrieve
             * @return Nothing if the filter module could not be found 
             *         Or the filter module requested if the name could be found
             */
            std::optional<rts::LIGO_FilterModule> get_filter_module_by_name(const std::string & filter_name);

            /**
             * @brief Retrieves a filter module by ID
             *
             * @param name The ID of the filter module you would like to retrieve
             * @return Nothing if the filter module could not be found 
             *         Or the filter module requested if found
             */
            std::optional<LIGO_FilterModule> get_filter_module_by_id(int module_index);

            /**
             * @brief Resets the requested filter module
             * 
             * Currently this clears out the filter module history
             *
             * @param filter_name The name of the filter module you would like to reset
             * @return True if a filter module with the requested name was found
             *         False if no module with the name was found
             */
            bool reset_filter_module(const std::string & filter_name);

            /**
             * @brief Reads the coefficients of the requested filter module stage
             *
             * TODO: document format of coefficients
             *
             * @param filter_name The name of the filter module you would like to
             *                    read coefficients on
             * @param stageIndexToRead The stages the coefficients should be read from
             * @return The coefficients of the requested filter section
             */
            std::vector<double> read_biquad_coefficients(const std::string & filter_name,
                                                         int stageIndexToRead);
            /**
             * @brief Loads the requested filter module's stages with
             *        the given coefficients
             * 
             * TODO: document format of coefficients
             *
             * @param filter_name The name of the filter module you would like to
             *                    load coefficients on
             * @param stageIndicesToLoad A vector of all the stages the coefficients
             *                           should be loaded into
             * @return True if a filter module with the requested name was found
             *         False if no module with the name was found
             */
            bool load_biquad_coefficients(const std::string & filter_name,
                                          const std::vector<int> & stageIndicesToLoad,
                                          const std::vector<double> & coefficients);
            /**
             * @brief Loads the requested filter module's stages with
             *        the given coefficients
             * 
             * The scipy format is documented here:
             * https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.sosfilt.html
             *
             * It is in the format of a list of lists with dimensions [N][6]
             * Where N is the number of second order sections (SOSs)
             * And the 6 elements are a SOS with the first three elements as the
             * numerator coefficients and the last three the denominator coefficients
             *
             * sp = scipy_coeffs[0]
             * H(z) = (sp[0] + sp[1] + sp[2]) / (sp[3] + sp[4] +sp[5])
             * 
             * For C++ we just swap the list for a vector[N][6]
             * 
             * This method will do a conversion to advligo format (TODO:cite) 
             * and a biquad conversion if the filter module is configured as such
             *
             * @param filter_name The name of the filter module you would like to
             *                    load coefficients on
             * @param stageIndicesToLoad A vector of all the stages the coefficients
             *                           should be loaded into
             * @return True if a filter module with the requested name was found
             *         False if no module with the name was found
             */
            bool load_scipy_sos_coef(const std::string & filter_name,
                                     const std::vector<int> & stageIndicesToLoad,
                                     const std::vector< std::vector<double> > & stages_and_coefficients);


            /**
             * @brief Reads the coefficients of the requested FIR filter module stage
             *
             * @param filter_name The name of the filter module you would like to
             *                    read coefficients from
             * @param stageIndexToRead The index of the stage to read from
             * @return The coefficients of the requested filter section
             */
            std::vector<double> read_fir_coefficients(const std::string & filter_name,
                                                      int stageIndexToRead);


            /**
             * Loads from default path
             *
             */
            bool load_from_filter_file(const std::string & filterFilePathName, const std::string & firFilterFilePathName);

            /**
             * @brief Gets the switch type of the requested filter module stage
             *
             * TODO: document format of switch type
             *
             * @param filter_name The name of the filter module you would like to
             *                    get a switch type on
             * @param section_index The stage the switch type should be read from
             * @return The switch type of the requested filter section
             */
            int get_filter_switch_type(const std::string & filter_name,
                                       int section_index);
            /**
             * @brief Sets the switch type of the requested filter module stage
             *
             * TODO: document format of switch type
             *
             * @param filter_name The name of the filter module you would like to
             *                    get a switch type on
             * @param section_index The stage the switch type should be set for
             * @return True if a filter module with the requested name was found
             *         False if no module with the name was found
             */
            bool set_filter_switch_type(const std::string & filter_name,
                                        int section_index,
                                        int switch_type);


            //
            // Statespace Part Control
            //
            /**
             * @brief Returns the number of statespace parts in the model
             *
             * @return The number of statespace parts in the model, 0 if 
             *         there are none.
             */
            int get_num_statespace_parts();

            /**
             * @brief Returns the name of all statespace parts in the model
             *
             * @return A vector with all statespace part names
             */
            std::vector< std::string > get_all_statespace_names();

            /**
             * @brief Find the statespace part with the requested name,
             *        and load the other parameters as the configuration.
             *
             * @param part_name The name of the part to load.
             * @param initial_state_vec The initial state vector that should be loaded into the part.
             *                          Expected len: state_vec_len
             * @param input_matrix The input matrix, flattened in row major order.
             *                     Expected len: state_vec_len * input_vec_len
             * @param output_matrix The output matrix, flattened in row major order.
             *                      Expected len: output_vec_len * state_vec_len
             * @param state_matrix The state matrix, flattened in row major order.
             *                     Expected len: state_vec_len * state_vec_len
             * @param feedthrough_matrix The feedthrough matrix, flattened in row major order.
             *                           Expected len: 1 or (output_vec_len * input_vec_len)
             * 
             * @return False if the part name was not found, or if the parameters
             *         were not as expected, matrix len, etc. True if the part with
             *         the given name was loaded.
             */
            bool load_statespace( const std::string & part_name, 
                                 const std::vector< double >  & initial_state_vec,
                                 const std::vector< double >  & input_matrix,
                                 const std::vector< double >  & output_matrix,
                                 const std::vector< double >  & state_matrix,
                                 const std::vector< double >  & feedthrough_matrix );

            /**
             * @brief Load all statespace parts with matching configuration from
             *        the file located at the path given.
             *
             * @param statespaceFilePathName The pathname of the statespace 
             *                               JSON configuration file you would 
             *                               like to load.
             * 
             * @return -1 if there was an issue with the configuration file, 
             *         bad format, not valid JSON, mismatched vector sizes to
             *         matrix sizes, etc.
             *         0 If no parts in the config matched parts in the model,
             *         or if no parts are in the model.
             *         Otherwise it returns the number of configs loaded.
             */
            int load_statespace_from_file( const std::string & statespaceFilePathName );


            //
            // Model runtime configuration
            //
            /**
             * @brief Sets the requested ADC channel so that its output is generated by 
             *        the given Generator object
             *
             * @param adcIndex The index of the ADC your are trying to set
             * @param chanIndex The index of the ADC's channel you are trying to set
             * @return None
             */
            void set_adc_channel_generator(int adcNum, int chanNum, std::shared_ptr<Generator> gen);

            /**
             * @brief Sets the requested Excitation point so that its output is generated by 
             *        the given Generator object
             *
             * @param adcIndex The index of the excitation your are trying to set
             * @return True if the excitation point was found and set, false if the excitation
             *         point could not be found
             */
            bool set_excitation_point_generator(const std::string & name, std::shared_ptr<Generator> gen);

            /**
             * @brief Sets up an AWG slot for the requested channel
             *
             * @return AWG slot number or error (< 0)
             */
            int get_awg_slot(const std::string & name);

            /**
             * @brief Releases an AWG slot
             *
             * @return 0 or error (< 0)
             */
            int free_awg_slot(int slot);

            /**
             * @brief Sets the requested IPE receiver so that its output is generated by 
             *        the given Generator object
             *
             * @param adcIndex The index of the IPC receiver your are trying to set
             * @return True if the IPC receiver was found and set, false if the excitation
             *         point could not be found
             */
            bool set_ipc_receiver_generator(const std::string & name, std::shared_ptr<Generator> gen);

            /**
             * @brief When set to true all DAC output will be recorded each time run_model() 
             *        id called. False will disable DAQ recording.
             * 
             * This may be useful to disable when you want to step your model into
             * the future without recording the DAC output. An allocation is done to 
             * store all the cycles requested of run_model(num_cycles), so care should
             * be taken when this is enabled and the model is cycled many times
             *
             * @param shouldRec The index of the ADC your are trying to set
             * @return None
             */
            void record_dac_output(bool shouldRec);

            /**
             * @brief Requests the samples recorded by the DAC channel after a call
             *        to run_model() has been completed.
             * 
             * Note that only the samples generated during the last call to run_model()
             * are returned.
             *
             * @param cardIndex The index of the DAC
             * @param chanIndex The index of the DAC channel
             * @return The samples recorded, or an empty vector if recording was not enabled
             */
            const std::vector< double > & get_dac_output_by_id(int cardIndex, int chanIndex);

            /**
             * @brief Runs the model for the requested number of cycles
             * 
             * This is the core of the library interface to a model. When a 
             * model is run ADC input is generated logic is computed and outputs
             * are calculated. 
             *
             * @param num_cycles The number of cycles the model should be run for
             * @return None
             */
            int run_model(unsigned num_cycles);


        private:

            Model(); // Use createInstance(...) to instantiate the class
            static inline bool _has_been_created;

            class Impl;
            std::unique_ptr<Impl> _impl;
    };

}

#endif //LIGO_MODEL_HH
