#ifndef GAUSSIAN_NOISE_GENERATOR_HH
#define GAUSSIAN_NOISE_GENERATOR_HH

#include "Generator.hh"

#include <random>

namespace rts
{
    class GaussianNoiseGenerator : public Generator
    {
        public:
            GaussianNoiseGenerator(double mean, double stddev) : _dist(mean, stddev)
            {
            }
            virtual double get_next_sample()
            {
                return _dist(_generator);
            }
            virtual void reset() {}
            virtual ~GaussianNoiseGenerator() {};
        private:

            std::default_random_engine _generator;
            std::normal_distribution<double> _dist;
    }; 
}

#endif //GAUSSIAN_NOISE_GENERATOR_HH

