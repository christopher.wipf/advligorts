#ifndef _AWGSTANDALONE_H
#define _AWGSTANDALONE_H

#ifdef __cplusplus
extern "C" {
#endif

/* AWG constants from map_v3.h, which conflicts with daqmap.h */

/** Identification for an LSC test point channel

    @author DS, Jul. 1998
    @see Arbitrary Waveform Generator
*********************************************************************/
#define	AWG_LSC_TESTPOINT		1

/** Identification for an ASC test point channel

    @author DS, Jul. 1998
    @see Arbitrary Waveform Generator
*********************************************************************/
#define	AWG_ASC_TESTPOINT		2

/** Identification for a DAC channel

    @author DS, Jul. 1998
    @see Arbitrary Waveform Generator
*********************************************************************/
#define	AWG_DAC_CHANNEL			3

/** Identification for a DS340 channel

    @author DS, Jul. 1998
    @see Arbitrary Waveform Generator
*********************************************************************/
#define	AWG_DS340_CHANNEL		4

/** Identification for a channel which gets written to file.

    @author DS, Jul. 1998
    @see Arbitrary Waveform Generator
*********************************************************************/
#define	AWG_FILE_CHANNEL		5

/** Identification for a channel which gets written to an aboslute
    address in reflective memory.

    @author DS, Jul. 1998
    @see Arbitrary Waveform Generator
*********************************************************************/
#define	AWG_MEM_CHANNEL			6


#include "awg.h"
#include "big_buffers.h"
#include "tconv.h"


void awgstandalone_set_model_rate_Hz(int);
void setTAInow (tainsec_t);
void tAWG(void);
awgbuf_t *get_awgbuf(int);


#ifdef __cplusplus
}
#endif

#endif /* _AWGSTANDALONE_H */
