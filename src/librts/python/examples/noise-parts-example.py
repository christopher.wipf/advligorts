#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig
import math




mod = model.create_instance()


#Mark part outputs so they are recorded
mod.record_model_var("OSC_UNIFORM_NOISE_OUT", mod.get_model_rate_Hz())
mod.record_model_var("OSC_GAUSSIAN_NOISE_OUT", mod.get_model_rate_Hz())


#Run model
mod.run_model( 2048 ) 


#Collect part outputs/inputs
uniform_out = mod.get_recorded_var("OSC_UNIFORM_NOISE_OUT")
gaussian_out = mod.get_recorded_var("OSC_GAUSSIAN_NOISE_OUT")


fig, axs = plt.subplots(2, 1)
fig.suptitle('cdsNoise Part')
fig.tight_layout()
axs[0].plot(uniform_out, 'ro', markersize=2)
axs[0].set_title('Raw outputs')
axs[1].hist(uniform_out, 2048)
axs[1].set_title('Count of each output, nbins=2048')


figg, axsg = plt.subplots(2, 1)
figg.suptitle('cdsGaussianNoiseGenerator Part')
figg.tight_layout()
axsg[0].plot(gaussian_out, 'ro', markersize=2)
axsg[0].set_title('Raw outputs')
axsg[1].hist(gaussian_out, 128)
axsg[1].set_title('Count of each output, nbins=128')




plt.show()