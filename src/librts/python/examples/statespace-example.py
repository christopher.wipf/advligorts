#!/usr/bin/env python3
import x1unittest_pybind as model #Built by rtslib
import matplotlib.pyplot as plt


mod = model.create_instance()

num_parts = mod.get_num_statespace_parts()

# Load the statespace configuration file into the model parts
res = mod.load_statespace_from_file("examples/data/statespace_config.json")
if res != num_parts:
    print(f"Could not load from requested file, res = {res}\n")
    exit(0)




mod.multicycle_record_control(True) #Record vars across multiple calls to run_model()
mod.record_model_var("STATESPACE_TEST_1_1_OUT", mod.get_model_rate_Hz())
mod.record_model_var("STATESPACE_TEST_4_2_OUT1", mod.get_model_rate_Hz())
mod.record_model_var("STATESPACE_TEST_4_2_OUT2", mod.get_model_rate_Hz())

# Test a reset of the part, the config file has a non-zero initial state, so the part is stabilizing
mod.run_model( 2 )
mod["STATESPACE_TEST_1IN_1OUT_RESET"] = 1
mod.run_model( 33 )

# Impulse one of the inputs so we can plot the response
mod["STATESPACE_TEST_1_1_IN"] = 1.0
mod.run_model( 1 ) 
mod["STATESPACE_TEST_1_1_IN"] = 0.0

# Run the model more so we can plot the outputs
mod.run_model( 35 )

# Get data from the runs above
ss_out = mod.get_recorded_var("STATESPACE_TEST_1_1_OUT")
four_to_one_out = mod.get_recorded_var("STATESPACE_TEST_4_2_OUT1")
four_to_one_out2 = mod.get_recorded_var("STATESPACE_TEST_4_2_OUT2")


# Trend data
p1 = plt.figure(1)
plt.plot(ss_out)
plt.title("1-1 Statespace output")

p1 = plt.figure(2)
plt.plot(four_to_one_out)
plt.plot(four_to_one_out2)
plt.title("4-2 Statespace outputs")

plt.show()
