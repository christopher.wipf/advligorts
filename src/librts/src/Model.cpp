#include "Model.hh"
#include "LIGO_FilterModule.hh"
#include "ModelVar.hh"
#include "DolphinBuffers.hh"
#include "SnapFile.hh"

#include "GaussianNoiseGenerator.hh"
#include "CannedSignalGenerator.hh"
#include "ConstantGenerator.hh"

//libspdlog-dev
#include "spdlog/spdlog.h"
#include "spdlog/version.h"
#if SPDLOG_VERSION >= 10500 
#include "spdlog/cfg/env.h"
#endif

//rapidJSON
#include "rapidjson/document.h" 


//module build
#include "var_lookup_table.h"

//advligorts
#include "fm10Gen.h" // checkFiltReset,
#include "util/macros.h" //COUNT_OF()
#include "util/lstring.h" //lstrlcpy()
#include "../fe/mapApp.h" //initmap, mapPciModules
#include "../fe/print_io_info.h" //print_io_info()
#include "../fe/rcguserCommon.h" //attach_shared_memory()
#include "../fe/demod/Demodulation.h"  //demodulation_init()
#include "feComms.h" //RFM_FE_COMMS
#include "controller.h"
#include "drv/rts-logger.h"
#include "fmReadCoeff.h"
#include "part_headers/statespace/stateSpacePart.h"


//awg
#include "awgstandalone.h"
#include "AwgGenerator.hh"
#define CYCLES_PER_EPOCH (MODEL_RATE_HZ/NUMBER_OF_EPOCHS)

#include <map>
#include <fstream>


using namespace rts;

//
// The RCG generated code does not define this at all in the 
// case where no IPCs are present in the model, that leads to 
// undefined refrencens, so we define it in that case
#if MODEL_NUM_IPCS_USED == 0
CDS_IPC_INFO ipcInfo[1];
#endif

class Model::Impl
{
    public:

        static std::unique_ptr<Model::Impl> create_instance( int log_level );
        virtual ~Impl(); 

        int get_model_rate_Hz();
        std::string get_model_name();
        int get_cycle_num();
        uint64_t get_gps_time();
        void set_gps_time(uint64_t gps);
        void set_log_level(int level);
        int get_num_filter_modules();
        std::vector<std::string> get_all_input_names();
        std::vector<std::string> get_all_adc_names();
        std::vector<std::string> get_all_dac_names();
        std::vector<std::string> get_all_filter_names();

        std::vector<std::string> get_all_model_var_names();

        bool set_var(const std::string& name, double val);
        bool set_var(const std::string& name, int val);

        bool set_var_momentary(const std::string& name, double val);

        template< class T>
        std::optional<T>  get_var(const std::string& name) const;

        bool record_model_var(const std::string & name, int record_rate_hz);
        bool stop_recording_model_var(const std::string & name);

        
        template< class T>
        std::optional<std::vector<T>> get_recorded_var(const std::string & name) const;

        void multicycle_record_control( bool enable );

        int load_snap_file(const std::string & pathname);

        std::optional<LIGO_FilterModule> get_filter_module_by_name(std::string name);
        std::optional<LIGO_FilterModule> get_filter_module_by_id(int module_index);
        bool reset_filter_module(const std::string & name);
        std::vector<double> read_biquad_coefficients(const std::string & filter_name,
                                                     int stageIndexToRead);
        bool load_biquad_coefficients(const std::string & filter_name,
                                      const std::vector<int> & stageIndicesToLoad,
                                      const std::vector<double> & coefficients);
        bool load_scipy_sos_coef(const std::string & filter_name,
                                 const std::vector<int> & stageIndicesToLoad,
                                 const std::vector< std::vector<double> > & sections_and_coefficients);
        std::vector<double> read_fir_coefficients(const std::string & filter_name, int stageIndexToRead);
        bool load_from_filter_file(const std::string & filterFilePathName, const std::string & firFilterFilePathName);
        int get_filter_switch_type(const std::string & filter_name,
                                    int section_index);
        bool set_filter_switch_type(const std::string & filter_name,
                                    int section_index,
                                    int switch_type);


        int get_num_statespace_parts();
        int load_statespace_from_file( const std::string & statespaceFilePathName );
        std::vector< std::string > get_all_statespace_names();
        bool load_statespace( const std::string & part_name, 
                             const std::vector< double >  & initial_state_vec,
                             const std::vector< double >  & input_matrix,
                             const std::vector< double > & output_matrix,
                             const std::vector< double >  & state_matrix,
                             const std::vector< double >  & feedthrough_matrix );


        void set_default_inputs();
        void set_adc_channel_generator(int adcNum, int chanNum, std::shared_ptr<Generator> gen);
        bool set_excitation_point_generator(const std::string & name, std::shared_ptr<Generator> gen);
        int get_awg_slot(const std::string & name);
        int free_awg_slot(int slot);
        bool set_ipc_receiver_generator(const std::string & name, std::shared_ptr<Generator> gen);
        void record_dac_output(bool shouldRec);
        const std::vector< double > & get_dac_output_by_id(int dacIndex, int chanIndex);




        int run_model(unsigned num_cycles);

    private:

        int _numPCIeCards;

        //Pointer to EPICS shared memory space
        RFM_FE_COMMS* _pEpicsComms;

        //Start filter data
        /// Pointer to filter coeffs local memory.
        COEF _dspCoeff[ NUM_SYSTEMS ]; // Local mem for SFM coeffs.
        //TODO: Should this be in shared memory?


        //Start Operatinal Variables
        int _cycleNum = 0;
        uint64_t _gpsTime = 1000000000;

        //A [cards][chans] multidimensional vector, storing all channel generators
        std::vector< std::vector< std::shared_ptr<Generator> > > _adcs;

        bool _recordDacOutput = true;
        std::vector< std::vector< std::vector< double > > > _dacOutputs;

        std::map< std::string, ModelVarLocation > _model_vars;
        std::map< std::string, int > _filter_modules;
        std::map< std::string, int > _statespace_name_to_index_map;

        bool _multicycle_record = false;

        typedef struct excitation_record_t {
            ModelVarLocation target_var;
            std::shared_ptr<Generator> exc_generator;
        } excitation_record_t;
        std::map< std::string, excitation_record_t > _excitation_points;
        //For legacy AWG interface support, we need to lookup the slot
        std::map< int, std::string > _awg_slot_to_excitation;
        
        
        struct ipc_data_gen_map_t { int ipc_index; int generator_index; };
        std::map< std::string, ipc_data_gen_map_t > _ipc_receivers;
        std::vector< std::shared_ptr<Generator> > _ipc_generators;

        std::map< std::string, ModelVarValue > _momentary_vars;
        

        //
        // Start runtime members
        typedef struct recored_var_info_t {
            int samples_per_record;
            ModelVarLocation var_info;
            std::vector< ModelVarValue > values;
        } recored_var_info_t;
        std::map< std::string, recored_var_info_t > _vars_to_record; //This is a list of currently recorded model vars

        //
        // Start Private Member Functions
        //
        Impl() {};
        void clear_dac_outputs();
        bool init_filter_modules();
        bool initialize_model_globals();

        template< class T>
        bool setVarTemplate(const std::string& name, T val);
        void setValueFromLocation(ModelVarValue & set_me, ModelVarLocation & with_me);

        // Filter module helpers
        std::vector<double> scipy_sos_to_adv_sos(const std::vector< std::vector<double> > & scipy_coeffs);
        std::vector<double> adv_df_sos_to_adv_biquad(const std::vector< double > coeffs);

        //Dolphin simulation helpers
        std::unique_ptr<DolphinBuffers> _dolphin_buffers_ptr;

        //Memory pointers when we use local memory instead of MBUFs or POSIX shmem
        std::unique_ptr< char[] > _epics_buffer_ptr;
        std::unique_ptr< char[] > _testpoint_buffer_ptr;
        std::unique_ptr< char[] > _awg_buffer_ptr;
        std::unique_ptr< char[] > _ipc_buffer_ptr;
        std::unique_ptr< char[] > _shmem_ipc_buffer_ptr;
        std::unique_ptr< char[] > _daq_buffer_ptr;
        std::unique_ptr< char[] > _io_mem_data_buffer_ptr;


};

std::unique_ptr<Model::Impl> Model::Impl::create_instance( int log_level)
{
    std::unique_ptr<Model::Impl> me_ptr (new Model::Impl());

    //Set the internal model log level
    me_ptr->set_log_level(log_level);


    if( !me_ptr->initialize_model_globals() ) return nullptr;

    //TODO: the generated var_lookup_table.h only has offsets because pLocalEpics is set on initialization
    //Maybe we can find a better way than this
    char* base = (char*)pLocalEpics;

    //Add common inputs
    for (const model_var_lookup_t & var : common_inputs)
    {
        spdlog::debug("Adding {} to known model vars, type: {}", var.var_name, SUPPORTED_TYPES_NAMES[var.type]);
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >( 
                                               std::string(var.var_name), ModelVarLocation( (uint64_t)var.val_ptr + base, var.type) ) );
    }

    //Add common outputs
    for (const model_var_lookup_t & var : common_outputs)
    {
        spdlog::debug("Adding {} to known model vars, type: {}", var.var_name, SUPPORTED_TYPES_NAMES[var.type]);
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                               std::string(var.var_name), ModelVarLocation( (uint64_t)var.val_ptr + base, var.type) ) );
    }

    //Add model variables
    for (const model_var_lookup_t & var : model_vars)
    {
        spdlog::debug("Adding {} to known model vars, type: {}", var.var_name, SUPPORTED_TYPES_NAMES[var.type]);
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                               std::string(var.var_name), ModelVarLocation( (uint64_t)var.val_ptr + base, var.type) ) );
    }

    //Add test points to model vars
    for (const auto & var : test_points)
    {
        spdlog::debug("Adding {} testpoint to known model vars, type: {}", var.name, SUPPORTED_TYPES_NAMES[rts::DOUBLE]);
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                           std::string(var.name), ModelVarLocation( testpoint[var.index], rts::DOUBLE) ));
    }

    //Add IN1, EXC, IN2, OUT, OUTPUT, OUT16, SWSTAT, OFFSET, GAIN, LIMIT, TRAMP for each FM
    for (const filter_module_lookup_t & var : filter_modules)
    {
        spdlog::debug("Adding {}(IN1, EXC, IN2, OUT, OUTPUT, OUT16, SWSTAT, OFFSET, GAIN, LIMIT, TRAMP) to known filter testpoints.", var.filter_name);
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_IN1"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].filterInput, rts::DOUBLE) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_EXC"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].exciteInput, rts::DOUBLE) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_IN2"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].inputTestpoint, rts::DOUBLE) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_OUT"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].testpoint, rts::DOUBLE) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_OUTPUT"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].output, rts::DOUBLE) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_OUT16"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].output16Hz, rts::DOUBLE) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_SWSTAT"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].swStatus, rts::INT) )); // FIXME: actually UINT32
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_OFFSET"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->inputs[ var.module_index ].offset, rts::FLOAT) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_GAIN"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->inputs[ var.module_index ].outgain, rts::FLOAT) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_LIMIT"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->inputs[ var.module_index ].limiter, rts::FLOAT) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_TRAMP"), 
                                        ModelVarLocation( &dspPtr[ 0 ]->inputs[ var.module_index ].gain_ramp_time, rts::FLOAT) ));
        me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                        std::string(var.filter_name) + std::string("_SWS"),
                                        ModelVarLocation( &pDsp[ 0 ]->inputs[ var.module_index ].opSwitchE, rts::INT) ));
    }


    //Add filter modules to filter map
    for (const filter_module_lookup_t & var : filter_modules)
    {
        spdlog::debug("Adding {} to known filter modules.", var.filter_name);
        me_ptr->_filter_modules.insert( std::pair< std::string, int >(
                                        std::string(var.filter_name), var.module_index ) );
    }


    //Add default excitation points
    int exc_index = 0;
    for (const auto & var : excitation_points)
    {
        spdlog::debug("Adding {} to known excitation points", var.name);
        me_ptr->_excitation_points.insert( std::pair< std::string, excitation_record_t >(
                                           std::string(var.name), 
                                           {
                                               ModelVarLocation( &xExc[exc_index], rts::DOUBLE),
                                               std::make_shared< ConstantGenerator >(0)
                                           }
                                           ) );
        ++exc_index;
    }


    //Add excitations from filter modules
    for (const filter_module_lookup_t & var : filter_modules)
    {
        spdlog::debug("Adding {}_EXC to known filter excitation points.", var.filter_name);
        me_ptr->_excitation_points.insert( std::pair< std::string, excitation_record_t >(
                                           std::string(var.filter_name) + "_EXC", 
                                           {
                                               ModelVarLocation( &dspPtr[ 0 ]->data[ var.module_index ].exciteInput, rts::DOUBLE),
                                               std::make_shared< ConstantGenerator >(0)
                                           }
                                           ) );
    }

    //Add IPC senders to model vars, so we can trend them
    for (int i = 0; i < MODEL_NUM_IPCS_USED; ++i)
    {
        if( ipcInfo[i].mode == ISND)
        {
            spdlog::debug("Adding {} IPC sender to known model vars", ipcInfo[i].name);
            me_ptr->_model_vars.insert( std::pair< std::string, ModelVarLocation >(
                                           std::string(ipcInfo[i].name), ModelVarLocation( &ipcInfo[i].data, rts::DOUBLE) ));
        }
    }


    //Find receivers that don't have senders in the same model, IPCs of diffrent network types
    //have duplicate IPC numbs so we combine type and num for unique index
    std::map<int, int> ipcs_map; //<(ipcnetType << 16 ) & ipc num, index>
    std::map<int, int>::iterator it;
    for (int i = 0; i < MODEL_NUM_IPCS_USED; ++i)
    {
        int unique_id = (ipcInfo[i].netType << 16) | ipcInfo[i].ipcNum;
        it = ipcs_map.find( unique_id );
        if (it != ipcs_map.end())
        {
            spdlog::warn("Removing duplicate IPC - name {}, id {}, type {}", ipcInfo[i].name, unique_id, ipcInfo[i].netType);
            ipcs_map.erase(it); //We found it again so we have both sender and receiver, remove it
        }
        else
        {
            ipcs_map[ unique_id ] = i; //If not found, we add to map
        }
    }

    //After the above loop the ipcs_map only has IPCs that don't have a sender and receiver in this model
    int gen_index = 0;
    for (auto const& [unique_id, ipcIndex] : ipcs_map)
    {
        if(ipcInfo[ipcIndex].mode == IRCV)
        {
            //spdlog::info("Adding {}, {} to known simulated IPC receivers", ipcInfo[ipcIndex].name,  ipcIndex);
            me_ptr->_ipc_receivers.insert( std::pair< std::string, Model::Impl::ipc_data_gen_map_t >(
                                           std::string(ipcInfo[ipcIndex].name), {ipcIndex, gen_index } ) );
            ++gen_index;
        }
    }

    //Dolphin IPCs (PCIE, RFM) used buffers allocated by moduleLoad


    //Consant 0s input to all ADCs and excitation points
    me_ptr->set_default_inputs();

    return me_ptr;

}

Model::Impl::~Impl()
{
    globStateSpaceFree();
    detach_shared_memory();
}


int Model::Impl::get_model_rate_Hz()
{
    return MODEL_RATE_HZ; //define from Kernel build vars
}

std::string Model::Impl::get_model_name()
{
    return SYSTEM_NAME_STRING_LOWER; 
}

int Model::Impl::get_cycle_num()
{
    return _cycleNum;
}

uint64_t Model::Impl::get_gps_time()
{
    return _gpsTime;
}

void Model::Impl::set_gps_time(uint64_t gps)
{
    _gpsTime = gps;
}

void Model::Impl::set_log_level(int level)
{
    rtslog_set_log_level(level);
}

int Model::Impl::get_num_filter_modules()
{
    return MAX_MODULES; //Global from <modelName>.h
}

void Model::Impl::setValueFromLocation(ModelVarValue & set_me, ModelVarLocation & with_me)
{
     switch ( with_me.get_type() )
     {
        case INT:
            set_me.value.asInt = *( static_cast<int*>( with_me.get_ptr() ) );
            break;
        case DOUBLE:
            set_me.value.asDouble = *( static_cast<double*>( with_me.get_ptr() ) );
            break;
        case FLOAT:
            set_me.value.asFloat = *( static_cast<float*>( with_me.get_ptr() ) );
            break;
        case CHAR:
            set_me.value.asChar = *( static_cast<char*>( with_me.get_ptr() ) );
            break;
        default:
            break;
     }
}


template< class T>
bool Model::Impl::setVarTemplate(const std::string& name, T val)
{
    auto it = _model_vars.find(name);
    if( it == _model_vars.end()) 
    {
        spdlog::error("setVarTemplate() - The var with the name {} could not be found.\n", name);
        return false;
    }

    switch ( it->second.get_type() )
    {
        case INT:
            *( static_cast<int*>( it->second.get_ptr() ) ) = static_cast<int>(val);
            break;
        case DOUBLE:
            *( static_cast<double*>( it->second.get_ptr() ) ) = static_cast<double>(val);
            break;
        case FLOAT:
            *( static_cast<float*>( it->second.get_ptr() ) ) = static_cast<float>(val);
            break;
        case CHAR:
            *( static_cast<char*>( it->second.get_ptr() ) ) = static_cast<char>(val);
            break;
        default:
            spdlog::error("setVarTemplate() - Hitting the default case with var name {}.\n", name);
            return false;
    }
    return true;


}

bool Model::Impl::set_var(const std::string& name, double val)
{
    return setVarTemplate<double>(name, val);
}

bool Model::Impl::set_var(const std::string& name, int val)
{
    return setVarTemplate<int>(name, val);
}

bool Model::Impl::set_var_momentary(const std::string& name, double val)
{
    auto it = _model_vars.find(name);
    if( it == _model_vars.end()) 
    {
        spdlog::error("set_var_momentary() - The var with the name {} could not be found.", name);
        return false;
    }    
    ModelVarValue stored_val;
    stored_val.value.asDouble = val;
    _momentary_vars.insert( std::pair<std::string, ModelVarValue>(name, stored_val) );
    return true;
}


template< class T>
std::optional<T>  Model::Impl::get_var(const std::string& name) const
{
    auto it = _model_vars.find(name);
    if( it == _model_vars.end())
    {
        spdlog::error("get_var() - The var with the name {} could not be found.", name);
        return {};
    }

    return it->second.get_val<T>();
}

bool Model::Impl::record_model_var(const std::string & name, int record_rate_hz)
{
    auto it = _model_vars.find(name);
    if( it == _model_vars.end())
    {
        spdlog::error("record_model_var() - The var with the name {} could not be found.", name);
        return false;
    }

    if(record_rate_hz > get_model_rate_Hz() )
    {
        spdlog::warn("record_model_var() - Requested record rate for {} was {}, but model only runs at {}. Using model rate.", name, record_rate_hz, get_model_rate_Hz());
        record_rate_hz = get_model_rate_Hz();
    }

    int cycles_per_record =  std::ceil(((double)get_model_rate_Hz() / record_rate_hz));

    auto fit = _vars_to_record.find(name); 
    if(fit != _vars_to_record.end())
    {
         //We are already recording the requested var, update the rate
         fit->second.samples_per_record = cycles_per_record;
    }
    else
    {
        _vars_to_record.insert( std::pair< std::string, recored_var_info_t > (name, {cycles_per_record, it->second, {}}));
    }
    
    return true;
}

bool Model::Impl::stop_recording_model_var(const std::string & name)
{
    auto it = _model_vars.find(name);
    if( it == _model_vars.end())
    {
        spdlog::warn("stop_recording_model_var() - The var with the name {} could not be found.", name);
        return false;
    }

    //Check if we are recording a var with the name
    auto fit = _vars_to_record.find(name);
    if ( fit == _vars_to_record.end() )
    {
        spdlog::warn("stop_recording_model_var() - The var with the name {} could not be found in the recording list.", name);
        return false;
    }

    _vars_to_record.erase(fit);

    return true;

}


template< class T>
std::optional<std::vector<T>> Model::Impl::get_recorded_var(const std::string & name) const
{
    auto fit = _vars_to_record.find(name);
    if ( fit == _vars_to_record.end() )
    {
        spdlog::error("get_recorded_var() - The var with the name {} could not be found in the recording list.", name);
        return {};
    }

    std::vector<T> trend;
    trend.resize(fit->second.values.size());


    for( unsigned i=0; i<fit->second.values.size(); ++i )
    {
        trend[i] = fit->second.values[i].get_val<T>(fit->second.var_info.get_type());
    }

    return trend;
}

void Model::Impl::multicycle_record_control( bool enable )
{
    _multicycle_record = enable;
}

int Model::Impl::load_snap_file(const std::string & pathname)
{
    SnapFile snap;
    int num_chans_loaded = 0;
    int ret = snap.load(pathname);
    std::optional<double> val_hold;


    if( ret <= 0 )
        return ret;


    for( SnapFile::iterator it = snap.begin(); it != snap.end(); ++it)
    {

        val_hold = snap.getVal( it->first );
        if( val_hold ) {

            const auto mod_var_it = _model_vars.find( it->first );
            if( mod_var_it != _model_vars.end()) {
                set_var(it->first, val_hold.value());
                ++num_chans_loaded;
            }
            else {
                spdlog::warn("load_snap_file() - The channel: {} was found in the snap file, but no model variable was found.", it->first );
            }
        }
        else {
            spdlog::error("load_snap_file() - SnapFile::getVal() could not parse the value '{}' for chan '{}'", std::get<0>( it->second ), it->first);
        }

    }

    return num_chans_loaded;

}


std::optional<LIGO_FilterModule> Model::Impl::get_filter_module_by_name(std::string name)
{
    auto it = _filter_modules.find(name);
    if ( it == _filter_modules.end())
        return {};

    return LIGO_FilterModule(it->second);
}

std::optional<LIGO_FilterModule> Model::Impl::get_filter_module_by_id(int module_index)
{
    if(module_index < 0 || module_index >= get_num_filter_modules())
        return {};
    return LIGO_FilterModule(module_index);
}

bool Model::Impl::reset_filter_module(const std::string & name)
{
    auto it = _filter_modules.find(name);
    if ( it == _filter_modules.end())
    {
        spdlog::error("reset_filter_module() - Could not find a filter with the name {}", name);
        return false;
    }

    int module_index = it->second;

   //Set filter module action to reset history 
    pDsp [ 0 ]->inputs[module_index].rset = FM_OP_IN_RSET_HISTORY;

    checkFiltReset( module_index,
                    dspPtr[ 0 ],
                    pDsp [ 0 ],
                    &_dspCoeff[ 0 ],
                    get_num_filter_modules(),
                    pCoeff[ 0 ]);
    spdlog::info("reset_filter_module() - rset: {}", pDsp [ 0 ]->inputs[module_index].rset);

    return true;
}

std::vector<double> Model::Impl::read_biquad_coefficients(const std::string & filter_name, 
                                                          int stageIndexToRead)
{

    std::vector<double> coefficients;
    auto it = _filter_modules.find(filter_name);
    if ( it == _filter_modules.end())
    {
        spdlog::error("read_biquad_coefficients() - Could not find a filter with the name {}", filter_name);
        return {};
    }

    if(stageIndexToRead >= 0 && stageIndexToRead < FILTERS)
    {
        for(int cof_index = 0; cof_index < 4*_dspCoeff[ 0 ].coeffs[ it->second ].filtSections[stageIndexToRead] + 1; ++cof_index)
            coefficients.push_back(_dspCoeff[ 0 ].coeffs[ it->second ].filtCoeff[stageIndexToRead][cof_index]);
        return coefficients;
    }
    else
    {
        spdlog::warn("read_biquad_coefficients() - Sub filter index {} skipped because it is out of the acceptable range [0-{}]", 
                     stageIndexToRead,
                     FILTERS-1);
        return {};
    }
}

bool Model::Impl::load_biquad_coefficients(const std::string & filter_name, 
                                         const std::vector<int> & stageIndicesToLoad, 
                                         const std::vector<double> & coefficients)
{

    if( (coefficients.size()-1) % 4 != 0)
    {
        spdlog::error("load_biquad_coefficients() - Len of coefficients-1({}) is not a multiple of 4.", coefficients.size()-1);
        return false;
    }

    auto it = _filter_modules.find(filter_name);
    if ( it == _filter_modules.end())
    {
        spdlog::error("load_biquad_coefficients() - Could not find a filter with the name {}", filter_name);
        return false;
    }

    for(const auto& sfi : stageIndicesToLoad)
    {
        if(sfi >= 0 && sfi < FILTERS)
        {
            for(unsigned cof_index = 0; cof_index < coefficients.size(); ++cof_index)
                _dspCoeff[ 0 ].coeffs[ it->second ].filtCoeff[sfi][cof_index] = coefficients[cof_index];
            _dspCoeff[ 0 ].coeffs[ it->second ].filtSections[sfi] = (coefficients.size() - 1) / 4; //We don't count the gain value
            _dspCoeff[ 0 ].coeffs[ it->second ].biquad = 1; //Because we are setting BiquadCoefficients we set to biquad form
        }
        else
        {
            spdlog::warn("load_biquad_coefficients() - Sub filter index {} skipped because it is out of the acceptable range [0-{}]",\
                         sfi,
                         FILTERS-1);
        }
    }
    return true;
}

bool Model::Impl::load_scipy_sos_coef(const std::string & filter_name,
                                  const std::vector<int> & stageIndicesToLoad,
                                  const std::vector< std::vector<double> > & sections_and_coefficients)
{
    auto it = _filter_modules.find(filter_name);
    if ( it == _filter_modules.end())
    {
        spdlog::error("load_scipy_sos_coef() - Could not find a filter with the name {}", filter_name);
        return false;
    }

    std::vector<double> adv_sos_coeffs = scipy_sos_to_adv_sos(sections_and_coefficients);
    if(adv_sos_coeffs.size() == 0)
    {
        spdlog::error("load_scipy_sos_coef() - Could not convert the given sections and coefficients to advLIGO format.");
        return false;
    }

    //Check if the filter we are trying to load is biquad, and do conversion
    if(_dspCoeff[ 0 ].coeffs[it->second].biquad == 1)
    {
        const auto & biquad_sos_coef = adv_df_sos_to_adv_biquad(adv_sos_coeffs);
        return load_biquad_coefficients(filter_name, stageIndicesToLoad, biquad_sos_coef);
    }
    else
    {
        spdlog::error("load_scipy_sos_coef() - Error unsupported filter type.");
        return false;
    }


    return true;
}

std::vector<double> Model::Impl::read_fir_coefficients(const std::string & filter_name, int stageIndexToRead)
{
    std::vector<double> taps({});

#ifdef FIR_FILTERS
    if( stageIndexToRead < 0 || stageIndexToRead > FILTERS)
    {
        spdlog::error("read_fir_coefficients() - stageIndexToRead ({}) outside of acceptable range.", stageIndexToRead);
        return taps;
    }

    auto it = _filter_modules.find(filter_name);
    if ( it == _filter_modules.end())
    {
        spdlog::error("read_fir_coefficients() - Could not find a filter with the name {}", filter_name);
        return taps;
    }

    if ( _dspCoeff[ 0 ].coeffs[ it->second ].filtSections[ stageIndexToRead ] == 0 )
    {
        spdlog::error("read_fir_coefficients() - Called on stage ({}) with no sections, have you loaded the filter?", stageIndexToRead);
        return taps;
    }


    if ( _dspCoeff[ 0 ].coeffs[ it->second ].filterType[ stageIndexToRead ] == 0 )
    {
        spdlog::error("read_fir_coefficients() - Called on stage ({}) that is not configured as a FIR filter.", stageIndexToRead);
        return taps;
    }


    int firFilterIndex = _dspCoeff[ 0 ].coeffs[ it->second ].filterType[stageIndexToRead] - 1;

    taps.resize( _dspCoeff[ 0 ].coeffs[ it->second ].filtSections[stageIndexToRead] * 4);
    //First element in firFiltCoeff is scale factor,  so we skip
    for(size_t i=1; i < taps.size(); ++i)
    {
        taps[i-1] = _dspCoeff[ 0 ].firFiltCoeff[ firFilterIndex ][ stageIndexToRead ][i]; 
    }
#else
    spdlog::error("read_fir_coefficients() - Called but there are no FIR filters present in the model");
#endif

    return taps;



}


bool Model::Impl::load_from_filter_file(const std::string & filterFilePathName, const std::string & firFilterFilePathName)
{

    fmSubSysMap fmmap0 [MAX_MODULES];

    for (auto const & pair: _filter_modules) { //first=name, second=index
        strncpy(fmmap0[ pair.second ].name, pair.first.c_str(), COUNT_OF(fmmap0[ pair.second ].name));
        fmmap0[ pair.second ].fmModNum = pair.second;
        fmmap0[ pair.second ].biquad = 1; //Looks like fmseq sets all these to 1, even for FIR, maybe not used in that case?
    }


    fmReadCoeff fmc = { "tst", "x1", "x1firtst", pCoeff[ 0 ], {{"", "", MAX_MODULES, fmmap0, 0}},
                      };

    fmReadOptions_t fileReadOps = {false, "", "", "", ""}; //No archive
    lstrlcpy(fileReadOps.filterFile, filterFilePathName.c_str(), COUNT_OF(fileReadOps.filterFile));
    lstrlcpy(fileReadOps.firFilterFile, firFilterFilePathName.c_str(), COUNT_OF(fileReadOps.firFilterFile));

    int ret = fmReadCoeffFile( &fmc, 
                               0, 
                               1337, //We don't archive the file, we read, so dummy gps time
                               &fileReadOps);
    if (ret != 0 ) //TODO 3rd arg needs to be gps time, if 0 it gets read from /proc/gps
    {
        spdlog::error("load_from_filter_file() - There was an error loading the filter file(s), erro num = {}, msg = {}", ret, fmReadErrMsg());
        return false;
    }

    //Filter info loaded into pCoeff[ 0 ], but not _dspCoeff[ 0 ]

    //Load coeffs from temp storage into model memory
    for (auto const & pair: _filter_modules) { //first=name, second=index
        pDsp [ 0 ]->inputs[ pair.second ].rset = FM_OP_IN_NEW_COEF_RDY;
        pDsp [ 0 ]->coef_load_error = 0;
        pDsp [ 0 ]->fm_pending_action_index = pair.second; //Mark the filter for load
    
        int num_to_load = 0; 
        do {
            ret =  doFilterLoadWork( dspPtr[ 0 ], pDsp[ 0 ], &_dspCoeff[ 0 ], pCoeff[ 0 ] );
            ++num_to_load;
        }
        while( ret );

        if ( pDsp [ 0 ]->coef_load_error == -1 ) {
            spdlog::error("load_from_filter_file() - The model did not accept the configured coeffs, FM: {}", pair.first);
            return false;
        }

        if ( pDsp [ 0 ]->fm_pending_action_index != -1 ) {
            spdlog::error("load_from_filter_file() - The model timed out loading the new coeffs, FM: {}", pair.first);
            return false;
        }

        spdlog::debug("It took {} calls to load {}", num_to_load, pair.first);

    }


    return true;

}

std::vector<double> Model::Impl::scipy_sos_to_adv_sos(const std::vector< std::vector<double> > & scipy_sections_and_coeffs)
{
    /* The scipy format is documented here:
     * https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.sosfilt.html
     *
     * It is in the format of a list of lists with dimensions [N][6]
     * Where N is the number of second order sections (SOSs)
     * And the 6 elements are a SOS with the first three elements as the
     * numerator coefficients and the last three the denominator coefficients
     *
     * sp = scipy_coeffs[0]
     * H(z) = (sp[0] + sp[1] + sp[2]) / (sp[3] + sp[4] +sp[5])
     *
     * The advligo format is [G, a_11, a_21, b_11, b_21, a_12, a_22, b_12, b_22, ... ]
     *
     */

    std::vector<double> flattened_adv_sos_format;
    flattened_adv_sos_format.resize(1);
    flattened_adv_sos_format[0] = 1.0; //Start gain factor at 1


    for(const auto& sos : scipy_sections_and_coeffs)
    {
        if (sos.size() != 6)
        {
            spdlog::error("scipy_sos_to_adv_sos() - coefficients length was not 6 as expected");
            return {}; 
        }
        flattened_adv_sos_format[0] *= sos[0]/sos[3];

        std::vector<double> temp{sos[4]/sos[3], sos[5]/sos[3], sos[1]/sos[0], sos[2]/sos[0]};

        flattened_adv_sos_format.insert(flattened_adv_sos_format.end(),
                                        temp.begin(),
                                        temp.end());
    }
    return flattened_adv_sos_format;
}

std::vector<double> Model::Impl::adv_df_sos_to_adv_biquad(const std::vector< double > coeffs)
{
    /*Returns advligorts SOS coefficients in direct form converted to biquad format.

    The biquad representation reduces quantization noise by avoiding
    large internal values. The relationship of its coefficients to those
    of the direct form II filter is given in:
    https://dcc.ligo.org/LIGO-G0900928
    */
    // reference implementation:
    // https://git.ligo.org/cds/advligorts/-/blob/master/src/drv/fmReadCoeff.c

    std::vector< double > result(coeffs.size());
    result[0] = coeffs[0]; 
    for (unsigned n=1; n < result.size(); n+=4)
    {
        /*
        a1, a2, b1, b2 = coef[n], coef[n+1], coef[n+2], coef[n+3]
        out[n] = -a1 - 1
        out[n+1] = -a2 - a1 - 1
        out[n+2] = b1 - a1
        out[n+3] = b2 - a2 + b1 - a1*/
        result[n] = -coeffs[n] - 1.0;
        result[n+1] = -coeffs[n+1] - coeffs[n] - 1.0;
        result[n+2] = coeffs[n+2] - coeffs[n];
        result[n+3] = coeffs[n+3] - coeffs[n+1] + coeffs[n+2] - coeffs[n];
    }
    return result;

}

int Model::Impl::get_filter_switch_type(const std::string & filter_name,
                                        int section_index)
{

    auto it = _filter_modules.find(filter_name);
    if ( it == _filter_modules.end())
    {
        spdlog::error("get_filter_switch_type() - Could not find a filter with the name {}", filter_name);
        return {};
    }

    if(section_index >= 0 && section_index < FILTERS)
    {
        return _dspCoeff[ 0 ].coeffs[ it->second ].sType[section_index];
    }
    else
    {
        spdlog::warn("get_filter_switch_type() - Sub filter index {} skipped because it is out of the acceptable range [0-{}]", \
                     section_index,
                     FILTERS-1);
        return -1;
    }
}

bool Model::Impl::set_filter_switch_type(const std::string & filter_name,
                                         int section_index,
                                         int switch_type)
{

    auto it = _filter_modules.find(filter_name);
    if ( it == _filter_modules.end())
    {
        spdlog::error("set_filter_switch_type() - Could not find a filter with the name {}", filter_name);
        return false;
    }

    if(section_index >= 0 && section_index < FILTERS)
    {
        _dspCoeff[ 0 ].coeffs[ it->second ].sType[section_index] = switch_type;
        return true;
    }
    else
    {
        spdlog::warn("load_biquad_coefficients() - Sub filter index {} skipped because it is out of the acceptable range [0-{}]", \
                     section_index,
                     FILTERS-1);
        return false;
    }
}

//
// Statespace
//
int Model::Impl::get_num_statespace_parts()
{
    return STATESPACE_NUM_PARTS;
}

std::vector< std::string > Model::Impl::get_all_statespace_names()
{
    char * part_name_ptr = NULL;
    std::vector< std::string > names( get_num_statespace_parts() );

    for(int i=0; i < get_num_statespace_parts(); ++i) {
        getPartNamePtrStateSpace(i, &part_name_ptr);
        names[i] = std::string(part_name_ptr);
    }
    return names;
}

bool Model::Impl::load_statespace( const std::string & part_name, 
                                  const std::vector< double >  & initial_state_vec,
                                  const std::vector< double >  & input_matrix,
                                  const std::vector< double >  & output_matrix,
                                  const std::vector< double >  & state_matrix,
                                  const std::vector< double >  & feedthrough_matrix )
{

    auto it = _statespace_name_to_index_map.find(part_name);
    if ( it == _statespace_name_to_index_map.end())
    {
        spdlog::error("load_statespace() - Could not find a statespace part with the name {}", part_name);
        return false;
    }

    struct stateSpace_part_config_t statespace_part_config;
    getConfigOfPartStateSpace(it->second, &statespace_part_config);

    //
    // The below checks verify the new configuration against the part as built
    // ie. inputs and outputs are fixed by the model, so we can't change them

    //Input matrix should be new_state_vec_len * const input_vec_len
    if( input_matrix.size() != initial_state_vec.size()*statespace_part_config.input_vec_len ) {
        spdlog::error("load_statespace() - The new input matrix size was {}, but we expected {}", 
                      input_matrix.size(), initial_state_vec.size()*statespace_part_config.input_vec_len);
        return false;
    }

    //Output matrix should be new_state_vec_len * const input_vec_len
    if( output_matrix.size() != initial_state_vec.size()*statespace_part_config.output_vec_len ) {
        spdlog::error("load_statespace() - The new output matrix size was {}, but we expected {}", 
                      output_matrix.size(), initial_state_vec.size()*statespace_part_config.output_vec_len);
        return false;
    }


    if ( feedthrough_matrix.size() != 0 &&
         feedthrough_matrix.size() != (statespace_part_config.input_vec_len*statespace_part_config.output_vec_len)) {

        spdlog::error("load_statespace() - The new feedthrough matrix size was {}, but we expected 0 or {}", 
                     feedthrough_matrix.size(), 
                     statespace_part_config.input_vec_len*statespace_part_config.output_vec_len);
        return false;
    }



    if (matrixConfigureStateSpace(
            it->second, //part_index
            statespace_part_config.input_vec_len, 
            statespace_part_config.output_vec_len, 
            initial_state_vec.size(),
            &initial_state_vec[0], 
            &state_matrix[0], 
            &input_matrix[0], 
            &output_matrix[0], 
            &feedthrough_matrix[0]) != 0)
        {
            spdlog::error("load_statespace_from_file() : Failed to init the requested part\n");
        }

        //We force load the new config because librts controls when the model will run
        loadNewMatrixNowStateSpace( it->second );



    return true;
}

int Model::Impl::load_statespace_from_file( const std::string & statespaceFilePathName )
{
    char error_msg [STATE_SPACE_ERROR_MSG_LEN] = {'\0'};

    //Open and read file
    std::ifstream t(statespaceFilePathName);
    std::stringstream file_buffer;
    file_buffer << t.rdbuf();

    rapidjson::Document document;
    if (document.Parse<rapidjson::kParseCommentsFlag>(file_buffer.str().c_str()).HasParseError()) {
        spdlog::error("load_statespace_from_file() - Failed to parse the input file {} as valid JSON.\n",
                statespaceFilePathName);
        return -1;
    }

    //Check required top level keys
    if ( !document.HasMember("state_space_configs") ) {
        spdlog::error("load_statespace_from_file() - Missing required key state_space_configs\n");
        return -1;
    }

    if( !document["state_space_configs"].IsArray() ) {
        spdlog::error("load_statespace_from_file() - Key: state_space_configs, \
                       does not have array value. Malformed config file.\n");
        return -1;
    }

    //Util function
    auto getArrayAsDoubleVec = []( const rapidjson::Value& array)
    {
        std::vector<double> res(array.Size());
        for (rapidjson::SizeType i = 0; i < array.Size(); i++) {
            res[i] = array[i].GetDouble();
        }
        return res;
    };

    const rapidjson::Value& configs = document["state_space_configs"];
    int num_inited = 0;

    //First check all the configs are valid
    for (rapidjson::SizeType i = 0; i < configs.Size(); i++) 
    {

        if ( verify_square_config_lengths(
            configs[i]["state_vec_len"].GetInt(),
            configs[i]["state_vector"].Size(),
            configs[i]["state_matrix"].Size(),
            configs[i]["input_vec_len"].GetInt(),
            configs[i]["input_matrix"].Size(),
            configs[i]["output_vec_len"].GetInt(),
            configs[i]["output_matrix"].Size(),
            configs[i]["feedthrough_matrix"].Size(),
            error_msg) 
        != 1) {
            spdlog::error("load_statespace_from_file() - verify_square_config_lengths() "
                            "failed, error_msg: {} \n", error_msg);
            return -1;
        }

    }

    for (rapidjson::SizeType i = 0; i < configs.Size(); i++) 
    {
        //unsigned input_vec_len = configs[i]["input_vec_len"].GetInt();
        //unsigned output_vec_len = configs[i]["output_vec_len"].GetInt();
        //unsigned state_vec_len = configs[i]["state_vec_len"].GetInt();

        std::string part_name = configs[i]["part_name"].GetString();
        std::vector<double> initial_state_vec = getArrayAsDoubleVec( configs[i]["state_vector"] );
        std::vector<double> state_matrix = getArrayAsDoubleVec( configs[i]["state_matrix"] );
        std::vector<double> input_matrix = getArrayAsDoubleVec( configs[i]["input_matrix"] );
        std::vector<double> output_matrix = getArrayAsDoubleVec( configs[i]["output_matrix"] );
        std::vector<double> feedthrough_matrix = getArrayAsDoubleVec( configs[i]["feedthrough_matrix"] );

        if ( load_statespace(part_name,
             initial_state_vec,
             input_matrix,
             output_matrix,
             state_matrix,
             feedthrough_matrix) != true) {
            
            spdlog::error("load_statespace_from_file() : Failed to init the requested part, named: {}\n", part_name);
        }
        else {
            ++num_inited;
        }
    }

    return num_inited;
}


std::vector<std::string> Model::Impl::get_all_filter_names()
{
    std::vector<std::string> all_filter_names;
    all_filter_names.reserve(_filter_modules.size());
    for (auto const & pair: _filter_modules) {
        all_filter_names.push_back(pair.first);
    }
    return all_filter_names;
}

std::vector<std::string> Model::Impl::get_all_adc_names()
{
    std::vector<std::string> all_adc_chans;
    for (int cur_card =0; cur_card < cdsPciModules.adcCount; ++cur_card)
    {
        for(int cur_chan=0; cur_chan < MAX_ADC_CHN_PER_MOD; ++cur_chan)
        {
            if(dWordUsed[cur_card][cur_chan] )
            {
               all_adc_chans.push_back("adc_" + std::to_string(cur_card) + "_" + std::to_string(cur_chan));
            }
        }
    }
    return all_adc_chans;
}

std::vector<std::string> Model::Impl::get_all_dac_names()
{
    std::vector<std::string> all_dac_chans;
    for (int cur_card =0; cur_card < cdsPciModules.dacCount; ++cur_card)
    {
        for(int cur_chan=0; cur_chan < MAX_DAC_CHN_PER_MOD; ++cur_chan)
        {
            if(dWordUsed[cur_card][cur_chan] )
            {
               all_dac_chans.push_back("dac_" + std::to_string(cur_card) + "_" + std::to_string(cur_chan));
            }
        }
    }
    return all_dac_chans;
}


std::vector<std::string> Model::Impl::get_all_input_names()
{
    std::vector<std::string> inputNames = get_all_adc_names();
    std::vector<std::string> dac = get_all_dac_names();

      inputNames.insert(
      inputNames.end(),
      std::make_move_iterator(dac.begin()),
      std::make_move_iterator(dac.end())
    );

    return inputNames;
}

std::vector<std::string> Model::Impl::get_all_model_var_names()
{
    std::vector<std::string> names;
    for (auto const& [key, val] : _model_vars)
    {
        names.push_back(key);
    } 
    return names;
}

void Model::Impl::set_default_inputs()
{
    //Set default ADC inputs
    _adcs.resize(cdsPciModules.adcCount);
    for (int cur_card =0; cur_card < cdsPciModules.adcCount; ++cur_card)
    {
        for(int cur_chan=0; cur_chan < MAX_ADC_CHN_PER_MOD; ++cur_chan)
        {
           _adcs[cur_card].push_back( std::make_shared< ConstantGenerator >(0) );
        }
    }

    
    //Set default ipc receiver inputs
    _ipc_generators.resize( _ipc_receivers.size() );
    for( unsigned i = 0; i < _ipc_receivers.size(); ++i )
    {
        _ipc_generators[i] = std::make_shared< ConstantGenerator >(0);
    }

}

void Model::Impl::set_adc_channel_generator(int adcNum, int chanNum, std::shared_ptr<Generator> gen)
{
    _adcs.at(adcNum).at(chanNum) = gen;
}

bool Model::Impl::set_excitation_point_generator(const std::string & name, std::shared_ptr<Generator> gen)
{
    auto it = _excitation_points.find(name);
    if(it == _excitation_points.end())
    {
        spdlog::error("set_excitation_point_generator() - Could not find an excitation point with the name {}", name);
        return false;
    }
    it->second.exc_generator = gen;
    return true;
}


int Model::Impl::get_awg_slot(const std::string & name)
{
    auto it = _excitation_points.find(name);

    if(it == _excitation_points.end())
        throw std::invalid_argument("Excitation channel not found: " + name);

    int excnum = std::distance(_excitation_points.begin(), it) + 1; //Offset so excnum is >= 1

    int slot = getIndexAWG(awgLSCtp, excnum, 0, 0);
    if (slot < 0)
        throw std::runtime_error("getIndexAWG error, ret: " + std::to_string(slot));

    it->second.exc_generator = std::make_unique<rts::AwgGenerator>(slot, _gpsTime, _cycleNum, MODEL_RATE_HZ);

    _awg_slot_to_excitation.insert( std::pair(slot, name) );

    return slot;

}


int Model::Impl::free_awg_slot(int slot)
{

    auto slot_lookup_it = _awg_slot_to_excitation.find(slot);
    if(slot_lookup_it == _awg_slot_to_excitation.end())
        throw std::invalid_argument("The slot number passed has not been allocated, and so cannot be freed: " + std::to_string(slot));

    auto it = _excitation_points.find( slot_lookup_it->second );

    it->second.exc_generator = std::make_unique< rts::ConstantGenerator > (0);
    _awg_slot_to_excitation.erase(slot_lookup_it); //Remove from allocated hold
    int ret = releaseIndexAWG(slot);
    if (ret != 0 )
        throw std::runtime_error("releaseIndexAWG error: " + std::to_string(ret));

    return ret;
}


bool Model::Impl::set_ipc_receiver_generator(const std::string & name, std::shared_ptr<Generator> gen)
{
    auto it = _ipc_receivers.find(name);
    if(it == _ipc_receivers.end())
    {
        spdlog::error("set_ipc_point_generator() - Could not find an ipc receiver with the name {}", name);
        return false;
    }
    _ipc_generators[it->second.generator_index] = gen;
    return true;
}

void Model::Impl::record_dac_output(bool shouldRec)
{
    _recordDacOutput = shouldRec;
}

const std::vector< double > & Model::Impl::get_dac_output_by_id(int dacIndex, int chanIndex)
{
    return _dacOutputs.at(dacIndex).at(chanIndex);
}

int  Model::Impl::run_model(unsigned num_cycles)
{

    double cycle_adc_input[MAX_ADC_MODULES][MAX_ADC_CHN_PER_MOD];
    double cycle_dac_output[MAX_DAC_MODULES][MAX_DAC_CHN_PER_MOD];

    for (auto const& [name, var] : _momentary_vars )
    {
        //Save old value, set momentary, save old value
        ModelVarValue hold;
        hold.value.asDouble = get_var<double>( name ).value();
        set_var(name, var.get_val<double>( rts::SUPPORTED_TYPES::DOUBLE ));
        _momentary_vars[name] = hold;
    }


    if( _recordDacOutput )
    {
        //Allocate space for DAC outputs is we are collecting them
        _dacOutputs.resize(cdsPciModules.dacCount);
        for (int cur_card =0; cur_card < cdsPciModules.dacCount; ++cur_card)
        {
            _dacOutputs[cur_card].resize(MAX_DAC_CHN_PER_MOD);
            for(int cur_chan=0; cur_chan < MAX_DAC_CHN_PER_MOD; ++cur_chan)
                _dacOutputs[cur_card][cur_chan].resize(num_cycles);
        }
    }
    else
    {
        //We don't want to have old samples returned when _recordDacOutput == false
        for (unsigned cur_card =0; cur_card < _dacOutputs.size(); ++cur_card)
        {
            for(int cur_chan=0; cur_chan < MAX_DAC_CHN_PER_MOD; ++cur_chan)
                _dacOutputs[cur_card][cur_chan].clear();
        }
    }

    // Set up space for any model vars we are recording
    for (auto & [key, val] : _vars_to_record)
    {
        //Clear data from last run, unless multicycle_record is set
        if ( ! _multicycle_record )
            val.values.clear();

        val.values.reserve(num_cycles);
    }


    //Run model for the requested number of cycles
    for(unsigned i=0; i<num_cycles; ++i)
    {

        //Generate samples from ADCs
        for (int cur_card =0; cur_card < cdsPciModules.adcCount; ++cur_card)
        {
            for(int cur_chan=0; cur_chan < MAX_ADC_CHN_PER_MOD; ++cur_chan)
            {
               cycle_adc_input[cur_card][cur_chan] = _adcs[cur_card][cur_chan]->get_next_sample();
            }
        }

        //Generate samples from excitations
        for (auto & [key, val] : _excitation_points)
        {
            val.target_var.set_val( val.exc_generator->get_next_sample() );
        }

        
        //Generate samples from ipc receivers with no senders in this model
        int rcvBlock = ( ( _cycleNum ) * ( IPC_MAX_RATE / IPC_RATE ) ) % IPC_BLOCKS;
        timeSec = 0;
        int cycle65k = 0;
        int ipc_num;
        for (auto const& x : _ipc_receivers)
        {
            cycle65k = ( ( _cycleNum * ipcInfo[ x.second.ipc_index ].sendCycle ) );
            ipc_num = ipcInfo[ x.second.ipc_index  ].ipcNum;

            //Some IPCs PCIE & RMFX, both dolphin IPCs aren't supported yet
            //and their pointers will be null, so we skip them
            if(ipcInfo[ x.second.ipc_index ].pIpcDataRead != NULL)
            {
                ipcInfo[ x.second.ipc_index ].pIpcDataRead
                                    ->dBlock[ rcvBlock ][ ipc_num ]
                                    .timestamp = cycle65k;

                ipcInfo[ x.second.ipc_index ].pIpcDataRead
                                ->dBlock[ rcvBlock ][ ipc_num ]
                                .data = _ipc_generators[x.second.generator_index].get()->get_next_sample();
            }
            else
            {
                spdlog::error("Found null ipc");
            }
            
        }

        //Run awg task once per epoch
        if (_cycleNum % CYCLES_PER_EPOCH == 0)
        {
            setTAInow(_gpsTime*_ONESEC + (_cycleNum/CYCLES_PER_EPOCH)*_EPOCH);
            tAWG();
        }

        feCode( _cycleNum,
                cycle_adc_input, //double[ADC NUM][Chan Num]
                cycle_dac_output, //double[DAC Num][Chan Num]
                dspPtr[ 0 ], //Global from controller.h, ptr to EPICS shmem dspSpace
                &_dspCoeff[ 0 ], //Global in rtcds system, but we make it local
                (struct CDS_EPICS*)pLocalEpics,
                0 );

        if( _recordDacOutput )
        {
            for (int cur_card =0; cur_card < cdsPciModules.dacCount; ++cur_card) {
                for(int cur_chan=0; cur_chan < MAX_DAC_CHN_PER_MOD; ++cur_chan) {
                _dacOutputs[cur_card][cur_chan][i] = cycle_dac_output[cur_card][cur_chan];
                }
            }
        }

        for (auto & [key, val] : _vars_to_record)
        {
            //Only record data at the configured rate, passed into record_model_var()
            if( _cycleNum % val.samples_per_record  == 0)
            {
                val.values.push_back( {} );
                setValueFromLocation( val.values.back(), val.var_info );
            }
        }

        //Clear any momentary vars we just set, 
        //we do this after dac and variable recording for the cycle
        for (const auto& [name, val]: _momentary_vars)
        {
            set_var(name, val.get_val<double>(rts::SUPPORTED_TYPES::DOUBLE)); //Reset the old value
        }
        _momentary_vars.clear();

        ++_cycleNum;
        _cycleNum %= MODEL_RATE_HZ;
        if (_cycleNum == 0)
        {
            _gpsTime++;
        }
    }





    //TODO: if we ever have alarms that stop model 
    //      simulation this could be less than requested
    return num_cycles;
}


//
// Start Private Impl Member Functions
//

void Model::Impl::clear_dac_outputs()
{
    for ( int ii = 0; ii < MAX_DAC_MODULES; ii++ )
    {
        for ( int jj = 0; jj < MAX_DAC_CHN_PER_MOD; jj++ )
        {
            dacOut[ ii ][ jj ] = 0.0;
            dacOutUsed[ ii ][ jj ] = 0;
        }
    }
}

bool Model::Impl::init_filter_modules( )
{
    int system, ii, jj, kk;

    /// \> Init IIR filter banks
    //   Initialize filter banks  *********************************************
    //We have all these loops over NUM_SYSTEMS but that might always be 1?
    for ( system = 0; system < NUM_SYSTEMS; system++ )
    {
        for ( ii = 0; ii < MAX_MODULES; ii++ )
        {
            for ( jj = 0; jj < FILTERS; jj++ )
            {
                for ( kk = 0; kk < MAX_COEFFS; kk++ )
                    _dspCoeff[ system ].coeffs[ ii ].filtCoeff[ jj ][ kk ] = 0.0;
                for( kk = 0; kk < MAX_HISTRY; ++kk)
                    _dspCoeff[ system ].coeffs[ ii ].filtHist[ jj ][ kk ] = 0.0;

                _dspCoeff[ system ].coeffs[ ii ].filtSections[ jj ] = 0;
                _dspCoeff[ system ].coeffs[ ii ].filterType[ jj ] = 0;
            }
        }

        #ifdef FIR_FILTERS
        //Zero out history
        std::memset( _dspCoeff[ system ].firHistory, 
                     0, 
                     sizeof(_dspCoeff[ system ].firHistory[0][0][0][0]) 
                     * MAX_FIR_MODULES * FILTERS * FIR_POLYPHASE_SIZE * FIR_TAPS);
        #endif


    }


    // Initialize all filter module excitation signals to zero
    // and gains to 0, this means everything ramps from 0 if you have
    // a non-zero ramp time on your module
    for ( ii = 0; ii < MAX_MODULES; ii++ )
    {
        std::memset(&pDsp[ 0 ]->data[ ii ], 0, sizeof(pDsp[ 0 ]->data[ ii ]));
        pDsp[ 0 ]->inputs[ ii ].offset = 0;
        pDsp[ 0 ]->inputs[ ii ].outgain = 0;
        pDsp[ 0 ]->inputs[ ii ].gain_ramp_time = 0;
        pDsp[ 0 ]->inputs[ ii ].limiter = 0;
        for ( jj = 0; jj < FILTERS; jj++ )
        {
            pDsp[ 0 ]->inputs[ ii ].rmpcmp[jj] = 0;
            pDsp[ 0 ]->inputs[ ii ].cnt[jj] = 0;
            pDsp[ 0 ]->inputs[ ii ].timeout[jj] = 0;
        }
    }

    /// \> Initialize IIR filter bank values
    if ( initVars( pDsp[ 0 ], pDsp[ 0 ], _dspCoeff, MAX_MODULES, pCoeff[ 0 ] ) )
    {
        spdlog::critical( "init_filter_modules() - Filter module init failed, exiting\n" );
        return false;
    }

    //Set up filters for output
    for(ii=0; ii<MAX_MODULES; ++ii)
    {
        _dspCoeff[ 0 ].coeffs[ii].biquad = 1;
        for ( jj = 0; jj < FILTERS; jj++ )
            _dspCoeff[ 0 ].coeffs[ii].sType[jj] = 21;
    }
   
   /* 
    for(ii=0; ii<MAX_MODULES; ++ii)
    {
        pDsp[ 0 ]->inputs[ ii ].outgain = 1;
        pDsp[ 0 ]->inputs[ ii ].gain_ramp_time = 1.0; //1s
        pDsp[ 0 ]->inputs[ ii ].limiter = 0;
        pDsp[ 0 ]->inputs[ ii ].opSwitchE = OPSWITCH_INPUT_ENABLE | OPSWITCH_OUTPUT_ENABLE;
        pDsp[ 0 ]->inputs[ ii ].rset = FM_OP_IN_NO_ACTION;
        pDsp[ 0 ]->inputs[ ii ].mask = 0;
        pDsp[ 0 ]->inputs[ ii ].control = 0;
    }
    */

    return true;
}

bool Model::Impl::initialize_model_globals()
{
    int status = 0;
    int dcuId;

    //Initialize awgstandalone
    awgstandalone_set_model_rate_Hz(MODEL_RATE_HZ);
    setTAInow(_gpsTime*_ONESEC);
    initAWG();

#ifdef LIBRTS_NO_SHMEM

    _dolphin_buffers_ptr = DolphinBuffers::create_instance(std::string(SYSTEM_NAME_STRING_LOWER), IPC_TOTAL_ALLOC_SIZE, IPC_TOTAL_ALLOC_SIZE);
    if ( _dolphin_buffers_ptr == nullptr)
    {
        spdlog::error("DolphinBuffers::createInstance() failed.");
        return false;
    }

    _epics_buffer_ptr = std::make_unique< char[] >(SHMEM_EPICS_SIZE);
    _epics_shm = (char *) _epics_buffer_ptr.get();
    _testpoint_buffer_ptr = std::make_unique< char[] >(SHMEM_TESTPOINT_SIZE);
    _tp_shm = (TESTPOINT_CFG *) _testpoint_buffer_ptr.get();
    _awg_buffer_ptr = std::make_unique< char[] >(SHMEM_AWG_SIZE);
    _awg_shm = (AWG_DATA *) _awg_buffer_ptr.get();
    _ipc_buffer_ptr = std::make_unique< char[] >(SHMEM_IOMEM_SIZE);
    _ipc_shm = (char *) _ipc_buffer_ptr.get();
    ioMemData = (IO_MEM_DATA*)( _ipc_shm + 0x4000 );
    _shmem_ipc_buffer_ptr =  std::make_unique< char[] >(SHMEM_IPCCOMMS_SIZE);
    _shmipc_shm = (char *) _shmem_ipc_buffer_ptr.get();
    _daq_buffer_ptr = std::make_unique< char[] >(SHMEM_MODEL_DAQ_COMMS_SIZE);
    _daq_shm = (char *) _daq_buffer_ptr.get();
    _io_mem_data_buffer_ptr = std::make_unique< char[] >(SHMEM_IOMEM_SIZE);
    if (!(_epics_shm && _tp_shm && _awg_shm && _ipc_shm && _shmipc_shm && _daq_shm && ioMemData))
    {
        spdlog::error("initialize_model_globals() - Failed to allocate memory.");
        return false;
    }
#else

    _dolphin_buffers_ptr = DolphinBuffers::create_instance_shmem(std::string(SYSTEM_NAME_STRING_LOWER), IPC_TOTAL_ALLOC_SIZE, IPC_TOTAL_ALLOC_SIZE);
    if ( _dolphin_buffers_ptr == nullptr)
    {
        spdlog::error("DolphinBuffers::createInstance_shmem() failed.");
        return false;
    }

    // Connect and allocate mbuf memory spaces
    if ( attach_shared_memory( SYSTEM_NAME_STRING_LOWER ) == -1 )
    {
        spdlog::error("initialize_model_globals() - Failed to attach to the shared memory.");
        return false;
    }
#endif


    memset((void*)_epics_shm, 0, SHMEM_EPICS_SIZE);
    memset((void*)_tp_shm, 0, SHMEM_TESTPOINT_SIZE);
    memset((void*)_awg_shm, 0, SHMEM_AWG_SIZE);
    memset((void*)_ipc_shm, 0, SHMEM_IOMEM_SIZE);
    memset((void*)_shmipc_shm, 0, SHMEM_IPCCOMMS_SIZE);
    memset((void*)_daq_shm, 0, SHMEM_MODEL_DAQ_COMMS_SIZE);

    // Set global pointer to EPICS shared memory, _epics_shm set by attach_shared_memory
    pLocalEpics = (CDS_EPICS*)&( (RFM_FE_COMMS*)_epics_shm )->epicsSpace;

    // Find and initialize all PCI I/O modules
    // ******************************************************* Following I/O
    // card info is from feCode
    _numPCIeCards = COUNT_OF( cards_used ); //This looks like we are hardcoding somthing
    spdlog::info( "initialize_model_globals () - configured to use {} cards", _numPCIeCards );
    cdsPciModules.cards = _numPCIeCards;
    cdsPciModules.cards_used = cards_used;
    spdlog::info( "initialize_model_globals() - Initializing PCI Modules..." );
    cdsPciModules.adcCount = 0;
    cdsPciModules.dacCount = 0;
    cdsPciModules.dioCount = 0;
    cdsPciModules.doCount = 0;
    cdsPciModules.rfmCount = 0;
    cdsPciModules.dolphinCount = 0;

    // If running as a control process, I/O card information is via ipc shared
    // memory
    spdlog::info( "initialize_model_globals() - {} PCI cards found", ioMemData->totalCards ); //TODO: Looks like the IOP is setting this

    // Because this might not be an IOP model (who sets this normally) we force set it
    ioMemData->struct_version = IO_MEM_DATA_VERSION;

    initmap( &cdsPciModules );
    /// Call PCI initialization routine in map.c file.
    status = mapPciModules( &cdsPciModules );

    // If no ADC cards were found, then control cannot run
    if ( !cdsPciModules.adcCount )
    {
        spdlog::error( "initialize_model_globals() - No ADC cards found - exiting\n" );
        return false;
    }
    spdlog::info( "{} PCI cards found ", status );
    if ( status < _numPCIeCards )
    {
        spdlog::error( "initialize_model_globals() - ERROR **** Did not find correct number of cards! Expected {} "
                "and Found {}",
                _numPCIeCards,
                status );
        cardCountErr = 1;
        //return false;
    }

    // Control app gets RFM module count from MASTER.
    cdsPciModules.rfmCount = ioMemData->rfmCount;
    cdsPciModules.dolphinCount = ioMemData->dolphinCount;
    cdsPciModules.dolphinPcieReadPtr = static_cast<volatile long unsigned int*>(_dolphin_buffers_ptr->get_PCIE_read_ptr());
    cdsPciModules.dolphinPcieWritePtr = static_cast<volatile long unsigned int*>(_dolphin_buffers_ptr->get_PCIE_write_ptr());
    cdsPciModules.dolphinRfmReadPtr = static_cast<volatile long unsigned int*>(_dolphin_buffers_ptr->get_RFM_read_ptr());
    cdsPciModules.dolphinRfmWritePtr = static_cast<volatile long unsigned int*>(_dolphin_buffers_ptr->get_RFM_write_ptr());
    cdsPciModules.stop_dolphin_ipcs = 0;

    // Print out all the I/O information
    print_io_info( SYSTEM_NAME_STRING_LOWER, &cdsPciModules);

    // Initialize buffer for daqLib.c code
    daqBuffer = (long)&daqArea[ 0 ];

    //We set the global run enable
    pLocalEpics->epicsInput.vmeReset = 0;

    /// **********************************************************************************************\n
    /// Start Initialization Process \n
    /// **********************************************************************************************\n

    spdlog::info("initialize_model_globals() - Starting Initialization Process ...\n");

    //memset( tempClock, 0, sizeof( tempClock ) );

    fz_daz( ); /// \> Kill the denorms!


    /// \> Init comms with EPICS processor */
    _pEpicsComms = (RFM_FE_COMMS*)_epics_shm;
    pLocalEpics = (CDS_EPICS*)&(_pEpicsComms->epicsSpace);
    pEpicsDaq = (char*)&( pLocalEpics->epicsOutput );

#ifdef OVERSAMPLE
    /// \> Zero out filter histories
    memset( dHistory, 0, sizeof( dHistory ) );
    memset( dDacHistory, 0, sizeof( dDacHistory ) );
#endif

    clear_dac_outputs();

    /// \> Set pointers to filter module data buffers. \n
    /// - ---- Prior to V2.8, separate local/shared memories for FILT_MOD
    /// data.\n
    /// - ---- V2.8 and later, code uses EPICS shared memory only. This was done
    /// to: \n
    /// - -------- Allow daqLib.c to retrieve filter module data directly from
    /// shared memory. \n
    /// - -------- Avoid copy of filter module data between to memory locations,
    /// which was slow. \n
    pDsp[ 0 ] = (FILT_MOD*)( &_pEpicsComms->dspSpace );
    pCoeff[ 0 ] = (VME_COEF*)( &_pEpicsComms->coeffSpace );
    dspPtr[ 0 ] = (FILT_MOD*)( &_pEpicsComms->dspSpace );

    /// \> Clear the FE reset which comes from Epics
    pLocalEpics->epicsInput.vmeReset = 0;

    // Need this FE dcuId to make connection to FB
    // TODO: WTF is this doing?
    dcuId = pLocalEpics->epicsInput.dcuId;
    pLocalEpics->epicsOutput.dcuId = dcuId;

    // Reset timing diagnostics
    pLocalEpics->epicsOutput.diagWord = 0;
    pLocalEpics->epicsOutput.timeDiag = 0;
    pLocalEpics->epicsOutput.timeErr = SYNC_SRC_MASTER;

    if (init_filter_modules() == false)
        return false;

    // Initialize some specific part types
    demodulation_init();

    // Initialize statespace parts
    int num_parts = get_num_statespace_parts();
    if ( globStateSpaceInit(num_parts) != 0 ) {
        spdlog::error( "initialize_model_globals() - Failed to initialize statespace parts, num: %d\n", num_parts );
        return false;
    }



    /// < Read in all Filter Module EPICS coeff settings
    for (int ii = 0; ii < MAX_MODULES; ii++ )
    {
        checkFiltReset( ii,
                        dspPtr[ 0 ],
                        pDsp[ 0 ],
                        &_dspCoeff[ 0 ],
                        MAX_MODULES,
                        pCoeff[ 0 ] );
    }


    double cycle_adc_input[MAX_ADC_MODULES][MAX_ADC_CHN_PER_MOD];
    double cycle_dac_output[MAX_DAC_MODULES][MAX_DAC_CHN_PER_MOD];
    iopDacEnable = feCode( 0,
                           cycle_adc_input,
                           cycle_dac_output,
                           dspPtr[ 0 ],
                           &_dspCoeff[ 0 ],
                           (struct CDS_EPICS*)pLocalEpics,
                           1 );


    //Statespace parts all have assigned names after feCode() init
    std::vector< std::string > ss_names = get_all_statespace_names();
    for( unsigned index=0; index < ss_names.size(); ++index) {
        _statespace_name_to_index_map[ss_names[index]] = index;
    }

    return true ;
}


//
// Start Interface Model Member functions
//
std::unique_ptr<Model> Model::create_instance( int log_level )
{

    if (Model::_has_been_created)
    {
        spdlog::error("Model::create_instance() - Only one instance of Model can be crested at once, "
                     "there are shared globals used by the real time model library that would conflict if "
                     "multiple Models are increated in the same process.");
        return nullptr;
    }

    std::unique_ptr<Model> me_ptr (new Model());

#if SPDLOG_VERSION >= 10500
    //Load SPDLOG level from env if we have a high enough version
    spdlog::cfg::load_env_levels();
#endif

    me_ptr->_impl = Model::Impl::create_instance( log_level );
    if(me_ptr->_impl == nullptr) return nullptr;

    Model::_has_been_created = true;
    return me_ptr;
}

Model::~Model() 
{
    Model::_has_been_created = false;
};

Model::Model() {};

int Model::get_model_rate_Hz()
{
    return _impl->get_model_rate_Hz();
}

std::string Model::get_model_name()
{
    return _impl->get_model_name();
}

int Model::get_cycle_num()
{
  return _impl->get_cycle_num();
}

uint64_t Model::get_gps_time()
{
    return _impl->get_gps_time();
}

void Model::set_gps_time(uint64_t gps)
{
    _impl->set_gps_time(gps);
}

void Model::set_log_level(int level)
{
    return _impl->set_log_level(level);
}

int Model::get_num_filter_modules()
{
    return _impl->get_num_filter_modules();
}

bool Model::set_var(const std::string& name, double val)
{
     return _impl->set_var(name, val);
}

bool Model::set_var(const std::string& name, float val)
{
     return _impl->set_var(name, val);
}

bool Model::set_var(const std::string& name, int val)
{
     return _impl->set_var(name, val);
}

bool Model::set_var_momentary(const std::string& name, double val)
{
    return _impl->set_var_momentary(name, val);
}


template <class T>
std::optional<T> Model::get_var(const std::string& name) const
{

    static_assert(std::is_same<T, int>::value ||
                  std::is_same<T, double>::value ||
                  std::is_same<T, float>::value ||
                  std::is_same<T, char>::value 
                  , "Model::get_var() - Only double, float, int, and char are supported types.");
    return _impl->get_var<T>(name);
}

// We need to be explicit about the types we are going to support
// Can't put these in the header because _impl is forward declared as
// part of the ptr to impl pattern.
// These need to match the static_assert above, so we push any misuse to 
// compile time.
template class std::optional<int> Model::get_var(const std::string& name) const;
template class std::optional<double> Model::get_var(const std::string& name) const;
template class std::optional<float> Model::get_var(const std::string& name) const;
template class std::optional<char> Model::get_var(const std::string& name) const;

std::vector<std::string> Model::get_all_input_names()
{
    return _impl->get_all_input_names();
}

std::vector<std::string> Model::get_all_model_var_names()
{
    return _impl->get_all_model_var_names();
}

std::vector<std::string> Model::get_all_filter_names()
{
    return _impl->get_all_filter_names();
}

void Model::set_adc_channel_generator(int adcNum, int chanNum, std::shared_ptr<Generator> gen)
{
    return _impl->set_adc_channel_generator(adcNum, chanNum, gen );
}

bool Model::set_excitation_point_generator(const std::string & name, std::shared_ptr<Generator> gen)
{
    return _impl->set_excitation_point_generator(name, gen);
}

int Model::get_awg_slot(const std::string & name)
{
    return _impl->get_awg_slot(name);
}

int Model::free_awg_slot(int slot)
{
    return _impl->free_awg_slot(slot);
}

bool Model::set_ipc_receiver_generator(const std::string & name, std::shared_ptr<Generator> gen)
{
    return _impl->set_ipc_receiver_generator(name, gen);
}

void Model::record_dac_output(bool shouldRec)
{
    _impl->record_dac_output(shouldRec);
}

bool Model::record_model_var(const std::string & name, int record_rate_hz)
{
    return _impl->record_model_var(name, record_rate_hz);
}

bool Model::stop_recording_model_var(const std::string & name)
{
    return _impl->stop_recording_model_var(name);
}

template< class T>
std::optional<std::vector<T>> Model::get_recorded_var(const std::string & name) const
{
    static_assert(std::is_same<T, int>::value ||
                  std::is_same<T, double>::value ||
                  std::is_same<T, float>::value ||
                  std::is_same<T, char>::value
                  , "Model::get_recorded_var() - Only double, float, int, and char are supported types.");
    return _impl->get_recorded_var<T>(name);
}
// See note above at getVar()
template class std::optional<std::vector<int>> Model::get_recorded_var(const std::string& name) const;
template class std::optional<std::vector<double>> Model::get_recorded_var(const std::string& name) const;
template class std::optional<std::vector<float>> Model::get_recorded_var(const std::string& name) const;
template class std::optional<std::vector<char>> Model::get_recorded_var(const std::string& name) const;

void Model::multicycle_record_control( bool enable )
{
    _impl->multicycle_record_control( enable );
}

int Model::load_snap_file(const std::string & pathname)
{
    return _impl->load_snap_file(pathname);
}

const std::vector< double > & Model::get_dac_output_by_id(int dacIndex, int chanIndex)
{
    return _impl->get_dac_output_by_id(dacIndex, chanIndex);
}

int Model::run_model(unsigned num_cycles)
{
    return _impl->run_model(num_cycles);
}

std::optional<LIGO_FilterModule> Model::get_filter_module_by_name(const std::string & name)
{
    return _impl->get_filter_module_by_name(name);
}

std::optional<LIGO_FilterModule> Model::get_filter_module_by_id(int module_index)
{
    return _impl->get_filter_module_by_id(module_index);
}


bool Model::reset_filter_module(const std::string & name)
{
    return _impl->reset_filter_module(name);
}

std::vector<double> Model::read_biquad_coefficients(const std::string & filter_name,
                                                    int stageIndexToRead)
{
    return _impl->read_biquad_coefficients(filter_name, stageIndexToRead);
}

bool Model::load_biquad_coefficients(const std::string & filter_name,
                                   const std::vector<int> & stageIndicesToLoad,
                                   const std::vector<double> & coefficients)
{
    return _impl->load_biquad_coefficients(filter_name, stageIndicesToLoad, coefficients);
}

bool Model::load_scipy_sos_coef(const std::string & filter_name,
                                  const std::vector<int> & stageIndicesToLoad,
                                  const std::vector< std::vector<double> > & sections_and_coefficients)
{
    return _impl->load_scipy_sos_coef(filter_name, stageIndicesToLoad, sections_and_coefficients);
}

std::vector<double> Model::read_fir_coefficients(const std::string & filter_name, int stageIndexToRead)
{
    return _impl->read_fir_coefficients(filter_name, stageIndexToRead);
}

bool Model::load_from_filter_file(const std::string & filterFilePathName, const std::string & firFilterFilePathName)
{
    return _impl->load_from_filter_file(filterFilePathName, firFilterFilePathName);
}

int Model::get_filter_switch_type(const std::string & filter_name,
                                  int section_index)
{
    return _impl->get_filter_switch_type(filter_name, section_index);
}

bool Model::set_filter_switch_type(const std::string & filter_name,
                                   int section_index,
                                   int switch_type)
{
    return _impl->set_filter_switch_type(filter_name, section_index, switch_type);
}

int Model::get_num_statespace_parts()
{
    return _impl->get_num_statespace_parts();
}


int Model::load_statespace_from_file( const std::string & statespaceFilePathName )
{
    return _impl->load_statespace_from_file( statespaceFilePathName );
}

std::vector< std::string >  Model::get_all_statespace_names()
{
    return _impl->get_all_statespace_names();
}

bool Model::load_statespace( const std::string & part_name, 
                      const std::vector< double >  & initial_state_vec,
                      const std::vector< double >  & input_matrix,
                      const std::vector< double >  & output_matrix,
                      const std::vector< double >  & state_matrix,
                      const std::vector< double >  & feedthrough_matrix )
{
    return _impl->load_statespace(part_name, initial_state_vec, input_matrix, 
                                  output_matrix, state_matrix, feedthrough_matrix);
}
