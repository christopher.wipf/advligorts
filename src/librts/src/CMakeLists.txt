project (${RTS_LIB_NAME})
find_package(spdlog REQUIRED)
if (spdlog_VERSION VERSION_LESS "1.4.0")
    #Older versions of spdlog don't have spdlog::spdlog_header_only
    #so we don't like at all (Deb 10)
    set(DONT_LINK_SPDLOG YES)
endif()

# Include generated FLAGS for the model we are going to build
include(${RCG_BUILDD}/models/${MODEL_NAME}/userspace/UserspaceVars.cmake)

# We need this in the librts build because LIGO_FilterModule.cpp includes headers than include inlineMath
if(${USE_STDLIB_MATH})
    list(APPEND CFLAGS "-DUSE_STDLIB_MATH=1")
endif()

list(APPEND CFLAGS "-fvisibility=hidden")


#Turn the ; seperated list into space seperated list and add to CFLAGS
string(REPLACE ";" " " CFLAGS_SPACE_SEPERATED "${CFLAGS}")
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CFLAGS_SPACE_SEPERATED} -Wall -Wno-register" )

if(${LIBRTS_NO_SHMEM})
    set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -DLIBRTS_NO_SHMEM=1")
endif()



#
# Start local source files
add_library(${PROJECT_NAME} 
            ${CMAKE_CURRENT_SOURCE_DIR}/Model.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/LIGO_FilterModule.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/SnapFile.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/DolphinBuffers.cpp)

target_include_directories(${PROJECT_NAME}
    PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/../include
        ${RCG_BUILDD}/models/${MODEL_NAME}/include/
        ${RCG_SRC}/src/include)
target_link_libraries(${PROJECT_NAME}
    PUBLIC
    -L${RCG_BUILDD}/models/${MODEL_NAME}/userspace/build/ -l${MODEL_NAME}
    awgstandalone
    m)
if(UNIX AND NOT APPLE)
    target_link_libraries(${PROJECT_NAME} PUBLIC rt pthread)
endif()
if( NOT DONT_LINK_SPDLOG )
    target_link_libraries(${PROJECT_NAME} PRIVATE spdlog::spdlog_header_only)
endif()

