#include <sstream>
#include <fstream>
#include <string_view>

//libspdlog-dev
#include "spdlog/spdlog.h"

#include "SnapFile.hh"

using namespace std;
using namespace rts;

std::map< std::string, int > SnapFile::_enum_val_to_num = {
    {"OFF", 0},
    {"ON", 1},
    {"ZNAM", 0},
    {"ONAM", 1},
    {"ZRST", 0}, 
    {"ONST", 1}, 
    {"TWST", 2}, 
    {"THST", 3}, 
    {"FRST", 4}, 
    {"FVST", 5}, 
    {"SXST", 6},
    {"SVST", 7}, 
    {"EIST", 8}, 
    {"NIST", 9}, 
    {"TEST", 10}, 
    {"ELST", 11}, 
    {"TVST", 12}, 
    {"TTST", 13},
    {"FTST", 14}, 
    {"FFST", 15}
};

SnapFile::SnapFile()
{
}

SnapFile::~SnapFile()
{
}

bool SnapFile::endsWith(std::string_view str, std::string_view suffix)
{
    return str.size() >= suffix.size() && 0 == str.compare(str.size()-suffix.size(), suffix.size(), suffix);
}

std::optional<double> SnapFile::getVal(const std::string & var_name)
{

    double val_hold;
    const auto var_it = _loaded_snap.find(var_name);

    if ( var_it == _loaded_snap.end() )
    {
        spdlog::error("getVal() - Called with var name {}, but could not be found.", var_name);
        return {};
    }

    try {
        val_hold = std::stod( std::get<0>( var_it->second ) );
    }
    catch (const std::exception& e) {
       
        //If we can't convert to double, try enum conversion 
        const auto enum_it = _enum_val_to_num.find( std::get<0>( var_it->second ) );
        if( enum_it != _enum_val_to_num.end() )
            return enum_it->second;
        else
            return {};


    }

    //We were able to do the double conversion, return value
    return val_hold;

}

std::optional<double> SnapFile::enumToVal(const std::string & enum_val)
{
    const auto enum_it = _enum_val_to_num.find( enum_val );
    if (enum_it != _enum_val_to_num.end() )
        return enum_it->second;
    else
        return {};
}


int SnapFile::load(const std::string & pathname)
{
    string line;
    string token;
    ifstream infile (pathname);

    if ( !infile.is_open() )
    {
        return -1;
    }

    stringstream ss;
    bool in_header = false;
    int num_tokens;
    std::string name_hold, value_hold;
    std::map< std::string, int > sw1_switch_hold;
    std::map< std::string, int > sw2_switch_hold;

    _loaded_snap.clear();

    while ( getline (infile, line) )
    {

        if( line[0] == '-' && in_header == false) {
            in_header = true;
            continue;
        }

        if( line[0] == '-' && in_header == true ) {
            in_header = false;
            continue;
        }

        if ( in_header == true ) continue; //In header so don't parse

        ss.clear();
        ss << line;

        num_tokens = 0;
        while (ss && ss >> token)
        {
            //spdlog::info("Token is: {}", token);

            if (num_tokens == 0) //Channel name
            {
                if (token[2] != ':')
                {
                    spdlog::error("SnapFile::load() malformed channel name {}", token);
                    return -1;
                }

                if (token[3] == 'F' && token[4] == 'E' && token[5] == 'C')
                {
                    name_hold = token.substr(10); //TODO: We remove IFO:FEC-N here
                }
                else
                {
                    name_hold = token.substr(7); //TODO: We remove IFO/SYS here
                }

            }
            else if (num_tokens == 1) {} //Unused
            else if (num_tokens == 2) //Value
            {
                value_hold = token;
            }
            else if (num_tokens == 3) //Monitored
            {
                if( endsWith(name_hold, "_SW1S") ) {
                    sw1_switch_hold[name_hold] = std::stoi(value_hold); 
                }
                else if( endsWith(name_hold, "_SW2S") ) {
                    sw2_switch_hold[name_hold] = std::stoi(value_hold);
                }
                else {
                    _loaded_snap[name_hold] = {value_hold, token};
                }
            }
            else if (num_tokens == 4) { break; } //Unused, only in fec.snap files


            ++num_tokens;

        }


    }


    //After parsing file, sort out filter SWS
    std::string temp;
    for( auto & [name, val] : sw1_switch_hold) {
        temp = name.substr(0, name.size() - 5 ) + "_SWS";
        //spdlog::info("Adding key {} ", temp);
       _loaded_snap[ temp ] = { std::to_string((val & 0xFFFF)), ""};
    }

    
    for(auto & [name, val] : sw2_switch_hold) {

        temp = name.substr(0, name.size() - 5 ) + "_SWS";
        auto it = sw2_switch_hold.find( temp );
        if ( it == sw2_switch_hold.end() ) {
            _loaded_snap[temp] = {std::to_string(val << 16), ""};
        }
        else {

            _loaded_snap[temp] = { std::to_string((val << 16) | (it->second & 0xFFFF)), ""};
        }
    }


    return _loaded_snap.size();
}
