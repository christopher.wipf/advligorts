#!/usr/bin/env bash
if [ -z "$1" ]
  then
    echo "Usage Error - Pass in the path to your userapps directory. ex: $0 /opt/rtcds/userapps/release/ <SITE>"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "Usage Error - Pass in the site name as the second argument. ex: $0 <USERAPPS_PATH> x1"
    exit 1
fi

site=`echo "$2" | awk '{print tolower($0)}'`

paths=`find $1/*/common/src/* -maxdepth 6 -name "*.c" -exec dirname {} \; | uniq` #Get common models
paths=${paths}$(echo -en "\n\b")`find $1/*/$site/* -maxdepth 6 -name "*.c" -exec dirname {} \; | uniq` #get site models

IFS=$(echo -en "\n\b")
col_paths=""
for path in $paths; do
    col_paths=$col_paths":"$path
done

echo $col_paths
