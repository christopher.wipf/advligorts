import os
import os.path
import time
import typing
import integration


##print(os.getcwd())

integration.Executable(name="local_dc", hints=["../local_dc",], description="local_dc")
integration.Executable(name="fe_simulated_streams", hints=["../fe_stream_test/fe_simulation",], description="data stream generator")
integration.Executable(name="check_for_dcu_existence", hints=[], description="")

ini_dir = integration.state.temp_dir('ini_files')
master_file = os.path.join(ini_dir, "master")

mod5ini = os.path.join(ini_dir, "mod5.ini")
mod5par = os.path.join(ini_dir, "tpchn_mod5.par")
mod6ini = os.path.join(ini_dir, "mod6.ini")
mod6par = os.path.join(ini_dir, "tpchn_mod6.par")
mod7ini = os.path.join(ini_dir, "mod7.ini")
mod7par = os.path.join(ini_dir, "tpchn_mod7.par")
mod250ini = os.path.join(ini_dir, "mod250.ini")
mod250par = os.path.join(ini_dir, "tpchn_mod250.par")
mod255ini = os.path.join(ini_dir, "mod255.ini")
mod255par = os.path.join(ini_dir, "tpchn_mod255.par")


fe_simulated_streamsTemp = integration.Process("fe_simulated_streams",
                                           [
                                               "-S",
                                               "-i", ini_dir,
                                               "-M", master_file,
                                               "-k", "300",
                                               "-D", "52"
                                           ])
fe_simulated_streams = integration.Process("fe_simulated_streams",
                                           [
                                               "-S",
                                               "-i", ini_dir,
                                               "-M", master_file,
                                               "-k", "300",
                                               "-D", "5,6,7,250,255"
                                           ])

local_dc = integration.Process("local_dc", [
                                            "-m", "140",
                                            "-s", "mod5 mod6 mod7 mod250 mod255 mod52:52",
                                            "-d", ini_dir,
                                            "-b", "local_dc"
                                            ])

check_for_dcu_existence = integration.Process("check_for_dcu_existence", [
                                            "-m", "100",
                                            "-b", "local_dc",
                                            "-p", "5",
                                            "--require-dcus", "5,6,7,250,255",
                                            "--prohibit-dcus", "52"

])

def echo(msg):
    def wrapper():
        print(msg)

    return wrapper

integration.Sequence(
    [
        integration.state.preserve_files,
        integration.require_readable(files=["/dev/gpstime", "/dev/mbuf"], description="required device files"),
        fe_simulated_streamsTemp.run,
        integration.wait(),
        echo("DCU52 streamer up"),
        fe_simulated_streams.run,
        integration.wait(),
        echo(" Regular streamer up"),
        local_dc.run,
        integration.wait(),
        echo(" local_dc streamer up"),
        fe_simulated_streamsTemp.ignore,
        integration.wait(),
        echo(" DCU52 streamer killed"),
        check_for_dcu_existence.run,








    ]
)
