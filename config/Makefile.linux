# Linux Epics IOC system build rules

MODEL_NAME := $(subst epics,,$(TARGET))

EPICS_VERSION := $(shell pkg-config --modversion epics-base)
EPICS_BIN_PATH := $(shell pkg-config --variable=bindir epics-base)
FMSEQ_OUTPUT_DIR :=$(MODEL_EPICS_DIR)/fmseq


# Sequencer synchronization; to fix broken seq package
SRC += $(RCG_SRC_DIR)/src/drv/sync.c

# Do not delete these intermediate files
.SECONDARY: $(MODEL_EPICS_SRC_DIR)/*.i

# Preprocessor is used on .st file to get .i which is later fed to SNC
PREPROCESS = /usr/bin/gcc -x c -E -P 

# Standard C and C++ compiler and C preprocessor flags
CFLAGS += $(EXTRA_CFLAGS)
CFLAGS += $(shell pkg-config --cflags epics-base)
CFLAGS += -I/usr/lib/rmnet -I/usr/lib/rfm2g
CFLAGS += -I$(MODEL_EPICS_DIR)/../include
CFLAGS += -I$(RCG_SRC_DIR)/src/include -I$(RCG_SRC_DIR)/src/include/drv
ifeq (,$(findstring tcsepics, $(TARGET)))
CFLAGS += -DRFM_EPICS -DNO_FM10GEN_C_CODE
endif
ifeq (rtlepics,$(findstring rtlepics, $(TARGET)))
CFLAGS += -DNO_RFM_EPICS_RECORD_SUPPORT
endif
CFLAGS += -D_POSIX_C_SOURCE=199506L -D_POSIX_THREADS  
CFLAGS += -DOSITHREAD_USE_DEFAULT_STACK  -D_X86_  -DUNIX  -D_BSD_SOURCE -Dlinux
CFLAGS += -D_REENTRANT -g -fPIC 

rtl_module := $(shell /sbin/lsmod | grep rtl)
ifeq ($(rtl_module),)
CFLAGS += -DNO_RTL=1
endif

CXXFLAGS = $(CFLAGS)
SNCFLAGS += +a -c -w
#SNCFLAGS += -e

# Sequencer file (.st) compiler
#SNC = $(EPICS_SEQ_PATH)/bin/$(EPICS_ARCH)/snc $(SNCFLAGS)
SNC = $(EPICS_BIN_PATH)/snc $(SNCFLAGS)

# registration code generator
REGGEN = $(EPICS_BIN_PATH)/registerRecordDeviceDriver.pl

# Sequencer generator files
FMSEQFILES = $(RCG_SRC_DIR)/src/epics/util/fmseq.pl $(RCG_SRC_DIR)/src/epics/util/skeleton.db $(RCG_SRC_DIR)/src/epics/util/skeleton.st

# Standard Epics IOC server library flags
LIBFLAGS += -g
#LIBFLAGS += -L$(EPICS_LIB_PATH)
#LIBFLAGS += -L$(EPICS_SEQ_PATH)/lib/$(EPICS_ARCH)
#LIBFLAGS += -L$(EPICS_EXT_PATH)/lib/$(EPICS_ARCH)
LIBFLAGS += $(shell pkg-config --libs epics-base)

# test for 3.15 libraries, otherwise use 3.14
#ifneq ("$(wildcard $(EPICS_LIB_PATH)/libdbCore.a)","")
ifneq ($(wordlist 1,2,$(subst '.',' ',$(EPICS_VERSION))), (3 15))
LIBFLAGS += -lseq -lpv -ldbRecStd -ldbCore
LIBFLAGS += -lca -lCom
else
LIBFLAGS += -lseq -lseqDev -lpv -lsoftDevIoc -lrecIoc
LIBFLAGS += -lmiscIoc -lrsrvIoc -ldbtoolsIoc
LIBFLAGS += -lasIoc -ldbIoc -lregistryIoc -ldbStaticIoc -lca -lCom
endif
LIBFLAGS += -lpthread -lreadline -lcurses -lrt

# User Makefiles's $(SRC) variable is split here in sequencer source
# and all the rest, which should be C files
SEQ_SRCS = $(filter %.st, $(SRC))
CSRCS = $(filter-out %.cc, $(filter-out %.st, $(SRC)))
# Required Epics files
CCSRCS += $(filter %.cc, $(SRC))
CCSRCS += $(MODEL_EPICS_SRC_DIR)/registerRecordDeviceDriver.cc

# Each .st file gets preprocessed twice to make one C and one C++
# file. C++ file has different name. Both files are exactly the same
# in content
SEQ_ISRC = $(addprefix $(MODEL_EPICS_SRC_DIR)/, $(notdir $(SEQ_SRCS:%st=%i)))
#SEQ_CCSRC = $(addprefix $(MODEL_EPICS_SRC_DIR)/, $(notdir $(SEQ_SRCS:%.st=%_foo.cc)))
SEQ_CCSRC += $(addprefix $(MODEL_EPICS_SRC_DIR)/, $(notdir $(SEQ_SRCS:%st=%c)))
SEQ_OBJ = $(addprefix $(MODEL_EPICS_SRC_DIR)/, $(notdir $(SEQ_SRCS:%st=%o)))
#SEQ_OBJ += $(addprefix $(MODEL_EPICS_SRC_DIR)/, $(notdir $(SEQ_SRCS:%.st=%_foo.o)))
SEQ_OBJ += $(addprefix $(MODEL_EPICS_SRC_DIR)/, $(notdir $(CSRCS:%.c=%.o)))
SEQ_OBJ += $(addprefix $(MODEL_EPICS_SRC_DIR)/, $(notdir $(CCSRCS:%.cc=%.o)))

# Standard Epics IOC database definition file
DBD += $(RCG_SRC_DIR)/src/epics/dbd/a.dbd $(MODEL_EPICS_SRC_DIR)/*.dbd

# Main target to make an Epics IOC server
all: target $(MODEL_EPICS_BUILD_DIR)/$(TARGET) install

# Install built epics IOC into build/$(TARGET)
install: $(DB)
	mkdir -p $(MODEL_EPICS_BUILD_DIR)/db
	/bin/rm -f $(MODEL_EPICS_BUILD_DIR)/db/*/autoBurt.req
	for ifo in $(IFO) ; do \
	  system=`echo $(subst epics,,$(TARGET)) | tr a-z A-Z | cut -c1-3`; \
	  model=`echo $(subst epics,,$(TARGET))`; \
	  ucmodel=`echo $(subst epics,,$(TARGET)) | tr a-z A-Z `; \
	  ifo0=`echo $$ifo | sed 's/[0-9]/0/g'`; \
	  mkdir -p $(MODEL_EPICS_BUILD_DIR)/db/$$ifo; \
	  /bin/rm -f $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd ; \
	  echo "dbLoadDatabase \"base.dbd\"" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
	  echo 'registerRecordDeviceDriver(pdbbase)' >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd ; \
	  for i in $(DB) ; do \
		cat $$i | sed s/%IFO%/$$ifo/g\;s/%IFO0%/$$ifo0/g\;s/%SYS%/$$system/g > $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i`; \
		grep record  $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i` | egrep 'DACKILL|record\(ao|record\(stringout' | egrep -v 'DACKILL_PANIC|DACKILL_BPTIME|_SDF_' | sed 's/.*"\(.*\)\".*/\1/g' | awk '{ printf "RO %s\n", $$0 }' >> $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/autoBurt.req; \
		grep record  $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i` | egrep 'record\(stringin' | egrep -v 'SDF_NAME | _SDF_' | sed 's/.*"\(.*\)\".*/\1/g' | awk '{ printf "%s\n", $$0 }' >> $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/autoBurt.req; \
		grep record  $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i` | egrep 'record\(ao' | egrep -v 'record\(mbbi' | egrep -v 'SWSTAT|DACKILL|_SDF_' | sed 's/.*"\(.*\)\".*/\1/g' | awk '{ printf "%s.HSV\n%s.LSV\n%s.HIGH\n%s.LOW\n", $$0, $$0, $$0, $$0 }' >> $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/autoBurt.req; \
                grep record  $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i` | egrep 'SWSTAT|DACKILL' | egrep -v 'DACKILL_PANIC' | sed 's/.*"\(.*\)\".*/\1/g' | awk '{ printf "RO %s.HSV\nRO %s.LSV\nRO %s.LOW\n", $$0, $$0, $$0 }' >> $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/autoBurt.req; \
		\
		grep record  $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i` | egrep 'record\(bo|record\(bi' | egrep -v '_SDF_' | sed 's/.*"\(.*\)\".*/\1/g' >> $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/autoBurt.req; \
		grep record  $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i` | egrep 'record\(bo|record\(bi' | egrep -v '_SDF_' | sed 's/.*"\(.*\)\".*/\1/g' | awk '{ printf "%s.OSV\n%s.ZSV\n", $$0, $$0 }' >> $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/autoBurt.req; \
		\
		grep record  $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i` | egrep 'DACKILL_BPTIME' |  sed 's/.*"\(.*\)\".*/\1/g' | awk '{ printf "%s\n", $$0 }' >> $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/autoBurt.req; \
		\
		grep record  $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i` | egrep -v 'DACKILL|SDF_RELOAD|_SDF_|record\(ao|record\(string|record\(bi|record\(bo'| sed 's/.*"\(.*\)\".*/\1/g' >> $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/autoBurt.req; \
		grep record  $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i` | egrep -v 'SWSTAT|DACKILL|_SDF_|record\(ao|record\(string|record\(bi|record\(bo|record\(mbbi' | sed 's/.*"\(.*\)\".*/\1/g' | awk '{ printf "%s.HSV\n%s.LSV\n%s.HIGH\n%s.LOW\n", $$0 , $$0, $$0, $$0 }' >> $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/autoBurt.req; \
		\
		echo "dbLoadRecords \"db/$${ifo}/`basename $$i`\"" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet DAQ_FILE /opt/rtcds/$(SITE)/${ifo}/chans/daq/$$ucmodel.ini" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet DAQ_DIR /opt/rtcds/$(SITE)/${ifo}/chans/daq/" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet FOTON_FILE /opt/rtcds/$(SITE)/${ifo}/chans/$$ucmodel.txt" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet FOTON_DIFF_FILE /opt/rtcds/$(SITE)/${ifo}/chans/tmp/$$ucmodel.diff" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet COEFF_FILE /opt/rtcds/$(SITE)/${ifo}/chans/tmp/$$ucmodel.txt" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet LOG_DIR /opt/rtcds/$(SITE)/${ifo}/log/$$model" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet SDF_DIR /opt/rtcds/$(SITE)/${ifo}/target/$$model/$(TARGET)/burt/" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
                echo "epicsEnvSet SDF_FILE safe" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
                echo "epicsEnvSet SDF_MODEL $$model" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet MODEL_SITE $(SITE)" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet MODEL_IFO ${ifo}" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet SYNC_SRC $(SYNC_SRC)" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo "epicsEnvSet SDF_FILE_LOADED 0" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		echo -n 'epicsEnvSet PREFIX '>> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
		grep _GRD_RB_STAT0  $(MODEL_EPICS_BUILD_DIR)/db/$$ifo/`basename $$i` | head -1 | sed 's/[^"]*"\([^"]*\)_GRD_RB_STAT0.*/\1/g' >>  $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
	  done ; \
	done
	for ifo in $(IFO) ; do \
	  echo "iocInit" >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd ; \
	  for i in $(SEQ) ; do \
		echo "seq &$$i" | sed s/%IFO%/$$ifo/g\;s/%SITE%/`if test $$ifo = L1 ; then echo llo ; else echo lho ; fi`/g >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)$$ifo.cmd; \
	  done ; \
	done
	for i in $(SEQH1) ; do \
		echo "seq &$$i" | sed s/%IFO%/H1/g\;s/%SITE%/lho/g >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)H1.cmd; \
	done
	for i in $(SEQH2) ; do \
		echo "seq &$$i" | sed s/%IFO%/H2/g\;s/%SITE%/lho/g >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)H2.cmd; \
	done
	for i in $(SEQL1) ; do \
		echo "seq &$$i" | sed s/%IFO%/L1/g\;s/%SITE%/llo/g >> $(MODEL_EPICS_BUILD_DIR)/$(TARGET)L1.cmd; \
	done
	echo 'This is generated directory. Please make changes in CVS and reinstall from scratch.' > $(MODEL_EPICS_BUILD_DIR)/README.making_changes
	echo 'Changes made to files in this directory will be lost.' >> $(MODEL_EPICS_BUILD_DIR)/README.making_changes
	echo >> $(MODEL_EPICS_BUILD_DIR)/README.making_changes
	echo 'Built on date' `date` >> $(MODEL_EPICS_BUILD_DIR)/README.making_changes

# Epics IOC server link target
$(MODEL_EPICS_BUILD_DIR)/$(TARGET): $(SEQ_OBJ)
	$(CXX) -o $@ $? $(LIBFLAGS)

# Object files depend on sources
$(SEQ_OBJ): $(SEQ_CCSRC) $(CCSRCS) $(CSRCS)

# Need to get .i files before we can run SNC to produce sources
$(SEQ_CCSRC): $(SEQ_ISRC)

# Make installation target directory
target:
	@mkdir -p $(MODEL_EPICS_SRC_DIR)
	@mkdir -p $(MODEL_EPICS_BUILD_DIR)

# Preprocess a sequencer file
$(MODEL_EPICS_SRC_DIR)/%.i: src/epics/seq/%.st
	$(PREPROCESS) $(CFLAGS) $< > $@

$(MODEL_EPICS_SRC_DIR)/%.i: src/epics/seq/lsc/%.st
	$(PREPROCESS) $(CFLAGS) $< > $@

$(MODEL_EPICS_SRC_DIR)/%.i: src/epics/seq/sus/%.st
	$(PREPROCESS) $(CFLAGS) $< > $@

$(MODEL_EPICS_SRC_DIR)/%.i: src/epics/seq/quad/%.st
	$(PREPROCESS) $(CFLAGS) $< > $@

# Preprocess generated sequencer file
# Preprocess generated sequencer file
.PRECIOUS: $(MODEL_EPICS_SRC_DIR)/%.st
$(MODEL_EPICS_SRC_DIR)/%.i: $(MODEL_EPICS_SRC_DIR)/%.st
	$(PREPROCESS) $(CFLAGS) $< > $@

# Generate sequencer file using fmseq.pl
# Warning the mvs below are not commented out, so be careful
$(MODEL_EPICS_SRC_DIR)/%.st: $(FMSEQ_OUTPUT_DIR)/% $(FMSEQFILES)
	(cd $(FMSEQ_OUTPUT_DIR); \
	 cat $(notdir $<) | cpp $(CFLAGS) -  | grep -v ^# | $(if $(FMSEQ), $(FMSEQ), $(RCG_SRC_DIR)/src/epics/util/fmseq.pl) $(notdir $<) &&\
	 mv epics_map.h ../../include/ &&\
	 mv $(notdir $<).* ../../../../$(MODEL_EPICS_SRC_DIR))


# Replicate sequencer source, because I am too lazy to write re-entrant code
# Following block of rules used to replicate fmseq.pl generated sequencers
$(MODEL_EPICS_SRC_DIR)/%1.st: $(MODEL_EPICS_SRC_DIR)/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\11/g > $@
$(MODEL_EPICS_SRC_DIR)/%2.st: $(MODEL_EPICS_SRC_DIR)/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\12/g > $@
$(MODEL_EPICS_SRC_DIR)/%3.st: $(MODEL_EPICS_SRC_DIR)/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\13/g > $@
$(MODEL_EPICS_SRC_DIR)/%4.st: $(MODEL_EPICS_SRC_DIR)/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\14/g > $@
$(MODEL_EPICS_SRC_DIR)/%5.st: $(MODEL_EPICS_SRC_DIR)/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\15/g > $@
$(MODEL_EPICS_SRC_DIR)/%6.st: $(MODEL_EPICS_SRC_DIR)/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\16/g > $@
$(MODEL_EPICS_SRC_DIR)/%7.st: $(MODEL_EPICS_SRC_DIR)/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\17/g > $@
$(MODEL_EPICS_SRC_DIR)/%8.st: $(MODEL_EPICS_SRC_DIR)/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\18/g > $@
$(MODEL_EPICS_SRC_DIR)/%9.st: $(MODEL_EPICS_SRC_DIR)/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\19/g > $@
$(MODEL_EPICS_SRC_DIR)/%10.st: $(MODEL_EPICS_SRC_DIR)/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\110/g > $@

# Replicate sequncers
$(MODEL_EPICS_SRC_DIR)/%1.st: $(RCG_SRC_DIR)/src/epics/seq/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\11/g > $@
$(MODEL_EPICS_SRC_DIR)/%2.st: $(RCG_SRC_DIR)/src/epics/seq/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\12/g > $@
$(MODEL_EPICS_SRC_DIR)/%3.st: $(RCG_SRC_DIR)/src/epics/seq/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\13/g > $@
$(MODEL_EPICS_SRC_DIR)/%4.st: $(RCG_SRC_DIR)/src/epics/seq/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\14/g > $@
$(MODEL_EPICS_SRC_DIR)/%5.st: $(RCG_SRC_DIR)/src/epics/seq/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\15/g > $@
$(MODEL_EPICS_SRC_DIR)/%6.st: $(RCG_SRC_DIR)/src/epics/seq/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\16/g > $@
$(MODEL_EPICS_SRC_DIR)/%7.st: $(RCG_SRC_DIR)/src/epics/seq/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\17/g > $@
$(MODEL_EPICS_SRC_DIR)/%8.st: $(RCG_SRC_DIR)/src/epics/seq/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\18/g > $@
$(MODEL_EPICS_SRC_DIR)/%9.st: $(RCG_SRC_DIR)/src/epics/seq/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\19/g > $@
$(MODEL_EPICS_SRC_DIR)/%10.st: $(RCG_SRC_DIR)/src/epics/seq/%.st
	cat $< | sed s/program\\w\*\\\(\.\*\\\)/program\ \\110/g > $@

# Build C++ version of sequencer code
#$(MODEL_EPICS_SRC_DIR)/%_foo.cc: $(MODEL_EPICS_SRC_DIR)/%.i
#	$(SNC) $< -o $@
# Build C version of sequencer code
%.c: %.i
	$(SNC) $< -o $@
	echo 'registrar('`basename $(@:%.c=%)`'Registrar)' > $(@:%c=%dbd) 

# Compile sequencer-related C code
$(MODEL_EPICS_SRC_DIR)/%o: $(RCG_SRC_DIR)/src/epics/seq/%c
	$(CC) -c $(CFLAGS) $< -o $@

# Compile driver C code
$(MODEL_EPICS_SRC_DIR)/%o: $(RCG_SRC_DIR)/src/drv/%c
	$(CC) -c $(CFLAGS) $< -o $@

# Compile sequencer-related C++ code
$(MODEL_EPICS_SRC_DIR)/%o: $(RCG_SRC_DIR)/src/epics/seq/%cc
	$(CXX) -c $(CFLAGS) $< -o $@

# Compile shmem C code
$(MODEL_EPICS_SRC_DIR)/%o: $(RCG_SRC_DIR)/src/cds-shmem/%c
	$(CC) -c $(CFLAGS) $< -o $@

# Compile generate C++ code
#$(MODEL_EPICS_SRC_DIR)/%_foo.o: $(MODEL_EPICS_SRC_DIR)/%_foo.cc
#	$(CXX) -c $(CFLAGS) $< -o $@

# Generate records and sequencers generation code
$(MODEL_EPICS_SRC_DIR)/registerRecordDeviceDriver.cc: 
	cat $(DBD) > $(MODEL_EPICS_SRC_DIR)/full.dbd
	$(REGGEN) $(MODEL_EPICS_SRC_DIR)/full.dbd  registerRecordDeviceDriver > $@
	/bin/rm -f $(MODEL_EPICS_SRC_DIR)/full.dbd


# Targets not files
.PHONY: target
.PHONY: install
